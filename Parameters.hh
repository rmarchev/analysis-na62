#ifndef _Parameters_HH
#define _Parameters_HH 1

#include "TMath.h"
#include "TVector3.h"

namespace Parameters {

    // Parameters Value.

    static const double mevtogev = 1./1000.;

    // K info.
    static const double KplusMomentum = 75.; // [ GeV ]
    static const double ThKxdirection = 0.001218; // [ rad ]

    // Magneti field.
    static const double EMRadSpeed = TMath::C()* 1.e-9 * 1.e-4 * 1.e-2; // EM radiation speed in particular unit (useful for the magnetic field).
    static const TVector3 MagneticField(0.,0.6928*10000,0.); // magnetic field (only in y direction) in particular unit.


    // Detectors parameters.
    // final collimator.
    static const double zTRIM5 = 101800.; // [ mm ]
    // fiducial region.
    static const double FRzmin = 110000.; // [ mm ]
    static const double FRzmax = 160000.; // [ mm ]
    // magnet.
    static const double zMagStart = 196345.; // [ mm ]
    static const double zMagEnd = 197645.; // [ mm ]
    static const double zMagnet = (zMagStart + zMagEnd)/2.; // [ mm ]
    static const double dMag = zMagEnd - zMagStart;
    // Spectrometer chamber.
    static const double zSTRAW_station[4] = {183508.0,194066.0,204459.0,218885.0}; // [ mm ]
    static const double xSTRAW_station[4] = {101.2,114.4,92.4,52.8}; // [ mm ]
    static const double rSTRAWmin = 75.; // [ mm ]
    static const double rSTRAWmax = 1000.; // [ mm ]
    // CHOD.
    static const double zCHOD = 239100.; // [ mm ] ----> RICONTROLLARE!!!
    static const double zCHODVL = 238960.; // [ mm ] Vertical layer (x)
    static const double zCHODHL = 239340.; // [ mm ] Horizontal layer (y)
    static const double CHODRMin = 125.; // [ mm ]
    static const double CHODRMax = 1100.; // [ mm ]
    // LKr.
    static const double zLKr = 241093.; // [ mm ]
    static const double LKrRMin = 150.; // [ mm ]
    static const double LKrLMax = 1130.; // [ mm ]
    // RICH.
    static const double zRICH = 219550.; // [ mm ]
    static const double zRICHFP = 219875.; // [ mm ] Focal plane
    static const double zRICHMI = 236875.; // [ mm ] Mirror
    static const double zRICHEntWindow = 219385.; // [ mm ] Entrance window
    static const double zRICHExtWindow = 237326.; // [ mm ] Exit window
    static const double lRICH = 17000.; // [ mm ]
    static const double RICHRMin = 90.; // [ mm ]
    static const double RICHRMax = 1100.; // [ mm ]
    static const double nNeon = 1.000062; // Neon (inside RICH) refractive index.
    // MUV1.
    static const double zMUV1 = 244341.; // [ mm ]
    static const double MUV1RMin = 130.; // [ mm ]
    static const double MUV1RMax = 1300.; // [ mm ]
    // MUV2.
    static const double zMUV2 = 245290.; // [ mm ] (2015 with MUV1).
    //static const double zMUV2 = 244435.; // [ mm ] (2014 without MUV1).
    static const double MUV2RMin = 130.; // [ mm ]
    static const double MUV2RMax = 1300.; // [ mm ]
    // MUV3.
    static const double zMUV3 = 246800.; // [ mm ]
    static const double MUV3RMin = 130.; // [ mm ]
    static const double MUV3RMax = 1100.; // [ mm ]

}

#endif
