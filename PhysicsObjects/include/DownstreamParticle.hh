#ifndef DownstreamParticle_h
#define DownstreamParticle_h

#include "Particle.hh"

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class DownstreamParticle : public Particle 
{
  public :
    DownstreamParticle();
    virtual ~DownstreamParticle();
    void Clear();
////    void Store(DownstreamParticle*);
    
  public:
    // General
    TLorentzVector GetMuonMomentum() {return fMuonMomentum;};                       
    TLorentzVector GetElectronMomentum() {return fElectronMomentum;};               
    void SetMuonMomentum(TLorentzVector val) {fMuonMomentum=val;};
    void SetElectronMomentum(TLorentzVector val) {fElectronMomentum=val;};

    // CHOD variables 
    Double_t       GetCHODTime() {return fCHODTime;};                               
    TVector3       GetCHODPosition() {return fCHODPosition;};                       
    void SetCHODTime(Double_t val) {fCHODTime=val;};
    void SetCHODPosition(Double_t x, Double_t y, Double_t z) {fCHODPosition=TVector3(x,y,z);};

    // RICH variables (2 different reconstructions)
    Bool_t         GetRICHMultiIsCandidate() {return fRICHMultiIsCandidate;};       
    TLorentzVector GetRICHMultiMomentum() {return fRICHMultiMomentum;};             
    Double_t       GetRICHMultiMass()     {return fRICHMultiMass;};                 
    Double_t       GetRICHMultiTime()     {return fRICHMultiTime;};                 
    Double_t       GetRICHMultiChi2()     {return fRICHMultiChi2;};                 
    Bool_t         GetRICHSingleIsCandidate() {return fRICHSingleIsCandidate;};     
    TLorentzVector GetRICHSingleMomentum() {return fRICHSingleMomentum;};           
    Double_t       GetRICHSingleMass()     {return fRICHSingleMass;};               
    Double_t       GetRICHSingleTime()     {return fRICHSingleTime;};               
    Double_t       GetRICHSingleChi2()     {return fRICHSingleChi2;};               
    void SetRICHMultiIsCandidate(Bool_t val) {fRICHMultiIsCandidate=val;}; 
    void SetRICHMultiMomentum(TLorentzVector val) {fRICHMultiMomentum=val;}; 
    void SetRICHMultiMass(Double_t val)           {fRICHMultiMass=val;};
    void SetRICHMultiTime(Double_t val)           {fRICHMultiTime=val;};
    void SetRICHMultiChi2(Double_t val)           {fRICHMultiChi2=val;};
    void SetRICHSingleIsCandidate(Bool_t val) {fRICHSingleIsCandidate=val;}; 
    void SetRICHSingleMomentum(TLorentzVector val) {fRICHSingleMomentum=val;}; 
    void SetRICHSingleMass(Double_t val)           {fRICHSingleMass=val;};
    void SetRICHSingleTime(Double_t val)           {fRICHSingleTime=val;};
    void SetRICHSingleChi2(Double_t val)           {fRICHSingleChi2=val;};

    // LKr variables
    Int_t GetLKrID() {return fLKrID;};                                              
    Double_t GetLKrTime() {return fLKrTime;};                                       
    Double_t GetLKrEovP() {return fLKrEovP;};                            
    TVector2 GetLKrPosition() {return fLKrPosition;};
    void SetLKrID(Int_t val) {fLKrID=val;}; 
    void SetLKrTime(Double_t val) {fLKrTime=val;};
    void SetLKrEovP(Double_t val) {fLKrEovP=val;};
    void SetLKrPosition(Double_t xval, Double_t yval) {fLKrPosition.Set(xval,yval);};

    // MUV1 variables
    Int_t GetMUV1ID() {return fMUV1ID;};                                            
    Double_t GetMUV1Energy() {return fMUV1Energy;};                                 
    Double_t GetMUV1Time() {return fMUV1Time;};                                     
    Double_t GetMUV1ShowerWidth() {return fMUV1ShowerWidth;};                       
    Double_t GetMUV1EMerged() {return fMUV1EMerged;};
    void SetMUV1ID(Int_t val) {fMUV1ID=val;}; 
    void SetMUV1Energy(Double_t val) {fMUV1Energy=val;};
    void SetMUV1Time(Double_t val) {fMUV1Time=val;};
    void SetMUV1ShowerWidth(Double_t val) {fMUV1ShowerWidth=val;};
    void SetMUV1EMerged(Double_t val) {fMUV1EMerged=val;};

    // MUV2 variables
    Int_t GetMUV2ID() {return fMUV2ID;};                                            
    Double_t GetMUV2Energy() {return fMUV2Energy;};                                 
    Double_t GetMUV2Time() {return fMUV2Time;};                                     
    Double_t GetMUV2ShowerWidth() {return fMUV2ShowerWidth;};                       
    void SetMUV2ID(Int_t val) {fMUV2ID=val;}; 
    void SetMUV2Energy(Double_t val) {fMUV2Energy=val;};
    void SetMUV2Time(Double_t val) {fMUV2Time=val;};
    void SetMUV2ShowerWidth(Double_t val) {fMUV2ShowerWidth=val;};

    // Total calorimetric energy
    Double_t GetCalorimetricEnergy() {return fCalorimetricEnergy;};                
    void SetCalorimetricEnergy(Double_t val) {fCalorimetricEnergy=val;};

    // MUV3 variables
    Int_t GetMUV3ID() {return fMUV3ID;};                                            
    Double_t GetMUV3Time() {return fMUV3Time;};                                     
    TVector2 GetMUV3Distance() {return fMUV3Distance;};                             
    void SetMUV3ID(Int_t val) {fMUV3ID=val;};
    void SetMUV3Time(Double_t val) {fMUV3Time=val;};
    void SetMUV3Distance(TVector2 val) {fMUV3Distance=val;};

    // Beam track associated to the downstream track
    Int_t GetUpstreamTrackID() {return fUpstreamTrackID;};                          
    void SetUpstreamTrackID(Int_t val) {fUpstreamTrackID=val;};

    // Position of the track at various planes
    TVector3 GetPositionAtGTK(Int_t j) {return fPosAtGTK[j];};
    TVector3 GetPositionAtStraw(Int_t j) {return fPosAtStraw[j];};
    TVector3 GetPositionAtRICH(Int_t j) {return fPosAtRICH[j];};
    TVector3 GetPositionAtLAV(Int_t j) {return fPosAtLAV[j];};
    TVector3 GetPositionAtCHOD(Int_t j) {return fPosAtCHOD[j];};
    TVector3 GetPositionAtIRC(Int_t j) {return fPosAtIRC[j];};
    TVector3 GetPositionAtLKr() {return fPosAtLKr;};
    TVector3 GetPositionAtMUV(Int_t j) {return fPosAtMUV[j];};
    void SetPositionAtGTK(Int_t j, TVector3 pos)   {fPosAtGTK[j] = pos;};
    void SetPositionAtStraw(Int_t j, TVector3 pos) {fPosAtStraw[j] = pos;};
    void SetPositionAtRICH(Int_t j, TVector3 pos)  {fPosAtRICH[j] = pos;};
    void SetPositionAtLAV(Int_t j, TVector3 pos)   {fPosAtLAV[j] = pos;};
    void SetPositionAtCHOD(Int_t j, TVector3 pos)  {fPosAtCHOD[j] = pos;};
    void SetPositionAtIRC(Int_t j, TVector3 pos)   {fPosAtIRC[j] = pos;};
    void SetPositionAtLKr(TVector3 pos)            {fPosAtLKr = pos;};
    void SetPositionAtMUV(Int_t j, TVector3 pos)   {fPosAtMUV[j] = pos;};

    // Photon candidates associated to the track
    Bool_t GetIsPhotonLKrCandidate() {return fIsPhotonLKrCandidate;};
    Bool_t GetIsPhotonLAVCandidate() {return fIsPhotonLAVCandidate;};
    Bool_t GetIsPhotonIRCCandidate() {return fIsPhotonIRCCandidate;};
    Bool_t GetIsPhotonSACCandidate() {return fIsPhotonSACCandidate;};
    void SetIsPhotonLKrCandidate(Bool_t val) {fIsPhotonLKrCandidate=val;};
    void SetIsPhotonLAVCandidate(Bool_t val) {fIsPhotonLAVCandidate=val;};
    void SetIsPhotonIRCCandidate(Bool_t val) {fIsPhotonIRCCandidate=val;};
    void SetIsPhotonSACCandidate(Bool_t val) {fIsPhotonSACCandidate=val;};

    // LKr Photon Candidate   
    Int_t    GetNPhotonLKrCandidates()              {return fNPhotonLKrCandidates;};
    Double_t GetPhotonLKrCandidateTime(Int_t j)     {return fPhotonLKrCandidateTime[j];}; 
    TVector2 GetPhotonLKrCandidatePosition(Int_t j) {return fPhotonLKrCandidatePosition[j];}; 
    Double_t GetPhotonLKrCandidateEnergy(Int_t j)   {return fPhotonLKrCandidateEnergy[j];}; 
    Int_t    GetPhotonLKrCandidateNCells(Int_t j)   {return fPhotonLKrCandidateNCells[j];}; 
    void     SetNPhotonLKrCandidates(Int_t val)                  {fNPhotonLKrCandidates=val;};
    void     SetPhotonLKrCandidateTime(Int_t j,Double_t val)     {fPhotonLKrCandidateTime[j]=val;}; 
    void     SetPhotonLKrCandidatePosition(Int_t j,Double_t valx, Double_t valy) {fPhotonLKrCandidatePosition[j]=TVector2(valx,valy);}; 
    void     SetPhotonLKrCandidateEnergy(Int_t j,Double_t val)   {fPhotonLKrCandidateEnergy[j]=val;}; 
    void     SetPhotonLKrCandidateNCells(Int_t j,Int_t val)      {fPhotonLKrCandidateNCells[j]=val;}; 

    // LAV Photon Candidate
    Int_t    GetNPhotonLAVCandidates()          {return fNPhotonLAVCandidates;};
    Double_t GetPhotonLAVCandidateTime(Int_t j) {return fPhotonLAVCandidateTime[j];};  
    Int_t    GetPhotonLAVCandidateID(Int_t j)   {return fPhotonLAVCandidateID[j];};
    void     SetNPhotonLAVCandidates(Int_t val)              {fNPhotonLAVCandidates=val;};
    void     SetPhotonLAVCandidateTime(Int_t j,Double_t val) {fPhotonLAVCandidateTime[j]=val;}; 
    void     SetPhotonLAVCandidateID(Int_t j,Int_t val)      {fPhotonLAVCandidateID[j]=val;}; 

    // IRC Photon Candidate
    Int_t    GetNPhotonIRCCandidates()          {return fNPhotonIRCCandidates;};
    Double_t GetPhotonIRCCandidateTime(Int_t j) {return fPhotonIRCCandidateTime[j];};  
    Int_t    GetPhotonIRCCandidateID(Int_t j)   {return fPhotonIRCCandidateID[j];};
    void     SetNPhotonIRCCandidates(Int_t val)              {fNPhotonIRCCandidates=val;};
    void     SetPhotonIRCCandidateTime(Int_t j,Double_t val) {fPhotonIRCCandidateTime[j]=val;}; 
    void     SetPhotonIRCCandidateID(Int_t j,Int_t val)      {fPhotonIRCCandidateID[j]=val;}; 

    // SAC Photon Candidate
    Int_t    GetNPhotonSACCandidates()          {return fNPhotonSACCandidates;};
    Double_t GetPhotonSACCandidateTime(Int_t j) {return fPhotonSACCandidateTime[j];};  
    Int_t    GetPhotonSACCandidateID(Int_t j)   {return fPhotonSACCandidateID[j];};
    void     SetNPhotonSACCandidates(Int_t val)              {fNPhotonSACCandidates=val;};
    void     SetPhotonSACCandidateTime(Int_t j,Double_t val) {fPhotonSACCandidateTime[j]=val;}; 
    void     SetPhotonSACCandidateID(Int_t j,Int_t val)      {fPhotonSACCandidateID[j]=val;}; 

  protected :

    TLorentzVector fMuonMomentum;            // 4-Momentum in muon hypothesis
    TLorentzVector fElectronMomentum;        // 4-Momentum in positron hypothesis 

    Double_t fCHODTime;                      // Time of the CHOD candidate matching the track wrt to the trigger time 
    TVector3 fCHODPosition;                  // Position of the CHOD candidate matching the track

    Bool_t         fRICHMultiIsCandidate;    // Flag indicating the presence of a multiring-RICH candidate
    TLorentzVector fRICHMultiMomentum;       // Momentum of the track as measured by the multiring-RICH candidate under the pion hypothesis
    Double_t       fRICHMultiMass;           // Mass of the Particle provided by the multiring-RICH candidate
    Double_t       fRICHMultiTime;           // Time of the multiring-RICH candidate 
    Double_t       fRICHMultiChi2;           // Chi2 of the multiring-RICH candidate
    Bool_t         fRICHSingleIsCandidate;   // Flag indicating the presence of a singlering-RICH candidate
    TLorentzVector fRICHSingleMomentum;      // Momentum of the track as measured by the singlering-RICH candidate under the pion hypothesis
    Double_t       fRICHSingleMass;          // Mass of the Particle provided by the singlering-RICH candidate
    Double_t       fRICHSingleTime;          // Time of the multiring-RICH candidate  
    Double_t       fRICHSingleChi2;          // Chi2 of the singlering-RICH candidate

    Int_t fLKrID;                            // ID of the LKr cluster matching the track
    Double_t fLKrTime;                       // Time of the LKr cluster matching the track
    Double_t fLKrEovP;                       // E/P of the LKr cluster matching the track
    TVector2 fLKrPosition;                   // XY of the LKr cluster matching the track

    Int_t fMUV1ID;                           // ID of the MUV1 cluster matching the track
    Double_t fMUV1Energy;                    // MUV1 energy associated to the track
    Double_t fMUV1Time;                      // Time of the MUV1 cluster matching the track
    Double_t fMUV1ShowerWidth;               // Shower width of the MUV1 cluster matching the track
    Double_t fMUV1EMerged;                                  

    Int_t fMUV2ID;                           // ID of the MUV2 cluster matching the track
    Double_t fMUV2Energy;                    // MUV2 energy associated to the track
    Double_t fMUV2Time;                      // Time of the MUV2 cluster matching the track
    Double_t fMUV2ShowerWidth;               // Shower width of the MUV2 cluster matching the track

    Double_t fCalorimetricEnergy;            // Calorimetric (LKr+MUV1+MUV2) associated to the track

    Int_t fMUV3ID;                           // ID of the MUV3 candidate matching the track 
    Double_t fMUV3Time;                      // Time of the MUV3 candidate matching the track
    TVector2 fMUV3Distance;                  // Distance of the MUV3 candidate from the track extrapolation

    Int_t fUpstreamTrackID;                  // ID of the upstream track associated to this track 

    TVector3 fPosAtGTK[3];                   // Position at GTK1,2,3
    TVector3 fPosAtStraw[4];                 // Position at Straw1,2,3,4
    TVector3 fPosAtRICH[2];                  // Position at RICH, in and out windows
    TVector3 fPosAtLAV[12];                  // Position at LAVs'
    TVector3 fPosAtCHOD[2];                  // Position at CHOD V,H
    TVector3 fPosAtIRC[2];                   // Position at IRC and SAC
    TVector3 fPosAtLKr;                      // Position at LKr
    TVector3 fPosAtMUV[3];                   // Position at MUV1,2,3

    // Photon candidates assoiacted to the downstream track
    Bool_t fIsPhotonLKrCandidate;            // Flag to indicate the existence of a photon candidate in lkr
    Bool_t fIsPhotonLAVCandidate;            // Flag to indicate the existence of a photon candidate in lav
    Bool_t fIsPhotonIRCCandidate;            // Flag to indicate the existence of a photon candidate in irc
    Bool_t fIsPhotonSACCandidate;            // Flag to indicate the existence of a photon candidate in sac

    Int_t fNPhotonLKrCandidates;               // Number of photon candidates in LKr
    Double_t fPhotonLKrCandidateTime[10];      // Time of a photon candidate in LKr
    TVector2 fPhotonLKrCandidatePosition[10];  // Position of a photon candidate in LKr
    Double_t fPhotonLKrCandidateEnergy[10];    // Energy of a photon candidate in LKr
    Int_t fPhotonLKrCandidateNCells[10];       // Number of cells forming a photon candidate in LKr

    Int_t fNPhotonLAVCandidates;               // Number of photon candidates in LAV
    Double_t fPhotonLAVCandidateTime[50];      // Time of a photon candidate in LAV
    Int_t fPhotonLAVCandidateID[50];           // ID of a photon candidate in LAV

    Int_t fNPhotonIRCCandidates;               // Number of photon candidates in IRC
    Double_t fPhotonIRCCandidateTime[20];      // Time of a photon candidate in IRC
    Int_t fPhotonIRCCandidateID[20];           // ID of a photon candidate in IRC

    Int_t fNPhotonSACCandidates;               // Number of photon candidates in SAC
    Double_t fPhotonSACCandidateTime[20];      // Time of a photon candidate in SAC
    Int_t fPhotonSACCandidateID[20];           // ID of a photon candidate in SAC
};

#endif
