#ifndef DownstreamPionTrack_HH
#define DownstreamPionTrack_HH

#include "TObject.h"

#include "TrackSTRAWCandidate.hh"
#include "TrackCHODCandidate.hh"

#include "TRecoSpectrometerCandidate.hh"
#include "TRecoCedarCandidate.hh"
#include "TRecoGigaTrackerCandidate.hh"
#include "TRecoCHANTICandidate.hh"
#include "TRecoLAVCandidate.hh"
#include "TRecoRICHCandidate.hh"
#include "TRecoIRCCandidate.hh"
#include "TRecoCHODCandidate.hh"
#include "TRecoLKrCandidate.hh"
#include "TRecoMUV1Candidate.hh"
#include "TRecoMUV2Candidate.hh"
#include "TRecoMUV3Candidate.hh"
#include "TRecoSACCandidate.hh"

class DownstreamPionTrack : public TObject {

public:
  DownstreamPionTrack ();
  virtual ~DownstreamPionTrack ();

  void Reset();

  void SetMomentum(TVector3 val){fMomentum=val;};
  void SetMomentum(Double_t x, Double_t y, Double_t z){fMomentum=TVector3(x,y,z);};
  void SetFourMomentum(Double_t x, Double_t y, Double_t z){fFourMomentum.SetPxPyPzE(x,y,z, TMath::Sqrt(x*x + y*y + z*z + fMass*fMass));};
  void SetCDA(Double_t val){ fkaonpioncda=val;};
  void SetVtxChi2(Double_t val){fvtxchi2=val;};

  Double_t GetCDA(){return fkaonpioncda;};

protected:
  Double_t fMass;
  Double_t fP;
  Double_t fkaonpioncda;
  Double_t fvtxchi2;
  Double_t fEopLKr;
  Double_t fEopMUV1;
  Double_t fEopMUV2;
  Double_t fMM2;

  TVector3 fMomentum;
  TLorentzVector fFourMomentum;


  ClassDef(DownstreamPionTrack,1);
};

#endif
