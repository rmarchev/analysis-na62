#ifndef CHODCandidate_h
#define CHODCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class CHODCandidate 
{
  public :
    CHODCandidate();
    virtual ~CHODCandidate();
    void Clear();

  public:
    void SetDiscriminant(Double_t val) {fDiscriminant=val;};
    void SetDeltaTime(Double_t val) {fDeltaTime=val;};
    void SetX(Double_t val) {fX=val;};
    void SetY(Double_t val) {fY=val;};
    void SetIsCHODCandidate(Bool_t val) {fIsCHODCandidate=val;};
    Double_t GetDiscriminant() {return fDiscriminant;};
    Double_t GetDeltaTime() {return fDeltaTime;};
    Double_t GetX() {return fX;};
    Double_t GetY() {return fY;};
    Bool_t   GetIsCHODCandidate() {return fIsCHODCandidate;};

  private:
    Double_t fDiscriminant;
    Double_t fDeltaTime;
    Double_t fX;
    Double_t fY;
    Bool_t fIsCHODCandidate;
};

#endif
