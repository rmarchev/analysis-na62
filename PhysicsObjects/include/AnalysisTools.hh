#ifndef AnalysisTools_H
#define AnalysisTools_H 1

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>
#include "TVector3.h"
#include "TLorentzVector.h"
using namespace std;

class BTubeField;
class BlueTubeTracker;
class TRecoSpectrometerCandidate;
class TRecoLKrCandidate;
class Particle;

class AnalysisTools
{
  public:
  AnalysisTools();
  static AnalysisTools* GetInstance();

  private:
  static AnalysisTools* fInstance;

  public:
  TVector3 SingleTrackVertex(TVector3,TVector3,TVector3,TVector3,Double_t&);
  TVector3 GetPositionAtZ(TRecoSpectrometerCandidate*, Double_t);
  TVector3 GetKaonPositionAtZ(TVector3 mom,TVector3 pos, Double_t zpos);
  TLorentzVector Get4Momentum(Double_t *partrack);
  void ClusterCorrections(Int_t,TRecoLKrCandidate *);
  void BTubeCorrection(TVector3*,Double_t*,Double_t*,Double_t);
  TVector3 BlueFieldCorrection(Particle*,Double_t);
  TVector3 BlueFieldCorrection(TVector3 *, TVector3 , Int_t , Double_t );
  Int_t GetLAVStation(Double_t);
  Bool_t TrackIsInAcceptance(TRecoSpectrometerCandidate*);
  Bool_t LKrGeometricalAcceptance(double , double );
  Int_t SelectTriggerMask(int triggerType, int type, int mask) ;
public:
  BTubeField *fBTubeField;
  BlueTubeTracker *fTracker;
  Double_t fMirrorPos[25][6][2];

  private:
  Bool_t fIsClusterCorrected;
};

#endif
