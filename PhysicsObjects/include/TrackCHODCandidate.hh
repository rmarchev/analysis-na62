#ifndef TrackCHODCandidate_HH
#define TrackCHODCandidate_HH

#include "TObject.h"
#include "TRecoCHODCandidate.hh"

class TrackCHODCandidate : public TObject
{
public :
  TrackCHODCandidate();
  virtual ~TrackCHODCandidate();
  void Reset();

public:

  void SetGoodTrack(Bool_t val)   {fGood=val;};
  void SetX(Double_t val) { fCHODVerticalPosition = val; }
  void SetY(Double_t val) { fCHODHorizontalPosition = val; }
  void SetCHODTime(Double_t val)   {fCHODTime=val;};
  void SetCHODdt(Double_t val) { fCHODdt = val; }
  void SetDiscriminant(Double_t val) { fCHODdisc = val; }
  void SetDistanceToHit(Double_t val) { fCHODdtohit = val; }
  void SetPosition(TVector3 Pos) { fCHODPosition = Pos; }
  void SetPosition(Double_t x, Double_t y, Double_t z) { fCHODPosition = TVector3(x,y,z); }

  Bool_t TrackIsGood()   {return fGood;};
  Double_t GetX() { return fCHODVerticalPosition; }
  Double_t GetY() { return fCHODHorizontalPosition; }
  Double_t GetCHODTime()   {return fCHODTime;};
  Double_t GetCHODdt() {return fCHODdt; }
  Double_t GetDiscriminant() { return fCHODdisc; }
  Double_t GetDistanceToHit() { return fCHODdtohit; }
  TVector3 GetPosition() { return fCHODPosition; }

private:

  //TRecoCHODCandidate* fCHOD;

  Double_t fCHODVerticalPosition;
  Double_t fCHODHorizontalPosition;
  Double_t fCHODdt;
  Double_t fCHODTime;
  Double_t fCHODdisc;
  Double_t fCHODdtohit;
  TVector3 fCHODPosition;
  Bool_t fGood;

  ClassDef(TrackCHODCandidate,1);
};

#endif
