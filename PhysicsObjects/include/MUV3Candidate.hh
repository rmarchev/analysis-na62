#ifndef MUV3Candidate_h
#define MUV3Candidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class MUV3Candidate 
{
  public :
    MUV3Candidate();
    virtual ~MUV3Candidate();
    void Clear();

  public:
    void SetIsMUV3Candidate(Bool_t val) {fIsMUV3Candidate=val;};
    void SetDiscriminant(Double_t val) {fDiscriminant=val;};
    void SetDeltaTime(Double_t val)    {fDeltaTime=val;   };
    void SetType(Int_t val)            {fType=val;           };

    Bool_t GetIsMUV3Candidate() {return fIsMUV3Candidate;};
    Double_t GetDiscriminant() {return fDiscriminant;};
    Double_t GetDeltaTime()    {return fDeltaTime;   };
    Int_t  GetType()         {return fType;           };

  private:
    Bool_t fIsMUV3Candidate;
    Double_t fDiscriminant;
    Double_t fDeltaTime;
    Int_t fType;
};

#endif
