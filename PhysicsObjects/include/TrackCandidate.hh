#ifndef TrackCandidate_HH
#define TrackCandidate_HH

#include "TObject.h"

#include "TrackSTRAWCandidate.hh"
#include "TrackCHODCandidate.hh"

#include "TRecoSpectrometerCandidate.hh"
#include "TRecoCedarCandidate.hh"
#include "TRecoGigaTrackerCandidate.hh"
#include "TRecoCHANTICandidate.hh"
#include "TRecoLAVCandidate.hh"
#include "TRecoRICHCandidate.hh"
#include "TRecoIRCCandidate.hh"
#include "TRecoCHODCandidate.hh"
#include "TRecoLKrCandidate.hh"
#include "TRecoMUV1Candidate.hh"
#include "TRecoMUV2Candidate.hh"
#include "TRecoMUV3Candidate.hh"
#include "TRecoSACCandidate.hh"

class TrackCandidate : public TObject {

public:
  TrackCandidate ();
  virtual ~TrackCandidate ();

  void Reset();

  //void SetCHODPresent(Bool_t val) { fCHODPresent = val;}




  //--------Spectrometer----------//

  void SetSpectrometer(TRecoSpectrometerCandidate* ptr, Int_t ind) { fSpectrometer = ptr; fSpectrometerIndex = ind; fSpectrometerPresent = true; }
  void SetSTRAW(TrackSTRAWCandidate* ptr) { fSTRAW = ptr; }
  Int_t GetSpectrometerIndex() { return  fSpectrometerIndex; }
  Bool_t SpectrometerPresent() { return  fSpectrometerPresent; }
  void SetTrackMomentumBeforeMagnet(TVector3 val){fMomentumBeforeMagnet=val;}

  TVector3 GetMomentumBeforeMagnet(){return fMomentumBeforeMagnet;}
  TRecoSpectrometerCandidate* GetSpectrometer() { return  fSpectrometer; }
  TrackSTRAWCandidate* GetSTRAW() {return fSTRAW; }
  Double_t GetSpectrometerTime() { return  fSpectrometerTime; }

  //--------CHOD----------//
  void SetCHOD(TrackCHODCandidate* ptr) { fCHODCandidate = ptr; }
  void SetCHODX(Double_t val) { fCHODVerticalPosition = val; }
  void SetCHODY(Double_t val) { fCHODHorizontalPosition = val; }
  void SetCHODTime(Double_t val) { fCHODTime = val; }
  void SetCHODdt(Double_t val) { fCHODdt = val; }
  void SetCHODDiscriminant(Double_t val) { fCHODdisc = val; }
  void SetCHODDistanceToHit(Double_t val) { fCHODdtohit = val; }
  void SetCHODPosition(TVector3 Pos) { fCHODPosition = Pos; }
  void SetCHODPosition(Double_t x, Double_t y, Double_t z) { fCHODPosition = TVector3(x,y,z); }

  TrackCHODCandidate* GetCHOD() {return fCHODCandidate; }
  Double_t GetCHODX() { return fCHODVerticalPosition; }
  Double_t GetCHODY() { return fCHODHorizontalPosition; }
  Double_t GetCHODDiscriminant() { return fCHODdisc; }
  Double_t GetCHODDistanceToHit() { return fCHODdtohit; }
  TVector3 GetCHODPosition() { return fCHODPosition; }
  Double_t GetCHODdt() { return fCHODdt;}
  Double_t GetCHODTime() { return fCHODTime;}
  Bool_t CHODPresent() { return fCHODPresent;}

  // --------- RICH --------------//
  void SetRICH(TRecoRICHCandidate* ptr, Int_t ind){fRICH = ptr; fRICHIndex = ind; fRICHPresent = true;}
  void SetRICHTime(Double_t val) {  fRICHTime = val;}
  Bool_t RICHPresent() { return fRICHPresent;}

  Double_t GetRICHTime() { return fRICHTime;}
  //--------LKr----------//
  void SetLKr(TRecoLKrCandidate* ptr, Int_t ind) { fLKr = ptr; fLKrIndex = ind; fLKrPresent = true; }
  Bool_t LKrPresent() { return fLKrPresent;}


  void SetLKrMatchedX(Double_t val) { fLKrMatchedX = val; }
  void SetLKrMatchedY(Double_t val) { fLKrMatchedY = val; }
  void SetLKrMatchedPosition(Double_t x, Double_t y, Double_t z) { fLKrMatchedPosition = TVector3(x,y,z); }
  void SetLKrEoP(Double_t val) { fLKrMatchedEoP = val; }
  void SetLKrEnergy(Double_t val) { fLKrMatchedEnergy = val; }
  void SetLKrShowerWidth(Double_t val) { fLKrShowerWidth = val; }
  void SetNExtraInTimeCl(Int_t val) { fLKrInTimeExtraCl = val; }
  void SetNExtraInTimeClAux(Int_t val) { fLKrInTimeExtraClAux = val; }
  void SetLKrISEM(Bool_t val) { MatchedISEM = val; }
  void SetLKrISMIP(Bool_t val) { MatchedISMIP = val; }
  void SetLKrISOther(Bool_t val) { MatchedISOther = val; }

  void AddExtraInTimeCl(Int_t ind,Int_t val) { fLKrInTimeIndex[ind] = val; }
  void AddExtraInTimeClAux(Int_t ind,Int_t val) { fLKrInTimeIndexAux[ind] = val; }

  TRecoLKrCandidate* GetLKr() { return  fLKr; }
  Double_t GetLKrMatchedX() { return fLKrMatchedX; }
  Double_t GetLKrMatchedY() { return fLKrMatchedY; }
  Double_t GetLKrShowerWidth(){ return fLKrShowerWidth;}
  Int_t GetNExtraInTimeCl() { return fLKrInTimeExtraCl; }
  Int_t GetNExtraInTimeClAux() { return fLKrInTimeExtraClAux; }
  Double_t GetExtraInTimeIndex(Int_t ind) { return fLKrInTimeIndex[ind]; }
  Int_t GetLkrMatchedIndex() {return fLKrIndex; }
  Double_t GetLKrEoP(){return fLKrMatchedEoP;}
  Double_t GetLKrEnergy(){return fLKrMatchedEnergy;}
  Bool_t GetLKrISEM(){return MatchedISEM;}
  Bool_t GetLKrISMIP(){return MatchedISMIP;}
  Bool_t GetLKrISOther(){return MatchedISOther;}

  //----------Cedar----------//
  void SetCedar(TRecoCedarCandidate* ptr, Int_t ind) { fCedar = ptr; fCedarIndex = ind; fCedarPresent = true; }
  void SetCedarPresent(){  fCedarPresent=true;}
  void SetCedarTime(Double_t val){ fCedarTime= val;}
  void SetCedardt(Double_t val){ fCedardT= val;}

  TRecoCedarCandidate* GetCedar() { return fCedar; }
  Double_t GetCedarTime(){ return fCedarTime;}
  Double_t GetCedardt(){ return fCedardT;}
  Int_t GetCedarIndex(){ return fCedarIndex;}
  Bool_t IsCedarPresent(){ return fCedarPresent;}

  // --------- GTK --------------//
  void SetGTK(TRecoGigaTrackerCandidate* ptr, Int_t ind) { fGigaTracker = ptr; fGTKIndex = ind; fGTKPresent = true; }
  void SetCDA(Double_t val){ fGTKcda= val;}
  void SetGTKTime(Double_t val){ fGTKTime= val;}
  void SetGTKPosition(TVector3 val){ fGTKPosition = val;};
  void SetGTKMomentum(TVector3 val){ fGTKMomentum = val;};
  void SetGTKVertex(TVector3 val){ fGTKVertex = val;};

  TRecoGigaTrackerCandidate* GetGTK() { return fGigaTracker; }
  Bool_t IsGTKPresent() { return fGTKPresent;}
  Double_t GetGTKTime(){ return fGTKTime;}
  Double_t GetCDA(){ return fGTKcda;}
  Int_t GetGTKIndex(){ return fGTKIndex;}
  TVector3 GetGTKPosition(){return fGTKPosition;};
  TVector3 GetGTKMomentum(){return fGTKMomentum;};
  TVector3 GetGTKVertex(){return fGTKVertex;};

  // --------- CHANTI --------------//
  void SetCHANTI(TRecoCHANTICandidate* ptr, Int_t ind) { fCHANTI = ptr; fCHANTIIndex = ind; fCHANTIPresent = true; }
  void SetCHANTITime(Double_t val){ fCHANTITime = val;}
  void SetCHANTIPresent() { fCHANTIPresent=true;}

  Bool_t IsCHANTIPresent() { return fCHANTIPresent;}
  TRecoCHANTICandidate* GetCHANTI(){ return fCHANTI;}
  Int_t GetCHANTIIndex(){ return fCHANTIIndex;}
  Double_t GetCHANTITime(){ return fCHANTITime;}

  // --------- MUV1 --------------//
  void SetMUV1(TRecoMUV1Candidate* ptr, Int_t ind){fMUV1 = ptr; fMUV1Index = ind; fMUV1Present = true;}
  void SetMUV1Time(Double_t val){ fMUV1Time = val;}
  void SetMUV1Energy(Double_t val){ fMUV1Energy = val;}
  void SetMUV1ShowerWidth(Double_t val){ fMUV1ShowerWidth = val;}
  void SetMUV1SeedRatio(Double_t val){ fMUV1SeedRatio = val;}

  Bool_t IsMUV1Present()  { return fMUV1Present;}
  Double_t GetMUV1Index() { return fMUV1Index;}
  Double_t GetMUV1Time()  { return fMUV1Time;}
  Double_t GetMUV1Energy(){ return fMUV1Energy;}
  Double_t GetMUV1ShowerWidth(){ return fMUV1ShowerWidth;}
  Double_t GetMUV1SeedRatio(){ return fMUV1SeedRatio;}

  // --------- MUV2 --------------//
  void SetMUV2(TRecoMUV2Candidate* ptr, Int_t ind){fMUV2 = ptr; fMUV2Index = ind; fMUV2Present = true;}
  void SetMUV2Time(Double_t val){ fMUV2Time = val;}
  void SetMUV2Energy(Double_t val){ fMUV2Energy = val;}
  void SetMUV2SeedRatio(Double_t val){ fMUV2SeedRatio = val;}
  void SetMUV2ShowerWidth(Double_t val){ fMUV2ShowerWidth = val;}

  Bool_t IsMUV2Present()  { return fMUV2Present;}
  Double_t GetMUV2ShowerWidth(){ return fMUV2ShowerWidth;}
  Double_t GetMUV2Index() { return fMUV2Index;}
  Double_t GetMUV2Time()  { return fMUV2Time;}
  Double_t GetMUV2Energy(){ return fMUV2Energy;}
  Double_t GetMUV2SeedRatio(){ return fMUV2SeedRatio;}

  // --------- MUV3 --------------//
  void SetMUV3(TRecoMUV3Candidate* ptr, Int_t ind){fMUV3 = ptr; fMUV3Index = ind; fMUV3Present = true;}
  void SetMUV3Time(Double_t val){ fMUV3Time = val;}

  Bool_t IsMUV3Present()  { return fMUV3Present;}
  Double_t GetMUV3Index() { return fMUV3Index;}
  Double_t GetMUV3Time()  { return fMUV3Time;}

  // --------- LAV --------------//
  void SetLAV(TRecoLAVCandidate* ptr, Int_t ind){fLAV = ptr; fLAVIndex = ind; fLAVPresent = true;}
  void SetLAVTime(Double_t val){ fLAVTime = val;}

  Bool_t IsLAVPresent()  { return fLAVPresent;}
  Double_t GetLAVIndex() { return fLAVIndex;}
  Double_t GetLAVTime()  { return fLAVTime;}

  // --------- SAC --------------//
  void SetSAC(TRecoSACCandidate* ptr, Int_t ind){fSAC = ptr; fSACIndex = ind; fSACPresent = true;}
  void SetSACTime(Double_t val){ fSACTime = val;}

  Bool_t IsSACPresent()  { return fSACPresent;}
  Double_t GetSACIndex() { return fSACIndex;}
  Double_t GetSACTime()  { return fSACTime;}

  // --------- IRC --------------//
  void SetIRC(TRecoIRCCandidate* ptr, Int_t ind){fIRC = ptr; fIRCIndex = ind; fIRCPresent = true;}
  void SetIRCTime(Double_t val){ fIRCTime = val;}

  Bool_t IsIRCPresent()  { return fIRCPresent;}
  Double_t GetIRCIndex() { return fIRCIndex;}
  Double_t GetIRCTime()  { return fIRCTime;}


protected:
  //STRAW
  TRecoSpectrometerCandidate* fSpectrometer;
  TrackSTRAWCandidate* fSTRAW;
  Int_t fSpectrometerIndex;
  Double_t fSpectrometerTime;
  Bool_t fSpectrometerPresent;
  TVector3 fMomentumBeforeMagnet;
  //CHOD
  TrackCHODCandidate*         fCHODCandidate;
  Double_t fCHODVerticalPosition;
  Double_t fCHODHorizontalPosition;
  Double_t fCHODdt;
  Double_t fCHODTime;
  Double_t fCHODdisc;
  Double_t fCHODdtohit;
  TVector3 fCHODPosition;
  Bool_t fCHODPresent;

  //LKr
  TRecoLKrCandidate*          fLKr;
  Int_t fLKrIndex;
  Double_t fLKrMatchedEnergy;
  Double_t fLKrMatchedEoP;
  Double_t fLKrInTimeExtraCl;
  Double_t fLKrInTimeExtraClAux;
  Double_t fLKrShowerWidth;
  Double_t fLKrMatchedX;
  Double_t fLKrMatchedY;
  TVector3 fLKrMatchedPosition;
  Bool_t fLKrPresent;

  Double_t fLKrInTimeIndex[10];
  Double_t fLKrInTimeIndexAux[10];


  Bool_t MatchedISMIP;
  Bool_t MatchedISEM;
  Bool_t MatchedISOther;

  //CEDAR
  TRecoCedarCandidate*        fCedar;
  Int_t fCedarIndex;
  Double_t fCedarTime;
  Double_t fCedardT;
  Bool_t fCedarPresent;

  //GTK
  TRecoGigaTrackerCandidate*  fGigaTracker;
  Bool_t fGTKPresent;
  Int_t fGTKIndex;
  Double_t fGTKTime;
  Double_t fGTKcda;
  TVector3 fGTKPosition;
  TVector3 fGTKVertex;
  TVector3 fGTKMomentum;

  //CHANTI
  TRecoCHANTICandidate*       fCHANTI;
  Double_t fCHANTITime;
  Int_t fCHANTIIndex;
  Bool_t fCHANTIPresent;

  //MUV1
  TRecoMUV1Candidate*         fMUV1;
  Int_t fMUV1Index;
  Double_t fMUV1Time;
  Double_t fMUV1Energy;
  Double_t fMUV1ShowerWidth;
  Double_t fMUV1SeedRatio;
  Bool_t fMUV1Present;

  //MUV2
  TRecoMUV2Candidate*         fMUV2;
  Int_t fMUV2Index;
  Double_t fMUV2Time;
  Double_t fMUV2Energy;
  Bool_t fMUV2Present;
  Double_t fMUV2ShowerWidth;
  Double_t fMUV2SeedRatio;

  //MUV3
  TRecoMUV3Candidate*         fMUV3;
  Int_t fMUV3Index;
  Double_t fMUV3Time;
  Double_t fMUV3Energy;
  Bool_t fMUV3Present;

  //LAV
  TRecoLAVCandidate*         fLAV;
  Int_t fLAVIndex;
  Double_t fLAVTime;
  Double_t fLAVEnergy;
  Bool_t fLAVPresent;


  //IRC
  TRecoIRCCandidate*         fIRC;
  Int_t fIRCIndex;
  Double_t fIRCTime;
  Double_t fIRCEnergy;
  Bool_t fIRCPresent;


  //SAC
  TRecoSACCandidate*         fSAC;
  Int_t fSACIndex;
  Double_t fSACTime;
  Double_t fSACEnergy;
  Bool_t fSACPresent;

  //RICH
  TRecoRICHCandidate*         fRICH;
  Double_t fRICHTime;
  Int_t fRICHIndex;
  Bool_t fRICHPresent;


  ClassDef(TrackCandidate,1);
};


#endif
