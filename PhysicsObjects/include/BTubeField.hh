#ifndef BTubeField_H
#define BTubeField_H 1

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include <TCanvas.h>
#include "TVector3.h"
#include "TLorentzVector.h"
using namespace std;

class BTubeField 
{
  public:
  BTubeField();
  ~BTubeField();
  void GetCorrection(TVector3*, Double_t*, Double_t*, Double_t);

  private:
  Double_t fEC;
  Double_t fby[40][29];
  Double_t fbx[40][29];
  Double_t fbz[40][29];
  Double_t fBxTube[17][17][40];
  Double_t fByTube[17][17][40];
  Double_t fBzTube[17][17][40];
  Double_t fZTube[40];

  private:
  void SetBTube();
  void FillTubeField(Int_t *, Double_t, Double_t);  
  TVector3 GetBTube(Int_t jx, Int_t jy, Int_t jz) { return TVector3(fBxTube[jx][jy][jz],fBzTube[jx][jy][jz],fBzTube[jx][jy][jz]); }
  TVector3 ComputeBFieldAtXY(Double_t,Double_t,Int_t);
  TVector3 ComputeBFieldAtZ(Double_t,Double_t,TVector3 *,Int_t);
};

#endif
