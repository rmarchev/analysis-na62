#ifndef MM2Errors_HH
#define MM2Errors_HH

#include <stdlib.h>
//#include <iostream>
#include <cmath>
#include <iostream>
#include "TMatrixD.h"
#include <vector>
#include "TRecoGigaTrackerCandidate.hh"
#include "TRecoSpectrometerCandidate.hh"

using namespace std;

class MM2Errors {

public:
  MM2Errors(vector<Double_t> &x, TRecoSpectrometerCandidate* track);
  ~MM2Errors();


  TMatrixD GetCovMatrix(){if(!IsCov){MakeCovMatrix();} return fCov;};
  TMatrixD GetHessianMatrix(){if(!IsHess){BuildHessian(); IsHess=true;} return fCov;};
  Double_t GetMM2MuonError(){ if(!IsErr){MakeCovMatrix(); ComputeMissingMassSquaredError(); IsErr=true;} return fmm2muerror;};
  Double_t GetMM2PionError(){ if(!IsErr){MakeCovMatrix(); ComputeMissingMassSquaredError(); IsErr=true;} return fmm2pierror;};
  Double_t GetMM2Muon(){return fmm2mu;};
  Double_t GetMM2Pion(){return fmm2pi;};

private:
  Int_t fdim;
  Double_t fmpion;
  Double_t fmmuon;
  Double_t fmkaon;
  Double_t fmm2mu;
  Double_t fmm2pi;
  Double_t fmm2muerror;
  Double_t fmm2pierror;
  std::vector<Double_t> fx;
  TLorentzVector fPionMomentum;
  TLorentzVector fMuonMomentum;
  TLorentzVector fKaonMomentum;
  TRecoSpectrometerCandidate* fTrack;
  TMatrix fCov;
  TMatrix fHessianMuon;
  TMatrix fHessianPion;
  Bool_t IsCov;
  Bool_t IsHess;
  Bool_t IsErr;

  Double_t ComputeMissingMassSquared(vector<Double_t> &x, Double_t mass);
  void MakeCovMatrix();
  void BuildHessian();
  void ComputeMissingMassSquaredError();
  Double_t ComputeDerivatives(Int_t i , Double_t mass);
  Double_t ComputeSecondDerivatives(Int_t i ,Int_t j, Double_t mass);
};

#endif
