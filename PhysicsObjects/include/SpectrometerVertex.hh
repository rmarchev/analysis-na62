#ifndef SpectrometerVertex_HH
#define SpectrometerVertex_HH

#include "stdlib.h"
#include <iostream>
#include "TVector3.h"

using namespace std;

class SpectrometerVertex{

public:
  SpectrometerVertex();
  virtual ~SpectrometerVertex() {}

  void Clear();
  void AddTrack(Int_t index, Int_t Charge, TVector3 Mom, TVector3 Mom0);

  void     SetNTracks (Int_t val)               { fNTracks  = val;           }
  void     SetCharge  (Int_t val)               { fCharge   = val;           }
  void     SetPosition(TVector3 val)            { fPos      = val;           }
  void     SetChi2    (Double_t val)            { fChi2     = val;           }
  void     SetTotalThreeMomentum (TVector3 val) { fTotalThreeMomentum = val; }

  Double_t GetNTracks(){return fNTracks;};
  Double_t GetCharge(){return fCharge;};
  Double_t GetChi2(){return fChi2;};
  TVector3 GetPosition(){return fPos;};
  TVector3 GetTotalThreeMomentum() {return fTotalThreeMomentum;};
  Double_t GetTotalMomentum() {return fTotalThreeMomentum.Mag();};

  Int_t    GetTrackIndex (Int_t i); ///< Track index in the TRecoSpectrometerCandidate array
  Int_t    GetTrackCharge(Int_t i); ///< Track charge (+-1)

  // "Corrected" values (output from the vertex fit)
  TVector3 GetTrackThreeMomentum(Int_t i);
  Double_t GetTrackMomentum(Int_t i);
  Double_t GetTrackSlopeX(Int_t i);
  Double_t GetTrackSlopeY(Int_t i);

  // "Uncorrected" values (spectrometer reconstruction input to the vertex fit)
  TVector3 GetTrackThreeMomentum0(Int_t i);
  Double_t GetTrackMomentum0(Int_t i);
  Double_t GetTrackSlopeX0(Int_t i);
  Double_t GetTrackSlopeY0(Int_t i);

  void     Print();
private:

  Double_t fNTracks;                  ///Number of tracks forming the vertex
  Double_t fCharge;                   ///< Total electrical charge of the tracks
  Double_t fChi2;                     ///< Vertex fit quality: chi2
  TVector3 fPos;                      ///< Vertex position
  TVector3 fTotalThreeMomentum;       ///< Total three-momentum of all vertex tracks
  std::vector<Int_t> fQTrack;         /// Tracks charges
  std::vector<Int_t> fTrackIndices;   /// Tracks indices
  std::vector<TVector3>fThreeMomenta; ///< Three-momenta (output from the vertex fit)
  std::vector<TVector3>fThreeMomenta0;///< Three-momenta (input to the vertex fit)

};

#endif
