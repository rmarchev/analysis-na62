#ifndef UpstreamParticle_h
#define UpstreamParticle_h

#include "Particle.hh"

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class UpstreamParticle : public Particle
{
  public :
    UpstreamParticle();
    virtual ~UpstreamParticle();
    void Clear();

  public :

    // Downstream particle
    Int_t GetDownstreamTrackID() {return fDownstreamTrackID;};
    void SetDownstreamTrackID(Int_t val) {fDownstreamTrackID = val;};

    // KTAG
    Bool_t GetIsKTAGCandidate() {return fIsKTAGCandidate;};
    Int_t GetKTAGID() {return fKTAGID;};
    Double_t GetKTAGTime() {return fKTAGTime;};
    void SetIsKTAGCandidate(Bool_t val) {fIsKTAGCandidate=val;};
    void SetKTAGID(Int_t val) {fKTAGID=val;};
    void SetKTAGTime(Double_t val) {fKTAGTime=val;};
    
    // CHANTI 
    Bool_t GetIsCHANTICandidate() {return fIsCHANTICandidate;};
    Int_t GetCHANTIID() {return fCHANTIID;};
    Double_t GetCHANTITime() {return fCHANTITime;};
    void SetIsCHANTICandidate(Bool_t val) {fIsCHANTICandidate=val;};
    void SetCHANTIID(Int_t val) {fCHANTIID=val;};
    void SetCHANTITime(Double_t val) {fCHANTITime=val;};

    // Matching with the downstream track 
    TVector3 GetVertex() {return fVertex;};
    Double_t GetCDA() {return fCDA;};
    void SetVertex(TVector3 val) {fVertex=val;};
    void SetCDA(Double_t val) {fCDA=val;};

  protected :

    Int_t fDownstreamTrackID; // ID of the matched downstream track

    Bool_t fIsKTAGCandidate;  // Flag idicating the presence of a KTAG candidat matching the GTK-track
    Int_t fKTAGID;            // ID of the KTAG candidate matching the GTK-track
    Double_t fKTAGTime;       // Time of the KTAG candidate matching the GTK-track 

    Bool_t fIsCHANTICandidate;  // Flag idicating the presence of a CHANTI candidate matching the GTK-track
    Int_t fCHANTIID;            // ID of the CHANTI candidate closest in time to the GTK-track
    Double_t fCHANTITime;       // Time of the CHANTI candidate closest in time to the GTK-track 

    TVector3 fVertex;         // Vertex formed with the downstream track
    Double_t fCDA;            // CDA of the vertex formed with the downstream track

};

#endif
