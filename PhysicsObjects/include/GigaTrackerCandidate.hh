#ifndef GigaTrackerCandidate_h
#define GigaTrackerCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class GigaTrackerCandidate 
{
  public :
    GigaTrackerCandidate();
    virtual ~GigaTrackerCandidate();
    void Clear();

  public:
    void SetIsGigaTrackerCandidate(Double_t val) {fIsGigaTrackerCandidate=val;};
    void SetMomentum(Double_t val) {fMomentum=val;};
    void SetSlopeXZ(Double_t val) {fSlopeXZ=val;};
    void SetSlopeYZ(Double_t val) {fSlopeYZ=val;};
    void SetTime(Double_t val) {fTime=val;};
    void SetPosition(TVector3 val) {fPosition=val;};
    void SetPosition(Double_t valx, Double_t valy, Double_t valz) {fPosition=TVector3(valx,valy,valz);};

    Bool_t   GetIsGigaTrackerCandidate() {return fIsGigaTrackerCandidate;};
    Double_t GetMomentum() {return fMomentum;};
    Double_t GetSlopeXZ() {return fSlopeXZ;};
    Double_t GetSlopeYZ() {return fSlopeYZ;};
    Double_t GetTime() {return fTime;};
    TVector3 GetPosition() {return fPosition;};

  private:
    Bool_t fIsGigaTrackerCandidate;
    Double_t fMomentum;
    Double_t fSlopeXZ;
    Double_t fSlopeYZ;
    Double_t fTime;
    TVector3 fPosition;
};

#endif
