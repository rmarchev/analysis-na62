#ifndef TrackSTRAWCandidate_HH
#define TrackSTRAWCandidate_HH

#include "TObject.h"
#include "TRecoSpectrometerCandidate.hh"

#include <stdlib.h>
#include <vector>
#include <TCanvas.h>

class TrackSTRAWCandidate : public TObject
{
public :
  TrackSTRAWCandidate();
  virtual ~TrackSTRAWCandidate();
  void Reset();

public:

  void SetSpectrometer(TRecoSpectrometerCandidate* ptr, Int_t ind) { fSpectrometer = ptr; fTrackID = ind; }
  void SetTrackID(Int_t val)      {fTrackID=val;};
  void SetGoodTrack(Bool_t val)   {fGood=val;};
  void SetCharge(Int_t val)   {fCharge=val;};
  void SetTime(Double_t val)   {fTime=val;};

  TRecoSpectrometerCandidate* GetSpectrometer() {return fSpectrometer; }
  Int_t GetTrackID()      {return fTrackID;};
  Int_t GetCharge()   {return fCharge;};
  Bool_t TrackIsGood()   {return fGood;};
  Double_t GetTime()   {return fTime;};

private:

  TRecoSpectrometerCandidate* fSpectrometer;
  Int_t fTrackID;
  Int_t fCharge;
  Double_t fTime;
  Bool_t fGood;

 ClassDef(TrackSTRAWCandidate,1);
};

#endif
