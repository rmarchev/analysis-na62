#ifndef StrawCandidate_h
#define StrawCandidate_h

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class StrawCandidate 
{
  public :
    StrawCandidate();
    virtual ~StrawCandidate();
    void Clear();

  public:
    void SetTrackID(Int_t val)      {fTrackID=val;};
    void SetGoodTrack(Bool_t val)   {fGoodTrack=val;};
    void SetCharge(Int_t val)   {fCharge=val;};
    void SetMultiVertex(Bool_t val) {fMultiVertex=val;};
    void SetAcceptance(Int_t val)   {fAcceptance=val;};
    Int_t GetTrackID()      {return fTrackID;};
    Bool_t GetGoodTrack()   {return fGoodTrack;};
    Int_t GetCharge()   {return fCharge;};
    Bool_t GetMultiVertex() {return fMultiVertex;};
    Int_t GetAcceptance()   {return fAcceptance;};

  private:
    Int_t fTrackID;
    Bool_t fGoodTrack; 
    Int_t fCharge;
    Bool_t fMultiVertex;
    Int_t fAcceptance;
};

#endif
