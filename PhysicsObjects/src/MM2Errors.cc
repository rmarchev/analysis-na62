#include "MM2Errors.hh"
#include <stdlib.h>
#include <cmath>
#include <vector>
#include <iostream>
#include "TMatrixD.h"
#include "TVectorD.h"
#include "TVector.h"

using namespace std;

MM2Errors::MM2Errors(vector<Double_t> &x, TRecoSpectrometerCandidate* track){

  fTrack = track;
  fdim = x.size();
  fx = x;
  fCov.ResizeTo(fdim,fdim);
  fHessianMuon.ResizeTo(fdim,fdim);
  fHessianPion.ResizeTo(fdim,fdim);
  fmmuon = 0.10565837;
  fmpion = 0.13957018;
  fmkaon=0.493677;
  IsCov = false;
  IsErr = false;
  IsHess = false;

  //MakeCovMatrix();
  //ComputeMissingMassSquaredError();
  fmm2pi =  ComputeMissingMassSquared(x,fmpion);
  fmm2mu =  ComputeMissingMassSquared(x,fmmuon);

}

MM2Errors::~MM2Errors(){

}

void MM2Errors::MakeCovMatrix(){

  ///Covariance matrix 6x6 using STRAW(1/p, thetaX, thetaY) and GTK(kp,kthetaX,kthetaY):
  ///Momentum is given GeV and angles in radians
  ///
  ///   | s2(1/p)  s(1/p)s(thetaX)  s(1/p)s(thetaY)   s(1/p)s(pk)    s(1/p)s(kthetaX)    s(1/p)s(kthetaY)   |
  ///   |            s2(thetaX)   s(thetaX)s(thetaY) s(thetaX)s(pk) s(thetaX)s(kthetaX) s(thetaX)s(kthetaY) |
  ///   |                              s2(thetaY)    s(thetaY)s(pk) s(thetaY)s(kthetaX) s(thetaY)s(kthetaY) |
  ///   |                                                s2(pk)      s(pk)s(kthetaX)     s(pk)s(kthetaY)    |
  ///   |                                                              s2(kthetaX)     s(kthetaX)s(kthetaY) |
  ///   |                                                                                  s2(kthetaY)      |

  ///Setting the Spectrometer part
  ///NOTE :: the information is not ordered in the same way as in the STRAW
  fCov[0][0] = fTrack->GetCovariance(4,4)*1e6;             // s2(1/p)    [1/GeV^2]
  fCov[1][1] = fTrack->GetCovariance(0,0);                 // s2(thetaX) [rad^2]
  fCov[2][2] = fTrack->GetCovariance(1,1);                 // s2(thetaY) [rad^2]
  fCov[0][1] = fCov[1][0] = fTrack->GetCovariance(0,4)*1e3; // s(1/p)s(thetaX) [rad/GeV]
  fCov[0][2] = fCov[2][0] = fTrack->GetCovariance(1,4)*1e3; // s(1/p)s(thetaY) [rad/GeV]
  fCov[1][2] = fCov[2][1] = fTrack->GetCovariance(0,1);     // s(thetaX)s(thetaY) [rad^2]


  ///Setting the GigaTracker part
  ///NOTE :: only the diagonal terms are added, no correlations are assumed
  fCov[3][3] = 22500*1e-6;                     // s2(pk)     [GeV^2]
  fCov[4][4] = 256*1e-12;                      // s2(kthetaX)[rad^2]
  fCov[5][5] = 256*1e-12;                      // s2(kthetaY)[rad^2]
  //fCov[3][3] = 2429000*1e-6;                     // s2(pk)     [GeV^2]
  //fCov[4][4] = 155.3*1e-12;                      // s2(kthetaX)[rad^2]
  //fCov[5][5] = 128*1e-12;                      // s2(kthetaY)[rad^2]
  double kthxkthy=TMath::Sqrt(fCov[4][4]*fCov[5][5]);
  double kthxpk  =TMath::Sqrt(fCov[3][3]*fCov[4][4]);
  double kthypk  =TMath::Sqrt(fCov[3][3]*fCov[5][5]);

  //External errors measured by Elisa and Plamen using MC
  double ext_kthxkthy=TMath::Sqrt(1.553e-10*1.28e-10);
  double ext_kthxpk  =TMath::Sqrt(1.553e-10*2.429e6);
  double ext_kthypk  =TMath::Sqrt(1.280e-10*2.429e6);

  double rhokthxkthy= -8.379e-12/ext_kthxkthy;
  double rhokthypk  = 4.181e-4/ext_kthxpk;
  double rhokthxpk  = 0.8369e-4/ext_kthypk;

  fCov[3][4] = fCov[4][3] = rhokthxpk*kthxpk;     // s(pk)s(thetaX) [rad*GeV]
  fCov[3][5] = fCov[5][3] = rhokthypk*kthypk;     // s(pk)s(thetaY) [rad*GeV]
  fCov[4][5] = fCov[5][4] = rhokthxkthy*kthxkthy; // s(thetaX) s(thetaY) [rad^2]
  //fCov[3][4] = fCov[4][3] = 0.8369e-7; // s(pk)s(thetaX) [rad*GeV]
  //fCov[3][5] = fCov[5][3] = 4.181e-7;  // s(pk)s(thetaY) [rad*GeV]
  //fCov[4][5] = fCov[5][4] = -8.379e-12; // s(thetaX) s(thetaY) [rad^2]

  //std::cout << "--------------------" << std::endl;
  //std::cout << "rhothxthy = " << rhokthxkthy << "rhothxpk " << rhokthxpk  << "rhothypk " << rhokthypk << std::endl;
  //std::cout << "| " << fCov[3][3] << " " << fCov[3][4] << " " << fCov[3][5] << " |"<< std::endl;
  //std::cout << "| " << "        " << fCov[4][4] << "    " << fCov[4][5] << " |"<< std::endl;
  //std::cout << "| " << "                   "  << fCov[5][5] <<   " |"<< std::endl;
}

void MM2Errors::ComputeMissingMassSquaredError(){

  TVectorD dfdxmu((Int_t)fdim);
  TVectorD dfdxpi((Int_t)fdim);

  for(int i = 0; i<fdim;i++){
    dfdxmu(i) = ComputeDerivatives(i,fmmuon);
    dfdxpi(i) = ComputeDerivatives(i,fmpion);
  }

  TVectorD covdfpi((Int_t)fdim);
  TVectorD covdfmu((Int_t)fdim);
  covdfpi  = dfdxpi;
  covdfmu  = dfdxmu;
  covdfpi *= fCov;
  covdfmu *= fCov;

  fmm2pierror = dfdxpi*covdfpi;
  fmm2pierror = sqrt(fmm2pierror);

  fmm2muerror = dfdxmu*covdfmu;
  fmm2muerror = sqrt(fmm2muerror);
}

Double_t MM2Errors::ComputeDerivatives(Int_t i , Double_t mass){

  Double_t temp [fdim];
  Double_t delta [fdim];
  vector<Double_t> minus;
  vector<Double_t> plus;

  for (int k = 0; k < fdim; k++){
    if(k!=i) delta[k]=0;
    else if(k==0) delta[k] = 1e-4;
    else if(k==3) delta[k] = 1e-4*fx[k];
    else delta[k] = 1e-6;

    temp[k]  = fx[k]    + delta[k];
    delta[k] = temp[k] - fx[k];
    plus.push_back(fx[k]    + delta[k]);
    minus.push_back(fx[k]    - delta[k]);
  }

  //if(i==4){
  //  std::cout << "Testinggggg ----" << std::endl;
  //  std::cout << "kthx = " << fx[i] << " dkthx == " << delta[i] << std::endl;
  //}else if (i==5){
  //  std::cout << "Testinggggg ----" << std::endl;
  //  std::cout << "kthy = " << fx[i] << " dkthy == " << delta[i] << std::endl;
  //
  //} else if (i==3){
  //  std::cout << "Testinggggg ----" << std::endl;
  //  std::cout << "pk = " << fx[i] << " dpk == " << delta[i] << std::endl;
  //
  //}
  return (ComputeMissingMassSquared(plus,mass) - ComputeMissingMassSquared(minus,mass))/(2*delta[i]);
}

void MM2Errors::BuildHessian(){

  for(int i = 0; i < fdim; i++){
    for(int j = 0; j < fdim; j++){
      fHessianPion[i][j] = ComputeSecondDerivatives(i,j,fmpion);
      fHessianMuon[i][j] = ComputeSecondDerivatives(i,j,fmmuon);
    }
  }

}

Double_t MM2Errors::ComputeSecondDerivatives(Int_t i ,Int_t j, Double_t mass){
  Double_t temp  [fdim];
  Double_t deltai[fdim];
  Double_t deltaj[fdim];
  vector<Double_t> vpp;
  vector<Double_t> vpm;
  vector<Double_t> vmm;
  vector<Double_t> vmp;
  Double_t d2fdxij;
  for (int k = 0; k < fdim; k++){

    if(k!=i) deltai[k] = 0;
    else if(k==0) deltai[k] = 1e-4;
    else if(k==3) deltai[k] = 1e-4*fx[k];
    else deltai[k] = 1e-5;


    if(k!=j) deltaj[k] = 0;
    else if(k==0) deltaj[k] = 1e-4;
    else if(k==3) deltaj[k] = 1e-4*fx[k];
    else deltaj[k] = 1e-5;

    vpp.push_back(fx[k] + deltai[k] + deltaj[k]);
    vmm.push_back(fx[k] - deltai[k] - deltaj[k]);

    if(j==k){
      vmp.push_back(fx[k] + deltai[k] - deltaj[k]);
      vpm.push_back(fx[k] - deltai[k] + deltaj[k]);
    }
    else {
      vmp.push_back(fx[k] + deltai[k] - deltaj[k]);
      vpm.push_back(fx[k] - deltai[k] - deltaj[k]);
    }
  }

  Double_t fpp = ComputeMissingMassSquared(vpp, mass);
  Double_t fmm = ComputeMissingMassSquared(vmm, mass);
  Double_t fpm = ComputeMissingMassSquared(vpm, mass);
  Double_t fmp = ComputeMissingMassSquared(vmp, mass);
  Double_t fxx = ComputeMissingMassSquared(fx, mass);
  if(i!=j)
    d2fdxij = (fpp + fmm - fpm - fmp)/(4*deltai[i]*deltaj[j]);
  else
    d2fdxij = (fpp + fmm - 2*fxx)/(4*deltai[i]*deltaj[j]);

  return d2fdxij;

}

Double_t MM2Errors::ComputeMissingMassSquared(vector<Double_t> &x, Double_t mass){


  //Pion or Muon(first 3 parameters)
  Double_t Epi;
  Double_t px,pz,py;
  Double_t u      = x[0]; // u = 1/p
  Double_t thetax = x[1];
  Double_t thetay = x[2];
  Double_t norm;

  norm = 1./sqrt(1 + thetax*thetax + thetay*thetay);
  px = thetax*norm/u;
  py = thetay*norm/u;
  pz = norm/u;

  //Kaon (second 3 parameters)
  Double_t Ek;
  Double_t kpx,kpz,kpy;
  Double_t pk      = x[3];
  Double_t kthetax = x[4];
  Double_t kthetay = x[5];
  Double_t knorm;

  knorm = 1./sqrt(1 + kthetax*kthetax + kthetay*kthetay);
  kpx = kthetax*knorm*pk;
  kpy = kthetay*knorm*pk;
  kpz = knorm*pk;

  TLorentzVector Track;
  TLorentzVector Kaon;
  Track.SetXYZM(px,py,pz,mass);
  Kaon.SetXYZM(kpx,kpy,kpz,fmkaon);

  return (Kaon - Track).M2();
}
