#include "AnalysisTools.hh"
#include "BTubeField.hh"
#include "TRecoSpectrometerCandidate.hh"
#include "TRecoLKrCandidate.hh"
#include "TMath.h"
#include "Geometry.hh"
#include "Particle.hh"
#include "BlueTubeTracker.hh"

AnalysisTools* AnalysisTools::fInstance = 0;

AnalysisTools::AnalysisTools() {
  fBTubeField = new BTubeField();
  fTracker = new BlueTubeTracker();
  //InitializeMirrorCoordinate();
  fIsClusterCorrected = 0;
}

AnalysisTools *AnalysisTools::GetInstance() {
  if (fInstance==0) {fInstance = new AnalysisTools();}
  return fInstance;
}

/////////////////////////
// Single track vertex //
/////////////////////////
TVector3 AnalysisTools::SingleTrackVertex(TVector3 vect1, TVector3 vect2, TVector3 vectb, TVector3 vectc, Double_t &cda) {
  TVector3 v1 = vect1;
  TVector3 v2 = vect2;
  TVector3 pos1 = vectb;
  TVector3 pos2 = vectc;
  TVector3 r12 = pos1-pos2;
  Double_t v1xv2 = v1.Dot(v2);
  Double_t det   = pow(v1xv2,2)-v1.Mag2()*v2.Mag2();
  if (!det) return TVector3(-9999,-9999,-9999);
  Double_t t1 = (v2.Mag2()*r12.Dot(v1)-v1.Dot(v2)*r12.Dot(v2))/det;
  Double_t t2 = (v1.Dot(v2)*r12.Dot(v1)-v1.Mag2()*r12.Dot(v2))/det;
  TVector3 q1 = pos1+t1*v1;
  TVector3 q2 = pos2+t2*v2;
  TVector3 vertex = 0.5*(q1+q2);
  cda = (q1-q2).Mag();
  return vertex;
}
///////////////////////////////////
// Get trigger mask //
///////////////////////////////////
Int_t AnalysisTools::SelectTriggerMask(int triggerType, int type, int mask) {
  int bitPhysics = 0;
  int bitControl = 4;
  int bitMinBias = 0;
  int bitPinunu = 1;


  if (type&0x2) return 0; // skip periodics


  // Select trigger
  if ((triggerType==0) && (type>>bitControl)&1) return 1; // control trigger.
  else if (triggerType==1) { // physics minimum bias trigger.
    if (!((type>>bitPhysics)&1)) return 0;
    if ((mask>>bitMinBias)&1) return 1;
  }
  else if (triggerType==2) { // physics pinunu trigger.
    if (!((type>>bitPhysics)&1)) return 0;
    if ((mask>>bitPinunu)&1) return 1;
  }
  else return 0; // trigger not found.
}


///////////////////////////////////
// Get track position at plane Z //
///////////////////////////////////
TVector3 AnalysisTools::GetPositionAtZ(TRecoSpectrometerCandidate *cand, Double_t zpos) {
  Double_t posx;
  Double_t posy;
  if (zpos<196345) { // front face of MNP33
    posx = cand->GetPositionBeforeMagnet().X()+cand->GetSlopeXBeforeMagnet()*(zpos-cand->GetPositionBeforeMagnet().Z());
    posy = cand->GetPositionBeforeMagnet().Y()+cand->GetSlopeYBeforeMagnet()*(zpos-cand->GetPositionBeforeMagnet().Z());
  } else {
    posx = cand->GetPositionAfterMagnet().X()+cand->GetSlopeXAfterMagnet()*(zpos-cand->GetPositionAfterMagnet().Z());
    posy = cand->GetPositionAfterMagnet().Y()+cand->GetSlopeYAfterMagnet()*(zpos-cand->GetPositionAfterMagnet().Z());
  }
  return TVector3(posx,posy,zpos);
}

///////////////////////////////////
// Get kaon position at plane Z //
///////////////////////////////////
TVector3 AnalysisTools::GetKaonPositionAtZ(TVector3 mom,TVector3 pos, Double_t zpos) {
  Double_t posx;
  Double_t posy;
  posx = pos.X()+(mom.X()/mom.Z())*(zpos-pos.Z());
  posy = pos.Y()+(mom.Y()/mom.Z())*(zpos-pos.Z());

  return TVector3(posx,posy,zpos);
}

////////////////////////////
// Get LAV station from Z //
////////////////////////////
Int_t AnalysisTools::GetLAVStation(Double_t z) {
  if (z>120000 && z<123000) return 0;
  if (z>128000 && z<130000) return 1;
  if (z>136000 && z<139000) return 2;
  if (z>142000 && z<146000) return 3;
  if (z>150000 && z<154000) return 4;
  if (z>164000 && z<168000) return 5;
  if (z>170000 && z<176000) return 6;
  if (z>178000 && z<182000) return 7;
  if (z>192000 && z<196000) return 8;
  if (z>202000 && z<206000) return 9;
  if (z>214000 && z<222000) return 10;
  if (z>236000 && z<240000) return 11;
  return -1;
}
//Get 4 momentum
TLorentzVector AnalysisTools::Get4Momentum(Double_t *partrack) {
  TLorentzVector pmom;
  Double_t thetaX = partrack[1];
  Double_t thetaY = partrack[2];
  Double_t pmag = partrack[0];
  Double_t pmomz = pmag/sqrt(1.+thetaX*thetaX+thetaY*thetaY);
  Double_t pmomx = pmomz*thetaX;
  Double_t pmomy = pmomz*thetaY;
  pmom.SetXYZM(pmomx/1000,pmomy/1000,pmomz/1000,partrack[3]);
  return pmom;
}


void AnalysisTools::BTubeCorrection(TVector3 *vertex, Double_t *dxdz, Double_t *dydz, Double_t pmom) {
  fBTubeField->GetCorrection(vertex,dxdz,dydz,pmom);
}

Bool_t AnalysisTools::TrackIsInAcceptance(TRecoSpectrometerCandidate *fTrack) {

  TVector3 track_position;
  double r = 0.;


  // rich entrance window.
  track_position = GetPositionAtZ(fTrack,zRICHEntWindow);
  r = TMath::Sqrt((track_position.X() - 34.)*(track_position.X() - 34.) + track_position.Y()*track_position.Y());
  if (r < RICHRMin || r > RICHRMax) return false;

  track_position = GetPositionAtZ(fTrack,zRICHExtWindow);
  r = TMath::Sqrt((track_position.X() -2.)*(track_position.X() - 2.) + track_position.Y()*track_position.Y());
  if (r < RICHRMin || r > RICHRMax) return false;

  // chod.
  track_position = GetPositionAtZ(fTrack,zCHODVL);
  r = TMath::Sqrt(track_position.X()*track_position.X() + track_position.Y()*track_position.Y());
  if (r < CHODRMin || r > CHODRMax) return false;

  track_position = GetPositionAtZ(fTrack,zCHODHL);
  r = TMath::Sqrt(track_position.X()*track_position.X() + track_position.Y()*track_position.Y());
  if (r < CHODRMin || r > CHODRMax) return false;

  // lkr.
  track_position = GetPositionAtZ(fTrack,zLKr);
  if(!LKrGeometricalAcceptance(track_position.X(),track_position.Y())) return false;

  // muv1.
  track_position = GetPositionAtZ(fTrack,zMUV1);
  if( track_position.X() < XMUV1min && track_position.Y() < XMUV1min) return false;
  if( track_position.X() > XMUV1max || track_position.Y() < XMUV1max) return false;
  // muv2.
  track_position = GetPositionAtZ(fTrack,zMUV2);
  if( track_position.X() < XMUV2min && track_position.Y() < XMUV2min) return false;
  if( track_position.X() > XMUV2max || track_position.Y() < XMUV2max) return false;
  // muv3.
  track_position = GetPositionAtZ(fTrack,zMUV3);
  if( track_position.X() < XMUV3min && track_position.Y() < XMUV3min) return false;
  if( track_position.X() > XMUV3max || track_position.Y() < XMUV3max) return false;

  return true;
}

// ---------------------------------------------------------------------------------------------------- //

TVector3 AnalysisTools::BlueFieldCorrection(TVector3 *pmom, TVector3 initPos, Int_t charge, Double_t zvertex) {
  Double_t zmax = 183311.;
  Double_t zmin = 101800.;
  Double_t gtom = 1000.;
  if (initPos.Z()>zmax) {
    initPos.SetX(initPos.X()+(pmom->X()/pmom->Z())*(zmax-initPos.Z()));
    initPos.SetY(initPos.Y()+(pmom->Y()/pmom->Z())*(zmax-initPos.Z()));
    initPos.SetZ(zmax);
  }
  if (initPos.Z()<zmin) {
    initPos.SetX(initPos.X()+(pmom->X()/pmom->Z())*(zmin-initPos.Z()));
    initPos.SetY(initPos.Y()+(pmom->Y()/pmom->Z())*(zmin-initPos.Z()));
    initPos.SetZ(zmin);
  }
  fTracker->SetCharge(charge);
  fTracker->SetInitialPosition(initPos); // Unit: mm
  fTracker->SetInitialMomentum(pmom->X()*gtom,pmom->Y()*gtom,pmom->Z()*gtom); // Unit: MeV
  Double_t zEndTracker = zvertex;
  if (zvertex<zmin) zEndTracker = zmin;
  if (zvertex>=zmax) zEndTracker = zmax;
  fTracker->SetZFinal(zEndTracker); // mm
  fTracker->TrackParticle(); //
  TVector3 vertexCorr = fTracker->GetFinalPosition(); // New vertex
  TVector3 correctedMomentum = fTracker->GetFinalMomentum();
  //std::cout << "Corr mom = " << correctedMomentum.Mag() << std::endl;
  pmom->SetXYZ(correctedMomentum.X()/gtom,correctedMomentum.Y()/gtom,correctedMomentum.Z()/gtom); // New momentum

  if (zvertex<zmin) vertexCorr = vertexCorr+correctedMomentum*(1./correctedMomentum.Z())*(zvertex-vertexCorr.Z());
  return vertexCorr;
}

// LKr geometrical acceptance.

Bool_t AnalysisTools::LKrGeometricalAcceptance(double x, double y) {

  double r2 = x*x + y*y;
  double ax = TMath::Abs(x);
  double ay = TMath::Abs(y);
  if(r2 <= LKrRMin*LKrRMin) return false;
  if(ax >= LKrLMax) return false;
  if(ay >= LKrLMax) return false;
  if((ax+ay) >= TMath::Sqrt(2.)*LKrLMax) return false;

  return true;
}

// ---------------------------------------------------------------------------------------------------- //



///////////////////////////
// Blue field correction //
///////////////////////////
TVector3 AnalysisTools::BlueFieldCorrection(Particle *track, Double_t zvertex) {
  Double_t zmax = 183311.;
  Double_t zmin = 101800.;
  Double_t gtom = 1000.;
  TVector3 initPos = track->GetPosition();
  if (initPos.Z()>zmax) {
    initPos.SetX(track->GetPosition().X()+(track->GetMomentum().X()/track->GetMomentum().Z())*(zmax-track->GetPosition().Z()));
    initPos.SetY(track->GetPosition().Y()+(track->GetMomentum().Y()/track->GetMomentum().Z())*(zmax-track->GetPosition().Z()));
    initPos.SetZ(zmax);
  }
  fTracker->SetCharge(1);
  fTracker->SetInitialPosition(initPos); // Unit: mm
  fTracker->SetInitialMomentum(track->GetMomentum().X()*gtom,track->GetMomentum().Y()*gtom,track->GetMomentum().Z()*gtom); // Unit: MeV
  Double_t zEndTracker = zvertex;
  if (zvertex<zmin) zEndTracker = zmin;
  if (zvertex>=zmax) zEndTracker = zmax;
  fTracker->SetZFinal(zEndTracker); // mm
  //  cout << "z interval " << zvertex << endl;
  fTracker->TrackParticle(); //
  TVector3 vertexCorr = fTracker->GetFinalPosition(); // New vertex
  TVector3 correctedMomentum = fTracker->GetFinalMomentum();
  TLorentzVector pionMomentumCorr;
  pionMomentumCorr.SetXYZM(correctedMomentum.X()/gtom,correctedMomentum.Y()/gtom,correctedMomentum.Z()/gtom,0.13957018); // New pion momentum
  if (zvertex<zmin) vertexCorr = vertexCorr+correctedMomentum*(1./correctedMomentum.Z())*(zvertex-vertexCorr.Z());
  track->SetMomentum(pionMomentumCorr);
  return vertexCorr;
}
// Track propagation.


// ---------------------------------------------------------------------------------------------------- //
