#include "CHODCandidate.hh"

CHODCandidate::CHODCandidate()
{}

CHODCandidate::~CHODCandidate()
{}

void CHODCandidate::Clear() {
  fDiscriminant = 99999999.;
  fDeltaTime = 99999999.;
  fX = -99999.;
  fY = -99999.;
  fIsCHODCandidate = 0;
}
