#include "TrackCHODCandidate.hh"

ClassImp(TrackCHODCandidate);

TrackCHODCandidate :: TrackCHODCandidate () : TObject () {

  Reset ();

}


TrackCHODCandidate :: ~TrackCHODCandidate () {

  Reset ();

}

void TrackCHODCandidate::Reset (){


  fCHODTime = -99999;
  fCHODdt = -99999;
  fCHODVerticalPosition=0;
  fCHODHorizontalPosition=0;
  fCHODdisc=0;
  fCHODdtohit=0;
  fCHODPosition=TVector3(0,0,0);

  fGood = false;

}
