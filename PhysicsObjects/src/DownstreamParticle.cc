#include "DownstreamParticle.hh"

DownstreamParticle::DownstreamParticle()
{ }

DownstreamParticle::~DownstreamParticle()
{ }

////void DownstreamParticle::Store(DownstreamParticle *right) {
////  fTrackID                 = right->GetTrackID();
////  fMomentum                = right->GetMomentum();
////  fPosition                = right->GetPosition() ;
////  fTime                    = right->GetTime() ;
////  fQuality                 = right->GetQuality();
////  fType                    = right->GetType();
////  fMuonMomentum            = right->GetMuonMomentum()  ;
////  fElectronMomentum        = right->GetElectronMomentum();
////  fCHODTime                = right->GetCHODTime();
////  fCHODPosition            = right->GetCHODPosition();
////  fRICHMultiIsCandidate    = right->GetRICHMultiIsCandidate();
////  fRICHMultiMass           = right->GetRICHMultiMass();
////  fRICHMultiMomentum       = right->GetRICHMultiMomentum() ;
////  fRICHMultiTime           = right->GetRICHMultiTime();
////  fRICHMultiChi2           = right->GetRICHMultiChi2();
////  fRICHSingleIsCandidate   = right->GetRICHSingleIsCandidate();
////  fRICHSingleMass          = right->GetRICHSingleMass();
////  fRICHSingleMomentum      = right->GetRICHSingleMomentum() ;
////  fRICHSingleTime          = right->GetRICHSingleTime();
////  fRICHSingleChi2          = right->GetRICHSingleChi2();
////  fLKrID                   = right->GetLKrID() ;
////  fLKrTime                 = right->GetLKrTime();
////  fLKrEovP                 = right->GetLKrEovP();
////  fMUV1ID                  = right->GetMUV1ID();
////  fMUV1Energy              = right->GetMUV1Energy();
////  fMUV1Time                = right->GetMUV1Time();
////  fMUV1ShowerWidth         = right->GetMUV1ShowerWidth();
////  fMUV2ID                  = right->GetMUV2ID();
////  fMUV2Energy              = right->GetMUV2Energy();
////  fMUV2Time                = right->GetMUV2Time();
////  fMUV2ShowerWidth         = right->GetMUV2ShowerWidth();
////  fCalorimetricEnergy      = right->GetCalorimetricEnergy();
////  fMUV3ID                  = right->GetMUV3ID();
////  fMUV3Time                = right->GetMUV3Time();
////  fMUV3Distance            = right->GetMUV3Distance();
////  fUpstreamTrackID         = right->GetUpstreamTrackID();
////  for (Int_t j=0; j<3; j++)  fPosAtGTK[j]    = right->GetPositionAtGTK(j);
////  for (Int_t j=0; j<4; j++)  fPosAtStraw[j]  = right->GetPositionAtStraw(j);
////  for (Int_t j=0; j<2; j++)  fPosAtRICH[j]   = right->GetPositionAtRICH(j);
////  for (Int_t j=0; j<12; j++) fPosAtLAV[j]    = right->GetPositionAtLAV(j);
////  for (Int_t j=0; j<2; j++)  fPosAtCHOD[j]   = right->GetPositionAtCHOD(j);
////  for (Int_t j=0; j<2; j++)  fPosAtIRC[j]    = right->GetPositionAtIRC(j);
////  fPosAtLKr = right->GetPositionAtLKr();
////  for (Int_t j=0; j<3; j++) fPosAtMUV[j] = right->GetPositionAtMUV(j);
////
////}

void DownstreamParticle::Clear()
{
    Particle::Clear();

    fMuonMomentum.SetXYZM(0.,0.,0.,0.);
    fElectronMomentum.SetXYZM(0.,0.,0.,0.);
    fCHODTime = -99999.;
    fCHODPosition.SetXYZ(0.,0.,0.);
    fRICHMultiIsCandidate = 0;
    fRICHMultiMass = -99999.;
    fRICHMultiMomentum.SetXYZM(0.,0.,0.,0.);
    fRICHMultiTime = -99999.;
    fRICHMultiChi2 = -99999.;
    fRICHSingleIsCandidate = 0;
    fRICHSingleMass = -99999.;
    fRICHSingleMomentum.SetXYZM(0.,0.,0.,0.);
    fRICHSingleTime = -99999.;
    fRICHSingleChi2 = -99999.;
    fLKrID = -1;
    fLKrTime = -99999.;
    fLKrEovP = -10.;
    fMUV1ID = -1;
    fMUV1Energy = 0.;
    fMUV1Time = -99999.;
    fMUV1ShowerWidth = -100.;
    fMUV1EMerged = 0.;
    fMUV2ID = -1;
    fMUV2Energy = 0.;
    fMUV2Time = -99999.;
    fMUV2ShowerWidth = -100.;
    fCalorimetricEnergy = 0.;
    fMUV3ID = -1;
    fMUV3Time = -99999.;
    fMUV3Distance.Set(-99999.,-99999.);
    fUpstreamTrackID = -1;
    for (Int_t j=0; j<3; j++) fPosAtGTK[j] = TVector3(0,0,0);
    for (Int_t j=0; j<4; j++) fPosAtStraw[j] = TVector3(0,0,0);
    for (Int_t j=0; j<2; j++) fPosAtRICH[j] = TVector3(0,0,0);
    for (Int_t j=0; j<12; j++) fPosAtLAV[j] = TVector3(0,0,0);
    for (Int_t j=0; j<2; j++) fPosAtCHOD[j] = TVector3(0,0,0);
    for (Int_t j=0; j<2; j++) fPosAtIRC[j] = TVector3(0,0,0);
    fPosAtLKr = TVector3(0,0,0);
    for (Int_t j=0; j<3; j++) fPosAtMUV[j] = TVector3(0,0,0);
    fIsPhotonLKrCandidate = 0;
    fIsPhotonLAVCandidate = 0;
    fIsPhotonIRCCandidate = 0;
    fIsPhotonSACCandidate = 0;
    fNPhotonLKrCandidates = 0;
    for (Int_t j=0; j<10; j++) {
        fPhotonLKrCandidateTime[j] = 999999.;
        fPhotonLKrCandidatePosition[j] = TVector2(-99999.,-99999.);
        fPhotonLKrCandidateEnergy[j] = 0.;
        fPhotonLKrCandidateNCells[j] = 0;
    }
    fNPhotonLAVCandidates = 0;
    for (Int_t j=0; j<50; j++) {
        fPhotonLAVCandidateTime[j] = 999999.;
        fPhotonLAVCandidateID[j] = 0.;
    }
    fNPhotonIRCCandidates = 0;
    for (Int_t j=0; j<20; j++) {
        fPhotonIRCCandidateTime[j] = 999999.;
        fPhotonIRCCandidateID[j] = 0.;
    }
    fNPhotonSACCandidates = 0;
    for (Int_t j=0; j<20; j++) {
        fPhotonSACCandidateTime[j] = 999999.;
        fPhotonSACCandidateID[j] = 0.;
    }

}
