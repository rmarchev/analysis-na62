#include "MUVCandidate.hh"

MUVCandidate::MUVCandidate()
{}

MUVCandidate::~MUVCandidate()
{}

void MUVCandidate::Clear() {
  fIsMUVCandidate = 0;
  fID = -1;
  fDiscriminant = 99999999.;
  fDeltaTime = 99999999.;
  fX = -99999.;
  fY = -99999.;
  fEnergy = 0.;
  fNCells = 0;
  fNMerged = 0;
  fEMerged = 0;
}
