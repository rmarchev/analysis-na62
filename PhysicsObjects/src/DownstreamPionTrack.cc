#include "DownstreamPionTrack.hh"

ClassImp(DownstreamPionTrack);


DownstreamPionTrack :: DownstreamPionTrack () : TObject () {

    Reset ();
    fMass=0.13957018; //Pion Mass in GeV!!!
}

DownstreamPionTrack :: ~DownstreamPionTrack () {

    Reset ();

}

void DownstreamPionTrack :: Reset () {

   fMass=0;
   fP=0;
   fkaonpioncda=-5;
   fvtxchi2=-999;
   fEopLKr =-999;
   fEopMUV1=-999;
   fEopMUV2=-999;
   fMM2=-999 ;
   fMomentum=TVector3(0,0,0);
   fFourMomentum=TLorentzVector(0.,0.,0.,0.);

}
