#include "StrawCandidate.hh"

StrawCandidate::StrawCandidate()
{}

StrawCandidate::~StrawCandidate()
{}

void StrawCandidate::Clear() {
  fTrackID = -1;
  fGoodTrack = 0;
  fCharge = 0;
  fMultiVertex = 0;
  fAcceptance = 0;
}
