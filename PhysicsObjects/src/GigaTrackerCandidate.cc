#include "GigaTrackerCandidate.hh"

GigaTrackerCandidate::GigaTrackerCandidate()
{}

GigaTrackerCandidate::~GigaTrackerCandidate()
{}

void GigaTrackerCandidate::Clear() {
  fIsGigaTrackerCandidate = 0;
  fMomentum = 0;
  fSlopeXZ = 9999999.;
  fSlopeYZ = 9999999.;
  fTime = 9999999.;
  fPosition.SetXYZ(-99999.,-99999.,-99999.);
}
