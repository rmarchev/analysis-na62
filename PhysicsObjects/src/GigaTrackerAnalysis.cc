#include "GigaTrackerAnalysis.hh"
#include "AnalysisTools.hh"
#include "Parameters.hh"
using namespace NA62Constants;

GigaTrackerAnalysis::GigaTrackerAnalysis(NA62Analysis::Core::BaseAnalysis *ba) {
  fPar = Parameters::GetInstance();

  // Geometry parameters
  fGTKOffset = 0.;
  fZGTK[0] = 79600.;
  fZGTK[1] = 92800.;
  fZGTK[2] = 102400.;

  // Some pointers
  fTools = AnalysisTools::GetInstance();
  fUserMethods = new UserMethods(ba);
  //Constructor(): fMethod(ba){}
  //UserMethods fMethod;



  // Histogram booking
  // Standard Reco < 15/09/2016
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_p","",90,60,90));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_tx","",200,-0.004,0.004));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_ty","",200,-0.004,0.004));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_time","",80,-5,5));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_chi2T","",100,-2.,2.,100,0,100.));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_chi2vsdeltap","",100,-10.,10.,100,0.,100.));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_chi2vsdeltax","",100,-0.0007,0.0007,100,0.,100.));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_chi2vsdeltay","",100,-0.0007,0.0007,100,0.,100.));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_chi2ev","",100.,-2.,2.,100,0,100));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_chi2evVSchi2","",100.,0.,100.,100,0,100));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_chi2tot","",100,0,100));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_step1_p","",90,60,90));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_step1_tx","",200,-0.004,0.004));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_step1_ty","",200,-0.004,0.004));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_step1_time","",80,-5,5));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_step1_yx","",70,-70,70,70,-70,70));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_step1_xvsx","",70,-70,70,50,0.,0.002));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_step1_yvsy","",70,-70,70,50,-0.001,0.001));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_step1_chi2evVSchi2","",100.,0.,100.,100,0,100));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_step1_cdadt","",100,-5,5,400,0,100));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_triplet_step1_dscriminant","",2000,0,1));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_dt12","",100.,-5.,5.,100,-5,5));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_dt13","",100.,-5.,5.,100,-5,5));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_triplet_dt23","",100.,-5.,5.,100,-5,5));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_p","",90,60,90));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_tx","",200,-0.004,0.004));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_ty","",200,-0.004,0.004));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_time","",80,-5,5));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_chi2T","",100,-2.,2.,100,0,100.));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_chi2vsdeltap","",100,-10.,10.,100,0.,100.));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_chi2vsdeltax","",100,-0.0007,0.0007,100,0.,100.));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_chi2vsdeltay","",100,-0.0007,0.0007,100,0.,100.));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_chi2ev","",100.,-2.,2.,100,0,100));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_chi2evVSchi2","",100.,0.,100.,100,0,100));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_chi2tot","",100,0,100));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_step1_p","",90,60,90));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_step1_tx","",200,-0.004,0.004));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_step1_ty","",200,-0.004,0.004));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_step1_time","",80,-5,5));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_step1_yx","",70,-70,70,70,-70,70));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_step1_xvsx","",70,-70,70,50,0.,0.002));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_step1_yvsy","",70,-70,70,50,-0.001,0.001));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_step1_chi2evVSchi2","",100.,0.,100.,100,0,100));
  fUserMethods->BookHisto(new TH2F("GTKAnalysis_candidate_doublet_step1_cdadt","",100,-5,5,400,0,100));
  fUserMethods->BookHisto(new TH1F("GTKAnalysis_candidate_doublet_step1_dscriminant","",200,0,1));

  // Alternative Reco >=15/09/2016
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt10","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt11","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt12","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt13","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt14","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt15","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt16","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt17","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt18","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt19","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt20","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt21","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt22","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt23","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt24","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt25","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt26","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt27","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt28","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt29","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt30","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt31","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt32","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt33","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt34","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt35","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt36","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt37","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt38","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_testdt39","",400,-20,20));
  fUserMethods->BookHisto(new TH1F("GTKReco_chi2ev","",100,0,100));
  fUserMethods->BookHisto(new TH1F("GTKReco_chi2","",200,0,200));

  // Candidates
  //for (int j(0); j<10; j++) fGigaTrackerCandidate[j] = new GigaTrackerCandidate();
  fGigaTrackerGTK1Hit = new int[50];
  fGigaTrackerGTK2Hit = new int[50];
  fGigaTrackerGTK3Hit = new int[50];

  // Reference detector
  fReferenceDetector = 0;

  // Projection discriminant
  DiscriminantNormalization();

  // TW corrections for 2016 (from E. Gamberini 10/2016)
  ApplyTimeCorrections();
}

GigaTrackerAnalysis::~GigaTrackerAnalysis()
{ }

void GigaTrackerAnalysis::StartBurst(Int_t year, Int_t runid, Int_t burstid) {
  fYear = year;
  fBurst = burstid;
  fRun = runid;
  //fIsFilter = fUserMethods->GetTree("Reco")->FindBranch("FilterWord") ? true : false;
  //if (fIsFilter) return;

  double triggeroffset = 0.1;
  TH1F *h[30];
  for (int iH(0); iH<30; iH++) h[iH] = new TH1F(Form("histgtk%d",iH),"",200,-10,10);
  TF1  *f1 = new TF1("f1","gaus",-100,+100);
  double tdcCalib = 24.951059536/256.0;
  TString variables = Form("(GigaTracker.fHits.fRawTime-RawHeader.fFineTime*%f):GigaTracker.fHits.fChipPixelID:GigaTracker.fHits.fToT:(GigaTracker.fHits.fChipID+10*GigaTracker.fHits.fStationNo)",tdcCalib);
  TString condi = "GigaTracker.fHits.fRawTime";
  TChain* reco = fUserMethods->GetTree("Reco");
  reco->GetTree()->SetEstimate(1e+07);
  reco->GetTree()->Draw(variables.Data(),condi.Data(),"goff",30000);
  int nentries = reco->GetTree()->GetSelectedRows();
  cout << "Total number of hits in the first 30K events: " << nentries << endl;
  double *dtime = reco->GetTree()->GetV1();
  double *pixel = reco->GetTree()->GetV2();
  double *tovth = reco->GetTree()->GetV3();
  double *chips = reco->GetTree()->GetV4();
  for (int iEv(0); iEv<nentries; iEv++) {
    int jH = (int)chips[iEv];
    int jSt = jH/10;
    int jCh = jH-jSt*10;
    int row = pixel[iEv]/40;
    if (jSt>2 || jCh>9) continue;
    double dtcorr = dtime[iEv]-fTW[jSt][jCh][row]->Eval(tovth[iEv]);
    h[jH]->Fill(dtcorr);
  }
  for (int iH(0); iH<30; iH++) {
    fTOffset[iH] = 0.;
    double xc = h[iH]->GetXaxis()->GetBinCenter(h[iH]->GetMaximumBin());
    int nen = h[iH]->Integral();
    if (nen<250) continue;
    h[iH]->Fit(f1,"q0","",xc-0.6,xc+0.6);
    double tmean = f1->GetParameter(1);
    double tsigm = f1->GetParameter(2);
    int iSt = iH/10;
    int iCh = iH-iSt*10;
    cout << iSt << " " << iCh << " " << nen << " " << tmean << " " << tsigm << endl;
    if (fabs(tsigm)<0.7) fTOffset[iH] = tmean-triggeroffset;
  }
  for (int j(0); j<30; j++) delete h[j];
  delete f1;

}


void GigaTrackerAnalysis::Clear() {
  fTimeShift = 0.;
  fNGigaTrackerCandidates = 0;
  fNGigaTrackerGTK1Hits = 0;
  fNGigaTrackerGTK2Hits = 0;
  fNGigaTrackerGTK3Hits = 0;
  //for (int j(0); j<10; j++) fGigaTrackerCandidate[j]->Clear();
  for (int k(0); k<50; k++) {
    fGigaTrackerGTK1Hit[k] = -1;
    fGigaTrackerGTK2Hit[k] = -1;
    fGigaTrackerGTK3Hit[k] = -1;
  }
  fIsTriplet = 0;
  fIsDoublet = 0;
}

////////////////////////////////
// ALTERNATIVE RECONSTRUCTION //
////////////////////////////////
void GigaTrackerAnalysis::ApplyTimeCorrections() {

  // Define function for tw correction
  for(int iS(0); iS<3; iS++){
    for(int iC(0); iC<10; iC++){
      for(int iR(0); iR<45; iR++){ fTW[iS][iC][iR] = new TF1("fToT_Corr_2nd","pol2",0,25);
      }
    }
  }

  // Define function parameters
  //  ifstream file("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/GTK_correction_tw_1141.dat");
  ifstream file("/afs/cern.ch/user/r/ruggierg/workspace/public/na62git/database/GTK_correction_tw_1181.dat");
  double par0, par1, par2;
  int chip;
  int station;
  int row;
  while(file>>station>>chip>>row>>par0>>par1>>par2) {
    fTW[station][chip][row]->SetParameters(par0, par1, par2);
  }
  file.close();
}

int GigaTrackerAnalysis::ReconstructCandidateNoGTK(TRecoSpectrometerCandidate *thisTrack) {
  TLorentzVector k4mom = GetMomentum(74.9*1000,0.00122,0.000026,0.493667);
  TLorentzVector p4mom = GetMomentum(thisTrack->GetMomentum(),thisTrack->GetSlopeXBeforeMagnet(),thisTrack->GetSlopeYBeforeMagnet(),0.13957018);
  TVector3 kmom = k4mom.Vect();
  TVector3 kpos(0,0,101800);
  TVector3 pmom = p4mom.Vect();
  TVector3 ppos = thisTrack->GetPositionBeforeMagnet();
  Double_t cda = -99999.;
  // TVector3 vtx = fTools->SingleTrackVertex(&kmom,&pmom,&kpos,&ppos,&cda);
  TVector3 vtx = fTools->SingleTrackVertex(kmom,pmom,kpos,ppos,cda);
  vtx = fTools->BlueFieldCorrection(&pmom,ppos,thisTrack->GetCharge(),vtx.Z());
  vtx = fTools->BlueFieldCorrection(&kmom,kpos,1,vtx.Z());
  vtx = fTools->SingleTrackVertex(kmom,pmom,kpos,ppos,cda);
  //vtx = fTools->SingleTrackVertex(&kmom,&pmom,&kpos,&ppos,&cda);
  Double_t discrmax = cda>25. ? 0 : 1.;
  fNGigaTrackerCandidates = 1;

  return 1;
}

int GigaTrackerAnalysis::ReconstructCandidates(TRecoGigaTrackerEvent *event, double reftime, bool remove) {

  // Remove existing candidates
  if (remove) {
    int nCand = event->GetNCandidates(); // mandatory because RemoveCandidate update NCandidates !
    // for (int iC(0);iC<nCand; iC++) event->RemoveCandidate(0);
    for (int iC(0);iC<nCand; iC++) event->RemoveCandidate(0);
  }

  // Select hits per station
  std::vector<int> idHit;
  int nhits = event->GetNHits();
  int jhit[3] = {0}; // counter of selected hits per stations
  TClonesArray& Hits = (*(event->GetHits()));
  for (int jHit(0);jHit<nhits;jHit++) {
    TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*)Hits[jHit];
    int jS = hit->GetStationNo();
    int jC = hit->GetChipID();
    if (jhit[jS]>=50) return 0; // reject too crowdy events
    CorrectHitTime(hit); // Temporary correction of the hit time
    double dt = hit->GetTime()-reftime-fTimeShift;
    if (fTimeShift==0) {
      fUserMethods->FillHisto(Form("GTKReco_testdt%d%d",jS+1,jC),dt);
    }
    if (fabs(dt)>fRecoTimingCut) continue;
    idHit.push_back(jHit);
    jhit[jS]++;
  }
  StationOrder so(event);
  std::sort(idHit.begin(),idHit.end(),so);
  int pS0 = jhit[0];
  int pS1 = jhit[0]+jhit[1];

  // Build candidates
  int nRecoCand = 0;
  vector<int>::iterator jh0=idHit.begin();
  while(jh0<next(begin(idHit),pS0)) {
    vector<int>::iterator jh1=idHit.begin();
    advance(jh1,pS0);
    while(jh1<next(begin(idHit),pS1)) {
      vector<int>::iterator jh2=idHit.begin();
      advance(jh2,pS1);
      while(jh2!=idHit.end()) {
        int nid = 1000000*(*jh2)+1000*(*jh1)+(*jh0);
        if (BuildTrack(nid,event)) nRecoCand++;
        jh2++;
      }
      jh1++;
    }
    jh0++;
  }

  return nRecoCand;
}

void GigaTrackerAnalysis::CorrectHitTime(TRecoGigaTrackerHit *hit) {
  //  double hitTimeOld = hit->GetTime(); // 6342 Version 1141 on reco r1141
  //  double tOffset[30] = {+0.192,+0.192,+0.192,+0.192,+0.192,+0.192,+0.192,+0.192,+0.192,+0.192,
  //                        +0.216,+0.216,+0.216,+0.216,+0.216,+0.216,+0.216,+0.216,+0.216,+0.216,
  //                        +0.219,+0.219,+0.219,+0.219,+0.219,+0.219,+0.219,+0.219,+0.219,+0.219}; // 6610 Version 1181 on reco r1211

  //double tOffset[30] = {0.05,+0.03,+0.07,-3.1,+0,-0.07,+0.03,+0.03,+0.04,-0.045,
  //                      0.08,+0.03,+0.05,+0.02,-0.03,+0.02,+0.04,+0.07,+0.06,+0.01,
  //                      0.02,+0.04,+0.13,+0.05,-0.12,+0.03,+0.04,+0.08,+0.04,+.060}; // 6610 Version 1181 on reco r1211
  //double globalOffset = 0.2;
  //  double tOffset[30] = {-0.15,-0.14,-0.13,+2.97,-0.06,-0.15,-0.20,-0.13,-0.08,-0.15,
  //                        -0.06,-0.11,-0.11,-0.10,-0.14,-0.08,-0.19,-0.05,-0.12,-0.14,
  //                        -0.20,-0.08,-0.08,-0.07,-0.10,-0.19,-0.14,-0.12,-0.14,-0.13}; // 6670 Version 1181 on reco r1211
  //  double globalOffset = 0.2; // 6670 914
  //  double globalOffset = 0.422; // 6670 1438
  double hitTimeOld = hit->GetRawTime();
  double tot = hit->GetToT();
  int station = hit->GetStationNo();
  int chip = hit->GetChipID();
  int pixel = hit->GetChipPixelID();
  int row = pixel/40;
  double hitTime = hitTimeOld - fTW[station][chip][row]->Eval(tot);
  //  hit->SetTime(hitTime+0.97); // Version 1141
  // hit->SetTime(hitTime-tOffset[10*station+chip]);
  hit->SetTime(hitTime-fTOffset[10*station+chip]);
}

Bool_t GigaTrackerAnalysis::BuildTrack(int nid, TRecoGigaTrackerEvent *event) {

  // Basic Geometrical Parameters for the Reconstruction
  double        magnetFieldStrength0 = -1.6678;//Tm
  double        magnetFieldStrength4 = -0.7505;//Tm
  double        magnetZLength1       = 2.5 * 1.e3;
  double        magnetZLength2       = 0.4 * 1.e3;
  double        detectorZPosition    = 0.5 * (79.580 + 102.420) * 1.e3;
  double        magnet1PositionZ     = 82.980 * 1.e3 - 0.5 * magnetZLength1 - detectorZPosition;
  double        magnet2PositionZ     = 86.580 * 1.e3 - 0.5 * magnetZLength1 - detectorZPosition;
  double        magnet5PositionZ     = 102.000 * 1.e3 - 0.5 * magnetZLength2 - detectorZPosition;
  double        stationZLength       = 200e-6*1e3;
  double        Z[3] = {79600.,92800.,102400.};
  double        station1PositionZ    = Z[0] - 0.5 * stationZLength - detectorZPosition;
  double        station2PositionZ    = Z[1] - 0.5 * stationZLength - detectorZPosition;
  double        station3PositionZ    = Z[2] - 0.5 * stationZLength - detectorZPosition;
  double        station2PositionY    = -60.e-3 * 1.e3;

  // Derived Parameters
  Double_t fClight                = TMath::C();
  Double_t fBLbend                = -1.0*magnetFieldStrength0*magnetZLength1;
  Double_t fDeltaBend             = magnet2PositionZ - magnet1PositionZ;
  Double_t fBeta                  = 1e-3*fClight * fBLbend * fDeltaBend ;
  Double_t fBLtrim                = magnetFieldStrength4*magnetZLength2;
  Double_t fDeltaTrim             = station3PositionZ - magnet5PositionZ;
  Double_t fDelta12               = station2PositionZ - station1PositionZ;
  Double_t fDelta13               = station3PositionZ - station1PositionZ;
  Double_t fDelta23               = station3PositionZ - station2PositionZ;
  Double_t fAlpha                 = fDelta12 / fDelta13;
  Double_t fDeltaZ                = -station2PositionY;

  // New variable declaration
  Double_t X[3];
  Double_t Xshift[3];
  Double_t Y[3];
  Double_t T[3];

  Int_t jhit[3] = {nid%1000,(nid%1000000)/1000,nid/1000000};
  TClonesArray& Hits = (*(event->GetHits()));
  TRecoGigaTrackerHit *hit[3];
  for(Int_t iStation=0; iStation < 3 ; iStation++){
    hit[iStation] = (TRecoGigaTrackerHit*)Hits[jhit[iStation]];
    X[iStation] =  hit[iStation]->GetPosition().X();
    Xshift[iStation] = X[iStation];
    Y[iStation] =  hit[iStation]->GetPosition().Y();
    T[iStation] =  hit[iStation]->GetTime(); //ns
  }

  // Compute kinematics and time
  Double_t p = fBeta / (Y[0] * (1.0 - fAlpha) - Y[1] + fDeltaZ + (fAlpha * Y[2]));
  Double_t fShiftTrim              = -((1e-3*fClight * fBLtrim) / p) * fDeltaTrim;
  Xshift[2] -= fShiftTrim ;   // Correct the TRIM5 effect
  Double_t dydz = (Y[2] - Y[0]) / fDelta13;
  Double_t dxdz = (X[2] - X[0]) / fDelta13 - (((1e-3*fClight * fBLtrim) / p) * (1. - (fDeltaTrim / fDelta13)));
  Double_t Time = (T[0] + T[1] + T[2]) / 3.0; //ns

  // Track fitting to straight lines for horizontal view (X), with specific offset for GTK3 (trim effect corrected)
  Double_t a,b,rho,chi2X;
  Double_t sigma[3] = {0.0866,0.0866,0.220};
  Double_t z[3]     = {0,fDelta12,fDelta13};
  LinearLeastSquareFit(z,Xshift,3,sigma,a,b,rho,chi2X);

  // Constraints on vertical view (Y)
  Double_t sigmaY12 = 1.42 ;
  Double_t sigmaY23 = 1.20 ;
  Double_t chi2Y = TMath::Power((Y[1] - Y[0])/sigmaY12 , 2.) + TMath::Power((Y[2] - Y[1])/sigmaY23 , 2.) ;

  // Constraints on relative cluster times
  Double_t sigmaT = 0.270;
  Double_t chi2Time = TMath::Power((T[1] - T[0]  )/sigmaT , 2.) + TMath::Power((T[2] - T[1] )/sigmaT , 2.) ;

  // Global Chi2
  Double_t chi2 = (chi2X + chi2Y + chi2Time) ;

  // Conditions
  double pkaon = 1e-6*p;
  if (pkaon<72000||pkaon>78000) return 0;
  if (dxdz>0.0016) return 0;
  if (dxdz<0.0009) return 0;
  if (dydz>0.0004) return 0;
  if (dydz<-0.0003) return 0;
  if (chi2X>20) return 0; // old >80
  if (chi2Y>20) return 0; // old >10
  if (chi2Time>30) return 0; // old not applyed

  // Corrections
  dxdz -= -4.2e-06+0.9e-06*X[2];
  pkaon *= (1-0.0035);

  // Fill pararmeters
  TRecoGigaTrackerCandidate* cand = (TRecoGigaTrackerCandidate*)event->AddCandidate();
  Double_t pz = pkaon/TMath::Sqrt(1.+dxdz*dxdz+dydz*dydz);
  cand->SetMomentum(TVector3(pz*dxdz,pz*dydz,pz));
  cand->SetTime(Time-fTimeShift);
  for (int jS(0); jS<3; jS++) cand->SetPosition(jS,TVector3(X[jS],Y[jS],Z[jS]));
  cand->SetChi2X(chi2X);
  cand->SetChi2Y(chi2Y);
  cand->SetChi2Time(chi2Time);
  cand->SetChi2(chi2);
  cand->SetType(123);


  // Test
  if (fTimeShift==0) {
    Double_t chi2ev = Chi2Event(cand);
    fUserMethods->FillHisto("GTKReco_chi2ev",chi2ev);
    fUserMethods->FillHisto("GTKReco_chi2",chi2);
  }

  return 1;
}
void GigaTrackerAnalysis::LinearLeastSquareFit(Double_t *x, Double_t *y, Int_t Nsample, Double_t *sigma, Double_t &a, Double_t &b, Double_t &rho, Double_t &chi2){

  Double_t xmean = TMath::Mean(Nsample,x);
  Double_t ymean = TMath::Mean(Nsample,y);
  Double_t varx = 0.;
  Double_t vary = 0.;
  Double_t covxy = 0.;

  for(Int_t i=0; i < Nsample ; i++){
    varx += (x[i] - xmean) * (x[i] - xmean) ;
    vary += (y[i] - ymean) * (y[i] - ymean) ;
    covxy += (x[i] - xmean) * (y[i] - ymean) ;
  }
  varx = varx / (Double_t)Nsample;
  vary = vary / (Double_t)Nsample;
  covxy = covxy / (Double_t)Nsample;
  b = covxy / varx;
  a = ymean - b * xmean;
  rho = covxy / (TMath::Sqrt(varx) * TMath::Sqrt(vary));

  chi2 = 0;
  for(Int_t i=0; i < Nsample ; i++){
    chi2 += ((y[i] - a - b*x[i])/sigma[i]) * ((y[i] - a - b*x[i])/sigma[i]) ;
  }

  return;
}

//////////////
// Matching //
//////////////
int GigaTrackerAnalysis::TrackCandidateMatching(TRecoGigaTrackerEvent *event, UpstreamParticle *kaon, int sC, int nC) { // Three pion only: true matching
  if (!(nC-sC)) return -1;
  vector<int> id(nC-sC);
  iota(begin(id),end(id),sC);
  Chi2TrueCondition co(this,event,kaon);
  return *min_element(id.begin(),id.end(),co);
}

int GigaTrackerAnalysis::TrackCandidateMatching(TRecoGigaTrackerEvent *event, TRecoSpectrometerCandidate *strack, Double_t timeref, int sC, int nC) { // One track matching
  if (!(nC-sC)) return -1;
  //cout << "nC == " << nC << " sC == " << sC <<endl;
  // Select the best track
  vector<int> id(nC-sC);
  iota(begin(id),end(id),sC);
  Chi2Condition co(this,event,timeref);
  vector<int>::iterator igood = partition(id.begin(),id.end(),co); // Condition on chi2 of the track to suppress fake combinations
  int nelements = distance(id.begin(),igood);
  if (!nelements) return -1;
  DiscriminantCondition dc(this,event,strack,timeref,0);
  sort(id.begin(),igood,dc); // Sort event according to increasing discriminant
  vector<int>::iterator ilast = id.begin();
  advance(ilast,nelements-1);
  int last = *ilast; // Track with largest discriminant best candidate
  if (nelements>2) { // Solve some ambiguities (to be tuned)
    DifferenceCondition nc(this,event,strack,timeref,last);
    vector<int>::iterator istop = partition(id.begin(),ilast+1,nc);
    int ncomb = distance(id.begin(),istop);
    if (ncomb>1) {
      DiscriminantCondition dc2(this,event,strack,timeref,1);
      sort(id.begin(),istop,dc2);
      ilast = id.begin();
      advance(ilast,ncomb-1);
      int ulast2 = *next(ilast,-1);
      double diff2 = DiffDiscr(event,strack,timeref,last,ulast2,1);
      if (diff2<0.1 && diff2>0) return -1;
    }
  }

  // Store the tracks in UpstreamParticle
  //if (nelements>10) return -1;
  //
  //// Loop on tracks saved in id -> igood and save them
  //fNGigaTrackerCandidates = nelements;
  //for (int k(0); k<nelements; k++) {
  //  TRecoGigaTrackerCandidate *cand = (TRecoGigaTrackerCandidate *)event->GetCandidate(id[k]);
  //  Double_t cda = 9999999.;
  //  TVector3 vtx = ComputeVertex(cand,strack,&cda);
  //  Double_t dt = cand->GetTime()-timeref;
  //  Double_t discriminant = Discriminant(cda,dt);
  //  fGigaTrackerCandidate[k]->SetMomentum(1e-3*cand->GetMomentum().Mag());
  //  fGigaTrackerCandidate[k]->SetSlopeXZ(cand->GetMomentum().X()/cand->GetMomentum().Z());
  //  fGigaTrackerCandidate[k]->SetSlopeYZ(cand->GetMomentum().Y()/cand->GetMomentum().Z());
  //  fGigaTrackerCandidate[k]->SetTime(cand->GetTime());
  //  fGigaTrackerCandidate[k]->SetPosition(cand->GetPosition(2));
  //  fGigaTrackerCandidate[k]->SetPosGTK1(cand->GetPosition(0));
  //  fGigaTrackerCandidate[k]->SetPosGTK2(cand->GetPosition(1));
  //  fGigaTrackerCandidate[k]->SetDiscriminant(discriminant);
  //  fGigaTrackerCandidate[k]->SetVertex(vtx);
  //  fGigaTrackerCandidate[k]->SetCDA(cda);
  //  fGigaTrackerCandidate[k]->SetChi2X(cand->GetChi2X());
  //  fGigaTrackerCandidate[k]->SetType(3);
  //  fGigaTrackerCandidate[k]->SetIsGigaTrackerCandidate(1);
  //}
  //
  //// Match and save hits in GTK3
  //for (int ist(0); ist<3; ist++) TrackHitMatching(event,strack,timeref,ist);
  //cout << " last == " << last << endl;
  // Return the index of the best track
  return last;
}

Double_t GigaTrackerAnalysis::DiffDiscr(TRecoGigaTrackerEvent *event, TRecoSpectrometerCandidate *strack, Double_t timeref, int i, int j, int iD) { // Compute a possible condition to solve the ambiguity
  TRecoGigaTrackerCandidate* c1 = (TRecoGigaTrackerCandidate*)event->GetCandidate(i);
  Double_t cda1 = 9999999.;
  TVector3 vtx1 = ComputeVertex(c1,strack,&cda1);
  Double_t dt1 = c1->GetTime()-timeref;
  Double_t discr1[2];
  Discriminant(cda1,dt1,discr1);
  TRecoGigaTrackerCandidate* c2 = (TRecoGigaTrackerCandidate*)event->GetCandidate(j);
  Double_t cda2 = 9999999.;
  TVector3 vtx2 = ComputeVertex(c2,strack,&cda2);
  Double_t dt2 = c2->GetTime()-timeref;
  Double_t discr2[2];
  Discriminant(cda2,dt2,discr2);
  return fabs(discr1[iD]-discr2[iD]);
}

Double_t GigaTrackerAnalysis::Chi2True(TRecoGigaTrackerCandidate *cand, UpstreamParticle *kaon) { // 3 pion testing purposes using true kaon
  Double_t sdd[5] = {0.3,3e-5,3e-5,2.,2.}; // GeV/c, rad, rad, mm, mm
  Double_t dd[5];
  dd[0] = 0.001*cand->GetMomentum().Mag()-kaon->GetMomentum().P();
  dd[1] = (cand->GetMomentum().X()/cand->GetMomentum().Z())-(kaon->GetMomentum().X()/kaon->GetMomentum().Z());
  dd[2] = (cand->GetMomentum().Y()/cand->GetMomentum().Z())-(kaon->GetMomentum().Y()/kaon->GetMomentum().Z());
  dd[3] = cand->GetPosition(2).X()-kaon->GetPosition().X();
  dd[4] = cand->GetPosition(2).Y()-kaon->GetPosition().Y();
  Double_t chi2true = 0.;
  for (int j(0); j<5; j++) chi2true += dd[j]*dd[j]/(sdd[j]*sdd[j]);
  return chi2true;
}

Double_t GigaTrackerAnalysis::Chi2TrueTest(TRecoGigaTrackerCandidate *cand, TRecoGigaTrackerCandidate *kaon) { // 3 pion testing purposes using true kaon candidate
  Double_t sdd[5] = {0.3,3e-5,3e-5,2.,2.}; // GeV/c, rad, rad, mm, mm
  Double_t dd[5];
  dd[0] = 0.001*cand->GetMomentum().Mag()-0.001*kaon->GetMomentum().Mag();
  dd[1] = (cand->GetMomentum().X()/cand->GetMomentum().Z())-(kaon->GetMomentum().X()/kaon->GetMomentum().Z());
  dd[2] = (cand->GetMomentum().Y()/cand->GetMomentum().Z())-(kaon->GetMomentum().Y()/kaon->GetMomentum().Z());
  dd[3] = cand->GetPosition(2).X()-kaon->GetPosition(2).X();
  dd[4] = cand->GetPosition(2).Y()-kaon->GetPosition(2).Y();
  Double_t chi2true = 0.;
  for (int j(0); j<5; j++) chi2true += dd[j]*dd[j]/(sdd[j]*sdd[j]);
  return chi2true;
}

Double_t GigaTrackerAnalysis::Chi2Event(TRecoGigaTrackerCandidate *cand) {
  Double_t pcand = 1e-3*cand->GetMomentum().Mag();
  Double_t txcand = cand->GetMomentum().X()/cand->GetMomentum().Z();
  Double_t tycand = cand->GetMomentum().Y()/cand->GetMomentum().Z();
  return (pcand-74.9)*(pcand-74.9)/(0.9*0.9)+(txcand-0.00122)*(txcand-0.00122)/(1.2e-4*1.2e-4)+(tycand-0.)*(tycand-0.)/(1.0e-4*1.0e-4);
}

TVector3 GigaTrackerAnalysis::ComputeVertex(TRecoGigaTrackerCandidate *cand, TRecoSpectrometerCandidate *strack, Double_t *cda) { // Analytical approach including blue field correction

  // Kaon moemntum and position
  TVector3 p3cand = cand->GetMomentum();
  TVector3 poscand = cand->GetPosition(2);

  // Pion momentum and position
  Double_t partrack[4] = {strack->GetMomentum(),strack->GetSlopeXBeforeMagnet(),strack->GetSlopeYBeforeMagnet(),1e-3*MPI};
  TVector3 p3pion = fTools->Get4Momentum(partrack).Vect();
  TVector3 pospion = strack->GetPositionBeforeMagnet();

  // Vertex: first iteration without momentum correction for blue field
  // TVector3 vtx = fTools->SingleTrackVertex(&p3cand,&p3pion,&poscand,&pospion,cda); // First interation: no blue field
  TVector3 vtx = fTools->SingleTrackVertex(p3cand,p3pion,poscand,pospion,*cda); // First interation: no blue field

  // Correct incoming and outcoming momentum at origin for blue field
  vtx = fTools->BlueFieldCorrection(&p3pion,pospion,strack->GetCharge(),vtx.Z());
  vtx = fTools->BlueFieldCorrection(&p3cand,poscand,1,vtx.Z());

  // Vertex: second iteration with momentum corrected for blue field
  // vtx = fTools->SingleTrackVertex(&p3cand,&p3pion,&poscand,&pospion,cda);
  vtx = fTools->SingleTrackVertex(p3cand,p3pion,poscand,pospion,*cda);

  return vtx;
}

//////////////////
// Discriminant //
//////////////////
void GigaTrackerAnalysis::DiscriminantNormalization() {
  double pass_cda = 0.01; // mm
  double pass_dt = 0.001; // ns

  fPDFKaonCDA.clear();
  fIntPDFKaonCDA = 0;
  fPDFPileCDA.clear();
  fIntPDFPileCDA = 0;
  for (int jbincda(0); jbincda<6000; jbincda++) { // < 60 mm
    double cda = pass_cda*jbincda+0.5*pass_cda;
    double pdfkc = PDFKaonCDA(cda);
    if (pdfkc<0) pdfkc=0;
    fIntPDFKaonCDA += pdfkc*pass_cda/0.25;
    if (fIntPDFKaonCDA>=1) fIntPDFKaonCDA = 1;
    fPDFKaonCDA.push_back(fIntPDFKaonCDA);
    double pdfpc = PDFPileCDA(cda);
    if (pdfpc<0) pdfpc=0;
    fIntPDFPileCDA += pdfpc*pass_cda/0.25;
    if (fIntPDFPileCDA>=1) fIntPDFPileCDA = 1;
    fPDFPileCDA.push_back(fIntPDFPileCDA);
  }
  fPDFKaonDT.clear();
  fIntPDFKaonDT = 0;
  fPDFPileDT.clear();
  fIntPDFPileDT = 0;
  for (int jbindt(0); jbindt<1000; jbindt++) { // +-1 ns
    double dt = pass_dt*jbindt+0.5*pass_dt;
    fIntPDFKaonDT += (PDFKaonDT(dt)+PDFKaonDT(-dt))*pass_dt/0.01;
    fPDFKaonDT.push_back(fIntPDFKaonDT);
    fIntPDFPileDT += (PDFPileDT(dt)+PDFPileDT(-dt))*pass_dt/0.01;
    fPDFPileDT.push_back(fIntPDFPileDT);
  }
}

Double_t GigaTrackerAnalysis::Discriminant(Double_t cda_val, Double_t dt_val) {

  double discr = 0;
  if (cda_val>59.9 || fabs(dt_val)>0.95) return discr; // range of validitz of the normalization

  double pass_cda = 0.01; // mm
  double pass_dt = 0.001;//0.05; // ns
  double pkcda = 0.;
  vector<double>::iterator ikcda = fPDFKaonCDA.begin();
  while(ikcda!=fPDFKaonCDA.end()) {
    double cda = (double)(distance(fPDFKaonCDA.begin(),ikcda)-1)*pass_cda;
    if ((pkcda=EvaluateCondition(cda,cda_val,*ikcda))>=0) break;
    ikcda++;
  }
  pkcda /= fIntPDFKaonCDA;
  double pkdt = 0.;
  vector<double>::iterator ikdt = fPDFKaonDT.begin();
  while(ikdt!=fPDFKaonDT.end()) {
    double dt = (double)(distance(fPDFKaonDT.begin(),ikdt)-1)*pass_dt;
    if ((pkdt=EvaluateCondition(fabs(dt),fabs(dt_val),*ikdt))>=0) break;
    ikdt++;
  }
  pkdt /= fIntPDFKaonDT;
  double ppcda = 0.;
  vector<double>::iterator ipcda = fPDFPileCDA.begin();
  while(ipcda!=fPDFPileCDA.end()) {
    double cda = (double)(distance(fPDFPileCDA.begin(),ipcda)-1)*pass_cda;
    if ((ppcda=EvaluateCondition(cda,cda_val,*ipcda))>=0) break;
    ipcda++;
  }
  ppcda /= fIntPDFPileCDA;
  double ppdt = 0.;
  vector<double>::iterator ipdt = fPDFPileDT.begin();
  while(ipdt!=fPDFPileDT.end()) {
    double dt = (double)(distance(fPDFPileDT.begin(),ipdt)-1)*pass_dt;
    if ((ppdt=EvaluateCondition(fabs(dt),fabs(dt_val),*ipdt))>=0) break;
    ipdt++;
  }
  ppdt /= fIntPDFPileDT;

  // Simple discriminant
  double discr_kaon = (1-pkcda)*(1-pkdt);
  double discr_pile = (1-ppcda)*(1-ppdt);
  discr = discr_kaon;

  return discr;
}

void GigaTrackerAnalysis::Discriminant(Double_t cda_val, Double_t dt_val, double *discr) { // Similar as above, but returning discriminant both in kaon and pileup hypothesis

  discr[0] = 0;
  discr[1] = 0;
  if (cda_val>59.9 || fabs(dt_val)>0.95) return;

  double pass_cda = 0.01; // mm
  double pass_dt = 0.001;//0.05; // ns
  double pkcda = 0.;
  vector<double>::iterator ikcda = fPDFKaonCDA.begin();
  while(ikcda!=fPDFKaonCDA.end()) {
    double cda = (double)(distance(fPDFKaonCDA.begin(),ikcda)-1)*pass_cda;
    if ((pkcda=EvaluateCondition(cda,cda_val,*ikcda))>=0) break;
    ikcda++;
  }
  pkcda /= fIntPDFKaonCDA;
  double pkdt = 0.;
  vector<double>::iterator ikdt = fPDFKaonDT.begin();
  while(ikdt!=fPDFKaonDT.end()) {
    double dt = (double)(distance(fPDFKaonDT.begin(),ikdt)-1)*pass_dt;
    if ((pkdt=EvaluateCondition(fabs(dt),fabs(dt_val),*ikdt))>=0) break;
    ikdt++;
  }
  pkdt /= fIntPDFKaonDT;
  double ppcda = 0.;
  vector<double>::iterator ipcda = fPDFPileCDA.begin();
  while(ipcda!=fPDFPileCDA.end()) {
    double cda = (double)(distance(fPDFPileCDA.begin(),ipcda)-1)*pass_cda;
    if ((ppcda=EvaluateCondition(cda,cda_val,*ipcda))>=0) break;
    ipcda++;
  }
  ppcda /= fIntPDFPileCDA;
  double ppdt = 0.;
  vector<double>::iterator ipdt = fPDFPileDT.begin();
  while(ipdt!=fPDFPileDT.end()) {
    double dt = (double)(distance(fPDFPileDT.begin(),ipdt)-1)*pass_dt;
    if ((ppdt=EvaluateCondition(fabs(dt),fabs(dt_val),*ipdt))>=0) break;
    ipdt++;
  }
  ppdt /= fIntPDFPileDT;

  discr[0] = (1-pkcda)*(1-pkdt);
  discr[1] = (1-ppcda)*(1-ppdt);

}

// PDF Projections (from fit on 3 pions: data 6342 filtered on /eos/na62/user/r/ruggierg/2016/filter_6342 with fit in output/analysis_onetrack_gigatracker.C)
Double_t GigaTrackerAnalysis::PDFKaonCDA(Double_t cda) {
  Double_t cdapar[8];
  cdapar[0] = 6.94193e-02;
  cdapar[1] = 1.55569e+00;
  cdapar[2] = 1.74896e-02;
  cdapar[3] = 2.79865e+00;
  cdapar[4] = 1.73571e-02;
  cdapar[5] = 3.33088e-01;
  cdapar[6] = 1.02705e-04;
  cdapar[7] =-4.55423e-06;
  Double_t cdag1 = cdapar[0]*exp(-0.5*(cda/cdapar[1])*(cda/cdapar[1]));
  Double_t cdag2 = cdapar[2]*exp(-0.5*(cda/cdapar[3])*(cda/cdapar[3]));
  Double_t cdaf1 = cdapar[4]*exp(-cdapar[5]*cda)+cdapar[6]+cdapar[7]*cda;
  if (cda>25.) return 0.;
  return cdag1+cdag2+cdaf1;
}

Double_t GigaTrackerAnalysis::PDFKaonDT(Double_t dt) {
  Double_t dtpar[6];

  // KTAG - GTK association
  if (fReferenceDetector==0) {
    dtpar[0] =2.07154e-02;
    dtpar[1] =1.10614e-02;
    dtpar[2] =1.69224e-01;
    dtpar[3] =9.86551e-04;
    dtpar[4] =8.74013e-02;
    dtpar[5] =5.00000e-01;
  }

  // CHOD - GTK association
  if (fReferenceDetector==1) {
    dtpar[0] =1.50288e-02;
    dtpar[1] =2.90724e-02;
    dtpar[2] =2.02709e-01;
    dtpar[3] =2.45957e-03;
    dtpar[4] =2.89432e-01;
    dtpar[5] =3.76790e-01;
  }

  Double_t dtg1 = dtpar[0]*exp(-0.5*((dt-dtpar[1])/dtpar[2])*((dt-dtpar[1])/dtpar[2]));
  Double_t dtg2 = dtpar[3]*exp(-0.5*((dt-dtpar[4])/dtpar[5])*((dt-dtpar[4])/dtpar[5]));
  return dtg1+dtg2;
}

Double_t GigaTrackerAnalysis::PDFPileCDA(Double_t cda) {
  Double_t cdapar[6];
  cdapar[0] =2.11500e-06;
  cdapar[1] =1.59891e-03;
  cdapar[2] =1.94381e+01;
  cdapar[3] =9.49407e+00;
  cdapar[4] =1.61751e-02;
  cdapar[5] =1.04761e+01;
  Double_t cdap0 = cdapar[0];
  Double_t cdag1 = cdapar[1]*exp(-0.5*((cda-cdapar[2])/cdapar[3])*((cda-cdapar[2])/cdapar[3]));
  Double_t cdag2 = cdapar[4]*exp(-0.5*((cda)/cdapar[5])*((cda)/cdapar[5]));
  return cdap0+cdag1+cdag2;
}

Double_t GigaTrackerAnalysis::PDFPileDT(Double_t dt) {
  Double_t dtpar[4];

  // KTAG - GTK association
  if (fReferenceDetector==0) {
    dtpar[0] = 7.72056e-03;
    dtpar[1] = 2.74024e-06;
    dtpar[2] =-1.04979e-02;
    dtpar[3] = 3.54554e-03;
  }

  // CHOD - GTK association
  if (fReferenceDetector==1) {
    dtpar[0] = 5.46309e-03;
    dtpar[1] = 5.13779e-05;
    dtpar[2] =-2.54249e-03;
    dtpar[3] =-4.18633e-04;
  }

  return dtpar[0]+dtpar[1]*dt+dtpar[2]*dt*dt+dtpar[3]*dt*dt*dt*dt;
}

TLorentzVector GigaTrackerAnalysis::GetMomentum(Double_t pmom, Double_t dxdz, Double_t dydz, Double_t mass) {
  Double_t partrack[4] = {pmom,dxdz,dydz,mass};
  return fTools->Get4Momentum(partrack);
}

///////////////////////
// Hit GTK3 Matching //
///////////////////////
void GigaTrackerAnalysis::TrackHitMatching(TRecoGigaTrackerEvent *event, TRecoSpectrometerCandidate *strack, Double_t timeref, int iST) {
  vector<int> id(event->GetNHits());
  iota(begin(id),end(id),0);

  // All hits of station iST (begin-iend)
  StationCondition so(event,iST);
  auto iend = partition(id.begin(),id.end(),so);

  // All hits of station iST in time (begin-iendinT)
  TimeCondition to(this,event,timeref);
  auto iendinT = partition(id.begin(),iend,to);
  int nhits = distance(id.begin(),iendinT);

  // Sort the hits of station iST in time per distance from the track
  if (iST==2) {
    PositionCondition po(this,event,strack);
    sort(id.begin(),iendinT,po);
  }

  // Save hits
  id.resize(nhits);
  if (iST==0) fNGigaTrackerGTK1Hits = nhits;
  if (iST==1) fNGigaTrackerGTK2Hits = nhits;
  if (iST==2) fNGigaTrackerGTK3Hits = nhits;
  int kk(0);
  for (auto iH : id) {
    if (iST==0) fGigaTrackerGTK1Hit[kk] = iH;
    if (iST==1) fGigaTrackerGTK2Hit[kk] = iH;
    if (iST==2) fGigaTrackerGTK3Hit[kk] = iH;
    kk++;
  }
}

TVector2 GigaTrackerAnalysis::GetGTK3Pos(TRecoSpectrometerCandidate *strack) {
  Double_t partrack[4] = {strack->GetMomentum(),strack->GetSlopeXBeforeMagnet(),strack->GetSlopeYBeforeMagnet(),1e-3*MPI};
  TVector3 p3pion = fTools->Get4Momentum(partrack).Vect();
  TVector3 posGTK3 = fTools->BlueFieldCorrection(&p3pion,strack->GetPositionBeforeMagnet(),strack->GetCharge(),fZGTK[2]);
  return posGTK3.XYvector();
}
