#include "UpstreamParticle.hh"

UpstreamParticle::UpstreamParticle()
{ }

UpstreamParticle::~UpstreamParticle()
{ }

void UpstreamParticle::Clear()
{
  Particle::Clear();

  fDownstreamTrackID = -1;
  fIsKTAGCandidate = 0;
  fKTAGID = -1;
  fKTAGTime = -99999.; 
  fIsCHANTICandidate = 0;
  fCHANTIID = -1;
  fCHANTITime = -99999.; 
  fVertex.SetXYZ(-99999.,-99999.,0.);
  fCDA = 9999999.;
}
