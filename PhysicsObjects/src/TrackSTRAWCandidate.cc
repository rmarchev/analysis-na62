#include "TrackSTRAWCandidate.hh"

ClassImp(TrackSTRAWCandidate);

TrackSTRAWCandidate :: TrackSTRAWCandidate () : TObject () {

    Reset ();

}


TrackSTRAWCandidate :: ~TrackSTRAWCandidate () {

    Reset ();

}

void TrackSTRAWCandidate::Reset (){


  fSpectrometer = nullptr;
  fTrackID = -1;
  fCharge = -99999;
  fTime = -99999;
  fGood = false;

}
