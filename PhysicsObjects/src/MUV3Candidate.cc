#include "MUV3Candidate.hh"

MUV3Candidate::MUV3Candidate()
{}

MUV3Candidate::~MUV3Candidate()
{}

void MUV3Candidate::Clear() {
  fIsMUV3Candidate = 0;
  fDiscriminant = 99999999.;
  fDeltaTime = 99999999.;
  fType = -1;
}
