#include "SpectrometerVertex.hh"
#include "SpectrometerVertex.hh"


SpectrometerVertex::SpectrometerVertex(){
  Clear();
}

void SpectrometerVertex::Clear() {
  fNTracks = 0;
  fTrackIndices.clear();
  fQTrack.clear();
  fThreeMomenta.clear();
  fThreeMomenta0.clear();
}

void SpectrometerVertex::AddTrack(Int_t index, Int_t Charge, TVector3 Mom, TVector3 Mom0) {
  fTrackIndices.push_back(index);
  fQTrack.push_back(Charge);
  fThreeMomenta.push_back(Mom);
  fThreeMomenta0.push_back(Mom0);
  fNTracks = fTrackIndices.size();
}

Int_t SpectrometerVertex::GetTrackIndex(Int_t i) {
  return (i>=0 && i<fNTracks) ? fTrackIndices[i] : -999;
}

Int_t SpectrometerVertex::GetTrackCharge(Int_t i) {
  return (i>=0 && i<fNTracks) ? fQTrack[i] : 0;
}

TVector3 SpectrometerVertex::GetTrackThreeMomentum(Int_t i) {
  return (i>=0 && i<fNTracks) ? fThreeMomenta[i] : TVector3(0.0,0.0,0.0);
}

TVector3 SpectrometerVertex::GetTrackThreeMomentum0(Int_t i) {
  return (i>=0 && i<fNTracks) ? fThreeMomenta0[i] : TVector3(0.0,0.0,0.0);
}

Double_t SpectrometerVertex::GetTrackMomentum(Int_t i) {
  return (i>=0 && i<fNTracks) ? fThreeMomenta[i].Mag() : 0.0;
}

Double_t SpectrometerVertex::GetTrackMomentum0(Int_t i) {
  return (i>=0 && i<fNTracks) ? fThreeMomenta0[i].Mag() : 0.0;
}

Double_t SpectrometerVertex::GetTrackSlopeX(Int_t i) {
  return (i>=0 && i<fNTracks) ? fThreeMomenta[i].X()/fThreeMomenta[i].Z() : 0.0;
}

Double_t SpectrometerVertex::GetTrackSlopeY(Int_t i) {
  return (i>=0 && i<fNTracks) ? fThreeMomenta[i].Y()/fThreeMomenta[i].Z() : 0.0;
}

Double_t SpectrometerVertex::GetTrackSlopeX0(Int_t i) {
  return (i>=0 && i<fNTracks) ? fThreeMomenta0[i].X()/fThreeMomenta0[i].Z() : 0.0;
}

Double_t SpectrometerVertex::GetTrackSlopeY0(Int_t i) {
  return (i>=0 && i<fNTracks) ? fThreeMomenta0[i].Y()/fThreeMomenta0[i].Z() : 0.0;
}

void SpectrometerVertex::Print() {
  std::cout <<
    "Vertex: Ntrk= " << fNTracks <<
    " Q= "           << fCharge  <<
    " Z= "           << fPos.Z()<<
    " P= "           << fTotalThreeMomentum.Mag()<<
    " Chi2= "        << fChi2 <<
    " Tracks=";
  for (Int_t i=0; i<fNTracks; i++) cout << " " << fTrackIndices[i];
  std::cout << std::endl;
}
