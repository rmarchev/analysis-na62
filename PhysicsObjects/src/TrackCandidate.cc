#include "TrackCandidate.hh"

ClassImp(TrackCandidate);


TrackCandidate :: TrackCandidate () : TObject () {

    Reset ();

}


TrackCandidate :: ~TrackCandidate () {

    Reset ();

}

void TrackCandidate::Reset (){

  fSTRAW = nullptr;

  fCedar = nullptr;
  fSpectrometer = nullptr;
  fGigaTracker = nullptr;
  fCHANTI = nullptr;
  fLAV = nullptr;
  fRICH = nullptr;
  fIRC = nullptr;
  fCHODCandidate = nullptr;
  fLKr = nullptr;
  fMUV1 = nullptr;
  fMUV2 = nullptr;
  fMUV3 = nullptr;
  fSAC = nullptr;

  fSpectrometerIndex=-1;
  fSpectrometerTime=-999;
  fMomentumBeforeMagnet=TVector3(-999999,-999999,-999999);

  fCHODVerticalPosition=-9999;
  fCHODHorizontalPosition=-9999;
  fCHODTime=-9999;
  fCHODdisc=-1;
  fCHODdt=-1;
  fCHODdtohit=-1;
  fCHODPosition=TVector3(-999999,-999999,-999999);
  fCedarIndex=-1;
  fGTKIndex=-1;

  fLKrMatchedX=-999999;
  fLKrMatchedY=-999999;
  fLKrShowerWidth=0;
  fLKrMatchedPosition=TVector3(-999999,-999999,-999999);
  fLKrMatchedEoP=0;
  fLKrMatchedEnergy=0;
  fLKrInTimeExtraCl=0;
  fLKrInTimeExtraClAux=0;

  fGTKPosition=TVector3(-999999,-999999,-999999);
  fGTKMomentum=TVector3(-999999,-999999,-999999);
  fGTKTime = -9999;
  fGTKcda  = -9999;

  fCHANTIIndex=-1;
  fCHANTITime=-9999;

  fMUV1Index=-1;
  fMUV1SeedRatio=0;
  fMUV1Time=-9999;
  fMUV1ShowerWidth=0;
  fMUV1Energy=0;
  fMUV1Present = false;

  fMUV2Index=-1;
  fMUV2Time=-9999;
  fMUV2ShowerWidth=0;
  fMUV2SeedRatio=0;
  fMUV2Energy=0;
  fMUV2Present = false;

  fMUV3Index=-1;
  fMUV3Time=-9999;
  fMUV3Present = false;

  fLAVIndex=-1;
  fLAVTime=-9999;
  fLAVPresent = false;

  fSACIndex=-1;
  fSACTime=-9999;
  fSACPresent = false;

  fIRCIndex=-1;
  fIRCTime=-9999;
  fIRCPresent = false;

  fRICHPresent = false;
  fRICHTime = -9999;
  fRICHIndex = -1;

  MatchedISOther=false;
  MatchedISMIP=false;
  MatchedISEM=false;
  fCedarPresent = false;
  fSpectrometerPresent = false;
  fGTKPresent = false;
  fCHANTIPresent = false;
  fCHODPresent = false;
  fLKrPresent = false;



}
