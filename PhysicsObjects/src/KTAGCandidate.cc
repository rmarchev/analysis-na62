#include "KTAGCandidate.hh"

KTAGCandidate::KTAGCandidate()
{}

KTAGCandidate::~KTAGCandidate()
{}

void KTAGCandidate::Clear() {
  fIsKTAGCandidate = 0;
  fTime = 9999999.;
  fID = -1;
}
