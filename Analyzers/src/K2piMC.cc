#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "K2piMC.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Geometry.hh"
#include "MM2Errors.hh"
#include "Persistency.hh"
#include "AnalysisTools.hh"
using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


K2piMC::K2piMC(Core::BaseAnalysis *ba) : Analyzer(ba, "K2piMC")
{
  fTool = AnalysisTools::GetInstance();

  RequestTree("LKr",new TRecoLKrEvent);
  RequestTree("Spectrometer",new TRecoSpectrometerEvent);
  //RequestTree("GigaTracker",new TRecoGigaTrackerEvent);
  RequestTree("CHOD",new TRecoCHODEvent);
  RequestTree("CHANTI",new TRecoCHANTIEvent);
  RequestTree("RICH",new TRecoRICHEvent);
  RequestTree("MUV1",new TRecoMUV1Event);
  RequestTree("Cedar",new TRecoCedarEvent);
  RequestTree("MUV2",new TRecoMUV2Event);
  RequestTree("MUV3",new TRecoMUV3Event);
  RequestTree("LAV",new TRecoLAVEvent);
  RequestTree("SAC",new TRecoSACEvent);
  RequestTree("IRC",new TRecoIRCEvent);


}

void K2piMC::InitOutput(){
}

void K2piMC::InitHist(){
  BookHisto(new TH1I("pdgID", "Non complete events : pdgID", 0, 0, 0));
  BookHisto(new TH1I("STRAW_NC", "Number of STRAW candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("GTK_NC", "Number of GTK candidates;NCandidates" ,50,0,50));

  BookHisto(new TH1D("fitquality","Ppattern_recognition - Pfit; #DeltaP [MeV/c]",400,-100000.,100000.));
  BookHisto(new TH1D("chi2","#chi^{2} fit; #chi^{2}",300,0.,300.));
  BookHisto(new TH1I("NChambers","N chambers  for the  tracks; Nchambers",5,0,5));
  BookHisto(new TH1I("trkcharge","track charge; Q_{track}",4,-2,2));

  BookHisto(new TH1I("LKr_NC"  , "Number of LKr candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("LAV_NC"  , "Number of LAV candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("MUV1_NC" , "Number of MUV1 candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("MUV2_NC" , "Number of MUV2 candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("MUV3_NC" , "Number of MUV3 candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("RICH_NC" , "Number of RICH candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("CHOD_NC" , "Number of CHOD candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("RICH_NC" , "Number of RICH candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cedar_NC" , "Number of Cedar candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("CHANTI_NC", "Number of CHANTI candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("SAC_NC", "Number of SAC candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("IRC_NC", "Number of IRC candidates;NCandidates" ,50,0,50));


  //CHOD
  BookHisto(new TH2F("trk_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 2400, -1200., 1200., 2400, -1200., 1200. ));
  BookHisto(new TH2F("CHOD_dXdY","track @ CHOD; dx @ CHOD [mm]; dy @ CHOD [mm]", 800, -400., 400., 800, -400., 400. ));
  BookHisto(new TH1F("CHOD_dt","T_{CHOD} - T_{STRAW} ; dT_{ STRAW}",800,-100.,100.));

  BookHisto(new TH1I("gtk_type" , ";GTK type" ,200,0,200));
  BookHisto(new TH1F("gtk_P", " GTK momentum;P_{K} [MeV/c]",100,65000.,85000));
  BookHisto(new TH1I("gtk_Nhits", "Number of GTK hits;Nhits" ,100,0,100));
  BookHisto(new TH1I("gtk_Ngood", "Number of good candidates;Ngood" ,10,0,10));
  BookHisto(new TH1F("gtk_choddt","T_{chod} - T_{gtk} ; dT_{chod - gtk}",400,-50.,50.));
  BookHisto(new TH1F("gtk_chi2","#chi^{2} ; #chi^{2}",300,0.,300.));
  BookHisto(new TH1F("gtk_chi2X","#chi^{2}X ; #chi^{2}",300,0.,300.));
  BookHisto(new TH1F("gtk_chi2Y","#chi^{2}Y ; #chi^{2}",300,0.,300.));
  BookHisto(new TH1F("gtk_chi2T","#chi^{2}T ; #chi^{2}",300,0.,300.));
  BookHisto(new TH2F("gtk_pos","GTK position; x @ GTK [mm]; y @ GTK [mm]", 100,-50,50,100,-50,50));

  BookHisto(new TH1F("gtk_dX_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dY_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dX_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dY_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dX_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dY_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_thetax", " ;#theta_{x GTK}  [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH1F("gtk_thetay", " ;#theta_{y GTK}  [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH2F("gtk_Nhits_vs_P", "Number of GTK hits vs P;Nhits;P" ,30,0,30,100,65000.,85000));

  BookHisto(new TH1F("pk_true", " GTK momentum;P_{K} [MeV/c]",100,65.,85));
  BookHisto(new TH1F("kthx_true", " ;#theta_{x GTK}  [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH1F("kthy_true", " ;#theta_{y GTK}  [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH1F("pk_smeared", " GTK momentum;P_{K} [MeV/c]",100,65.,85));
  BookHisto(new TH1F("kthx_smeared", " ;#theta_{x GTK}  [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH1F("kthy_smeared", " ;#theta_{y GTK}  [rad]", 1000, -0.04, 0.04));

  BookHisto(new TH2F("trkp_vs_mm2_wogtk", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));
  BookHisto(new TH2F("trkp_vs_mm2_wgtk", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));


  BookHisto(new TH1F("trkp", " track p ; P_{track} [MeV]", 200, 0, 100000));
  BookHisto(new TH1F("xvtx", " X vertex ; Xvtx [mm]", 1000, -500, 500));
  BookHisto(new TH1F("yvtx", " Y vertex ; Yvtx [mm]", 1000, -500, 500));
  BookHisto(new TH1F("zvtx", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH1F("cda", "  cda ; cda [mm]",300, 0, 300));

  BookHisto(new TH2F("trkp_vs_theta", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];#theta_{x}",200,0,100000,200,-0.05,0.05));
  BookHisto(new TH2F("trkp_vs_mm2", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));

  BookHisto(new TH2F("trkp_vs_sigmamm2", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  BookHisto(new TH2F("mm2_vs_sigmamm2", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));

  //LKr
  BookHisto(new TH2F("trk_atLKr","track @ LKr; x @ LKr [mm]; y @ LKr [mm]", 2400, -1200., 1200., 2400, -1200., 1200. ));
  BookHisto(new TH2F("LKr_dXdY","track @ LKr; dx @ LKr [mm]; dy @ LKr [mm]", 800, -400., 400., 800, -400., 400. ));
  BookHisto(new TH1F("LKr_Energy",";E_{LKr}[MeV]",200,0,100000));


  //MUV1
  BookHisto(new TH2F("trk_atMUV1","track @ MUV1; x @ MUV1 [mm]; y @ MUV1 [mm]", 2400, -1200., 1200., 2400, -1200., 1200. ));
  BookHisto(new TH2F("MUV1_dXdY","track @ MUV1; dx @ MUV1 [mm]; dy @ MUV1 [mm]", 800, -400., 400., 800, -400., 400. ));
  BookHisto(new TH1F("MUV1_dt","T_{MUV1} - T_{STRAW} ; dT_{ STRAW}",800,-100.,100.));
  BookHisto(new TH1F("MUV1_Energy",";E_{MUV1}[MeV]",200,0,100000));

  //MUV2
  BookHisto(new TH2F("trk_atMUV2","track @ MUV2; x @ MUV2 [mm]; y @ MUV2 [mm]", 2400, -1200., 1200., 2400, -1200., 1200. ));
  BookHisto(new TH2F("MUV2_dXdY","track @ MUV2; dx @ MUV2 [mm]; dy @ MUV2 [mm]", 800, -400., 400., 800, -400., 400. ));
  BookHisto(new TH1F("MUV2_dt","T_{MUV2} - T_{STRAW} ; dT_{ STRAW}",800,-100.,100.));
  BookHisto(new TH1F("MUV2_Energy",";E_{MUV2}[MeV]",200,0,100000));



  BookHisto(new TH2F("trkp_vs_mm2_acccut", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));
  BookHisto(new TH2F("trkp_vs_mm2_pcut", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));
  BookHisto(new TH2F("trkp_vs_mm2_vtxcut", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));
  BookHisto(new TH2F("trkp_vs_mm2_nolav", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));
  BookHisto(new TH2F("trkp_vs_mm2_nosav", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));
  BookHisto(new TH2F("trkp_vs_mm2_muv1", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));
  BookHisto(new TH2F("trkp_vs_mm2_muv2", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));
  BookHisto(new TH2F("trkp_vs_mm2_lkr", " P_{track} vs MM2  ;P_{#pi} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100000, 300, -0.15, 0.15));

  BookHisto(new TH2F("g1_atLKr","#gamma 2 @ LKr; x @ LKr [mm]; y @ LKr [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("g2_atLKr","#gamma 2 @ LKr; x @ LKr [mm]; y @ LKr [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));


  BookHisto(new TH2F("trkp_vs_theta_final", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];#theta_{x}",200,0,100000,200,-0.05,0.05));
  BookHisto(new TH2F("trkp_vs_sigmamm2_final", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  BookHisto(new TH2F("mm2_vs_sigmamm2_final", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH1I("LKr_NC_final"  , "Number of LKr candidates;NCandidates" ,50,0,50));

  BookHisto(new TH2F("mm2_vs_sigmamm2_k2pi", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("mm2_vs_sigmamm2_2g", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("mm2_vs_sigmamm2_3g", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("trk_atLKr_3g","track @ LKr; x @ LKr [mm]; y @ LKr [mm]", 2400, -1200., 1200., 2400, -1200., 1200. ));
  BookHisto(new TH2F("LKr_dXdY_3g","track @ LKr; dx @ LKr [mm]; dy @ LKr [mm]", 800, -400., 400., 800, -400., 400. ));
  BookHisto(new TH2F("g1_atLKr_3g","#gamma 2 @ LKr; x @ LKr [mm]; y @ LKr [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("g2_atLKr_3g","#gamma 2 @ LKr; x @ LKr [mm]; y @ LKr [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));

  BookHisto(new TH2F("mm2_vs_sigmamm2_3plg", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));

}

void K2piMC::DefineMCSimple(){
  /// \MemberDescr
  /// Setup of fMCSimple. You must specify the generated MC particles you want.\n
  /// Add particles you want to recover from fMCSimple\n
  ///     \code
  ///     int particleID = fMCSimple.AddParticle(parentID, pdgCode)
  ///     \endcode
  /// parentID :  0=no parent (=beam particle)\n
  ///     ...\n
  /// Example : you want to retrieve the kaon from the beam, the pi0 an pi+ from the beam kaon and the 2 photons coming from the previous pi0 decay :\n
  ///     \code
  ///     int kaonID = fMCSimple.AddParticle(0, 321) //Ask beam kaon (sequence ID=1)
  ///     fMCSimple.AddParticle(kaonID, 211) //Ask pi+ from previous kaon (sequence ID=2)
  ///     int pi0ID = fMCSimple.AddParticle(kaonID, 111) //Ask pi0 from previous kaon (sequence ID=3)
  ///     fMCSimple.AddParticle(pi0ID, 22) //Ask first gamma from previous pi0 (sequence ID=4)
  ///     fMCSimple.AddParticle(pi0ID, 22) //Ask second gamma from previous pi0 (sequence ID=4)
  ///     \endcode
  ///
  /// @see ROOT TDatabasePDG for a list of PDG codes and particle naming convention
  /// \EndMemberDescr
  kID = fMCSimple.AddParticle(0, 321);//kaon
  fMCSimple.AddParticle(kID, 211);//piplus
  //fMCSimple.AddParticle(kID, 22);//gamma from kaon
  //pi0ID = fMCSimple.AddParticle(kID, 111);//pi0
  fMCSimple.AddParticle(kID, 22);//g1
  fMCSimple.AddParticle(kID, 22);//g2
}

void K2piMC::StartOfRunUser(){

}

void K2piMC::StartOfBurstUser(){

}

void K2piMC::Process(int iEvent){
  ///     KinePart *candidate = CreateStandardCandidate("treeName");
  /// \endcode
  //Are we working with correct MC
  bool withMC = true;
  OutputState state;
  if(fMCSimple.fStatus == MCSimple::kMissing){
    Event* MCTruthEvent = GetMCEvent();
    for(int i=0; i<MCTruthEvent->GetNKineParts(); i++){
      FillHisto("pdgID", ((KinePart*)MCTruthEvent->GetKineParts()->At(i))->GetParticleName(), 1);
    }
    withMC = false;
  }
  if(fMCSimple.fStatus == MCSimple::kEmpty) withMC = false;
  if(fMCSimple.fStatus == MCSimple::kMissing) return;

  //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
  //if(fMCSimple.fStatus == MCSimple::kEmpty){printNoMCWarning();return;}
  fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetOutput("GigaTrackerEvtReco.GigaTrackerEvent",state);
  fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
  fLKrEvent = (TRecoLKrEvent*)GetEvent("LKr");
  fCHODEvent = (TRecoCHODEvent*)GetEvent("CHOD");
  fCHANTIEvent = (TRecoCHANTIEvent*)GetEvent("CHANTI");
  fRICHEvent = (TRecoRICHEvent*)GetEvent("RICH");
  fCedarEvent = (TRecoCedarEvent*)GetEvent("Cedar");
  fMUV1Event = (TRecoMUV1Event*)GetEvent("MUV1");
  fMUV2Event = (TRecoMUV2Event*)GetEvent("MUV2");
  fMUV3Event = (TRecoMUV3Event*)GetEvent("MUV3");
  fLAVEvent = (TRecoLAVEvent*)GetEvent("LAV");
  fSACEvent = (TRecoSACEvent*)GetEvent("SAC");
  fIRCEvent = (TRecoIRCEvent*)GetEvent("IRC");

  int NSTRAW_Cand = fSpectrometerEvent->GetNCandidates();
  int NGTK_Cand = fGigaTrackerEvent->GetNCandidates();
  FillHisto("STRAW_NC", NSTRAW_Cand);
  FillHisto("GTK_NC", NGTK_Cand);

  if(NGTK_Cand != 1) return;
  if(NSTRAW_Cand != 1) return;
  fSpectrometerCandidate = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(0);
  Double_t ppatreco = fSpectrometerCandidate->GetMomentumBeforeFit();
  Double_t momentum = fSpectrometerCandidate->GetMomentum();
  Double_t chi2 = fSpectrometerCandidate->GetChi2();
  int charge = fSpectrometerCandidate->GetCharge();
  int nchambers = fSpectrometerCandidate->GetNChambers();

  FillHisto("fitquality", ppatreco - momentum);
  FillHisto("trkcharge", charge);
  FillHisto("NChambers", nchambers);
  FillHisto("chi2", chi2);
  if(chi2 > 20) return;
  if(nchambers < 4 ) return;
  if(fabs(ppatreco - momentum) > 20000 ) return;
  if(charge != 1 ) return;

  FillHisto("trkp",momentum);
  int NLKr_Cand = fLKrEvent->GetNCandidates();

  FillHisto("LKr_NC", NLKr_Cand);

  if(NLKr_Cand < 1) return;
  int NCHOD_Cand = fCHODEvent->GetNCandidates();
  //int NCHOD_GoodCand = GoodCHODTracks->GetEntries();

  FillHisto("CHOD_NC", NCHOD_Cand);

  int NCedar_Cand = fCedarEvent->GetNCandidates();

  FillHisto("Cedar_NC", NCedar_Cand);

  int NLAV_Cand = fLAVEvent->GetNCandidates();

  FillHisto("LAV_NC", NLAV_Cand);

  int NRICH_Cand = fRICHEvent->GetNCandidates();

  FillHisto("RICH_NC", NRICH_Cand);

  int NMUV1_Cand = fMUV1Event->GetNCandidates();

  FillHisto("MUV1_NC", NMUV1_Cand);

  int NMUV2_Cand = fMUV2Event->GetNCandidates();

  FillHisto("MUV2_NC", NMUV2_Cand);

  int NMUV3_Cand = fMUV3Event->GetNCandidates();

  FillHisto("MUV3_NC", NMUV3_Cand);

  int NCHANTI_Cand = fCHANTIEvent->GetNCandidates();
  FillHisto("CHANTI_NC", NCHANTI_Cand);

  int NIRC_Cand = fCHANTIEvent->GetNCandidates();
  FillHisto("IRC_NC", NIRC_Cand);
  int NSAC_Cand = fCHANTIEvent->GetNCandidates();
  FillHisto("SAC_NC", NSAC_Cand);

  KinePart* photonCandidate;
  KinePart* piplus;
  //KinePart *fkplus;
  KinePart* gamma1;
  KinePart* gamma2;
  kplus  = fMCSimple["K+"][0]; //for the kaon
  piplus = fMCSimple["pi+"][0]; //for the positive pion
  gamma1 = fMCSimple["gamma"][0]; //for the photon1
  gamma2 = fMCSimple["gamma"][1]; //for the photon2
  //Candidates
  //TRecoLKrCandidate *lkrCand;
  TLorentzVector pk,ppi;
  pk.SetPxPyPzE(kplus->GetFinalMomentum().X(),kplus->GetFinalMomentum().Y(),kplus->GetFinalMomentum().Z(),kplus->GetFinalEnergy());
  ppi.SetPxPyPzE(piplus->GetInitialMomentum().X(),piplus->GetInitialMomentum().Y(),piplus->GetInitialMomentum().Z(),piplus->GetInitialEnergy());

  std::map<Double_t,int> photonenergy;

  for(int i = 0; i < NLKr_Cand;i++){
    TRecoLKrCandidate* lkr = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(i);
    Double_t clenergy = lkr->GetClusterEnergy();
    photonenergy[clenergy] = i;
  }


  Bool_t chodmatch = CHODTrack();
  if(!chodmatch) return;

  MakeNominalKaonVertex();



  //Bool_t goodgtkvtx = IsGoodGTKVtx();
  //if(!goodgtkvtx) return;
  Double_t ptrack = momentum*0.001;// [GeV]
  Double_t thx = fSpectrometerCandidate->GetSlopeXBeforeMagnet();
  Double_t thy = fSpectrometerCandidate->GetSlopeYBeforeMagnet();
  // Double_t kthx= fKMomentum.X()/fKMomentum.Z();
  // Double_t kthy= fKMomentum.Y()/fKMomentum.Z();
  // Double_t input[6]= {1./ptrack,thx,thy,fKMomentum.Mag(),kthx,kthy};
  Double_t kthx= pk.Px()/pk.Pz();
  Double_t kthy= pk.Py()/pk.Pz();
  Double_t pkaon=pk.P()*fmevtogev;

  FillHisto("pk_true",pkaon);
  FillHisto("kthx_true",kthx);
  FillHisto("kthy_true",kthy);
  pkaon = fRandom[0].Gaus(pkaon,0.15);
  kthx  = fRandom[1].Gaus(kthx,16e-6);
  kthy  = fRandom[2].Gaus(kthy,16e-6);
  // Double_t input[6]= {1./ptrack,thx,thy,pk.P()*fmevtogev,kthx,kthy};
  Double_t input[6]= {1./ptrack,thx,thy,pkaon,kthx,kthy};
  //Double_t input[6]= {1./ptrack,thx,thy,74.95,kthx,kthy};
  vector<Double_t> par;
  FillHisto("pk_smeared",pkaon);
  FillHisto("kthx_smeared",kthx);
  FillHisto("kthy_smeared",kthy);
  //cout << "-----------------------" << endl;
  for (int i = 0; i < 6;i++){
    par.push_back(input[i]);
    //    cout << "  " << input[i] ;
  }
  //cout << "" << endl;
  MM2Errors mm2eror(par,fSpectrometerCandidate);

  TLorentzVector Pnu = fK4Momentum - fPi4Momentum;
  //Double_t mm2 = Pnu.M2();
  Double_t mm2 = mm2eror.GetMM2Pion();
  //cout << " m2 =" << mm2 << endl;
  Double_t deltamm2=1e3*mm2eror.GetMM2PionError();
  Double_t thetax = fSpectrometerCandidate->GetSlopeXBeforeMagnet();
  Double_t thetay = fSpectrometerCandidate->GetSlopeYBeforeMagnet();
  Double_t theta  = TMath::Sqrt(thetax*thetax + thetay*thetay);
  FillHisto("trkp_vs_mm2_wogtk",momentum,mm2);


  //if(!goodgtkvtx) return;




  FillHisto("zvtx",fVertex.Z());
  FillHisto("xvtx",fVertex.X());
  FillHisto("yvtx",fVertex.Y());
  FillHisto("cda",fcda);
  FillHisto("trkp_vs_mm2",momentum,mm2);
  FillHisto("trkp_vs_theta",momentum,theta);
  FillHisto("trkp_vs_sigmamm2",1/par[0],deltamm2);
  FillHisto("mm2_vs_sigmamm2",deltamm2,mm2);


  if(!Acceptance()) return;
  FillHisto("trkp_vs_mm2_acccut",momentum,mm2);

  //CUTComment:: Cuts on Vertex and momentum
  //1. Closest Approached Distance > 15mm.
  //2. Zvtx to be incide of the fiducial volume of NA62 detector (110 - 160 m)
  //3. Momentum between 15 GeV/c and 65 GeV/c.
  if(momentum < 15000 || momentum > 65000) return;
  FillHisto("trkp_vs_mm2_pcut",momentum,mm2);
  if(fcda > 15.) return;
  if( fVertex.Z() < FRzmin || fVertex.Z() > FRzmax) return;
  //if( fabs(fVertex.X()) > 20) return;
  if( fabs(fVertex.Y()) > 20) return;
  FillHisto("trkp_vs_mm2_vtxcut",momentum,mm2);

  //No lav
  if(fLAVEvent->GetNHits() !=0) return;
  FillHisto("trkp_vs_mm2_nolav",momentum,mm2);
  if(fSACEvent->GetNHits() !=0) return;
  if(fIRCEvent->GetNHits() !=0) return;
  //No sav
  FillHisto("trkp_vs_mm2_nosav",momentum,mm2);

  Bool_t muv1match = MUV1Track();
  Bool_t muv2match = MUV2Track();
  Int_t lkrmatch  = LKrTrack();

  if(fMUV1Event->GetNCandidates() !=0 && !muv1match) return;
  FillHisto("trkp_vs_mm2_muv1",momentum,mm2);

  if(fMUV2Event->GetNCandidates() !=0 && !muv2match) return;
  FillHisto("trkp_vs_mm2_muv2",momentum,mm2);



  if(!lkrmatch ) return;
  FillHisto("trkp_vs_mm2_lkr",momentum,mm2);
  FillHisto("LKr_NC_final", NLKr_Cand);

  TRecoLKrCandidate* lkr = (TRecoLKrCandidate*)fLKrEvent->GetCandidate(lkrmatch);


  TVector3 g1_atlkr = fTool->GetKaonPositionAtZ(gamma1->GetInitialMomentum(),gamma1->GetProdPos().Vect(),zLKr);
  TVector3 g2_atlkr = fTool->GetKaonPositionAtZ(gamma2->GetInitialMomentum(),gamma2->GetProdPos().Vect(),zLKr);
  Double_t g1ax = TMath::Abs( g1_atlkr.X() );
  Double_t g1ay = TMath::Abs( g1_atlkr.Y() );
  Double_t g2ax = TMath::Abs( g2_atlkr.X() );
  Double_t g2ay = TMath::Abs( g2_atlkr.Y() );
  Double_t rg1lkr = TMath::Sqrt( pow(g1_atlkr.X(),2) + pow(g1_atlkr.Y(),2) );
  Double_t rg2lkr = TMath::Sqrt( pow(g2_atlkr.X(),2) + pow(g2_atlkr.Y(),2) );

  FillHisto("g1_atLKr",g1_atlkr.X(),g1_atlkr.Y());
  FillHisto("g2_atLKr",g2_atlkr.X(),g2_atlkr.Y());


  if(rg1lkr <= LKrRMin || rg2lkr <= LKrRMin ) return;
  if(g1ax >= LKrLMax || g2ax >= LKrLMax) return;
  if(g1ay >= LKrLMax || g2ay >= LKrLMax) return;
  if((g1ax+g1ay) >= TMath::Sqrt(2.)*LKrLMax) return;
  if((g2ax+g2ay) >= TMath::Sqrt(2.)*LKrLMax) return;
  if(lkr->GetClusterEnergy()/momentum < 0.15) return;
  FillHisto("trkp_vs_theta_final",momentum,theta);
  FillHisto("trkp_vs_sigmamm2_final",1/par[0],deltamm2);

  if(1/par[0] > 35 || 1/par[0] < 15) return;
  FillHisto("mm2_vs_sigmamm2_final",deltamm2,mm2);

  //if(mm2 < 0.07 && mm2 > 0.028){

    //std::cout << "--------------------------" << std::endl;
    //std::cout << "Eg1P = " << gamma1->GetInitialMomentum().Mag()<< std::endl;
    //std::cout << "Produced at (X,Y,Z)= " << gamma1->GetProdPos().X() << "," << gamma1->GetProdPos().Y() << ","<< gamma1->GetProdPos().Z() << std::endl;
    //std::cout << "Eg2P = " << gamma2->GetInitialMomentum().Mag()<< std::endl;
    //std::cout << "ElkrPi+ = " << lkr->GetClusterEnergy() << std::endl;
    //std::cout << "Ppi+ = " << 1/par[0]  << "Ppi+ true = " << ppi.P() << std::endl;
    //std::cout << "Ppi+ thx = " << par[1] << "true = " << ppi.Px()/ppi.Pz() << std::endl;
    //std::cout << "Ppi+ thy = " << par[2] << "true = " << ppi.Py()/ppi.Pz() << std::endl;
    ////std::cout << "Total Momentum = " << gamma1->GetInitial4Momentum().P() + gamma2->GetInitial4Momentum().P()+ momentum << std::endl;
    //std::cout << "K momentum = " << pk.P()<< std::endl;
    //std::cout << "Decayed at (X,Y,Z)= " << kplus->GetEndPos().X() << "," << kplus->GetEndPos().Y() << ","<< kplus->GetEndPos().Z() << std::endl;
    //std::map<Double_t,int>::iterator it = photonenergy.begin();
    //for(; it != photonenergy.end();it++){
    //  std::cout << "energy = " << it->first << "index = " << it->second << std::endl;
    //}

  //}

  if(NLKr_Cand < 3) return;
  FillHisto("mm2_vs_sigmamm2_k2pi",deltamm2,mm2);


  if(fLKrEvent->GetNCandidates() == 2){
    FillHisto("mm2_vs_sigmamm2_2g",deltamm2,mm2);
  }
  if(fLKrEvent->GetNCandidates() == 3){
    if(mm2 < 0.07 && mm2 > 0.028){
      TVector3 track_atlkr = fTool->GetPositionAtZ(fSpectrometerCandidate,zLKr);

      FillHisto("trk_atLKr_3g",track_atlkr.X(),track_atlkr.Y());
      FillHisto("LKr_dXdY_3g",track_atlkr.X() - piplus->GetPosLKrEntry().X() ,track_atlkr.Y() - piplus->GetPosLKrEntry().Y());

      std::cout << "--------------------------" << std::endl;
      //std::cout << "Eg1P = " << gamma1->GetInitialMomentum().Mag()<< std::endl;
      //std::cout << "Produced at (X,Y,Z)= " << gamma1->GetProdPos().X() << "," << gamma1->GetProdPos().Y() << ","<< gamma1->GetProdPos().Z() << std::endl;
      //std::cout << "Eg2P = " << gamma2->GetInitialMomentum().Mag()<< std::endl;
      //std::cout << "ElkrPi+ = " << lkr->GetClusterEnergy() << std::endl;
      std::cout << "Ppi+ funeral= " << piplus->GetEndProcessName().Data() << std::endl;
      std::cout << "Z vtx = " << fVertex.Z() << std::endl;
      std::cout << "deltamm2 = " << deltamm2 << std::endl;
      std::cout << "Ppi+ = " << 1/par[0]  << "Ppi+ true = " << ppi.P() << std::endl;
      std::cout << "Ppi+ final pos X= " << piplus->GetEndPos().X()  << std::endl;
      std::cout << "Ppi+ final pos Y= " << piplus->GetEndPos().Y()  << std::endl;
      std::cout << "Ppi+ final pos Z= " << piplus->GetEndPos().Z()  << std::endl;
      std::cout << "Ppi+ true at LKr = " << piplus->GetMomLKrEntry().P()  << std::endl;
      std::cout << "Ppi+ x true at LKr = " << piplus->GetPosLKrEntry().X()  << std::endl;
      std::cout << "Ppi+ y true at LKr = " << piplus->GetPosLKrEntry().Y()  << std::endl;
      std::cout << "Ppi+ thx = " << par[1] << "true = " << ppi.Px()/ppi.Pz() << std::endl;
      std::cout << "Ppi+ thy = " << par[2] << "true = " << ppi.Py()/ppi.Pz() << std::endl;
      std::cout << "Total Momentum = " << gamma1->GetInitial4Momentum().P() + gamma2->GetInitial4Momentum().P()+ momentum << std::endl;
      std::cout << "K momentum = " << pk.P()<< std::endl;
      std::cout << "Decayed at (X,Y,Z)= " << kplus->GetEndPos().X() << "," << kplus->GetEndPos().Y() << ","<< kplus->GetEndPos().Z() << std::endl;
      std::map<Double_t,int>::iterator it = photonenergy.begin();
      for(; it != photonenergy.end();it++){
        std::cout << "energy = " << it->first << "index = " << it->second << std::endl;
      }

    }

    FillHisto("g1_atLKr_3g",g1_atlkr.X(),g1_atlkr.Y());
    FillHisto("g2_atLKr_3g",g2_atlkr.X(),g2_atlkr.Y());

    FillHisto("mm2_vs_sigmamm2_3g",deltamm2,mm2);
    //cout << "LKr = " << fLKrEvent->GetNCandidates() << "Lav = " << fLAVEvent->GetNCandidates() << "SAV = " << fSACEvent->GetNCandidates() + fIRCEvent->GetNCandidates() << endl;
  }
  if(fLKrEvent->GetNCandidates() == 4){
    FillHisto("mm2_vs_sigmamm2_3plg",deltamm2,mm2);
  }
}

void K2piMC::PostProcess(){

}

void K2piMC::EndOfBurstUser(){
}

void K2piMC::EndOfRunUser(){
  SaveAllPlots();

}

void K2piMC::DrawPlot(){

}

K2piMC::~K2piMC(){
}

void K2piMC::MakeNominalKaonVertex(){


  //Beam position vector at trim5 and beam momentum
  //(at the momenPolt no GTK assuming 75 GeV Definitions.h)
  //Double_t tethax =  0.0012;
  //Double_t tethay =  0.;
  //Double_t pkaon  = 74.95;
  //Double_t pkaonz = pkaon/sqrt(1.+tethax*tethax+tethay*tethay);
  //Double_t pkaonx = pkaonz*tethax;
  //Double_t pkaony = pkaonz*tethay;


  Double_t ppion    = fSpectrometerCandidate->GetMomentum()*fmevtogev;
  Double_t pitethax = fSpectrometerCandidate->GetSlopeXBeforeMagnet();
  Double_t pitethay = fSpectrometerCandidate->GetSlopeYBeforeMagnet();
  Double_t ppionz   = ppion/sqrt(1.+pitethax*pitethax+pitethay*pitethay);
  Double_t ppionx   = ppionz*pitethax;
  Double_t ppiony   = ppionz*pitethay;
  fPi4Momentum.SetXYZM(ppionx,ppiony,ppionz,PiMass); //MeV
  //fK4Momentum.SetXYZM(pkaonx,pkaony,pkaonz,KMass);
  fK4Momentum.SetPxPyPzE(kplus->GetFinalMomentum().X(),kplus->GetFinalMomentum().Y(),kplus->GetFinalMomentum().Z(),kplus->GetFinalEnergy());
  fKMomentum = fK4Momentum.Vect();

  TVector3 KaonPos(0,0,101800.);


  fVertex = fTool->SingleTrackVertex( fSpectrometerCandidate->GetThreeMomentumBeforeMagnet(), fKMomentum, fSpectrometerCandidate->GetPositionBeforeMagnet(), KaonPos, fcda);

}

Bool_t K2piMC::IsGoodGTKVtx(){

  TRecoGigaTrackerCandidate* GTK = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(0);
  int type = GTK->GetType();
  double gtktime = GTK->GetTime();
  double gtkchi2X = GTK->GetChi2X();
  double gtkchi2Y = GTK->GetChi2Y();
  double gtkchi2T = GTK->GetChi2Time();
  double gtkchi2  = GTK->GetChi2();
  int gtknhits = GTK->GetNHits();

  TVector3 vertex;
  TVector3 gtkpos= GTK->GetPosition(2);
  TVector3 gtkpos1= GTK->GetPosition(0);
  TVector3 gtkpos2= GTK->GetPosition(1);
  TVector3 gtkmomentum= GTK->GetMomentum();
  TLorentzVector Pgtk(gtkmomentum, KMass);

  FillHisto("gtk_type", type);

  if(type < 100) return false;
  FillHisto("gtk_P", gtkmomentum.Mag());
  //FillHisto("gtk_choddt",  gtkchoddt);
  FillHisto("gtk_chi2X", gtkchi2X);
  FillHisto("gtk_chi2Y", gtkchi2Y);
  FillHisto("gtk_chi2T", gtkchi2T);
  FillHisto("gtk_chi2", gtkchi2);
  FillHisto("gtk_Nhits", gtknhits);
  FillHisto("gtk_pos", gtkpos.X(),gtkpos.Y());

  double dx12 = gtkpos1.X() - gtkpos2.X();
  double dx23 = gtkpos2.X() - gtkpos.X() ;
  double dx13 = gtkpos1.X() - gtkpos.X() ;
  double dy12 = gtkpos1.Y() - gtkpos2.Y();
  double dy23 = gtkpos2.Y() - gtkpos.Y() ;
  double dy13 = gtkpos1.Y() - gtkpos.Y() ;
  double thetax= dy13/ (gtkpos1.Z() - gtkpos.Z());
  double thetay = dx13/ (gtkpos1.Z() - gtkpos.Z());
  FillHisto("gtk_dX_12",dx12 );
  FillHisto("gtk_dX_23",dx23 );
  FillHisto("gtk_dX_13",dx13 );
  FillHisto("gtk_dY_12",dy12 );
  FillHisto("gtk_dY_23",dy23 );
  FillHisto("gtk_dY_13",dy13 );
  FillHisto("gtk_thetax", thetax);
  FillHisto("gtk_thetay", thetay);

  FillHisto("gtk_Nhits_vs_P", (Double_t)gtknhits, gtkmomentum.Mag());

  if(gtknhits == 0) return false;
  if(gtkmomentum.Mag() > 77200 || gtkmomentum.Mag() < 72700) return false;
  //if(fcda > 30) return false;

  Double_t ppion    = fSpectrometerCandidate->GetMomentum()*fmevtogev;
  Double_t pitethax = fSpectrometerCandidate->GetSlopeXBeforeMagnet();
  Double_t pitethay = fSpectrometerCandidate->GetSlopeYBeforeMagnet();
  Double_t ppionz   = ppion/sqrt(1.+pitethax*pitethax+pitethay*pitethay);
  Double_t ppionx   = ppionz*pitethax;
  Double_t ppiony   = ppionz*pitethay;

  fVertex = fTool->SingleTrackVertex( fSpectrometerCandidate->GetThreeMomentumBeforeMagnet(), gtkmomentum, fSpectrometerCandidate->GetPositionBeforeMagnet(), gtkpos, fcda);

  fPi4Momentum.SetXYZM(ppionx,ppiony,ppionz,PiMass); //GeV
  fKMomentum.SetXYZ(gtkmomentum.X()*fmevtogev,gtkmomentum.Y()*fmevtogev,gtkmomentum.Z()*fmevtogev);

  fK4Momentum.SetXYZM(fKMomentum.X(),fKMomentum.Y(),fKMomentum.Z(),KMass);
  return true;
}

Bool_t K2piMC::Acceptance(){
  Bool_t acceptanceFlag = true;
  TVector3 pos(0,0,0);
  Double_t r2 = 999999.;

  // Straw acceptance
  for (Int_t j=0; j<4; j++) {
    pos = fTool->GetPositionAtZ(fSpectrometerCandidate,zSTRAW_station[j]);
    r2 = (pos.X()-xSTRAW_station[j])*(pos.X()-xSTRAW_station[j])+pos.Y()*pos.Y();
    if (r2<75.*75. || r2>1000*1000) acceptanceFlag = false;
  }

  // RICH acceptance
  pos = fTool->GetPositionAtZ(fSpectrometerCandidate,219385.);
  r2 = (pos.X()-34.)*(pos.X()-34.)+pos.Y()*pos.Y();
  if (r2<90.*90. || r2>1100.*1100.) acceptanceFlag = false;
  pos = fTool->GetPositionAtZ(fSpectrometerCandidate,237326.);
  r2 = (pos.X()-2.)*(pos.X()-2.)+pos.Y()*pos.Y();
  if (r2<90.*90. || r2>1100.*1100.) acceptanceFlag = false;

  // CHOD acceptance
  pos = fTool->GetPositionAtZ(fSpectrometerCandidate,238960.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<125.*125. || r2>1100.*1100.) acceptanceFlag = false;
  pos = fTool->GetPositionAtZ(fSpectrometerCandidate,239340.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<125.*125. || r2>1100*1100) acceptanceFlag  = false;

  // LKr acceptance
  Bool_t isLKr=false;
  pos = fTool->GetPositionAtZ(fSpectrometerCandidate,241093.);
  if (fTool->LKrGeometricalAcceptance(pos.X(),pos.Y())) isLKr = true;
  if(!isLKr) acceptanceFlag = false;

  // MUV2 acceptance
  pos = fTool->GetPositionAtZ(fSpectrometerCandidate,244341.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<130.*130. || r2>1100*1100) acceptanceFlag = false;


  // MUV2 acceptance
  pos = fTool->GetPositionAtZ(fSpectrometerCandidate,245290.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<130.*130. || r2>1100*1100) acceptanceFlag = false;

  // MUV3 acceptance
  pos = fTool->GetPositionAtZ(fSpectrometerCandidate,246800.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<130.*130. || r2>1100*1100) acceptanceFlag = false;

  return acceptanceFlag;
}

Int_t K2piMC::LKrTrack(){

  //LKr matching and acceptance
  TVector3 Track_atLKr;
  Track_atLKr = fTool->GetPositionAtZ(fSpectrometerCandidate, zLKr);

  Int_t count = 0;
  Double_t mindtrkcl = -999;


  int bestlkr = FindClosestCluster(fLKrEvent, Track_atLKr, "LKr", mindtrkcl);

  if(bestlkr < 0) return 0;

  TRecoLKrCandidate* LKr = (TRecoLKrCandidate*)fLKrEvent->GetCandidate(bestlkr);

  TVector3 LKrPos(LKr->GetClusterX(),LKr->GetClusterY(),zLKr);
  double LKrCandTime = LKr->GetTime();
  double LKrdt = LKrCandTime - fSpectrometerCandidate->GetTime();

  FillHisto("trk_atLKr", Track_atLKr.X(),Track_atLKr.Y());
  FillHisto("LKr_dXdY", Track_atLKr.X() - LKrPos.X(),  Track_atLKr.Y() - LKrPos.Y());

  //CUTComment:: Search square in the LKr with side 6cm (to be done momentum dependent)
  if(TMath::Abs( Track_atLKr.X() - LKrPos.X() ) > 50 || TMath::Abs( Track_atLKr.Y() - LKrPos.Y() ) > 50) return false;


  FillHisto("LKr_Energy", LKr->GetClusterEnergy());

  return bestlkr;

}
Bool_t K2piMC::MUV1Track(){


  //MUV1 matching and acceptance
  TVector3 Track_atMUV1;
  Track_atMUV1 = fTool->GetPositionAtZ(fSpectrometerCandidate, zMUV1);

  Int_t count = 0;
  Double_t mindtrkcl = -999;


  int bestmuv1 = FindClosestCluster(fMUV1Event, Track_atMUV1, "MUV1", mindtrkcl);

  if(bestmuv1 < 0) return false;

  TRecoMUV1Candidate* MUV1 = (TRecoMUV1Candidate*)fMUV1Event->GetCandidate(bestmuv1);

  TVector3 MUV1Pos(MUV1->GetX(),MUV1->GetY(),zMUV1);
  double MUV1CandTime = MUV1->GetTime();
  double MUV1dt = MUV1CandTime - fSpectrometerCandidate->GetTime();

  FillHisto("trk_atMUV1", Track_atMUV1.X(),Track_atMUV1.Y());
  FillHisto("MUV1_dXdY", Track_atMUV1.X() - MUV1Pos.X(),  Track_atMUV1.Y() - MUV1Pos.Y());

  //CUTComment:: Search square in the MUV1 with side 6cm (to be done momentum dependent)
  if(TMath::Abs( Track_atMUV1.X() - MUV1Pos.X() ) > 100 || TMath::Abs( Track_atMUV1.Y() - MUV1Pos.Y() ) > 100) return false;
  FillHisto("MUV1_dt", MUV1dt);
  //if(TMath::Abs(MUV1dt) < 10) count++;


  FillHisto("MUV1_Energy", MUV1->GetEnergy());

  return true;
}
Bool_t K2piMC::MUV2Track(){


  //MUV2 matching and acceptance
  TVector3 Track_atMUV2;
  Track_atMUV2 = fTool->GetPositionAtZ(fSpectrometerCandidate, zMUV2);

  Int_t count = 0;
  Double_t mindtrkcl = -999;


  int bestmuv2 = FindClosestCluster(fMUV2Event, Track_atMUV2, "MUV2", mindtrkcl);

  if(bestmuv2 < 0) return false;

  TRecoMUV2Candidate* MUV2 = (TRecoMUV2Candidate*)fMUV2Event->GetCandidate(bestmuv2);

  TVector3 MUV2Pos(MUV2->GetX(),MUV2->GetY(),zMUV2);
  double MUV2CandTime = MUV2->GetTime();
  double MUV2dt = MUV2CandTime - fSpectrometerCandidate->GetTime();

  FillHisto("trk_atMUV2", Track_atMUV2.X(),Track_atMUV2.Y());
  FillHisto("MUV2_dXdY", Track_atMUV2.X() - MUV2Pos.X(),  Track_atMUV2.Y() - MUV2Pos.Y());

  //CUTComment:: Search square in the MUV2 with side 8cm (to be done momentum dependent)
  if(TMath::Abs( Track_atMUV2.X() - MUV2Pos.X() ) > 120 || TMath::Abs( Track_atMUV2.Y() - MUV2Pos.Y() ) > 120) return false;
  FillHisto("MUV2_dt", MUV2dt);
  //if(TMath::Abs(MUV2dt) < 10) count++;


  FillHisto("MUV2_Energy", MUV2->GetEnergy());

  return true;
}
Bool_t K2piMC::CHODTrack(){


  //CHOD matching and acceptance
  TVector3 Track_atCHOD;
  Track_atCHOD = fTool->GetPositionAtZ(fSpectrometerCandidate, zCHOD);

  Int_t count = 0;
  Double_t mindtrkcl = -999;


  int bestchod = FindClosestCluster(fCHODEvent, Track_atCHOD, "CHOD", mindtrkcl);

  if(bestchod < 0) return false;

  TRecoCHODCandidate* CHOD = (TRecoCHODCandidate*)fCHODEvent->GetCandidate(bestchod);

  TVector3 CHODPos(CHOD->GetHitPosition().X(),CHOD->GetHitPosition().Y(),zCHOD);
  double CHODCandTime = CHOD->GetTime();
  double CHODdt = CHODCandTime - fSpectrometerCandidate->GetTime();

  FillHisto("trk_atCHOD", Track_atCHOD.X(),Track_atCHOD.Y());
  FillHisto("CHOD_dXdY", Track_atCHOD.X() - CHODPos.X(),  Track_atCHOD.Y() - CHODPos.Y());

  //CUTComment:: Search square in the CHOD with side 8cm (to be done momentum dependent)
  if(TMath::Abs( Track_atCHOD.X() - CHODPos.X() ) > 60 || TMath::Abs( Track_atCHOD.Y() - CHODPos.Y() ) > 60) return false;
  FillHisto("CHOD_dt", CHODdt);

  return true;
}

Int_t K2piMC::FindClosestCluster( TRecoVEvent* Event, TVector3 Extrap_track, string detector_type, double& minimum){

  std::vector<double> dtrkcl;
  int position= -1;
  for (int iCand=0; iCand < Event->GetNCandidates(); iCand++){
    if(detector_type == "LKr"){
      TRecoLKrCandidate* Cluster = ((TRecoLKrCandidate*)Event->GetCandidate(iCand));
      double clusterx = Cluster->GetClusterX();
      double clustery = Cluster->GetClusterY();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }
    if(detector_type == "MUV1"){
      TRecoMUV1Candidate* Cluster = ((TRecoMUV1Candidate*)Event->GetCandidate(iCand));
      //Old Reco
      //double clusterx = Cluster->GetPosition().X()+ 60;
      //double clustery = Cluster->GetPosition().Y()+ 60;
      //New Reco
      double clusterx = Cluster->GetPosition().X();
      double clustery = Cluster->GetPosition().Y();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }
    if(detector_type == "MUV2"){
      TRecoMUV2Candidate* Cluster = ((TRecoMUV2Candidate*)Event->GetCandidate(iCand));
      double clusterx = Cluster->GetPosition().X();
      double clustery = Cluster->GetPosition().Y();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }
    if(detector_type == "MUV3"){
      TRecoMUV3Candidate* Cluster = ((TRecoMUV3Candidate*)Event->GetCandidate(iCand));
      double clusterx = Cluster->GetX();
      double clustery = Cluster->GetY();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }
    if(detector_type == "CHOD"){
      TRecoCHODCandidate* Cluster = ((TRecoCHODCandidate*)Event->GetCandidate(iCand));
      double clusterx = Cluster->GetHitPosition().X();
      double clustery = Cluster->GetHitPosition().Y();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }

  }
  if(dtrkcl.size() !=0){
    minimum= *min_element(dtrkcl.begin(), dtrkcl.end());
    position = distance(dtrkcl.begin(), min_element(dtrkcl.begin(), dtrkcl.end()));
    dtrkcl.clear();

  }
  return position;

}
