#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "PinunuAna.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "AnalysisTools.hh"
#include "TrackCandidate.hh"
#include "TrackSTRAWCandidate.hh"
#include "TrackCHODCandidate.hh"
#include "RawHeader.hh"
#include "L0TPData.hh"
#include "MM2Errors.hh"
#include "LAVMatching.hh"
#include "SAVMatching.hh"
#include "Geometry.hh"
#include "utl.hh"
using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

struct CompareSecond
{
  template <typename T>
  bool operator()(const T& left, const T& right )
  {
    return left.second < right.second;
  }
};
struct CompareFirst
{
  template <typename T>
  bool operator()(const T& left, const T& right )
  {
    return left.first < right.first;
  }
};

PinunuAna::PinunuAna(Core::BaseAnalysis *ba) : Analyzer(ba, "PinunuAna")
{
  fAnalyzerName = "PinunuAna";
  //Analysis tools
  fTool = AnalysisTools::GetInstance();
  fLAVMatch = new LAVMatching();
  fSAVMatch = new SAVMatching();
  fSpectrometerCandidate = new TRecoSpectrometerCandidate();

  zSTRAW_station[0] = 183508.0;
  zSTRAW_station[1] = 194066.0;
  zSTRAW_station[2] = 204459.0;
  zSTRAW_station[3] = 218885.0; // [ mm ]
  xSTRAW_station[0] = 101.2;
  xSTRAW_station[1] = 114.4;
  xSTRAW_station[2] = 92.4;
  xSTRAW_station[3] = 52.8; // [ mm ]

  //fTree = new TTree("Secondary", "A tree");
  //fTree->Branch("TrackThetaX",&thx);
  //fTree->Branch("TrackThetaY",&thy);
  //fTree->Branch("TrackP",&momtrack);
  //fTree->Branch("KaonThetaX",&kthx);
  //fTree->Branch("KaonThetaY",&kthy);
  //fTree->Branch("KaonP",&momk);
  //fTree->Branch("MM2",&mm2);
  //fTree->Branch("SigmaMM2",&sigmamm2);
  //fTree->Branch("dmdp",&dm2dx[0]);
  //fTree->Branch("dmdthx",&dm2dx[1]);
  //fTree->Branch("dmdthy",&dm2dx[2]);
  //fTree->Branch("dmdpk",&dm2dx[3]);
  //fTree->Branch("dmdkthx",&dm2dx[4]);
  //fTree->Branch("dmdkthy",&dm2dx[5]);

  RequestTree("LKr",new TRecoLKrEvent);

  //RequestTree("GigaTracker",new TRecoGigaTrackerEvent);

  RequestTree("Spectrometer",new TRecoSpectrometerEvent);
  RequestTree("CHOD",new TRecoCHODEvent);
  RequestTree("CHANTI",new TRecoCHANTIEvent);
  RequestTree("RICH",new TRecoRICHEvent);
  RequestTree("MUV1",new TRecoMUV1Event);
  RequestTree("Cedar",new TRecoCedarEvent);
  RequestTree("MUV2",new TRecoMUV2Event);
  RequestTree("MUV3",new TRecoMUV3Event);
  RequestTree("LAV",new TRecoLAVEvent);
  RequestTree("SAC",new TRecoSACEvent);
  RequestTree("IRC",new TRecoIRCEvent);
  //RequestL0Data();

  AddParam("Filter" , &fFilter , 1);

  fGigaTrackerAnalysis = new GigaTrackerAnalysis(ba);
  GoodTrack = new TrackCandidate();
}

void PinunuAna::InitOutput(){
  ///     RegisterOutput("outputName", &variableName)
}

void PinunuAna::InitHist(){

  for (int i=0; i < 4; i++) {



    fnhitschss[i] = new TH1I(Form("Nhitschamber%d_smallsigma",i+1), Form("Number of STRAW hits for chamber %d;Nhits",i+1) ,100,0,100);
    fnhitschls[i] = new TH1I(Form("Nhitschamber%d_largesigma",i+1), Form("Number of STRAW hits for chamber %d;Nhits",i+1) ,100,0,100);
    fnviewschss[i] = new TH1I(Form("Nviewschamber%d_smallsigma",i+1),Form("Number of STRAW views for chamber %d;Nviews",i+1 ),5,0,5);
    fnviewschls[i] = new TH1I(Form("Nviewschamber%d_largesigma",i+1), Form("Number of STRAW views for chamber %d;Nviews",i+1) ,5,0,5);

    ftrkpos_stationss[i]      = new TH2D(Form("Track_position_station%d_smallsigma",i+1),Form("track @ station %d; x [mm]; y [mm]",i+1),2000,-1000.,1000.,2000,-1000.,1000.);
    ftrkpos_stationls[i]      = new TH2D(Form("Track_position_station%d_largesigma",i+1),Form("track @ station %d; x [mm]; y [mm]",i+1),2000,-1000.,1000.,2000,-1000.,1000.);
    fdnhits_vs_mm2_ch[i]      = new TH2F(Form("dnHits_vs_mm2_ch%d",i+1), Form("dnHits for chamber %d vs MM2;dnHits;MM2",i+1),200,-100,100 ,300,-0.15,0.15);
    fsigmamm2_vs_dnhits_ch[i] = new TH2F(Form("sigmamm2_vs_dnHits_ch%d",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;#sigma(MM2);Nhits",i+1),800,0,5 ,200,-100,100);
    fp_vs_dnhits_chss [i]     = new TH2F(Form("P_vs_dnHits_ch%d_ss",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;P_{track}[GeV/c];Nhits",i+1),200,0,100 ,200,-100,100);
    fp_vs_dnhits_chls [i]     = new TH2F(Form("P_vs_dnHits_ch%d_ls",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;P_{track}[GeV/c];Nhits",i+1),200,0,100 ,200,-100,100);
    fp_vs_nclhits_chss[i]     = new TH2F(Form("P_vs_ncloseHits_ch%d_ss",i+1), "Number of STRAW hits for chamber 1 vs P;P_{track}[GeV/c];Nhits",200,0,100 ,200,-100,100);
    fp_vs_nclhits_chls[i]     = new TH2F(Form("P_vs_ncloseHits_ch%d_ls",i+1), "Number of STRAW hits for chamber 1 vs P;P_{track}[GeV/c];Nhits",200,0,100 ,200,-100,100);



   fp_vs_dnhits_ch_t1[i]        = new TH2F(Form("P_vs_dnHits_ch%d_type1",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;P_{track}[GeV/c];Nhits",i+1),200,0,100 ,200,-100,100);
   fp_vs_dnhits_ch_t2[i]        = new TH2F(Form("P_vs_dnHits_ch%d_type2",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;P_{track}[GeV/c];Nhits",i+1),200,0,100 ,200,-100,100);
   fp_vs_dnhits_ch_t3[i]        = new TH2F(Form("P_vs_dnHits_ch%d_type3",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;P_{track}[GeV/c];Nhits",i+1),200,0,100 ,200,-100,100);
   fp_vs_dnhits_ch_t4[i]        = new TH2F(Form("P_vs_dnHits_ch%d_type4",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;P_{track}[GeV/c];Nhits",i+1),200,0,100 ,200,-100,100);
   fp_vs_dnhits_ch_t5[i]        = new TH2F(Form("P_vs_dnHits_ch%d_type5",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;P_{track}[GeV/c];Nhits",i+1),200,0,100 ,200,-100,100);

   fdnhits_vs_mm2_ch_t1[i]      = new TH2F(Form("sigmamm2_vs_dnHits_ch%d_type1",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;#sigma(MM2);Nhits",i+1),800,0,5 ,200,-100,100);
   fdnhits_vs_mm2_ch_t2[i]      = new TH2F(Form("sigmamm2_vs_dnHits_ch%d_type2",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;#sigma(MM2);Nhits",i+1),800,0,5 ,200,-100,100);
   fdnhits_vs_mm2_ch_t3[i]      = new TH2F(Form("sigmamm2_vs_dnHits_ch%d_type3",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;#sigma(MM2);Nhits",i+1),800,0,5 ,200,-100,100);
   fdnhits_vs_mm2_ch_t4[i]      = new TH2F(Form("sigmamm2_vs_dnHits_ch%d_type4",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;#sigma(MM2);Nhits",i+1),800,0,5 ,200,-100,100);
   fdnhits_vs_mm2_ch_t5[i]      = new TH2F(Form("sigmamm2_vs_dnHits_ch%d_type5",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers vs P;#sigma(MM2);Nhits",i+1),800,0,5 ,200,-100,100);

   fsigmamm2_vs_dnhits_ch_t1[i] = new TH2F(Form("dnHits_vs_mm2_ch%d_type1",i+1), Form("dnHits for chamber %d vs MM2;dnHits;MM2",i+1),200,-100,100 ,300,-0.15,0.15);
   fsigmamm2_vs_dnhits_ch_t2[i] = new TH2F(Form("dnHits_vs_mm2_ch%d_type2",i+1), Form("dnHits for chamber %d vs MM2;dnHits;MM2",i+1),200,-100,100 ,300,-0.15,0.15);
   fsigmamm2_vs_dnhits_ch_t3[i] = new TH2F(Form("dnHits_vs_mm2_ch%d_type3",i+1), Form("dnHits for chamber %d vs MM2;dnHits;MM2",i+1),200,-100,100 ,300,-0.15,0.15);
   fsigmamm2_vs_dnhits_ch_t4[i] = new TH2F(Form("dnHits_vs_mm2_ch%d_type4",i+1), Form("dnHits for chamber %d vs MM2;dnHits;MM2",i+1),200,-100,100 ,300,-0.15,0.15);
   fsigmamm2_vs_dnhits_ch_t5[i] = new TH2F(Form("dnHits_vs_mm2_ch%d_type5",i+1), Form("dnHits for chamber %d vs MM2;dnHits;MM2",i+1),200,-100,100 ,300,-0.15,0.15);




    BookHisto(fnhitschss[i]  , 0 , "StrawAnalysis");
    BookHisto(fnhitschls[i]  , 0 , "StrawAnalysis");
    BookHisto(fnviewschss[i] , 0 , "StrawAnalysis");
    BookHisto(fnviewschls[i] , 0 , "StrawAnalysis");

    BookHisto(ftrkpos_stationss[i]      , 0 , "StrawAnalysis");
    BookHisto(ftrkpos_stationls[i]      , 0 , "StrawAnalysis");
    BookHisto(fdnhits_vs_mm2_ch[i]      , 0 , "StrawAnalysis");
    BookHisto(fsigmamm2_vs_dnhits_ch[i] , 0 , "StrawAnalysis");
    BookHisto(fp_vs_dnhits_chss [i]     , 0 , "StrawAnalysis");
    BookHisto(fp_vs_dnhits_chls [i]     , 0 , "StrawAnalysis");
    BookHisto(fp_vs_nclhits_chss[i]     , 0 , "StrawAnalysis");
    BookHisto(fp_vs_nclhits_chls[i]     , 0 , "StrawAnalysis");

    BookHisto(fp_vs_dnhits_ch_t1[i]       , 0 , "StrawAnalysis");
    BookHisto(fp_vs_dnhits_ch_t2[i]       , 0 , "StrawAnalysis");
    BookHisto(fp_vs_dnhits_ch_t3[i]       , 0 , "StrawAnalysis");
    BookHisto(fp_vs_dnhits_ch_t4[i]       , 0 , "StrawAnalysis");
    BookHisto(fp_vs_dnhits_ch_t5[i]       , 0 , "StrawAnalysis");

    BookHisto(fdnhits_vs_mm2_ch_t1[i]     , 0 , "StrawAnalysis");
    BookHisto(fdnhits_vs_mm2_ch_t2[i]     , 0 , "StrawAnalysis");
    BookHisto(fdnhits_vs_mm2_ch_t3[i]     , 0 , "StrawAnalysis");
    BookHisto(fdnhits_vs_mm2_ch_t4[i]     , 0 , "StrawAnalysis");
    BookHisto(fdnhits_vs_mm2_ch_t5[i]     , 0 , "StrawAnalysis");

    BookHisto(fsigmamm2_vs_dnhits_ch_t1[i], 0 , "StrawAnalysis");
    BookHisto(fsigmamm2_vs_dnhits_ch_t2[i], 0 , "StrawAnalysis");
    BookHisto(fsigmamm2_vs_dnhits_ch_t3[i], 0 , "StrawAnalysis");
    BookHisto(fsigmamm2_vs_dnhits_ch_t4[i], 0 , "StrawAnalysis");
    BookHisto(fsigmamm2_vs_dnhits_ch_t5[i], 0 , "StrawAnalysis");



    //BookHisto(new TH1I(Form("dnHits_ch%d",i+1), Form("Number of STRAW hits for chamber %d - hits in chambers;Nhits",i+1) ,100,-50,50));
    //BookHisto(new TH1I(Form("ncloseHits_ch%d",i+1), "Number of STRAW hits for chamber 1;Nhits" ,100,0,100));


    for (int j=0; j < 4; j++) {

      fhitsdt_ch_view[i][j]     = new TH1F(Form("Hitsdt_ch%d_view%d",i+1,j),Form(" ; dT^{%d, %d}_{ straw hits}",i+1,j),400,-200.,200.);
      fhits_dist_ch_view[i][j]  = new TH1F(Form("Hits_dist_ch%d_view%d",i+1,j),Form("; Hitdist_{%d,%d} [mm]",i+1,j), 2400., 0., 2400.);
      fnclosehits_ch_view[i][j] = new TH1I(Form("ncloseHits_ch%d_view%d",i+1,j), Form("Number of STRAW hits for chamber %d view %d;Nhits",i+1,j) ,200,-100,100);

      BookHisto(fhitsdt_ch_view    [i][j], 0 , "StrawAnalysis");
      BookHisto(fhits_dist_ch_view [i][j], 0 , "StrawAnalysis");
      BookHisto(fnclosehits_ch_view[i][j], 0 , "StrawAnalysis");

    }
  }


  fdnhits_vs_mm2      = new TH2F(Form("dnHits_vs_mm2"), Form("dnHits vs MM2 total;dnHits;MM2"),200,-100,100 ,300,-0.15,0.15);
  fsigmamm2_vs_dnhits = new TH2F(Form("sigmamm2_vs_dnHits"), Form("Number of STRAW hits for all chambers - hits in all chambers vs P;#sigma(MM2);Nhits"),800,0,5 ,200,-100,100);
  fp_vs_dnhits_ss     = new TH2F(Form("P_vs_dnHits_ss"), Form("Number of STRAW hits for all chamber - hits in all chambers vs P small sigma;P_{track}[GeV/c];Nhits"),200,0,100 ,200,-100,100);
  fp_vs_dnhits_ls     = new TH2F(Form("P_vs_dnHits_ls"), Form("Number of STRAW hits for all chamber - hits in all chambers vs P large sigma ;P_{track}[GeV/c];Nhits"),200,0,100 ,200,-100,100);

  BookHisto(fdnhits_vs_mm2     , 0 , "StrawAnalysis");
  BookHisto(fsigmamm2_vs_dnhits, 0 , "StrawAnalysis");
  BookHisto(fp_vs_dnhits_ss    , 0 , "StrawAnalysis");
  BookHisto(fp_vs_dnhits_ls    , 0 , "StrawAnalysis");



  //Preliminary plots before any selection
  BookHisto(new TH1I("NGood","N good STRAW tracks;NGoodtracks",50,0,50));
  BookHisto(new TH1I("BurstID","Burst  ID;BurstID",4000,0,4000));

  BookHisto(new TH1I("Cut0_LKr_NC"  , "Number of LKr candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_GTK_NC"  , "Number of GTK candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_LAV_NC"  , "Number of LAV candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_MUV1_NC" , "Number of MUV1 candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_MUV2_NC" , "Number of MUV2 candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_MUV3_NC" , "Number of MUV3 candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_RICH_NC" , "Number of RICH candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_CHOD_NC" , "Number of CHOD candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_RICH_NC" , "Number of RICH candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_Cedar_NC" , "Number of Cedar candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("Cut0_CHANTI_NC", "Number of CHANTI candidates;NCandidates" ,50,0,50));

  BookHisto(new TH1F("CHOD_triggdt","T_{trigger} - T_{CHOD} ; dT_{ Cedar}",800,-100.,100.));

  //With cedar
  BookHisto(new TH1F("Cedar_good_dt","T_{Cedar} - T_{CHOD} ; dT_{ Cedar}",800,-100.,100.));
  BookHisto(new TH1F("trk_P_wcedar", " Track momentum;P_{track} [MeV/c]",200,0.,100000));
  BookHisto(new TH1F("MM2_wcedar", " Missing mass squared ; (P_{K} - P_{#pi})^{2} [MeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_MM2_wcedar", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_MM2_wocedar", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  //RICH
  BookHisto(new TH1I("RICHNHits", " number of hits for the matched RICH track", 50, 0, 50));
  BookHisto(new TH1F("RICHChi2","#chi^{2} ; #chi^{2}",300,0.,30.));
  BookHisto(new TH1F("RICH_dt","",1200,-300,300));
  BookHisto(new TH1F("RICHRadius", "RICH radius", 300, 0., 300.));
  BookHisto(new TH1F("RICHAngle", "RICH angle", 400, -0.1, 0.1));
  BookHisto(new TH1F("RICHMass", "Mass computed with RICH", 1000, 0., 1000.));
  BookHisto(new TH2F("RICHPvsM", "Momentum vs RICH mass", 200, 0., 100000., 1000, 0., 1000.));
  BookHisto(new TH2F("RICHPvsR", "Momentum vs Radius", 200, 0., 100000., 300, 0., 300.));
  BookHisto(new TH1I("RICH_Nsmalldt","T_{RICH} - T_{CHOD} ; dT_{ RICH}",4,0.,4.));

  BookHisto(new TH2F("notRICH_atRICH","track @ RICH ; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("trk_atRICH","track @ RICH ; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("trk_atRICH_FP","track @ RICH front plane; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("trk_atRICH_Mirr","track @ RICH mirror; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("trk_atRICH_EntW","track @ RICH entrance window; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("trk_atRICH_ExW","track @ RICH exit window; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("RICH_dXdY","dx dy @ RICH  ; dx @ RICH [mm]; dy @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));

  BookHisto(new TH1F("RICH_dslopey", " ;#theta_{Y RICH} - #theta_{Y track} [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH1F("RICH_dslopex", " ;#theta_{X RICH} - #theta_{X track} [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH2F("RICH_slopes", "Slopes taken from the RICH ;dxdz[rad];dydz[rad]", 1000, -0.1, 0.1, 1000, -0.1, 0.1));
  BookHisto(new TH2F("RICH_dslope", "Slope difference between STRAW and RICH ; xslope_{RICH} - xslope_{STRAW} [rad]; yslope_{RICH} - yslope_{STRAW}[rad]", 1000, -0.04, 0.04, 1000, -0.04, 0.04));
  BookHisto(new TH2F("RICH_pos_min_slope"," ; x @ RICH_{center} - STRAW[mm]; y @ RICH_{center} - STRAW [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));

  //After cedar
  BookHisto(new TH1F("trk_EoP_all",";E/p for all matched tracks after ",120,0,1.2));
  BookHisto(new TH2F("trk_EoP_vs_P_all","; P[MeV] ;E_{LKr}/P",200, 0, 100000, 120, 0, 1.2 ));
  BookHisto(new TH2F("trk_atLKr_all","track @ LKr; x @ LKr [mm]; y @ LKr [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));


  BookHisto(new TH2F("P_vs_MM2_cedar_rich", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  BookHisto(new TH1F("Vertex_Z_afterrich", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH1F("Vertex_X_afterrich", " X vertex ; Xvtx [mm]", 1000, -500, 500));
  BookHisto(new TH1F("Vertex_Y_afterrich", " Y vertex ; Yvtx [mm]", 1000, -500, 500));

  BookHisto(new TH1F("gtk_P", " GTK momentum;P_{K} [MeV/c]",100,65000.,85000));
  BookHisto(new TH1I("gtk_Nhits", "Number of GTK hits;Nhits" ,100,0,100));
  BookHisto(new TH1F("gtk_cedardt","T_{Cedar} - T_{gtk} ; dT_{Cedar - gtk}",800,-100.,100.));
  BookHisto(new TH1F("gtk_choddt","T_{chod} - T_{gtk} ; dT_{chod - gtk}",400,-50.,50.));
  BookHisto(new TH1F("gtk_chi2","#chi^{2} ; #chi^{2}",300,0.,300.));
  BookHisto(new TH1F("gtk_chi2X","#chi^{2}X ; #chi^{2}",300,0.,300.));
  BookHisto(new TH1F("gtk_chi2Y","#chi^{2}Y ; #chi^{2}",300,0.,300.));
  BookHisto(new TH1F("gtk_chi2T","#chi^{2}T ; #chi^{2}",300,0.,300.));
  BookHisto(new TH2F("gtk_pos","GTK position; x @ GTK [mm]; y @ GTK [mm]", 100,-50,50,100,-50,50));
  BookHisto(new TH2F("gtk_dt_vs_cda", " cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -50, 50,300, 0, 300));

  BookHisto(new TH1F("gtk_dy2", "  ;#Delta y_{proj - real}", 60, -10, 10));
  BookHisto(new TH1F("gtk_dx12", "  ;#Delta x_{gtk1 - gtk2}", 60, -10, 10));
  BookHisto(new TH1F("gtk_dxrproj", "  ;#Delta x^{real - proj}_{@gtk3}", 60, -10, 10));
  BookHisto(new TH1F("gtk_dxrbend", "  ;#Delta x^{real - bend}_{@gtk3}", 60, -10, 10));
  BookHisto(new TH1F("gtk_dxbendproj", "  ;#Delta x^{bend - proj}_{@gtk3}", 60, -10, 10));
  BookHisto(new TH1F("gtk_kthx12", "  ;Kaon #theta_{x}",200, -0.004, 0.004));
  BookHisto(new TH1F("gtk_kth1", "  ;Kaon #theta_{1}",200, -0.004, 0.004));
  BookHisto(new TH1F("gtk_kth2", "  ;Kaon #theta_{2}",400, -0.1, 0.1));
  BookHisto(new TH1F("gtk_kthbend", "  ;Kaon #theta_{bend}",400, -0.1, 0.1));

  BookHisto(new TH2D("gtk_pk_vs_kthbend", "PKaon vs Kaon #theta_{1} + #theta_{2}",100,65.,85. , 400, -0.1, 0.1));
  BookHisto(new TH2D("gtk_pk_vs_dx12", "PKaon vs #Delta x_{gtk1 - gtk2}",100,65.,85. , 60, -10, 10));
  BookHisto(new TH2D("gtk_pk_vs_kthx12", "PKaon vs Kaon #theta_{x}}",100,65.,85. , 200, -0.004,0.004));

  BookHisto(new TH1F("CHANTI_cedardt","T_{Cedar} - T_{CHANTI} ; dT_{ CHANTI - Cedar}",800,-100.,100.));
  BookHisto(new TH1F("CHANTI_choddt","T_{chod} - T_{CHANTI} ; dT_{ CHANTI - Cedar}",800,-100.,100.));
  BookHisto(new TH1F("CHANTI_gtkdt","T_{gtk} - T_{CHANTI} ; dT_{ CHANTI - Cedar}",800,-100.,100.));

  BookHisto(new TH2F("P_vs_MM2_vtxcut", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_MM2_muv1match", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_MM2_muv2match", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  BookHisto(new TH1I("NInTimeClusters"," N in time clusters",30,0,30));
  BookHisto(new TH1I("NInTimeClustersAux"," N in time clusters",30,0,30));

  BookHisto(new TH2F("total_P_vs_EoP","; P[MeV] ;E_{tot}/P",200, 0, 100000, 120, 0, 1.2 ));
  BookHisto(new TH2F("total_EoP_vs_MM2", " Ecal/P vs Missing mass squared  ;E_{cal}/P;(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",120,0,1.2, 300, -0.15, 0.15));
  BookHisto(new TH1F("Total_Energy",";E_{MUV1 + MUV2 + LKr}[MeV]",200,0,100000));
  BookHisto(new TH2F("P_vs_Etot","; P[MeV] ;E_{calorimeter}",200, 0, 100000 ,200, 0, 100000 ));
  BookHisto(new TH2F("Hcal_vs_Ecal",";E_{MUV1 + MUV2}[MeV];E_{LKr}[MeV]",200,0,100000,200,0,100000));
  BookHisto(new TH2F("fracMUV1_vs_fracMUV2","; E_{MUV1}/E_{cal} ; E_{MUV2}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("fracMUV1_vs_fracLKr","; E_{MUV1}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("fracMUV2_vs_fracLKr","; E_{MUV2}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));

  BookHisto(new TH2F("P_vs_MM2_nolav", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_MM2_nosav", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_MM2_nopositron", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));


  BookHisto(new TH1F("test_discriminant","",240,0,2.4 ));

  //K2pi selection
  BookHisto(new TH1I("K2pi_NInTimeClusters"," N in time clusters",30,0,30));
  BookHisto(new TH1F("Photon1_Energy",";E_{#gamma 1}[MeV]",200,0,100000));
  BookHisto(new TH1F("Photon2_Energy",";E_{#gamma 2}[MeV]",200,0,100000));

  BookHisto(new TH1F("Photon12_distance",";dist_{#gamma 1 #gamma 2} [mm]",400, 0, 2000));
  BookHisto(new TH2F("Pi0MM2_vs_zvtx", " Track momentum vs Missing mass squared ;#pi^{0} z vtx [mm] ;(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",300, 0, 300000, 300, -0.15, 0.15));
  BookHisto(new TH1F("Pi0Mass", "  ;#pi^{0} mass [GeV]", 200, 0.1, 0.2));
  BookHisto(new TH1F("Pi0_Energy",";E_{#pi 0}[MeV]",200,0,100000));
  BookHisto(new TH1F("Pi0_Kmom",";P_{K +}[MeV]",200,0,100000));
  BookHisto(new TH1F("dzvtx", " z vtx with 2 photons from LKr - zvtx with gtk ;#Delta zvtx [mm]",100, -50000, 50000));
  BookHisto(new TH2F("K2pi_piplus_expected_dXdY","track @ CHOD; dx @ CHOD [mm]; dy @ CHOD [mm]", 2400, -1200., 1200., 2400, -1200., 1200. ));
  BookHisto(new TH2F("K2pi_piplus_expected_dslope", "; xslope_{#pi +} - xslope_{STRAW} [rad]; yslope_{#pi +} - yslope_{STRAW}[rad]", 1000, -0.01, 0.01, 1000, -0.01, 0.01));
  BookHisto(new TH1F("K2pi_piplus_expected_dp", "; P_{#pi +} - P_{track} [MeV/c]", 120, -30000, 30000));

  BookHisto(new TH2F("K2pi_P_vs_MM2_photoncuts", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_trk_atLKr","track @ LKr; x @ LKr [mm]; y @ LKr [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH1F("K2pi_Total_Energy",";E_{MUV1 + MUV2 + LKr}[MeV]",200,0,100000));
  BookHisto(new TH2F("K2pi_Hcal_vs_Ecal",";E_{MUV1 + MUV2}[MeV];E_{LKr}[MeV]",200,0,100000,200,0,100000));
  BookHisto(new TH2F("K2pi_trk_EoP_vs_P","; P[MeV] ;E_{LKr}/P",200, 0, 100000, 120, 0, 1.2 ));
  BookHisto(new TH2F("K2pi_total_EoP_vs_P","; P[MeV] ;E_{cal}/P",200, 0, 100000, 120, 0, 1.2 ));
  BookHisto(new TH2F("K2pi_P_vs_Etot","; P[MeV] ;E_{cal}",200, 0, 100000,200, 0, 100000));
  BookHisto(new TH2F("K2pi_fracMUV1_vs_fracMUV2","; E_{MUV1}/E_{cal} ; E_{MUV2}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("K2pi_fracMUV1_vs_fracLKr","; E_{MUV1}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("K2pi_fracMUV2_vs_fracLKr","; E_{MUV2}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("K2pi_muv2MIP_P_vs_MM2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_nomuv2MIP_P_vs_MM2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  //Error problems
  //for(int i = 0;i<3;i++){
  //  for(int j = 0;j<3;j++){
  //    if(i<=j){
  //      BookHisto(new TH2F(Form("P_vs_sigmaMM2_%d_%d",i,j), " Track momentum vs sigma Missing mass squared  ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  //      BookHisto(new TH2F(Form("Thetax_vs_sigmaMM2_%d_%d",i,j), " Thetax vs sigma Missing mass squared ;#theta_{x} ;#sigma(MM2)",400,-0.1,0.1,800,0,5));
  //      BookHisto(new TH2F(Form("Thetay_vs_sigmaMM2_%d_%d",i,j), " Thetay vs sigma Missing mass squared ;#theta_{y} ;#sigma(MM2)",400,-0.1,0.1,800,0,5));
  //    }
  //  }
  //}

  BookHisto(new TH2F("K2pi_Pk_vs_thetax", " Track momentum vs Missing mass squared  ;P_{K} [GeV/c];#theta_{x}",100,65.,85.,200,-0.05,0.05));
  BookHisto(new TH2F("K2pi_Pk_vs_thetay", " Track momentum vs Missing mass squared  ;P_{K} [GeV/c];#theta_{y}",100,65.,85.,200,-0.05,0.05));
  //Ultimate test
  BookHisto(new TH2F("K2pi_Pk_vs_sigmaMM2", " Kaon momentum vs sigma Missing mass squared ;P_{K}[GeV/c];#sigma(MM2)",100,65.,85.,800,0,5));
  BookHisto(new TH2F("K2pi_phi_vs_MM2", " Track momentum vs Missing mass squared  ;#phi ;(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]", 628, -1.57, 1.57,300,-0.15,0.15));
  BookHisto(new TH2F("K2pi_kthetax_vs_sigmaMM2", "  ;Kaon #theta_{x};#sigma(MM2)",400, -0.002, 0.002,800,0,5));
  BookHisto(new TH2F("K2pi_kthetay_vs_sigmaMM2", "  ;Kaon #theta_{y};#sigma(MM2)",800, -0.004, 0.004,800,0,5));

  BookHisto(new TH1F("K2pi_SeedRatio_MUV1",";E_{seed}/E_{clus}",110,0,1.1));
  BookHisto(new TH1F("K2pi_SeedRatio_MUV2",";E_{seed}/E_{clus}",110,0,1.1));

  //Dependencies
  BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_thxgr0", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_thxless0", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));

  BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_thygr0", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_thyless0", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));

  //BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_strip1", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  //BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_strip2", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  //BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_strip3", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  //BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_strip4", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  //BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_strip5", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  //Dependencies
  BookHisto(new TH1F("K2pi_NLKrcand_notmuv3",";NLKrCand ",50,0,50));
  BookHisto(new TH2F("K2pi_P_vs_MM2_notmuv3", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH1F("K2pi_zvtx_notmuv3", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("K2pi_fracMUV1_vs_fracMUV2_notmuv3","; E_{MUV1}/E_{cal} ; E_{MUV2}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("K2pi_fracMUV1_vs_fracLKr_notmuv3","; E_{MUV1}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("K2pi_fracMUV2_vs_fracLKr_notmuv3","; E_{MUV2}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));


  BookHisto(new TH1F("K2pi_shower_width_MUV1","Shower width MUV1; ShowerWidth_{MUV1} [mm]",500,0,500));
  BookHisto(new TH1F("K2pi_shower_width_LKr","Shower width LKr; ShowerWidth_{LKr} [mm]",500,0,500));
  BookHisto(new TH1F("K2pi_shower_width_MUV2","Shower width MUV2; ShowerWidth_{MUV2} [mm]",500,0,500));
  BookHisto(new TH1F("K2pi_shower_width_average","Total shower width; ShowerWidth [mm]",400,0,400));

  BookHisto(new TH2F("K2pi_P_vs_MM2_sw", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_P_vs_MM2_strawhits", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_P_vs_MM2_final", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH1F("K2pi_zvtx_sw", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("K2pi_dMM2_vs_MM2", "  ;#Delta mm2;(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,-0.01,0.01, 300, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_dMM2_vs_MM2_smallsigma", "  ;#Delta mm2;(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,-0.01,0.01, 300, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_dMM2_vs_MM2_largesigma", "  ;#Delta mm2;(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,-0.01,0.01, 300, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_P_vs_sigmaMM2", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  BookHisto(new TH2F("K2pi_MM2_vs_sigmaMM2", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_P_vs_thetax", " Track momentum vs #theta_{x}  ;P_{#pi} [GeV/c];#theta_{x}",200,0.,100.,200,-0.05,0.05));
  BookHisto(new TH2F("K2pi_P_vs_thetay", " Track momentum vs #theta_{y}  ;P_{#pi} [GeV/c];#theta_{y}",200,0.,100.,200,-0.05,0.05));
  BookHisto(new TH2F("K2pi_P_vs_theta", " Track momentum vs #theta  ;P_{#pi} [GeV/c];#theta",200,0.,100.,200,-0.05,0.05));




  BookHisto(new TH2F("STRAW_Nhits_vs_sigmaMM2", "Number of STRAW hits;Nhits;#sigma(MM2)" ,100,0,100,800,0,5));
  BookHisto(new TH2F("STRAW_Nhits_vs_sigmaMM2_plt20", "Number of STRAW hits;Nhits;#sigma(MM2)" ,100,0,100,800,0,5));



  BookHisto(new TH1I(Form("evttype1"), Form("event type Cahmber 1;Event Type") ,20,0,20));
  BookHisto(new TH1I(Form("evttype2"), Form("event type Cahmber 2;Event Type") ,20,0,20));
  BookHisto(new TH1I(Form("evttype3"), Form("event type Cahmber 3;Event Type") ,20,0,20));
  BookHisto(new TH1I(Form("evttype4"), Form("event type Cahmber 4;Event Type") ,20,0,20));


  BookHisto(new TH2F("STRAW_Nhits_vs_sigmaMM2_smallsigma", "Number of STRAW hits;Nhits;#sigma(MM2)" ,100,0,100,800,0,5));

  BookHisto(new TH2F("STRAW_Nhits_vs_sigmaMM2_largesigma", "Number of STRAW hits;Nhits;#sigma(MM2)" ,100,0,100,800,0,5));


  BookHisto(new TH2F("K2pi_theta_vs_sigmaMM2_below35GeV", " Sigma Missing mass squared vs Track theta  ;#theta_{track}[rad];#sigma(MM2)",200,-0.05,0.05,800,0,5));
  BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_below35GeV", " Track momentum vs sigma Missing mass squared  ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  BookHisto(new TH2F("K2pi_MM2_vs_sigmaMM2_below35GeV", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_P_vs_sigmaMM2_above35GeV", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  BookHisto(new TH2F("K2pi_MM2_vs_sigmaMM2_above35GeV", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("K2pi_theta_vs_sigmaMM2_above35GeV", " Sigma Missing mass squared vs Track theta  ;#theta_{track}[rad];#sigma(MM2)",200,-0.05,0.05,800,0,5));

  BookHisto(new TH2F("P_vs_MM2_nogamma", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_thetax_nogamma", " Track momentum vs #theta_{x}  ;P_{#pi} [GeV/c];#theta_{x}",200,0.,100.,200,-0.05,0.05));
  BookHisto(new TH2F("P_vs_thetay_nogamma", " Track momentum vs #theta_{y}  ;P_{#pi} [GeV/c];#theta_{y}",200,0.,100.,200,-0.05,0.05));
  BookHisto(new TH2F("P_vs_theta_nogamma", " Track momentum vs #theta  ;P_{#pi} [GeV/c];#theta",200,0.,100.,200,-0.05,0.05));

  BookHisto(new TH2F("fracMUV1_vs_fracMUV2_notmuv3","; E_{MUV1}/E_{cal} ; E_{MUV2}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("fracMUV1_vs_fracLKr_notmuv3","; E_{MUV1}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("fracMUV2_vs_fracLKr_notmuv3","; E_{MUV2}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));

  BookHisto(new TH2F("P_vs_MM2_notmuv3", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_thetax_notmuv3", " Track momentum vs #theta_{x}  ;P_{#pi} [GeV/c];#theta_{x}",200,0.,100.,200,-0.05,0.05));
  BookHisto(new TH2F("P_vs_thetay_notmuv3", " Track momentum vs #theta_{y}  ;P_{#pi} [GeV/c];#theta_{y}",200,0.,100.,200,-0.05,0.05));
  BookHisto(new TH2F("P_vs_theta_notmuv3", " Track momentum vs #theta  ;P_{#pi} [GeV/c];#theta",200,0.,100.,200,-0.05,0.05));
  BookHisto(new TH1F("shower_width_MUV1_notmuv3","Shower width MUV1; ShowerWidth_{MUV1} [mm]",500,0,500));
  BookHisto(new TH1F("shower_width_LKr_notmuv3","Shower width LKr; ShowerWidth_{LKr} [mm]",500,0,500));
  BookHisto(new TH1F("shower_width_MUV2_notmuv3","Shower width MUV2; ShowerWidth_{MUV2} [mm]",500,0,500));
  BookHisto(new TH1F("shower_width_average_notmuv3","Total shower width; ShowerWidth [mm]",400,0,400));
  BookHisto(new TH2F("P_vs_MM2_sw", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_MM2_strawhits", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  BookHisto(new TH1F("SeedRatio_MUV1_notmuv3",";E_{seed}/E_{clus}",110,0,1.1));
  BookHisto(new TH1F("SeedRatio_MUV2_notmuv3",";E_{seed}/E_{clus}",110,0,1.1));

  BookHisto(new TH2F("P_vs_sigmaMM2_notmuv3", " Track momentum vs sigma Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100,800,0,5));
  BookHisto(new TH2F("MM2_vs_sigmaMM2_notmuv3", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_sigmaMM2_below35GeV_notmuv3", " Track momentum vs sigma Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100,800,0,5));
  BookHisto(new TH2F("MM2_vs_sigmaMM2_below35GeV_notmuv3", " Track momentum vs Missing mass squared ;P[GeV];#sigma(MM2)",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_sigmaMM2_above35GeV_notmuv3", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  BookHisto(new TH2F("MM2_vs_sigmaMM2_above35GeV_notmuv3", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));


  BookHisto(new TH2F("P_vs_MM2_lkrmip", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("P_vs_MM2_muv12mip", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  BookHisto(new TH1F("Kmu2_trk_P", " Track momentum;P_{track} [MeV/c]",200,0.,100000));
  BookHisto(new TH2F("Kmu2_trk_atLKr","track @ LKr; x @ LKr [mm]; y @ LKr [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("Kmu2_trk_EoP_vs_P","; P[MeV] ;E_{LKr}/P",200, 0, 100000, 120, 0, 1.2 ));
  BookHisto(new TH2F("Kmu2_total_EoP_vs_P","; P[MeV] ;E_{LKr}/P",200, 0, 100000, 120, 0, 1.2 ));
  BookHisto(new TH2F("Kmu2_P_vs_Etot","; P[MeV] ;E_{cal}",200, 0, 100000,200, 0, 100000));
  BookHisto(new TH2F("Kmu2_Hcal_vs_Ecal",";E_{MUV1 + MUV2}[MeV];E_{LKr}[MeV]",200,0,100000,200,0,100000));
  BookHisto(new TH1F("Kmu2_Total_Energy",";E_{MUV1 + MUV2 + LKr}[MeV]",200,0,100000));

  BookHisto(new TH2F("Kmu2_fracMUV1_vs_fracMUV2","; E_{MUV1}/E_{cal} ; E_{MUV2}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("Kmu2_fracMUV1_vs_fracLKr","; E_{MUV1}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("Kmu2_fracMUV2_vs_fracLKr","; E_{MUV2}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));

  BookHisto(new TH2F("Kmu2_P_vs_MM2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  BookHisto(new TH1F("Kmu2_shower_width_MUV1","Shower width MUV1; ShowerWidth_{MUV1} [mm]",500,0,500));
  BookHisto(new TH1F("Kmu2_shower_width_LKr","Shower width LKr; ShowerWidth_{LKr} [mm]",500,0,500));
  BookHisto(new TH1F("Kmu2_shower_width_MUV2","Shower width MUV2; ShowerWidth_{MUV2} [mm]",500,0,500));
  BookHisto(new TH1F("Kmu2_shower_width_average","Average shower width; ShowerWidth [mm]",200,0,200));
  BookHisto(new TH1F("Kmu2_SeedRatio_MUV1",";E_{seed}/E_{clus}",110,0,1.1));
  BookHisto(new TH1F("Kmu2_SeedRatio_MUV2",";E_{seed}/E_{clus}",110,0,1.1));


  BookHisto(new TH2F("Kmu2_P_vs_sigmaMM2", " Track momentum vs sigma Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100,800,0,5));
  BookHisto(new TH2F("Kmu2_MM2_vs_sigmaMM2", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));

  BookHisto(new TH2F("Kmu2_P_vs_MM2_f", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH2F("Kmu2_MM2_vs_sigmaMM2_f", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("Kmu2_fracMUV1_vs_fracMUV2_f","; E_{MUV1}/E_{cal} ; E_{MUV2}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("Kmu2_fracMUV1_vs_fracLKr_f","; E_{MUV1}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("Kmu2_fracMUV2_vs_fracLKr_f","; E_{MUV2}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));

  BookHisto(new TH2F("Kmu2_P_vs_sigmaMM2_below35GeV", " Track momentum vs sigma Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100,800,0,5));
  BookHisto(new TH2F("Kmu2_MM2_vs_sigmaMM2_below35GeV", " Track momentum vs Missing mass squared ;P[GeV];#sigma(MM2)",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("Kmu2_theta_vs_sigmaMM2_below35GeV", " Sigma Missing mass squared vs Track theta  ;#theta_{track}[rad];#sigma(MM2)",200,-0.05,0.05,800,0,5));
  BookHisto(new TH2F("Kmu2_P_vs_sigmaMM2_above35GeV", " Track momentum vs sigma Missing mass squared ;P[GeV];#sigma(MM2)",200,0,100,800,0,5));
  BookHisto(new TH2F("Kmu2_MM2_vs_sigmaMM2_above35GeV", " Track momentum vs Missing mass squared  ;#sigma(MM2);(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",800,0,5, 900, -0.15, 0.15));
  BookHisto(new TH2F("Kmu2_theta_vs_sigmaMM2_above35GeV", " Sigma Missing mass squared vs Track theta  ;#theta_{track}[rad];#sigma(MM2)",200,-0.05,0.05,800,0,5));


}

void PinunuAna::DefineMCSimple(){

}

void PinunuAna::StartOfRunUser(){
}
void PinunuAna::StartOfJobUser(){
}

void PinunuAna::StartOfBurstUser(){
  fHeader = GetRawHeader();
  //fGigaTrackerAnalysis->StartBurst(2016,fHeader->GetRunID(),fHeader->GetBurstID());
}

void PinunuAna::Process(int iEvent){

  OutputState state;

  fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetOutput("GigaTrackerEvtReco.GigaTrackerEvent",state);
  //fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker");
  fLKrEvent = (TRecoLKrEvent*)GetEvent("LKr");
  fCHODEvent = (TRecoCHODEvent*)GetEvent("CHOD");
  fCHANTIEvent = (TRecoCHANTIEvent*)GetEvent("CHANTI");
  fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
  fRICHEvent = (TRecoRICHEvent*)GetEvent("RICH");
  fCedarEvent = (TRecoCedarEvent*)GetEvent("Cedar");
  fMUV1Event = (TRecoMUV1Event*)GetEvent("MUV1");
  fMUV2Event = (TRecoMUV2Event*)GetEvent("MUV2");
  fMUV3Event = (TRecoMUV3Event*)GetEvent("MUV3");
  fLAVEvent = (TRecoLAVEvent*)GetEvent("LAV");
  fSACEvent = (TRecoSACEvent*)GetEvent("SAC");
  fIRCEvent = (TRecoIRCEvent*)GetEvent("IRC");

  /*
    rado::StrawRapper straw(GetEvent("Spectrometer"));


   */



  fCedarCandidate = nullptr;
  //fGigaTrackerAnalysis->Clear();
  fL0Data = fHeader->GetL0TPData();
  //Getting the output provided by the TracksCheck Analyser :
  // Tobjarray with the number of good quality STRAW tracks
  //const TObjArray *GoodSTRAWTracks = GetOutput<TObjArray>("SpectrometerAnalyser.Tracks",state);
  //const TObjArray *GoodCHODTracks = GetOutput<TObjArray>("CHODAnalyzer.GoodCHODCandidates",state);
  const TObjArray *GoodTracks = GetOutput<TObjArray>("LKrAnalyzer.GoodLKrCandidates",state);
  FillHisto("NGood", GoodTracks->GetEntries());

  //if(GoodSTRAWTracks->GetEntries() == 0 ) return;
  //if(GoodCHODTracks->GetEntries() == 0 ) return;
  if(GoodTracks->GetEntries() == 0 ) return;
  //if(GoodTracks->GetEntries() == 0 ) return;
  //Checking number of candidates
  int NGTK_Cand = fGigaTrackerEvent->GetNCandidates();

  FillHisto("Cut0_GTK_NC", NGTK_Cand);

  int NLKr_Cand = fLKrEvent->GetNCandidates();

  FillHisto("Cut0_LKr_NC", NLKr_Cand);

  int NCHOD_Cand = fCHODEvent->GetNCandidates();
  //int NCHOD_GoodCand = GoodCHODTracks->GetEntries();

  FillHisto("Cut0_CHOD_NC", NCHOD_Cand);

  int NCedar_Cand = fCedarEvent->GetNCandidates();

  FillHisto("Cut0_Cedar_NC", NCedar_Cand);

  int NLAV_Cand = fLAVEvent->GetNCandidates();

  FillHisto("Cut0_LAV_NC", NLAV_Cand);

  int NRICH_Cand = fRICHEvent->GetNCandidates();

  FillHisto("Cut0_RICH_NC", NRICH_Cand);

  int NMUV1_Cand = fMUV1Event->GetNCandidates();

  FillHisto("Cut0_MUV1_NC", NMUV1_Cand);

  int NMUV2_Cand = fMUV2Event->GetNCandidates();

  FillHisto("Cut0_MUV2_NC", NMUV2_Cand);

  int NMUV3_Cand = fMUV3Event->GetNCandidates();

  FillHisto("Cut0_MUV3_NC", NMUV3_Cand);

  int NCHANTI_Cand = fCHANTIEvent->GetNCandidates();
  FillHisto("Cut0_CHANTI_NC", NCHANTI_Cand);

  //GoodTrack->Reset();

  for(int i = 0; i < GoodTracks->GetEntries(); i++){

    GoodTrack = (TrackCandidate*) GoodTracks->At(i);
    fGTKCandidate = nullptr;
    fSpectrometerCandidate = (TRecoSpectrometerCandidate*)GoodTrack->GetSpectrometer();

    double triggertime = fHeader->GetFineTime()*fFineTimeScale;
    Double_t Momentum = fSpectrometerCandidate->GetMomentum();
    TVector3 Momentum_withbtube_corr = GoodTrack->GetMomentumBeforeMagnet();

    FillHisto("CHOD_triggdt", triggertime - GoodTrack->GetCHODTime());

    if(fabs(triggertime - GoodTrack->GetCHODTime()) > 5) continue;

    fKgoodGTK = GoodTrack->IsGTKPresent();
    fKgoodCHANTI = GoodTrack->IsCHANTIPresent();
    fKgoodCedar = GoodTrack->IsCedarPresent();

    if(!fKgoodGTK ) continue;
    if(!fKgoodCHANTI ) continue;
    if(fKgoodCedar) fKgood = true;
    else fKgood = false;

    fGTKCandidate = (TRecoGigaTrackerCandidate*)GoodTrack->GetGTK();

    for (int s=0;s < fGigaTrackerEvent->GetNCandidates(); s++){
        TRecoGigaTrackerCandidate* pfff = (TRecoGigaTrackerCandidate*)fGigaTrackerEvent->GetCandidate(s);

        //cout << "Cand " << s << " wtime == " << pfff->GetTime() << endl;
    }

    TRecoGigaTrackerCandidate* gtkcandi = ((TRecoGigaTrackerCandidate*)fGigaTrackerEvent->GetCandidate(GoodTrack->GetGTKIndex()));

    //INFO("asdf"+std::to_string(23)+"sdfsdf");
    if( fGTKCandidate->GetTime() - GoodTrack->GetGTKTime()  != 0){

      stringstream a;
      a<<"Problem with candidates with a different = "<<fGTKCandidate->GetTime() - GoodTrack->GetGTKTime();
      WARNING(a.str());
      continue;
    }
    if( fGTKCandidate->GetTime() - GoodTrack->GetGTKTime()  != 0) continue;

    //coutk << __FILE__<< Form("%d",__LINE__) << ":  enter pinunuana == "<< endl;
    //cout << " index from trk = " << GoodTrack->GetGTKIndex() << endl;
    //cout << "gtk time --" << gtkcandi->GetTime() <<endl;
    //cout << "gtk time --" << fGTKCandidate->GetTime() << " saved time in candidate === " << GoodTrack->GetGTKTime() << endl;

    fKMomentum = GoodTrack->GetGTKMomentum();
    fVertex = GoodTrack->GetGTKVertex();
    fcda = GoodTrack->GetCDA();

    fK4Momentum.SetXYZM(fKMomentum.X()*fmevtogev,fKMomentum.Y()*fmevtogev,fKMomentum.Z()*fmevtogev,KMass);


    //Testing what will happen if we use y slopes between GTK 2-3
    TVector3 gtkpos1= fGTKCandidate->GetPosition(0);
    TVector3 gtkpos2= fGTKCandidate->GetPosition(1);
    TVector3 gtkpos3= fGTKCandidate->GetPosition(2);
    double dy23 = gtkpos2.Y() - gtkpos3.Y() ;



    double kthetax_23 = (gtkpos3.X() + 0.72*(1 - (fKMomentum.Mag()*fmevtogev)/75)- gtkpos2.X())/(gtkpos3.Z() - gtkpos2.Z()) + 0.09/ (fKMomentum.Mag()*fmevtogev);
    double kthetay_23 = dy23/ (gtkpos2.Z() - gtkpos3.Z());

    TVector3 proj = fTool->GetKaonPositionAtZ(fKMomentum,gtkpos3, gtkpos2.Z());


    Double_t ptrack = Momentum*0.001;// [GeV]
    Double_t thetax = fSpectrometerCandidate->GetSlopeXBeforeMagnet();
    Double_t thetay = fSpectrometerCandidate->GetSlopeYBeforeMagnet();
    Double_t kthetax= fKMomentum.X()/fKMomentum.Z();
    Double_t kthetay= fKMomentum.Y()/fKMomentum.Z();

    //std::cout << "-------------------" << std::endl;
    // std::cout << "dx     = "<< kthetax_23 - kthetax << std::endl;
    //std::cout << "kthx     = "<<  (gtkpos1.X() - gtkpos2.X()) /(gtkpos1.Z() - gtkpos2.Z()) << std::endl;

    //GTK trim5 magnet kick tests
    double kthetax_12 = (gtkpos2.X()- gtkpos1.X())/(gtkpos2.Z() - gtkpos1.Z());
    double x3proj = gtkpos2.X() - (gtkpos2.Z() - gtkpos1.Z())*kthetax_12;
    double x3bend  = x3proj + (zGTK3 - zTRIM5)*kthetax_12;
    double th1 = (x3proj - x3bend)/(zGTK3 - zTRIM5);
    double th2 = (gtkpos3.X() - x3bend)/(zGTK3 - zTRIM5);

    FillHisto("gtk_dy2",gtkpos2.Y() - proj.Y() );
    FillHisto("gtk_dx12",gtkpos1.X() - gtkpos2.X() );
    FillHisto("gtk_dxrproj",gtkpos3.X() - x3proj );
    FillHisto("gtk_dxrbend",gtkpos3.X() - x3bend );
    FillHisto("gtk_dxbendproj",x3proj - x3bend );
    FillHisto("gtk_kthx12", kthetax_12 );
    FillHisto("gtk_kth1", th1 );
    FillHisto("gtk_kth2", th2 );
    FillHisto("gtk_kthbend", th1+th2 );
    FillHisto("gtk_pk_vs_kthbend", fKMomentum.Mag()*fmevtogev,th1+th2 );

    FillHisto("gtk_pk_vs_kthx12", fKMomentum.Mag()*fmevtogev,kthetax_12 );
    FillHisto("gtk_pk_vs_dx12", fKMomentum.Mag()*fmevtogev,(gtkpos2.X() - gtkpos1.X()) );

    Double_t input[6]= {1./ptrack,thetax,thetay,GoodTrack->GetGTKMomentum().Mag()*fmevtogev,kthetax,kthetay};
    vector<Double_t> par;

    for (int i = 0; i < 6;i++)
      par.push_back(input[i]);



    MM2Errors mm2eror(par,fSpectrometerCandidate);
    par[4] = kthetax_23;


    MM2Errors mm2eror23(par,fSpectrometerCandidate);

    Double_t computedmm2 = mm2eror.GetMM2Pion();
    Double_t computedmm2kmu2 = mm2eror.GetMM2Muon();
    Double_t mm2_23 = mm2eror23.GetMM2Pion();

    //std::cout << "mm2eror = " << mm2eror.GetMM2PionError() << std::endl;

    if(fKgood){

      FillHisto("Cedar_good_dt", GoodTrack->GetCedarTime() - GoodTrack->GetGTKTime());
      FillHisto("trk_P_wcedar",Momentum);
      FillHisto("MM2_wcedar", computedmm2);
      FillHisto("P_vs_MM2_wcedar",Momentum*fmevtogev, computedmm2);

    } else {
      FillHisto("P_vs_MM2_wocedar",Momentum*fmevtogev, computedmm2);
    }


    if(!fKgoodCedar) continue;
    if(!GoodTrack->LKrPresent()) continue;

    Bool_t goodrich = RICHMatch();

    FillHisto("trk_EoP_all", GoodTrack->GetLKrEoP());
    FillHisto("trk_atLKr_all", GoodTrack->GetLKrMatchedX(),GoodTrack->GetLKrMatchedY());
    FillHisto("trk_EoP_vs_P_all", Momentum,GoodTrack->GetLKrEoP());

    if(!goodrich) continue;

    FillHisto("P_vs_MM2_cedar_rich",Momentum*fmevtogev, computedmm2);
    FillHisto("Vertex_Z_afterrich",fVertex.Z());
    FillHisto("Vertex_X_afterrich",fVertex.X());
    FillHisto("Vertex_Y_afterrich",fVertex.Y());

    fLAVMatch->SetReferenceTime(GoodTrack->GetCHODTime());
    fSAVMatch->SetReferenceTime(GoodTrack->GetCHODTime());

    Bool_t islav   = fLAVMatch->LAVHasTimeMatching(fLAVEvent);
    Int_t  issav   = fSAVMatch->SAVHasTimeMatching(fIRCEvent, fSACEvent);

    //CUTComment:: Cuts on Vertex and momentum
    //1. Closest Approached Distance > 15mm.
    //2. Zvtx to be incide of the fiducial volume of NA62 detector (110 - 160 m)
    //3. Momentum between 15 GeV/c and 65 GeV/c.
    if(Momentum < 15000 || Momentum > 65000) continue;
    if(fcda > 15.) continue;
    if( fVertex.Z() < FRzmin || fVertex.Z() > FRzmax) continue;
    if( fabs(fVertex.X()) < -20 || fabs(fVertex.X()) > 1420) continue;
    if( fabs(fVertex.Y()) > 20) continue;


    FillHisto("P_vs_MM2_vtxcut",Momentum*fmevtogev,computedmm2);

    //Just plot distributions
    PlotCHANTI();
    PlotGTK(i);

    if(NMUV1_Cand > 0 && !GoodTrack->IsMUV1Present()) continue;
    FillHisto("P_vs_MM2_muv1match",Momentum*fmevtogev,computedmm2);

    if(NMUV2_Cand > 0 && !GoodTrack->IsMUV2Present()) continue;
    FillHisto("P_vs_MM2_muv2match",Momentum*fmevtogev,computedmm2);

     Int_t nInTimeLKrCl =GoodTrack->GetNExtraInTimeCl() ;
     Int_t nInTimeLKrClAux =GoodTrack->GetNExtraInTimeClAux() ;
     FillHisto("NInTimeClusters", nInTimeLKrCl);
     FillHisto("NInTimeClustersAux", nInTimeLKrClAux);

    // FillHisto("NInTimeClusters", nInTimeLKrCl);

    double calEnergy= (GoodTrack->GetLKrEnergy()+ GoodTrack->GetMUV1Energy() + GoodTrack->GetMUV2Energy());
    double muv1frac = GoodTrack->GetMUV1Energy()/calEnergy;
    double muv2frac = GoodTrack->GetMUV2Energy()/calEnergy;
    double lkrfrac  = GoodTrack->GetLKrEnergy()/calEnergy;
    double calEoP = calEnergy/Momentum;

    FillHisto("total_P_vs_EoP", Momentum,calEoP);
    FillHisto("P_vs_Etot", Momentum,calEnergy);
    FillHisto("total_EoP_vs_MM2", calEoP,computedmm2);

    FillHisto("Total_Energy", calEnergy);
    FillHisto("Hcal_vs_Ecal", GoodTrack->GetMUV1Energy() + GoodTrack->GetMUV2Energy(), GoodTrack->GetLKrEnergy());

    FillHisto("fracMUV1_vs_fracMUV2",muv1frac,muv2frac);
    FillHisto("fracMUV1_vs_fracLKr",muv1frac,lkrfrac);
    FillHisto("fracMUV2_vs_fracLKr",muv2frac,lkrfrac);

    if(islav) continue;
    FillHisto("P_vs_MM2_nolav",Momentum*fmevtogev,computedmm2);

    if( issav !=0) continue;
    FillHisto("P_vs_MM2_nosav",Momentum*fmevtogev,computedmm2);

    Bool_t muv1mip=false;
    Bool_t muv2mip = false;
    Bool_t muv12mip=false;

    if(GoodTrack->IsMUV1Present() && GoodTrack->IsMUV2Present()){
      if(GoodTrack->GetMUV2Energy() < 3000. && GoodTrack->GetMUV1Energy() < 3000.)
        muv12mip=true;
    }

    if(GoodTrack->IsMUV1Present()){
      if(GoodTrack->GetMUV1Energy() < 3000.)
        muv1mip=true;
    }
    if(GoodTrack->IsMUV2Present()){
      if(GoodTrack->GetMUV2Energy() < 3000.)
        muv2mip=true;
    }
    if(GoodTrack->GetLKrEoP() > 0.8 ) continue;
    FillHisto("P_vs_MM2_nopositron",Momentum*fmevtogev,computedmm2);

    Double_t test_disc = (muv1frac/lkrfrac + muv2frac/lkrfrac);
    FillHisto("test_discriminant",test_disc);
    Double_t deltamm2=1e3*mm2eror.GetMM2PionError();
    fmm2=computedmm2;
    fmm2expectederror=deltamm2;


    Int_t eventtype[4]={0.};
    StrawHitAnalysis(eventtype);
    //std::cout << "type1 =" << eventtype[0] << "type2 =" << eventtype[1] << "type3 =" << eventtype[2] << "type3 =" << eventtype[3]<< std::endl;
    FillHisto("evttype1",eventtype[0]);
    FillHisto("evttype2",eventtype[1]);
    FillHisto("evttype3",eventtype[2]);
    FillHisto("evttype4",eventtype[3]);

    //K2pi plots
    if(nInTimeLKrCl==2 ){

      //Making the Pi0
      TRecoLKrCandidate* Photons[2];
      Double_t photonenergy[2];
      Double_t photonpos[2][2];
      Double_t Pi0energy=0;

      for (int i = 0; i<2; i++) {
        Photons[i] = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(GoodTrack->GetExtraInTimeIndex(i));
        photonenergy[i] = Photons[i]->GetClusterEnergy();
        photonpos[i][0] = Photons[i]->GetClusterX();
        photonpos[i][1] = Photons[i]->GetClusterY();
        Pi0energy+=photonenergy[i];
      }



      Double_t photon12dist = TMath::Sqrt( TMath::Power(photonpos[0][0]-photonpos[1][0],2) +TMath::Power(photonpos[0][1]-photonpos[1][1],2));

      TVector3 Pi0vtx;
      TVector3 GTK3Pos = fGTKCandidate->GetPosition(2);


      Pi0vtx.SetZ(zLKr - photon12dist*TMath::Sqrt(photonenergy[0]*photonenergy[1]*fmevtogev*fmevtogev)/(PiMass));
      Pi0vtx.SetX(GTK3Pos.X() + fKMomentum.X()/fKMomentum.Z()*(Pi0vtx.Z() - GTK3Pos.Z()));
      Pi0vtx.SetY(GTK3Pos.Y() + fKMomentum.Y()/fKMomentum.Z()*(Pi0vtx.Z() - GTK3Pos.Z()));


      TVector3 gamma_r[2];
      TLorentzVector gamma_p[2];


      for (Int_t ig=0;ig<2;ig++) {
        gamma_r[ig].SetXYZ(photonpos[ig][0],photonpos[ig][1],zLKr);
        gamma_r[ig] = gamma_r[ig] - Pi0vtx;

        gamma_p[ig].SetVect(gamma_r[ig]*(photonenergy[ig]*fmevtogev/gamma_r[ig].Mag()));
        gamma_p[ig].SetE(photonenergy[ig]*fmevtogev);
      }


      TLorentzVector Pi0P = gamma_p[0]+gamma_p[1];
      TLorentzVector PiPlusP = fK4Momentum - Pi0P;
      Double_t Pi0kmom =  Pi0energy + Momentum;


      FillHisto("K2pi_NInTimeClusters", nInTimeLKrCl);

      FillHisto("Photon1_Energy", photonenergy[0]);
      FillHisto("Photon2_Energy", photonenergy[1]);
      FillHisto("Photon12_distance", photon12dist);
      FillHisto("Pi0Mass",Pi0P.M());
      FillHisto("Pi0_Energy", Pi0energy);
      FillHisto("Pi0_Kmom",Pi0kmom);
      FillHisto("Pi0MM2_vs_zvtx",Pi0vtx.Z(),PiPlusP.M2());
      FillHisto("dzvtx", Pi0vtx.Z() - fVertex.Z());


      if(photon12dist > 200 && Pi0kmom < 79000 && Pi0kmom > 71000 && Pi0vtx.Z() < 165000 && Pi0vtx.Z() > 105000){


        Int_t goodpion = FindPionMatch(PiPlusP, Pi0vtx);
        FillHisto("K2pi_P_vs_MM2_photoncuts",Momentum*fmevtogev, computedmm2);
        FillHisto("K2pi_trk_atLKr", GoodTrack->GetLKrMatchedX(),GoodTrack->GetLKrMatchedY());
        FillHisto("K2pi_Total_Energy", calEnergy);
        FillHisto("K2pi_P_vs_Etot", Momentum,calEnergy);
        FillHisto("K2pi_Hcal_vs_Ecal", GoodTrack->GetMUV1Energy() + GoodTrack->GetMUV2Energy(), GoodTrack->GetLKrEnergy());
        FillHisto("K2pi_trk_EoP_vs_P", Momentum,GoodTrack->GetLKrEoP());
        FillHisto("K2pi_total_EoP_vs_P", Momentum,calEoP);


        if(goodpion == 0) continue;
        FillHisto("K2pi_fracMUV1_vs_fracMUV2",muv1frac,muv2frac);
        FillHisto("K2pi_fracMUV1_vs_fracLKr",muv1frac,lkrfrac);
        FillHisto("K2pi_fracMUV2_vs_fracLKr",muv2frac,lkrfrac);


        if(GoodTrack->IsMUV2Present() && GoodTrack->GetMUV2Energy() < 3000. && calEnergy > 5000){
          FillHisto("K2pi_muv2MIP_P_vs_MM2",Momentum*fmevtogev, computedmm2);
        }

        if(GoodTrack->IsMUV2Present() && GoodTrack->GetMUV2Energy() > 3000. && calEnergy > 5000){
          FillHisto("K2pi_nomuv2MIP_P_vs_MM2",Momentum*fmevtogev, computedmm2);
        }


        if(!GoodTrack->IsMUV3Present() && !muv12mip){
          if(muv2mip) continue;
          if(calEnergy < 7000.) continue;
          if(calEoP < 0.15 || calEoP > 1.0) continue;
          if(lkrfrac > 0.1 && lkrfrac < 0.4 && muv1frac > 0.2 && muv1frac < 0.7 && (lkrfrac+muv1frac) < 0.85 && fabs(lkrfrac-muv1frac) < 0.07) continue;
          if(lkrfrac > 0.1 && lkrfrac < 0.4 && muv2frac > 0.2 && muv2frac < 0.7 && (lkrfrac+muv2frac) < 0.85 && fabs(lkrfrac-muv2frac) < 0.07) continue;
          if(muv1frac > 0.2 && muv1frac < 0.75 && muv2frac > 0.2 && muv2frac < 0.75 && (muv1frac+muv2frac) < 0.95) continue;

          //Double_t deltamm2=1e3*mm2eror.GetMM2PionError();


          FillHisto("K2pi_Pk_vs_thetax",par[3],par[1]);
          FillHisto("K2pi_Pk_vs_thetay",par[3],par[2]);
          FillHisto("K2pi_Pk_vs_sigmaMM2",par[3],deltamm2);
          FillHisto("K2pi_phi_vs_MM2",TMath::ATan(Momentum_withbtube_corr.Y()/Momentum_withbtube_corr.X()) ,computedmm2);
          FillHisto("K2pi_kthetax_vs_sigmaMM2",par[4],deltamm2);
          FillHisto("K2pi_kthetay_vs_sigmaMM2",par[5],deltamm2);


          if(par[1] > 0) FillHisto(Form("K2pi_P_vs_sigmaMM2_thxgr0"), 1/par[0],deltamm2);
          if(par[1] <= 0) FillHisto(Form("K2pi_P_vs_sigmaMM2_thxless0"), 1/par[0],deltamm2);
          if(par[2] > 0) FillHisto(Form("K2pi_P_vs_sigmaMM2_thygr0"), 1/par[0],deltamm2);
          if(par[2] <= 0) FillHisto(Form("K2pi_P_vs_sigmaMM2_thyless0"), 1/par[0],deltamm2);


          //if( (par[5] > 1*1e-5 && par[5] < 4*1e-5) || ( par[5] < 0 && par[5] > -3*1e-5)) FillHisto(Form("K2pi_P_vs_sigmaMM2_strip1"), 1/par[0],deltamm2);;
          //if( (par[5] > 5*1e-5 && par[5] < 8*1e-5) || ( par[5] < -4*1e-5 && par[5] > -8*1e-5)) FillHisto(Form("K2pi_P_vs_sigmaMM2_strip2"), 1/par[0],deltamm2);;
          //if( (par[5] > 9*1e-5 && par[5] < 12*1e-5) || ( par[5] < -9*1e-5 && par[5] > -12*1e-5)) FillHisto(Form("K2pi_P_vs_sigmaMM2_strip3"), 1/par[0],deltamm2);;
          //if( (par[5] > 13*1e-5 && par[5] < 16*1e-5) || ( par[5] < -13*1e-5 && par[5] > -16*1e-5)) FillHisto(Form("K2pi_P_vs_sigmaMM2_strip4"), 1/par[0],deltamm2);;
          //if( (par[5] > 17*1e-5 && par[5] < 20*1e-5) || ( par[5] < -17*1e-5 && par[5] > -20*1e-5)) FillHisto(Form("K2pi_P_vs_sigmaMM2_strip5"), 1/par[0],deltamm2);;

          //for (int i=0;i<3;i++){
          //  for (int j=0;j<3;j++){
          //
          //    if(i<=j){
          //      //cout << " " << dmm2dx[i]*cov[i][j]*dmm2dx[j] ;
          //      //cout << " \t er " << 1e3*sqrt(2*dmm2dx[i]*cov[i][j]*dmm2dx[j]) ;
          //      //Divide by two if i=j
          //      if(i==j){
          //        FillHisto(Form("P_vs_sigmaMM2_%d_%d",i,j), 1/par[0],1e3*sqrt(dmm2dx[i]*cov[i][j]*dmm2dx[j]));
          //        FillHisto(Form("Thetax_vs_sigmaMM2_%d_%d",i,j), par[1],1e3*sqrt(fabs(dmm2dx[i]*cov[i][j]*dmm2dx[j])));
          //        FillHisto(Form("Thetay_vs_sigmaMM2_%d_%d",i,j), par[2],1e3*sqrt(fabs(dmm2dx[i]*cov[i][j]*dmm2dx[j])));
          //      } else {
          //        FillHisto(Form("P_vs_sigmaMM2_%d_%d",i,j), 1/par[0],1e3*sqrt(2*fabs(dmm2dx[i]*cov[i][j]*dmm2dx[j])));
          //        FillHisto(Form("Thetax_vs_sigmaMM2_%d_%d",i,j), par[1],1e3*sqrt(2*fabs(dmm2dx[i]*cov[i][j]*dmm2dx[j])));
          //        FillHisto(Form("Thetay_vs_sigmaMM2_%d_%d",i,j), par[2],1e3*sqrt(2*fabs(dmm2dx[i]*cov[i][j]*dmm2dx[j])));
          //      }
          //    }
          //  }
          //}
          FillHisto("K2pi_NLKrcand_notmuv3",NLKr_Cand);
          FillHisto("K2pi_fracMUV1_vs_fracMUV2_notmuv3",muv1frac,muv2frac);
          FillHisto("K2pi_fracMUV1_vs_fracLKr_notmuv3",muv1frac,lkrfrac);
          FillHisto("K2pi_fracMUV2_vs_fracLKr_notmuv3",muv2frac,lkrfrac);
          FillHisto("K2pi_P_vs_MM2_notmuv3",Momentum*fmevtogev, computedmm2);
          FillHisto("K2pi_MM2_vs_sigmaMM2",deltamm2,computedmm2);
          FillHisto("K2pi_P_vs_sigmaMM2",Momentum*fmevtogev,deltamm2);
          FillHisto("K2pi_P_vs_thetax",Momentum*fmevtogev,par[1]);
          FillHisto("K2pi_P_vs_thetay",Momentum*fmevtogev,par[2]);
          FillHisto("K2pi_P_vs_theta" ,Momentum*fmevtogev,TMath::Sqrt(par[1]*par[1] + par[2]*par[2]));
          FillHisto("K2pi_SeedRatio_MUV1",GoodTrack->GetMUV1SeedRatio());
          FillHisto("K2pi_SeedRatio_MUV2",GoodTrack->GetMUV2SeedRatio());
          FillHisto("K2pi_zvtx_notmuv3",fVertex.Z());
          if(GoodTrack->GetMUV2SeedRatio() > 0.95) continue;
          if(GoodTrack->GetMUV1SeedRatio() > 0.95 && GoodTrack->GetMUV2SeedRatio() < 0.015) continue;
          double sw = GoodTrack->GetLKrShowerWidth() + GoodTrack->GetMUV1ShowerWidth() + GoodTrack->GetMUV2ShowerWidth();
          FillHisto("K2pi_shower_width_MUV2",GoodTrack->GetMUV2ShowerWidth());
          FillHisto("K2pi_shower_width_MUV1",GoodTrack->GetMUV1ShowerWidth());
          FillHisto("K2pi_shower_width_LKr",GoodTrack->GetLKrShowerWidth());
          FillHisto("K2pi_shower_width_average",sw);
          if(sw < 11) continue;
          FillHisto("K2pi_zvtx_sw",fVertex.Z());
          FillHisto("K2pi_P_vs_MM2_sw",Momentum*fmevtogev, computedmm2);
          FillHisto("K2pi_dMM2_vs_MM2", computedmm2 - mm2_23, computedmm2);
          //if(fVertex.Z() < 110000.) continue;
          FillHisto("K2pi_P_vs_MM2_final",Momentum*fmevtogev, computedmm2);

          if(eventtype[0] == 2 || eventtype[1] == 2 || eventtype[2] == 2 || eventtype[3] == 2) continue;
          if(eventtype[0] == 3 || eventtype[1] == 3 || eventtype[2] == 3 || eventtype[3] == 3) continue;

          FillHisto("K2pi_P_vs_MM2_strawhits",Momentum*fmevtogev,computedmm2);



          //if(computedmm2 > 0.022 || computedmm2 < 0.014){

            //INFO("mm2 = "+std::to_string(computedmm2)+"  tchod = "+std::to_string(GoodTrack->GetCHODTime())+"  tktag = "+std::to_string(GoodTrack->GetGTKTime())+" tgtk ="+std::to_string(GoodTrack->GetGTKTime()));
          //}
          if(deltamm2 < 1.2)
            FillHisto("K2pi_dMM2_vs_MM2_smallsigma", computedmm2 - mm2_23, computedmm2);
          else
            FillHisto("K2pi_dMM2_vs_MM2_largesigma", computedmm2 - mm2_23, computedmm2);

          FillHisto("STRAW_Nhits_vs_sigmaMM2",fSpectrometerCandidate->GetNHits(),deltamm2);
          TVector3 Track_atSTRAW[4];


          if(fFilter)
            FilterAccept();

          if(Momentum < 25000){

            FillHisto("STRAW_Nhits_vs_sigmaMM2_plt20",fSpectrometerCandidate->GetNHits(),deltamm2);




            for(int i=0;i<4;i++){
              Track_atSTRAW[i] = fTool->GetPositionAtZ(fSpectrometerCandidate,zSTRAW_station[i]);
              if(deltamm2 < 1.31){

                FillHisto(Form("Nhitschamber%d_smallsigma",i+1),fSpectrometerCandidate->GetNTotalHitPerChamber(i));
                FillHisto(Form("Nviewschamber%d_smallsigma",i+1),fSpectrometerCandidate->GetNViewsPerChamber(i));

                FillHisto(Form("Track_position_station%d_smallsigma",i+1),Track_atSTRAW[i].X(),Track_atSTRAW[i].Y());

                if(i==0)

                  FillHisto("STRAW_Nhits_vs_sigmaMM2_smallsigma",fSpectrometerCandidate->GetNHits(),deltamm2);

              }
              else{

                FillHisto(Form("Nhitschamber%d_largesigma",i+1),fSpectrometerCandidate->GetNTotalHitPerChamber(i));
                FillHisto(Form("Nviewschamber%d_largesigma",i+1),fSpectrometerCandidate->GetNViewsPerChamber(i));
                FillHisto(Form("Track_position_station%d_largesigma",i+1),Track_atSTRAW[i].X(),Track_atSTRAW[i].Y());

                if(i==0)
                  FillHisto("STRAW_Nhits_vs_sigmaMM2_largesigma",fSpectrometerCandidate->GetNHits(),deltamm2);
              }

            }
          }
          if(Momentum <= 35000.){
            FillHisto("K2pi_MM2_vs_sigmaMM2_below35GeV",deltamm2,computedmm2);
            FillHisto("K2pi_P_vs_sigmaMM2_below35GeV",Momentum*fmevtogev, deltamm2);
            FillHisto("K2pi_theta_vs_sigmaMM2_below35GeV",TMath::Sqrt(par[1]*par[1] + par[2]*par[2]),deltamm2);
          } else {
            FillHisto("K2pi_MM2_vs_sigmaMM2_above35GeV",deltamm2,computedmm2);
            FillHisto("K2pi_P_vs_sigmaMM2_above35GeV",Momentum*fmevtogev, deltamm2);
            FillHisto("K2pi_theta_vs_sigmaMM2_above35GeV",TMath::Sqrt(par[1]*par[1] + par[2]*par[2]),deltamm2);
          }

        }
      }
    }

    if(nInTimeLKrCl!=0 ) continue;
    FillHisto("P_vs_thetax_nogamma",Momentum*fmevtogev,par[1]);
    if(nInTimeLKrCl==0 && nInTimeLKrClAux != 0) continue;
    FillHisto("P_vs_MM2_nogamma",Momentum*fmevtogev,computedmm2);
    FillHisto("P_vs_thetay_nogamma",Momentum*fmevtogev,par[2]);
    FillHisto("P_vs_theta_nogamma" ,Momentum*fmevtogev,TMath::Sqrt(par[1]*par[1] + par[2]*par[2]));
    double swpinunu = GoodTrack->GetLKrShowerWidth() + GoodTrack->GetMUV1ShowerWidth() + GoodTrack->GetMUV2ShowerWidth();



    if(!GoodTrack->IsMUV3Present() && !muv12mip){
      if(muv2mip) continue;
      if(calEnergy < 7000.) continue;
      if(calEoP < 0.15 || calEoP > 1.0) continue;
      if(lkrfrac > 0.1 && lkrfrac < 0.4 && muv1frac > 0.2 && muv1frac < 0.7 && (lkrfrac+muv1frac) < 0.85 && fabs(lkrfrac-muv1frac) < 0.07) continue;
      if(lkrfrac > 0.1 && lkrfrac < 0.4 && muv2frac > 0.2 && muv2frac < 0.7 && (lkrfrac+muv2frac) < 0.85 && fabs(lkrfrac-muv2frac) < 0.07) continue;
      if(muv1frac > 0.2 && muv1frac < 0.75 && muv2frac > 0.2 && muv2frac < 0.75 && (muv1frac+muv2frac) < 0.95) continue;

      FillHisto("fracMUV1_vs_fracMUV2_notmuv3",muv1frac,muv2frac);
      FillHisto("fracMUV1_vs_fracLKr_notmuv3",muv1frac,lkrfrac);
      FillHisto("fracMUV2_vs_fracLKr_notmuv3",muv2frac,lkrfrac);
      FillHisto("P_vs_MM2_notmuv3",Momentum*fmevtogev,computedmm2);
      FillHisto("P_vs_sigmaMM2_notmuv3",Momentum*fmevtogev, deltamm2);
      FillHisto("MM2_vs_sigmaMM2_notmuv3",deltamm2,computedmm2);
      FillHisto("SeedRatio_MUV1_notmuv3",GoodTrack->GetMUV1SeedRatio());
      FillHisto("SeedRatio_MUV2_notmuv3",GoodTrack->GetMUV2SeedRatio());
      if(GoodTrack->GetMUV2SeedRatio() > 0.95) continue;
      if(GoodTrack->GetMUV1SeedRatio() > 0.95 && GoodTrack->GetMUV2SeedRatio() < 0.015) continue;


      //momtrack = par[0];
      //thx  = par[1];
      //thy  = par[2];
      //momk = par[3];
      //kthx = par[4];
      //kthy = par[5];
      //mm2=computedmm2;
      //sigmamm2=deltamm2;
      //fTree->Fill();

      FillHisto("shower_width_MUV2_notmuv3",GoodTrack->GetMUV2ShowerWidth());
      FillHisto("shower_width_MUV1_notmuv3",GoodTrack->GetMUV1ShowerWidth());
      FillHisto("shower_width_LKr_notmuv3",GoodTrack->GetLKrShowerWidth());
      FillHisto("shower_width_average_notmuv3",swpinunu);
      if(swpinunu < 11) continue;
      FillHisto("P_vs_MM2_sw",Momentum*fmevtogev,computedmm2);

      if(eventtype[0] == 2 || eventtype[1] == 2 || eventtype[2] == 2 || eventtype[3] == 2) continue;
      if(eventtype[0] == 3 || eventtype[1] == 3 || eventtype[2] == 3 || eventtype[3] == 3) continue;

      FillHisto("P_vs_MM2_strawhits",Momentum*fmevtogev,computedmm2);

      if(computedmm2 < 0.024 && computedmm2 > 0.012){
        FillHisto("P_vs_thetax_notmuv3",Momentum*fmevtogev,par[1]);
        FillHisto("P_vs_thetay_notmuv3",Momentum*fmevtogev,par[2]);
        FillHisto("P_vs_theta_notmuv3" ,Momentum*fmevtogev,TMath::Sqrt(par[1]*par[1] + par[2]*par[2]));
      }

      if(Momentum <= 35000.){
        FillHisto("MM2_vs_sigmaMM2_below35GeV_notmuv3",deltamm2,computedmm2);
        FillHisto("P_vs_sigmaMM2_below35GeV_notmuv3",Momentum*fmevtogev, deltamm2);
      } else {
        FillHisto("MM2_vs_sigmaMM2_above35GeV_notmuv3",deltamm2,computedmm2);
        FillHisto("P_vs_sigmaMM2_above35GeV_notmuv3",Momentum*fmevtogev, deltamm2);
      }

    }
    /// Kmu2 selection
    if(GoodTrack->GetLKrEoP() > 0.1 || !GoodTrack->GetLKrISMIP() ) continue;
    //if(GoodTrack->GetLKrEoP() > 0.2) continue;
    FillHisto("P_vs_MM2_lkrmip",Momentum*fmevtogev,computedmm2);

    //if(!muv12mip) continue;
    FillHisto("P_vs_MM2_muv12mip",Momentum*fmevtogev,computedmm2);

    if(!GoodTrack->IsMUV3Present()) continue;

    FillHisto("Kmu2_trk_P",Momentum);
    FillHisto("Kmu2_trk_atLKr", GoodTrack->GetLKrMatchedX(),GoodTrack->GetLKrMatchedY());
    FillHisto("Kmu2_trk_EoP_vs_P", Momentum,GoodTrack->GetLKrEoP());
    FillHisto("Kmu2_total_EoP_vs_P", Momentum,calEoP);
    FillHisto("Kmu2_P_vs_Etot", Momentum,calEoP);
    FillHisto("Kmu2_Hcal_vs_Ecal", GoodTrack->GetMUV1Energy() + GoodTrack->GetMUV2Energy(), GoodTrack->GetLKrEnergy());
    FillHisto("Kmu2_Total_Energy", calEnergy);
    FillHisto("Kmu2_fracMUV1_vs_fracMUV2",muv1frac,muv2frac);
    FillHisto("Kmu2_fracMUV1_vs_fracLKr",muv1frac,lkrfrac);
    FillHisto("Kmu2_fracMUV2_vs_fracLKr",muv2frac,lkrfrac);

    if(calEnergy > 7000.) continue;
    if(lkrfrac < 0.1  || lkrfrac > 0.4) continue;
    if(muv1frac < 0.2 || muv1frac > 0.7) continue;
    if(muv2frac < 0.2 || muv2frac > 0.7) continue;
    if((lkrfrac+muv1frac) > 0.85) continue;
    if((lkrfrac+muv2frac) > 0.85) continue;
    if((muv1frac+muv2frac) > 0.95) continue;
    //if(fabs(lkrfrac-muv1frac)  > 0.07) continue;
    //if(fabs(lkrfrac-muv2frac)  > 0.07) continue;



    //TLorentzVector Muon;
    //TVector3 MuonBefore= Momentum_withbtube_corr;

    //Muon.SetXYZM(MuonBefore.X()*fmevtogev,MuonBefore.Y()*fmevtogev,MuonBefore.Z()*fmevtogev,MuMass);
    //Double_t deltamm2kmu2=1e3*mm2eror.GetMM2MuonError();

    FillHisto("Kmu2_MM2_vs_sigmaMM2",deltamm2,computedmm2);


    FillHisto("Kmu2_P_vs_MM2",Momentum*fmevtogev, computedmm2);
    FillHisto("Kmu2_P_vs_sigmaMM2",Momentum*fmevtogev,deltamm2);
    FillHisto("Kmu2_SeedRatio_MUV1",GoodTrack->GetMUV1SeedRatio());
    FillHisto("Kmu2_SeedRatio_MUV2",GoodTrack->GetMUV2SeedRatio());
    FillHisto("Kmu2_shower_width_MUV2",GoodTrack->GetMUV2ShowerWidth());
    FillHisto("Kmu2_shower_width_MUV1",GoodTrack->GetMUV1ShowerWidth());
    FillHisto("Kmu2_shower_width_LKr",GoodTrack->GetLKrShowerWidth());
    FillHisto("Kmu2_shower_width_average",GoodTrack->GetLKrShowerWidth() + GoodTrack->GetMUV1ShowerWidth() + GoodTrack->GetMUV2ShowerWidth());
    if(swpinunu > 11) continue;
    if(GoodTrack->GetMUV1SeedRatio() < 0.9) continue;
    if(GoodTrack->GetMUV2SeedRatio() < 0.9) continue;
    FillHisto("Kmu2_P_vs_MM2_f",Momentum*fmevtogev, computedmm2);
    FillHisto("Kmu2_MM2_vs_sigmaMM2_f",deltamm2,computedmm2);
    FillHisto("Kmu2_fracMUV1_vs_fracMUV2_f",muv1frac,muv2frac);
    FillHisto("Kmu2_fracMUV1_vs_fracLKr_f",muv1frac,lkrfrac);
    FillHisto("Kmu2_fracMUV2_vs_fracLKr_f",muv2frac,lkrfrac);

    if(Momentum <= 35000.){
      FillHisto("Kmu2_MM2_vs_sigmaMM2_below35GeV",deltamm2,computedmm2);
      FillHisto("Kmu2_P_vs_sigmaMM2_below35GeV",Momentum*fmevtogev, deltamm2);
      FillHisto("Kmu2_theta_vs_sigmaMM2_below35GeV",TMath::Sqrt(par[1]*par[1] + par[2]*par[2]),deltamm2);
    } else {
      FillHisto("Kmu2_MM2_vs_sigmaMM2_above35GeV",deltamm2,computedmm2);
      FillHisto("Kmu2_P_vs_sigmaMM2_above35GeV",Momentum*fmevtogev, deltamm2);
      FillHisto("Kmu2_theta_vs_sigmaMM2_above35GeV",TMath::Sqrt(par[1]*par[1] + par[2]*par[2]),deltamm2);
    }


    GoodTrack = nullptr;

  }
}




void PinunuAna::PostProcess(){



}

void PinunuAna::EndOfBurstUser(){
}

void PinunuAna::EndOfRunUser(){

}
void PinunuAna::EndOfJobUser(){
  SaveAllPlots();
}
void PinunuAna::DrawPlot(){
}

PinunuAna::~PinunuAna(){
}


Bool_t PinunuAna::PlotGTK(int i){

  if(!GoodTrack->IsGTKPresent()) return false;

  TRecoGigaTrackerCandidate* GTK = fGTKCandidate;//(TRecoGigaTrackerCandidate*) GoodTrack->GetGTK();

  double gtktime = GTK->GetTime();
  double gtkchi2X = GTK->GetChi2X();
  double gtkchi2Y = GTK->GetChi2Y();
  double gtkchi2T = GTK->GetChi2Time();
  double gtkchi2  = GTK->GetChi2();
  int gtknhits = GTK->GetNHits();
  double gtkchoddt=GoodTrack->GetCHODTime() - gtktime;
  double gtkcedardt=GoodTrack->GetCedarTime() - gtktime;
  TVector3 vertex;
  TVector3 gtkpos= GTK->GetPosition(2);
  TVector3 gtkmomentum= GTK->GetMomentum();
  TLorentzVector Pgtk(gtkmomentum, KMass);
  FillHisto("gtk_P", gtkmomentum.Mag());
  FillHisto("gtk_choddt",  gtkchoddt);
  if(fKgoodCedar)
    FillHisto("gtk_cedardt",  gtkcedardt);
  FillHisto("gtk_chi2", gtkchi2);
  FillHisto("gtk_chi2X", gtkchi2X);
  FillHisto("gtk_chi2Y", gtkchi2Y);
  FillHisto("gtk_chi2T", gtkchi2T);

  FillHisto("gtk_Nhits", gtknhits);
  FillHisto("gtk_pos", gtkpos.X(),gtkpos.Y());

  FillHisto("gtk_dt_vs_cda",gtkchoddt, fcda);
  if(fabs(gtkchoddt) > 4 || fabs(gtkcedardt) > 2 ){
      cout << __FILE__<< Form("%d",__LINE__) << ":  enter pinunuana plot gtk== "<< endl;
      std::cout << " i = "  << i << std::endl;
      cout << "time gtk = " << gtktime << "time cedar =" << GoodTrack->GetCedarTime()<< "time chod =" << GoodTrack->GetCHODTime() <<  "time gtkreal = " << GoodTrack->GetGTKTime()  << endl;
  }


  return true;
}


Bool_t PinunuAna::PlotCHANTI(){

  if(fCHANTIEvent->GetNCandidates()==0) return true;
  if(!GoodTrack->IsCHANTIPresent()) return true;


  TRecoCHANTICandidate* chanti = (TRecoCHANTICandidate*) GoodTrack->GetCHANTI();
  double chantitime = chanti->GetTime();
  double chantidt = GoodTrack->GetCedarTime() - chantitime;
  double chantigtkdt = GoodTrack->GetGTKTime() - chantitime;
  double chantichoddt = GoodTrack->GetCHODTime() - chantitime;

  FillHisto("CHANTI_choddt", chantichoddt);
  FillHisto("CHANTI_cedardt", chantidt);
  FillHisto("CHANTI_gtkdt", chantigtkdt);

  return true;

}



Bool_t PinunuAna::RICHMatch(){


  //RICH matching and acceptance
  TVector3 Track_atRICH;
  TVector3 Track_atRICH_fp  ;
  TVector3 Track_atRICH_mirr;
  TVector3 Track_atRICH_EntW;
  TVector3 Track_atRICH_ExW ;
  Track_atRICH     = fTool->GetPositionAtZ(fSpectrometerCandidate, zRICH);
  Track_atRICH_fp  = fTool->GetPositionAtZ(fSpectrometerCandidate, zRICHFP);
  Track_atRICH_mirr= fTool->GetPositionAtZ(fSpectrometerCandidate, zRICHMI);
  Track_atRICH_EntW= fTool->GetPositionAtZ(fSpectrometerCandidate, zRICHEntWindow);
  Track_atRICH_ExW = fTool->GetPositionAtZ(fSpectrometerCandidate, zRICHExtWindow);

  Int_t count = 0;
  Double_t dt = -999;


  // int bestrich = FindClosestCluster(fRICHEvent, Track_atRICH, "RICH", mindtrkcl);
  int bestrich =  FindClosestTimeCand( fRICHEvent,"RICH", dt);
  if(bestrich < 0) return false;

  TRecoRICHCandidate* RICH = (TRecoRICHCandidate*)fRICHEvent->GetRingCandidate(bestrich);

  TVector2 RingPos = RICH->GetRingCenter();
  double chi2 = RICH->GetRingChi2();
  double radius = RICH->GetRingRadius();
  double nhits = RICH->GetNHits();
  double RICHCandTime = RICH->GetTime();
  double RICHdt = RICHCandTime - GoodTrack->GetCHODTime();
  double NeonN       = 1.000067;
  double RICHAngle   = TMath::ATan( radius/17000. );
  double RICHMass    = fSpectrometerCandidate->GetMomentum()*sqrt( pow(NeonN*TMath::Cos(RICHAngle), 2 ) - 1.);
  double slopex      = RingPos.X()/17000;
  double slopey      = RingPos.Y()/17000.;

  FillHisto("RICHNHits",nhits);
  FillHisto("RICHChi2",chi2);
  FillHisto("RICHRadius",radius);
  FillHisto("RICHAngle",RICHAngle);
  FillHisto("RICHMass",RICHMass);
  FillHisto("RICHPvsM",fSpectrometerCandidate->GetMomentum() , RICHMass);
  FillHisto("RICHPvsR",fSpectrometerCandidate->GetMomentum() , radius);
  FillHisto("RICH_dslopex", slopex - fSpectrometerCandidate->GetSlopeXAfterMagnet());
  FillHisto("RICH_dslopey", slopey - fSpectrometerCandidate->GetSlopeYAfterMagnet());
  FillHisto("RICH_slopes", slopex, slopey);
  FillHisto("RICH_dslope", slopex - fSpectrometerCandidate->GetSlopeXAfterMagnet(), slopey- fSpectrometerCandidate->GetSlopeYAfterMagnet());
  FillHisto("RICH_pos_min_slope", RingPos.X() - fSpectrometerCandidate->GetSlopeXAfterMagnet()*17000, RingPos.Y() - fSpectrometerCandidate->GetSlopeYAfterMagnet()*17000.);
  FillHisto("trk_atRICH", Track_atRICH.X(),Track_atRICH.Y());
  FillHisto("trk_atRICH_FP", Track_atRICH_fp.X(),Track_atRICH_fp.Y());
  FillHisto("trk_atRICH_Mirr", Track_atRICH_mirr.X(),Track_atRICH_mirr.Y());
  FillHisto("trk_atRICH_EntW", Track_atRICH_EntW.X(),Track_atRICH_EntW.Y());
  FillHisto("trk_atRICH_ExW", Track_atRICH_ExW.X(),Track_atRICH_ExW.Y());
  FillHisto("RICH_dXdY", 300 - Track_atRICH_fp.X() - RingPos.X(), 300 - Track_atRICH_fp.Y() - RingPos.Y());


  //CUTComment:: Search square in the RICH with side 25cm (to be done momentum dependent)
  //if(TMath::Abs( Track_atRICH.X() - RICHPos.X() ) > 220 || TMath::Abs( Track_atRICH.Y() - RICHPos.Y() ) > 220) return false;
  FillHisto("RICH_dt", RICHdt);
  if(nhits < 4) return false;
  if(chi2 > 3) return false;
  if(TMath::Abs(RICHdt) < 3.) count++;
  //if( chi2 > 3.)
  //FillHisto("notRICH_atRICH", Track_atRICH_fp.X() ,  Track_atRICH_fp.Y());
  //count++;
  FillHisto("RICH_Nsmalldt", count);

  if(count  != 0 ){
    GoodTrack->SetRICH(RICH, bestrich);
    GoodTrack->SetRICHTime(RICHCandTime);

    return true;

  }
  return false;
}





int PinunuAna::FindClosestCluster( TRecoVEvent* Event, TVector3 Extrap_track, string detector_type, double& minimum){

  std::vector<double> dtrkcl;
  int position= -1;
  for (int iCand=0; iCand < Event->GetNCandidates(); iCand++){
    if(detector_type == "LKr"){
      TRecoLKrCandidate* Cluster = ((TRecoLKrCandidate*)Event->GetCandidate(iCand));
      double clusterx = Cluster->GetClusterX();
      double clustery = Cluster->GetClusterY();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }
    if(detector_type == "MUV1"){
      TRecoMUV1Candidate* Cluster = ((TRecoMUV1Candidate*)Event->GetCandidate(iCand));
      //Old Reco
      //double clusterx = Cluster->GetPosition().X()+ 60;
      //double clustery = Cluster->GetPosition().Y()+ 60;
      //New Reco
      double clusterx = Cluster->GetPosition().X();
      double clustery = Cluster->GetPosition().Y();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }
    if(detector_type == "MUV2"){
      TRecoMUV2Candidate* Cluster = ((TRecoMUV2Candidate*)Event->GetCandidate(iCand));
      double clusterx = Cluster->GetPosition().X();
      double clustery = Cluster->GetPosition().Y();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }
    if(detector_type == "MUV3"){
      TRecoMUV3Candidate* Cluster = ((TRecoMUV3Candidate*)Event->GetCandidate(iCand));
      double clusterx = Cluster->GetX();
      double clustery = Cluster->GetY();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }
    if(detector_type == "CHOD"){
      TRecoCHODCandidate* Cluster = ((TRecoCHODCandidate*)Event->GetCandidate(iCand));
      double clusterx = Cluster->GetHitPosition().X();
      double clustery = Cluster->GetHitPosition().Y();
      double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;
      dtrkcl.push_back(distance);
    }

  }
  if(dtrkcl.size() !=0){
    minimum= *min_element(dtrkcl.begin(), dtrkcl.end());
    position = distance(dtrkcl.begin(), min_element(dtrkcl.begin(), dtrkcl.end()));
    dtrkcl.clear();

  }
  return position;

}

int PinunuAna::FindPionMatch( TLorentzVector pion, TVector3 pi0vtx){

  TVector3 pion3mom = pion.Vect();
  TVector3 track_atCHOD;
  Double_t slopex = pion3mom.X()/pion3mom.Z();
  Double_t slopey = pion3mom.Y()/pion3mom.Z();
  //Double_t slopex = fSpectrometerCandidate->GetSlopeXAfterMagnet();
  //Double_t slopey = fSpectrometerCandidate->GetSlopeYAfterMagnet();
  track_atCHOD.SetX(pi0vtx.X() + slopex*(zCHOD - pi0vtx.Z()));
  track_atCHOD.SetY(pi0vtx.Y() + slopey*(zCHOD - pi0vtx.Z()));

  FillHisto("K2pi_piplus_expected_dXdY", track_atCHOD.X() - GoodTrack->GetCHODX(),  track_atCHOD.Y() - GoodTrack->GetCHODY());
  FillHisto("K2pi_piplus_expected_dslope", slopex - fSpectrometerCandidate->GetSlopeXBeforeMagnet(), slopey - fSpectrometerCandidate->GetSlopeYBeforeMagnet());
  FillHisto("K2pi_piplus_expected_dp", pion.P()*fgevtomev - fSpectrometerCandidate->GetMomentum());

  if(fabs(pion.P()*fgevtomev - fSpectrometerCandidate->GetMomentum()) > 3000) return 0;
  return 1;

}
int PinunuAna::FindClosestTimeCand( TRecoVEvent* Event, string detector_type, double& minimum){

  std::vector<double> dt;
  int position= -1;
  for (int iCand=0; iCand < Event->GetNCandidates(); iCand++){
    if(detector_type == "GTK"){
      TRecoGigaTrackerCandidate* Cluster = ((TRecoGigaTrackerCandidate*)Event->GetCandidate(iCand));
      double time = Cluster->GetTime();
      double dtchod = fabs(time - GoodTrack->GetCHODTime()) ;
      dt.push_back(dtchod);
    }

    if(detector_type == "Cedar"){
      TRecoCedarCandidate* Cluster = ((TRecoCedarCandidate*)Event->GetCandidate(iCand));
      double time = Cluster->GetTime();
      double dtchod = fabs(time - GoodTrack->GetGTKTime()) ;
      dt.push_back(dtchod);
    }

    if(detector_type == "CHANTI"){
      TRecoCHANTICandidate* Cluster = ((TRecoCHANTICandidate*)Event->GetCandidate(iCand));
      double time = Cluster->GetTime();
      double dtchod = fabs(time - GoodTrack->GetGTKTime()) ;
      dt.push_back(dtchod);
    }


  }
  if(detector_type == "RICH"){
    for (int iCand=0; iCand < fRICHEvent->GetNRingCandidates(); iCand++){
      //TRecoRICHEvent* richevent = (TRecoRICHEvent*)Event;
      TRecoRICHCandidate* Cluster = ((TRecoRICHCandidate*)fRICHEvent->GetRingCandidate(iCand));
      double time = Cluster->GetTime();
      double dtchod = fabs(time - GoodTrack->GetCHODTime()) ;
      dt.push_back(dtchod);
    }
  }

  if(dt.size() !=0){
    minimum= *min_element(dt.begin(), dt.end());
    position = distance(dt.begin(), min_element(dt.begin(), dt.end()));
    dt.clear();

  }
  return position;

}


//Double_t PinunuAna::GetYCoordinate(Double_t xpos1, Int_t viewid1, Double_t xpos2, Int_t viewid2) {
//  Double_t sq2 = sqrt(2.);
//  if (viewid1==0 && viewid2==1) return (-xpos1+xpos2)/sq2; // uv OK
//  if (viewid1==0 && viewid2==2) return -sq2*xpos1+xpos2;   // ux OK
//  if (viewid1==1 && viewid2==0) return (-xpos2+xpos1)/sq2; // vu OK
//  if (viewid1==1 && viewid2==2) return -xpos2+sq2*xpos1;   // vx OK
//  if (viewid1==2 && viewid2==0) return -sq2*xpos2+xpos1;   // xu OK
//  if (viewid1==2 && viewid2==1) return -xpos1+sq2*xpos2;   // xv OK
//  return 9999.;
//}
//
//Double_t PinunuAna::GetXCoordinate(Double_t xpos1, Int_t viewid1, Double_t xpos2, Int_t viewid2) {
//  Double_t sq2 = sqrt(2.);
//  if (viewid1==0 && viewid2==1) return (xpos1+xpos2)/sq2; // uv OK
//  if (viewid1==0 && viewid2==2) return xpos2;
//  if (viewid1==1 && viewid2==0) return (xpos2+xpos1)/sq2; // vu OK
//  if (viewid1==1 && viewid2==2) return xpos2;
//  if (viewid1==2 && viewid2==0) return xpos1;
//  if (viewid1==2 && viewid2==1) return xpos1;
//  return 9999.;
//}

Int_t PinunuAna::StrawHitAnalysis(Int_t evttype[]){

  TClonesArray& Hits = (*(fSpectrometerEvent->GetHits()));

  TVector3 trackathitpos[4][4];
  double candPos_view[4][4];
  double rhit[4][4];
  //double hind[4][4]={-1};
  double nhitsintimech[4]={0};
  double nhitsintime[4][4]={0.};
  double ptrack=fSpectrometerCandidate->GetMomentum()*fmevtogev;
  Int_t* ihitcan = fSpectrometerCandidate->GetHitsIndexes();
  Int_t nhinc=0 ;
  //if(fSpectrometerEvent->GetNHits() > 30) return 0;
  //  if(fSpectrometerCandidate->GetNHits() == 34 && fSpectrometerEvent->GetNHits()==46){

  // cout << " ================= Event start  ================= " << endl;
  // cout << " Neventhits  ================= " << fSpectrometerEvent->GetNHits() << endl;
  // cout << " Ncandhits  ================= " << fSpectrometerCandidate->GetNHits() << endl;
  //}

  //if(fSpectrometerCandidate->GetNHits() == 34 && fSpectrometerEvent->GetNHits()==46)
      for (int j=0; j< fSpectrometerEvent->GetNHits(); j++) {

    TRecoSpectrometerHit* hit = (TRecoSpectrometerHit*)Hits[j];

    double jhittime=hit->GetTime() + hit->GetTimeWidth();
    TVector3 jhitpos=hit->GetLocalPosition();
    //cout << "Entering cand loop for jHIT ================= " << j << endl;
    for(int q=0; q< fSpectrometerCandidate->GetNHits(); q++){

      if(j==ihitcan[q]){
        nhinc++;

        // std::cout << "jhit = " << j << std::endl;
        // std::cout << "ChamberID = " << hit->GetChamberID() << std::endl;
        // std::cout << "ViewID = " << hit->GetViewID() << std::endl;
        // std::cout << "StrawID = " << hit->GetStrawID() << std::endl;
        // std::cout << "PlaneID = " << hit->GetPlaneID() << std::endl;
        // std::cout << "ncandhits = " << nhinc << std::endl;
          //          cout << "hitindex cand = " << ihitcan[q]<< endl;
          //cout << "j = " << j<< endl;
        //}

      }
    }
    // cout << "Exiting cand loop for jHIT ================= " << j << endl;

    // cout << "Entering chamber loop for chid ================= " << hit->GetChamberID() << endl;
    for(int k=0; k<4;k++){
      if(hit->GetChamberID() != k) continue;


      // cout << "Entering chamber loop for ch and viewid ================= " << hit->GetChamberID() << "v " << hit->GetViewID()<< endl;
      for(int l=0; l<4;l++){
        if(hit->GetViewID() != l) continue;
        trackathitpos[k][l] = fTool->GetPositionAtZ(fSpectrometerCandidate,hit->GetLocalPosition().Z());
        if(l==0)
          candPos_view[k][0] = (trackathitpos[k][0].X() - trackathitpos[k][0].Y())/TMath::Sqrt(2);
        if(l==1)
          candPos_view[k][1] = (trackathitpos[k][1].X() + trackathitpos[k][1].Y())/TMath::Sqrt(2);
        if(l==2)
          candPos_view[k][2] = trackathitpos[k][2].X();
        if(l==3)
          candPos_view[k][3] = trackathitpos[k][3].Y();

        rhit[k][l]          = hit->GetLocalPosition().X();

        //cout << j << k << l << endl;
        // std::cout << "straw ------- ch" << k << "view" << l << std::endl;
        // std::cout << "jhit = " << j << std::endl;
        // std::cout << "jLocalpos X = " << hit->GetLocalPosition().X() << std::endl;
        // std::cout << "jLocalpos Y = " << hit->GetLocalPosition().Y() << std::endl;
        // std::cout << "jLocalpos Z = " << hit->GetLocalPosition().Z() << std::endl;
        // std::cout << "candPos_view" << k<<l<<" = "<< candPos_view[k][l] << std::endl;
        // std::cout << "ChamberID = " << hit->GetChamberID() << std::endl;
        // std::cout << "ViewID = " << hit->GetViewID() << std::endl;
        // std::cout << "StrawID = " << hit->GetStrawID() << std::endl;
        // std::cout << "PlaneID = " << hit->GetPlaneID() << std::endl;
        // std::cout << "HalfViewID = " << hit->GetHalfViewID() << std::endl;
        // std::cout << "Distance = " << fabs(candPos_view[k][l]-rhit[k][l]) << std::endl;
        // std::cout << "dt = " <<(jhittime - fSpectrometerCandidate->GetTime()) << std::endl;

        FillHisto(Form("Hitsdt_ch%d_view%d",k+1,l),jhittime - fSpectrometerCandidate->GetTime() - 150);
        FillHisto(Form("Hits_dist_ch%d_view%d",k+1,l),fabs(candPos_view[k][l]-rhit[k][l]));

        //Open a window in the view of +-300 mm  around the track position
        if(l==0 || l==1)
            if(fabs(candPos_view[k][l]-rhit[k][l]) > 300) continue;

        if(l==2 || l==3)
            if(fabs(candPos_view[k][l]-rhit[k][l]) > 300) continue;

        if(fabs(jhittime - fSpectrometerCandidate->GetTime()) > 250 ||
           fabs(jhittime - fSpectrometerCandidate->GetTime()) < 50
           ) continue;

        nhitsintime[k][l]++;
        fnhitintime[k][l]++;
        //std::cout << "Nhits = " << nhitsintime[k][l] << std::endl;

      }


      // nhitsintimech[k]+=nhitsintime[k][0]+nhitsintime[k][1]+nhitsintime[k][2]+nhitsintime[k][3];

    }
  }

  bool bad = false;
  //if(fSpectrometerCandidate->GetNHits() == 34 && fSpectrometerEvent->GetNHits()==46)
  for(int j=0;j <4;j++){
      if(nhitsintimech[j]  < fSpectrometerCandidate->GetNTotalHitPerChamber(j)) bad = true;
      //std::cout << "Chamber   " << j<< std::endl;

    for(int k=0;k <4;k++){
      nhitsintimech[j]+=nhitsintime[j][k];

      //std::cout << "Nhitsfor ch v " << j<< k<< " = " << nhitsintime[j][k] << std::endl;

    }
    //cout << "Nh new= " << nhitsintimech[j] << "  from cand = " << fSpectrometerCandidate->GetNTotalHitPerChamber(j) << endl;
  }

  //cout << "Nhits in common ======================= " << nhinc << endl;
  //cout << "Nhits from cand ======================= " << fSpectrometerCandidate->GetNHits() << endl;
  //cout << " ================= Event end  ================= " << endl;
  //if(bad){
  //    cout << "ALEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEERT CHECK PRINTOUTS ----------------" << endl;
  //    cout << "ALEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEERT CHECK PRINTOUTS ----------------" << endl;
  //    cout << "ALEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEERT CHECK PRINTOUTS ----------------" << endl;
  //    cout << "ALEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEERT CHECK PRINTOUTS ----------------" << endl;
  //}
  //TESTSSSSSSSSSSSSSSSSSSSSs

  //nhinc=0;
  //for(int q=0; q< fSpectrometerCandidate->GetNHits(); q++){
  //  for (int j=0; j< fSpectrometerEvent->GetNHits(); j++) {
  //
  //    //cout << "qcandHIT ================= " << q << endl;
  //
  //    if(j==ihitcan[q]){
  //      nhinc++;
  //
  //      cout << "j = " << j<< endl;
  //      cout << "hitindex cand = " << ihitcan[q]<< endl;
  //
  //      std::cout << "nhits in common = " << nhinc << std::endl;
  //
  //    }
  //  }
  //}


  //if(fSpectrometerCandidate->GetNHits() == 34 && fSpectrometerEvent->GetNHits()==46){


  //}

  double ndifftot=0;
  //Int_t evttype[4] = {0};

  //std::cout << "Event start -------------------------" << std::endl;

  for(int j=0;j <4;j++){

    // FillHisto(Form("ncloseHits_ch%d",j+1),nhitsintimech[j]);
    // FillHisto(Form("dnHits_ch%d",j+1),nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j) );

    FillHisto(Form("sigmamm2_vs_dnHits_ch%d",j+1),fmm2expectederror,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j) );
    FillHisto(Form("dnHits_vs_mm2_ch%d",j+1),nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j), fmm2);

    ndifftot+=nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j);

    if(fmm2expectederror<1.31){

      FillHisto(Form("P_vs_ncloseHits_ch%d_ss",j+1),ptrack,nhitsintimech[j]);
      FillHisto(Form("P_vs_dnHits_ch%d_ss",j+1),ptrack,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j) );

    } else {

      FillHisto(Form("P_vs_ncloseHits_ch%d_ls",j+1),ptrack,nhitsintimech[j]);
      FillHisto(Form("P_vs_dnHits_ch%d_ls",j+1),ptrack,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j) );

    }

    //std::cout << "Chamber --- " << j << std::endl;
    //std::cout << "Diff = " << nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j)<< std::endl;
    //std::cout << "v0 = " << nhitsintime[j][0] << "v1 = " << nhitsintime[j][1] << "v2 = " << nhitsintime[j][2] << "v3 = " << nhitsintime[j][3] << std::endl;

    //Type 1 events only clean tracks
    if(nhitsintime[j][0] ==  nhitsintime[j][1] &&
       nhitsintime[j][1] ==  nhitsintime[j][2] &&
       nhitsintime[j][2] ==  nhitsintime[j][3] ){

      FillHisto(Form("P_vs_dnHits_ch%d_type1",j+1),ptrack,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j) );
      FillHisto(Form("dnHits_vs_mm2_ch%d_type1",j+1),nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j), fmm2);
      FillHisto(Form("sigmamm2_vs_dnHits_ch%d_type1",j+1),fmm2expectederror,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j));

      evttype[j] = 1;
      //Type 2 events - dnHits is increasing in each view starting from the the first one
    } else if(nhitsintime[j][0] < nhitsintime[j][1] &&
       nhitsintime[j][1] < nhitsintime[j][2] &&
       nhitsintime[j][2] < nhitsintime[j][3]
       ){
      FillHisto(Form("P_vs_dnHits_ch%d_type2",j+1),ptrack,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j) );
      FillHisto(Form("dnHits_vs_mm2_ch%d_type2",j+1),nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j), fmm2);
      FillHisto(Form("sigmamm2_vs_dnHits_ch%d_type2",j+1),fmm2expectederror,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j));
      evttype[j] = 2;
      //Type 3 events - dnHits is increasing in each view starting from the the second one
    }else if(nhitsintime[j][0] == nhitsintime[j][1] &&
       nhitsintime[j][1] < nhitsintime[j][2] &&
       nhitsintime[j][2] < nhitsintime[j][3]
       ){
      FillHisto(Form("P_vs_dnHits_ch%d_type3",j+1),ptrack,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j) );
      FillHisto(Form("dnHits_vs_mm2_ch%d_type3",j+1),nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j), fmm2);
      FillHisto(Form("sigmamm2_vs_dnHits_ch%d_type3",j+1),fmm2expectederror,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j));
      evttype[j] = 3;
    //Type 4 events - dnHits is increasing in each view starting from the the third one
    } else if(nhitsintime[j][0] == nhitsintime[j][1] &&
       nhitsintime[j][1] == nhitsintime[j][2] &&
       nhitsintime[j][2] < nhitsintime[j][3]
       ){
      FillHisto(Form("P_vs_dnHits_ch%d_type4",j+1),ptrack,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j) );
      FillHisto(Form("dnHits_vs_mm2_ch%d_type4",j+1),nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j), fmm2);
      FillHisto(Form("sigmamm2_vs_dnHits_ch%d_type4",j+1),fmm2expectederror,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j));
      evttype[j] = 4;
    //Type 5 events - dnHits is increasing in each view starting from the the third one
    } else if(nhitsintime[j][0] > nhitsintime[j][1]  &&
       nhitsintime[j][1] == nhitsintime[j][2]  &&
       nhitsintime[j][2] == nhitsintime[j][3] ){
      FillHisto(Form("P_vs_dnHits_ch%d_type5",j+1),ptrack,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j) );
      FillHisto(Form("dnHits_vs_mm2_ch%d_type5",j+1),nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j), fmm2);
      FillHisto(Form("sigmamm2_vs_dnHits_ch%d_type5",j+1),fmm2expectederror,nhitsintimech[j] - fSpectrometerCandidate->GetNTotalHitPerChamber(j));
      evttype[j] = 5;
    }

    for(int k=0;k <4;k++){
      FillHisto(Form("ncloseHits_ch%d_view%d",j+1,k),nhitsintime[j][k]);
    }
  }



  FillHisto(Form("sigmamm2_vs_dnHits"),fmm2expectederror,ndifftot);
  FillHisto(Form("dnHits_vs_mm2"),ndifftot, fmm2);

  if(fmm2expectederror<1.31){
    FillHisto(Form("P_vs_dnHits_ss"),ptrack,ndifftot);
  } else {
    FillHisto(Form("P_vs_dnHits_ls"),ptrack,ndifftot );
  }


  return 1;
}

Int_t PinunuAna::WhichType(){

  Int_t eventtype=0;
  for (int i=0; i<4; i++) {
    if(fnhitintime[i][0] < fnhitintime[i][1]) eventtype++;
    if(fnhitintime[i][1] < fnhitintime[i][2]) eventtype++;
    if(fnhitintime[i][2] < fnhitintime[i][3]) eventtype++;
    if(i==0 || i==2)
      if(fnhitintime[i][3] < fnhitintime[i+1][0]) eventtype++;

  }

  return eventtype;
}
