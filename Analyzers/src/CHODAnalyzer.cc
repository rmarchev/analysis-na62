#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "CHODAnalyzer.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Geometry.hh"
#include "Persistency.hh"
#include "AnalysisTools.hh"
#include "TrackCandidate.hh"
#include "TrackSTRAWCandidate.hh"
#include "TrackCHODCandidate.hh"
#include "Parameters.hh"
#include "BlueTubeTracker.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

CHODAnalyzer::CHODAnalyzer(Core::BaseAnalysis *ba) : Analyzer(ba, "CHODAnalyzer")
{

  fAnalyzerName = "CHODAnalyzer";
  RequestTree("CHOD",new TRecoCHODEvent);
  RequestTree("Cedar",new TRecoCedarEvent);

  //RequestTree("GigaTracker",new TRecoGigaTrackerEvent);

  Parameters *par = Parameters::GetInstance();


  par->LoadParameters("/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/chod_par/chod_slab_parameters.dat");
  par->LoadParameters("/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/chod_par/chodallt0.dat");
  //par->LoadParameters("/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/chod_par/CHOD_T0.dat");
  par->LoadParameters("/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/chod_par/chodallslewslope.dat");
  par->LoadParameters("/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/chod_par/chodallslewconst.dat");
  par->StoreCHODParameters();
  fCHODPosV = (Double_t *)par->GetCHODPosV();
  fCHODPosH = (Double_t *)par->GetCHODPosH();
  fCHODAllT0 = (Double_t **)par->GetCHODAllT0();
  fCHODAllSlewSlope = (Double_t **)par->GetCHODAllSlewSlope();
  fCHODAllSlewConst = (Double_t **)par->GetCHODAllSlewConst();

  //Analysis tools
  fTools = AnalysisTools::GetInstance();
  fSTRAWCandidate = new TrackSTRAWCandidate();
  fCHODCandidate = new TrackCHODCandidate();
  fTracker = new BlueTubeTracker();
  GoodTrack = new TrackCandidate();
  fGigaTrackerAnalysis = new GigaTrackerAnalysis(ba);
}

void CHODAnalyzer::InitOutput(){

  RegisterOutput("GoodCHODCandidates",&fGoodCHODCandidates);

}

void CHODAnalyzer::InitHist(){

  BookHisto(new TH1I("NGood","N good STRAW tracks;NGoodtracks",50,0,50));
  BookHisto(new TH1I("CHOD_Candidates" , "Number of CHOD candidates;NCandidates" ,50,0,50));
  BookHisto(new TH1I("CHOD_Hits" , "Number of CHOD hits;NHits" ,500,0,500));


  BookHisto(new TH2F("dxdy_atCHOD","dx vs dy @ CHOD; dx @ CHOD [mm]; dy @ CHOD [mm]", 2200, -1100., 1100., 2200, -1100., 1100. ));


  BookHisto(new TH1I("Cedar_Candidates","Cedar Candidates ; CedarCand",10,0.,10.));
  BookHisto(new TH1I("Cedar_NSectors","Cedar Nsectors ; NSectors",10,0.,10.));
  BookHisto(new TH1F("Cedar_dt","T_{Cedar} - T_{CHOD} ; dT_{ Cedar}",800,-100.,100.));

  BookHisto(new TH1D("Track_p","track momentum ; P [MeV/c]",200,0.,100000.));
  BookHisto(new TH2F("Track_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 2200, -1100., 1100., 2200, -1100., 1100. ));
  BookHisto(new TH1F("dhittime","HitTime; t_{hit} [ns]", 800, -200, 200));
  BookHisto(new TH1F("hit_dist","Distance to hit;dist [mm]",400, 0, 2000));
  BookHisto(new TH1F("hit_chi2","CHOD Discriminant ;#chi^{2} ",200, 0, 50));
  BookHisto(new TH2F("hit_dtime_vs_dist","dt_{Hit} vs Distance to hit; dt_{hit} [ns];dist [mm]", 200, -100, 100, 400, 0, 2000));
  BookHisto(new TH2F("hit_chi2_vs_dtime","HitTime vs dt; #chi^{2} ;di2st [mm]", 200, 0, 50, 200, -100, 100));
  BookHisto(new TH2F("hit_chi2_vs_distance"," Discriminant vs Distance to hit; #chi^{2};dist [mm]", 200, 0, 50, 400, 0, 2000));

  BookHisto(new TH1F("CHODdt","HitTime; t_{hit} [ns]", 800, -200, 200));
  BookHisto(new TH1F("dist","Distance to hit;dist [mm]",400, 0, 2000));
  BookHisto(new TH1F("chi2","CHOD Discriminant ;#chi^{2} ",200, 0, 50));
  BookHisto(new TH2F("dtime_vs_dist","dt_{Hit} vs Distance to hit; dt_{hit} [ns];dist [mm]", 200, -100, 100, 100, 0, 2000));
  BookHisto(new TH2F("chi2_vs_dtime","HitTime vs dt; #chi^{2} ;dist [mm]", 200, 0, 50, 200, -100, 100));
  BookHisto(new TH2F("chi2_vs_dist"," Discriminant vs Distance to hit; #chi^{2};dist [mm]", 200, 0, 50, 400, 0, 2000));

  BookHisto(new TH1D("GoodTrack_p","track momentum ; P [MeV/c]",200,0.,100000.));
  BookHisto(new TH2F("GoodTrack_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 2200, -1100., 1100., 2200, -1100., 1100. ));
  BookHisto(new TH2F("GoodTrack_dxdy_atCHOD","dx vs dy @ CHOD; dx @ CHOD [mm]; dy @ CHOD [mm]", 2200, -1100., 1100., 2200, -1100., 1100. ));
  BookHisto(new TH1F("GoodTrack_CHODdt","HitTime; t_{hit} [ns]", 800, -200, 200));
  BookHisto(new TH1F("GoodTrack_dist","Distance to hit;dist [mm]",400, 0, 2000));
  BookHisto(new TH1F("GoodTrack_chi2","CHOD Discriminant ;#chi^{2} ",200, 0, 50));
  BookHisto(new TH2F("GoodTrack_dtime_vs_dist","dt_{Hit} vs Distance to hit; dt_{hit} [ns];dist [mm]", 200, -100, 100, 100, 0, 2000));
  BookHisto(new TH2F("GoodTrack_chi2_vs_dtime","HitTime vs dt; #chi^{2} ;dist [mm]", 200, 0, 50, 200, -100, 100));
  BookHisto(new TH2F("GoodTrack_chi2_vs_dist"," Discriminant vs Distance to hit; #chi^{2};dist [mm]", 200, 0, 50, 400, 0, 2000));


  BookHisto(new TH1F("testdt1","",800,-20,20));
  BookHisto(new TH1F("testdt2","",800,-20,20));
  BookHisto(new TH1F("testdt3","",800,-20,20));
  BookHisto(new TH2F("testdp1","",300,-30,30,300,-30,30));
  BookHisto(new TH2F("testdp2","",300,-30,30,300,-30,30));
  BookHisto(new TH2F("testdp3","",300,-30,30,300,-30,30));
  BookHisto(new TH2F("testdx1","",30,-30,30,300,-30,30));
  BookHisto(new TH2F("testdx2","",30,-30,30,300,-30,30));
  BookHisto(new TH2F("testdx3","",30,-30,30,300,-30,30));
  BookHisto(new TH2F("testdy1","",30,-30,30,300,-30,30));
  BookHisto(new TH2F("testdy2","",30,-30,30,300,-30,30));
  BookHisto(new TH2F("testdy3","",30,-30,30,300,-30,30));
  BookHisto(new TH2F("testxy1","",30,-30,30,30,-30,30));
  BookHisto(new TH2F("testxy2","",30,-30,30,30,-30,30));
  BookHisto(new TH2F("testxy3","",30,-30,30,30,-30,30));


  BookHisto(new TH1I("gtk_type" , ";GTK type" ,200,0,200));
  BookHisto(new TH1F("gtk_P", " GTK momentum;P_{K} [MeV/c]",100,65000.,85000));
  BookHisto(new TH1I("gtk_Nhits", "Number of GTK hits;Nhits" ,100,0,100));
  BookHisto(new TH1I("gtk_Ngood", "Number of good candidates;Ngood" ,10,0,10));
  BookHisto(new TH1F("gtk_choddt","T_{chod} - T_{gtk} ; dT_{chod - gtk}",400,-50.,50.));
  BookHisto(new TH1F("gtk_chi2","#chi^{2} ; #chi^{2}",300,0.,300.));
  BookHisto(new TH1F("gtk_chi2X","#chi^{2}X ; #chi^{2}",300,0.,300.));
  BookHisto(new TH1F("gtk_chi2Y","#chi^{2}Y ; #chi^{2}",300,0.,300.));
  BookHisto(new TH1F("gtk_chi2T","#chi^{2}T ; #chi^{2}",300,0.,300.));
  BookHisto(new TH2F("gtk_pos","GTK position; x @ GTK [mm]; y @ GTK [mm]", 100,-50,50,100,-50,50));
  BookHisto(new TH2F("gtk_dt_vs_cda", "  cda vs T_{chod} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -50, 50,300, 0, 300));
  BookHisto(new TH1F("gtk_discriminant","GTK Discriminant ;#chi^{2} ",200, 0, 1));

  BookHisto(new TH1F("gtk_dX_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dY_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dX_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dY_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dX_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_dY_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
  BookHisto(new TH1F("gtk_thetax", " ;#theta_{x GTK}  [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH1F("gtk_thetay", " ;#theta_{y GTK}  [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH2F("gtk_Nhits_vs_P", "Number of GTK hits vs P;Nhits;P" ,30,0,30,100,65000.,85000));
  BookHisto(new TH2F("goodvtx_trk_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH1F("goodvtx_MM2", " Missing mass squared ; (P_{K} - P_{#pi})^{2} [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto(new TH1F("goodvtx_Vertex_Z", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("goodvtx_zvtx_vs_P", " Closest distance approached between the kaon and the track ; zvtx [mm];P_{track} [MeV/c]", 300, 0, 300000,200,0.,100000));
  BookHisto(new TH2F("goodvtx_P_vs_MM2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));


  BookHisto(new TH1F("CHANTI_cedardt","T_{Cedar} - T_{CHANTI} ; dT_{Cedar - CHANTI}",400,-50.,50.));
  BookHisto(new TH1F("CHANTI_choddt","T_{chod} - T_{CHANTI} ; dT_{chod - CHANTI}",400,-50.,50.));
  BookHisto(new TH1F("CHANTI_gtkdt","T_{gtk} - T_{CHANTI} ; dT_{ gtk - CHANTI}",800,-100.,100.));
  BookHisto(new TH2F("goodvtx_wchanti_trk_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH1F("goodvtx_wchanti_MM2", " Missing mass squared ; (P_{K} - P_{#pi})^{2} [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto(new TH1F("goodvtx_wchanti_Vertex_Z", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("goodvtx_wchanti_zvtx_vs_P", " Closest distance approached between the kaon and the track ; zvtx [mm];P_{track} [MeV/c]", 300, 0, 300000,200,0.,100000));
  BookHisto(new TH2F("goodvtx_wchanti_P_vs_MM2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  BookHisto(new TH1F("Cedar_gtkdt","T_{Cedar} - T_{gtk} ; dT_{ cedar - gtk}",800,-100.,100.));
  BookHisto(new TH1F("Cedar_choddt","T_{Cedar} - T_{chod} ; dT_{ cedar - chod}",800,-100.,100.));

  BookHisto(new TH2F("goodvtx_wchanti_wcedar_trk_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH1F("goodvtx_wchanti_wcedar_MM2", " Missing mass squared ; (P_{K} - P_{#pi})^{2} [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto(new TH1F("goodvtx_wchanti_wcedar_Vertex_Z", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("goodvtx_wchanti_wcedar_zvtx_vs_P", " Closest distance approached between the kaon and the track ; zvtx [mm];P_{track} [MeV/c]", 300, 0, 300000,200,0.,100000));
  BookHisto(new TH2F("goodvtx_wchanti_wcedar_P_vs_MM2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));


  BookHisto(new TH2F("goodvtx_wchanti_wocedar_trk_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH1F("goodvtx_wchanti_wocedar_MM2", " Missing mass squared ; (P_{K} - P_{#pi})^{2} [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto(new TH1F("goodvtx_wchanti_wocedar_Vertex_Z", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("goodvtx_wchanti_wocedar_zvtx_vs_P", " Closest distance approached between the kaon and the track ; zvtx [mm];P_{track} [MeV/c]", 300, 0, 300000,200,0.,100000));
  BookHisto(new TH2F("goodvtx_wchanti_wocedar_P_vs_MM2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  BookHisto(new TH2F("goodvtx_wochanti_trk_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH1F("goodvtx_wochanti_MM2", " Missing mass squared ; (P_{K} - P_{#pi})^{2} [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto(new TH1F("goodvtx_wochanti_Vertex_Z", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("goodvtx_wochanti_zvtx_vs_P", " Closest distance approached between the kaon and the track ; zvtx [mm];P_{track} [MeV/c]", 300, 0, 300000,200,0.,100000));
  BookHisto(new TH2F("goodvtx_wochanti_P_vs_MM2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));


  BookHisto(new TH2F("badvtx_trk_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH1F("badvtx_MM2", " Missing mass squared ; (P_{K} - P_{#pi})^{2} [GeV^{2}/c^{4}]", 300, -0.15, 0.15));
  BookHisto(new TH1F("badvtx_Vertex_Z", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("badvtx_zvtx_vs_P", " Closest distance approached between the kaon and the track ; zvtx [mm];P_{track} [MeV/c]", 300, 0, 300000,200,0.,100000));
  BookHisto(new TH2F("badvtx_P_vs_MM2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  BookHisto(new TH1D("BadTrack_p","track momentum ; P [MeV/c]",200,0.,100000.));
  BookHisto(new TH2F("BadTrack_atCHOD","track @ CHOD; x @ CHOD [mm]; y @ CHOD [mm]", 2200, -1100., 1100., 2200, -1100., 1100. ));
  BookHisto(new TH2F("BadTrack_dxdy_atCHOD","dx vs dy @ CHOD; dx @ CHOD [mm]; dy @ CHOD [mm]", 2200, -1100., 1100., 2200, -1100., 1100. ));
  BookHisto(new TH1F("BadTrack_CHODdt","HitTime; t_{hit} [ns]", 800, -200, 200));
  BookHisto(new TH1F("BadTrack_dist","Distance to hit;dist [mm]",400, 0, 2000));
  BookHisto(new TH1F("BadTrack_chi2","CHOD Discriminant ;#chi^{2} ",200, 0, 50));
  BookHisto(new TH2F("BadTrack_dtime_vs_dist","dt_{Hit} vs Distance to hit; dt_{hit} [ns];dist [mm]", 200, -100, 100, 100, 0, 2000));
  BookHisto(new TH2F("BadTrack_chi2_vs_dtime","HitTime vs dt; #chi^{2} ;dist [mm]", 200, 0, 50, 200, -100, 100));
  BookHisto(new TH2F("BadTrack_chi2_vs_dist"," Discriminant vs Distance to hit; #chi^{2};dist [mm]", 200, 0, 50, 400, 0, 2000));


  BookHisto(new TH1I("Ngood","Ngood", 4, 0,4));


}

void CHODAnalyzer::DefineMCSimple(){

}


void CHODAnalyzer::StartOfJobUser(){

}

void CHODAnalyzer::StartOfRunUser(){

}

void CHODAnalyzer::StartOfBurstUser(){
  fHeader = GetRawHeader();
  fGigaTrackerAnalysis->StartBurst(2016,fHeader->GetRunID(),fHeader->GetBurstID());
}

void CHODAnalyzer::Process(int iEvent){


  //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
  //if(fMCSimple.fStatus == MCSimple::kEmpty){printNoMCWarning();return;}
  fGoodCHODCandidates.Clear();
  OutputState state;

  //fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker");
  fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetOutput("GigaTrackerEvtReco.GigaTrackerEvent",state);
  fCHODEvent  = (TRecoCHODEvent*)GetEvent("CHOD");
  fCHANTIEvent  = (TRecoCHANTIEvent*)GetEvent("CHANTI");
  fCedarEvent  = (TRecoCedarEvent*)GetEvent("Cedar");

  fGigaTrackerAnalysis->Clear();

  //Getting the output provided by the TracksCheck Analyser :
  // Tobjarray with the number of good quality STRAW tracks
  const TObjArray *GoodCandidates = GetOutput<TObjArray>("SpectrometerAnalyser.TrackCandidates",state);
  //if(fGigaTrackerEvent->GetNCandidates() == 0) return;

  int NGood = GoodCandidates->GetEntries();
  FillHisto("NGood", NGood);

  if(NGood == 0 ) return;

  int NCHOD_Cand = fCHODEvent->GetNCandidates();
  int NCHOD_Hits = fCHODEvent->GetNHits();
  int NCedar_Cand = fCedarEvent->GetNCandidates();
  FillHisto("CHOD_Candidates", NCHOD_Cand);
  FillHisto("Cedar_Candidates", NCedar_Cand);
  FillHisto("CHOD_Hits", NCHOD_Hits);

  vector<int> TrackIndex;
  vector<bool> good;

  for(int icedar = 0; icedar < fCedarEvent->GetNCandidates();icedar++){

    TRecoCedarCandidate* cedarcand = (TRecoCedarCandidate*)fCedarEvent->GetCandidate(icedar);

    FillHisto("Cedar_NSectors", cedarcand->GetNSectors());

  }


  GoodTrack->Reset();

  for(int i = 0; i < NGood; i++){

    GoodTrack = (TrackCandidate*) GoodCandidates->At(i);
    fSTRAWCandidate = GoodTrack->GetSTRAW();




    TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*)fSTRAWCandidate->GetSpectrometer();
    //TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*)GoodTrack->GetSpectrometer();

    fSpectrometerCandidate = track;
    //MatchCHOD(GoodTrack, track);

    TVector3 Track_atCHOD = AnalysisTools::GetInstance()->GetPositionAtZ(track,zCHOD);
    int goodchodmatch = MatchCHODCandidate(track->GetTime(),Track_atCHOD);

    if(goodchodmatch < 0) continue;

    //if(!cedar){ continue;}
    TVector3 VPos_atCHOD = fTools->GetPositionAtZ(track,238960 );
    TVector3 HPos_atCHOD = fTools->GetPositionAtZ(track,239340 );
    TVector3 Pos_atCHOD = (VPos_atCHOD+HPos_atCHOD)*0.5;
    FillHisto("Track_p", track->GetMomentum());
    FillHisto("Track_atCHOD", Pos_atCHOD.X(),Pos_atCHOD.Y());
    FillHisto("chi2", fCHODCandidate->GetDiscriminant());
    FillHisto("dist", fCHODCandidate->GetDistanceToHit());
    FillHisto("CHODdt", fCHODCandidate->GetCHODdt());
    FillHisto("dtime_vs_dist",fCHODCandidate->GetCHODdt(), fCHODCandidate->GetDistanceToHit());
    FillHisto("chi2_vs_dist",fCHODCandidate->GetDiscriminant(), fCHODCandidate->GetDistanceToHit());
    FillHisto("chi2_vs_dtime",fCHODCandidate->GetDiscriminant(), fCHODCandidate->GetCHODdt());
    FillHisto("dxdy_atCHOD",Pos_atCHOD.X() - fCHODCandidate->GetX(), Pos_atCHOD.Y() - fCHODCandidate->GetY());

    if( fCHODCandidate->GetDiscriminant() < 15){

      FillHisto("GoodTrack_chi2", fCHODCandidate->GetDiscriminant());
      FillHisto("GoodTrack_dist", fCHODCandidate->GetDistanceToHit());
      FillHisto("GoodTrack_CHODdt", fCHODCandidate->GetCHODdt());
      FillHisto("GoodTrack_dtime_vs_dist",fCHODCandidate->GetCHODdt(), fCHODCandidate->GetDistanceToHit());
      FillHisto("GoodTrack_chi2_vs_dist",fCHODCandidate->GetDiscriminant(), fCHODCandidate->GetDistanceToHit());
      FillHisto("GoodTrack_chi2_vs_dtime",fCHODCandidate->GetDiscriminant(), fCHODCandidate->GetCHODdt());
      FillHisto("GoodTrack_dxdy_atCHOD",Pos_atCHOD.X() - fCHODCandidate->GetX(), Pos_atCHOD.Y() - fCHODCandidate->GetY());

      FillHisto("GoodTrack_p", track->GetMomentum());
      FillHisto("GoodTrack_atCHOD", Pos_atCHOD.X(),Pos_atCHOD.Y());
      FillHisto("Ngood",(int)true);

      GoodTrack->SetCHOD(fCHODCandidate);


      //cout <<" wowwww = " <<nGoodTrack  << endl;
      Bool_t goodvtx = IsGoodGTKVtx();
      TLorentzVector kaonmomentum;
      kaonmomentum.SetXYZM(fKMomentum.X()*fmevtogev,fKMomentum.Y()*fmevtogev,fKMomentum.Z()*fmevtogev,KMass);

      TLorentzVector Pion;
      TVector3 PionBefore= fSpectrometerCandidate->GetThreeMomentumBeforeMagnet();

      Pion.SetXYZM(PionBefore.X()*fmevtogev,PionBefore.Y()*fmevtogev,PionBefore.Z()*fmevtogev,PiMass);

      TLorentzVector PNu;
      PNu = kaonmomentum - Pion;

      if(goodvtx){

        FillHisto("goodvtx_Vertex_Z",fVertex.Z());
        FillHisto("goodvtx_zvtx_vs_P",fVertex.Z(), track->GetMomentum());
        FillHisto("goodvtx_MM2", PNu.M2());
        FillHisto("goodvtx_P_vs_MM2",track->GetMomentum()*fmevtogev, PNu.M2());
        FillHisto("goodvtx_trk_atCHOD", Pos_atCHOD.X(),Pos_atCHOD.Y());

        Bool_t gtkcedarmathed = IsGoodCedar();
        Bool_t gtkchantimathed = NoCHANTI();

        if(gtkchantimathed){

          FillHisto("goodvtx_wchanti_Vertex_Z",fVertex.Z());
          FillHisto("goodvtx_wchanti_zvtx_vs_P",fVertex.Z(), track->GetMomentum());
          FillHisto("goodvtx_wchanti_MM2", PNu.M2());
          FillHisto("goodvtx_wchanti_P_vs_MM2",track->GetMomentum()*fmevtogev, PNu.M2());
          FillHisto("goodvtx_wchanti_trk_atCHOD", Pos_atCHOD.X(),Pos_atCHOD.Y());

          //Add the track only if no CHANTI candidate in time with GTK

          //cout << "Start --------" << endl;
          //cout << goodvtx << gtkchantimathed << gtkcedarmathed << endl;
          //cout << GoodTrack->IsGTKPresent() << GoodTrack->IsCHANTIPresent() << GoodTrack->IsCedarPresent();
          fGoodCHODCandidates.Add(GoodTrack);


          if(gtkcedarmathed) {

            FillHisto("goodvtx_wchanti_wcedar_Vertex_Z",fVertex.Z());
            FillHisto("goodvtx_wchanti_wcedar_zvtx_vs_P",fVertex.Z(), track->GetMomentum());
            FillHisto("goodvtx_wchanti_wcedar_MM2", PNu.M2());
            FillHisto("goodvtx_wchanti_wcedar_P_vs_MM2",track->GetMomentum()*fmevtogev, PNu.M2());
            FillHisto("goodvtx_wchanti_wcedar_trk_atCHOD", Pos_atCHOD.X(),Pos_atCHOD.Y());

          } else {

            FillHisto("goodvtx_wchanti_wocedar_Vertex_Z",fVertex.Z());
            FillHisto("goodvtx_wchanti_wocedar_zvtx_vs_P",fVertex.Z(), track->GetMomentum());
            FillHisto("goodvtx_wchanti_wocedar_MM2", PNu.M2());
            FillHisto("goodvtx_wchanti_wocedar_P_vs_MM2",track->GetMomentum()*fmevtogev, PNu.M2());
            FillHisto("goodvtx_wchanti_wocedar_trk_atCHOD", Pos_atCHOD.X(),Pos_atCHOD.Y());

          }


        } else {

          FillHisto("goodvtx_wochanti_Vertex_Z",fVertex.Z());
          FillHisto("goodvtx_wochanti_zvtx_vs_P",fVertex.Z(), track->GetMomentum());
          FillHisto("goodvtx_wochanti_MM2", PNu.M2());
          FillHisto("goodvtx_wochanti_P_vs_MM2",track->GetMomentum()*fmevtogev, PNu.M2());
          FillHisto("goodvtx_wochanti_trk_atCHOD", Pos_atCHOD.X(),Pos_atCHOD.Y());
        }

      } else {

        FillHisto("badvtx_Vertex_Z",fVertex.Z());
        FillHisto("badvtx_zvtx_vs_P",fVertex.Z(), track->GetMomentum());
        FillHisto("badvtx_MM2", PNu.M2());
        FillHisto("badvtx_P_vs_MM2",track->GetMomentum()*fmevtogev, PNu.M2());
        FillHisto("badvtx_trk_atCHOD", Pos_atCHOD.X(),Pos_atCHOD.Y());

      }


    } else {

      FillHisto("BadTrack_p", track->GetMomentum());
      FillHisto("BadTrack_atCHOD", Pos_atCHOD.X(),Pos_atCHOD.Y());

      FillHisto("BadTrack_chi2", fCHODCandidate->GetDiscriminant());
      FillHisto("BadTrack_dist", fCHODCandidate->GetDistanceToHit());
      FillHisto("BadTrack_CHODdt", fCHODCandidate->GetCHODdt());
      FillHisto("BadTrack_dtime_vs_dist",fCHODCandidate->GetCHODTime(), fCHODCandidate->GetDistanceToHit());
      FillHisto("BadTrack_chi2_vs_dist",fCHODCandidate->GetDiscriminant(), fCHODCandidate->GetDistanceToHit());
      FillHisto("BadTrack_chi2_vs_dtime",fCHODCandidate->GetDiscriminant(), fCHODCandidate->GetCHODdt());
      FillHisto("BadTrack_dxdy_atCHOD",Pos_atCHOD.X() - fCHODCandidate->GetX(), Pos_atCHOD.Y() - fCHODCandidate->GetY());

      FillHisto("Ngood",(int)false);
    }

  }

}

void CHODAnalyzer::PostProcess(){
  //fCHODCandidate->Reset();
  //fSTRAWCandidate->Reset();
}

void CHODAnalyzer::EndOfBurstUser(){

}

void CHODAnalyzer::EndOfRunUser(){

}

void CHODAnalyzer::EndOfJobUser(){
  SaveAllPlots();
  fGigaTrackerAnalysis->GetUserMethods()->SaveAllPlots();
}
void CHODAnalyzer::DrawPlot(){
}

//void CHODAnalyzer::MakeNominalKaonVertex(TRecoSpectrometerCandidate* track){
//
//
//  //Beam position vector at trim5 and beam momentum
//  //(at the moment no GTK assuming 75 GeV Definitions.h)
//  Double_t tethax =  0.0012;
//  Double_t tethay =  0.;
//  Double_t pkaon = 74.95;
//  Double_t pkaonz = pkaon/sqrt(1.+tethax*tethax+tethay*tethay);
//  Double_t pkaonx = pkaonz*tethax;
//  Double_t pkaony = pkaonz*tethay;
//  TLorentzVector kaonmomentum;
//  kaonmomentum.SetXYZM(pkaonx,pkaony,pkaonz,0.493667);
//
//  TVector3 Kaon = kaonmomentum.Vect();
//  TVector3 KaonPos(0,0,101800.);
//
//
//  //fVertex = fTools->SingleTrackVertex( track->GetThreeMomentumBeforeMagnet(), Kaon, track->GetPositionBeforeMagnet(), KaonPos, fcda);
//
//
//
//}

int CHODAnalyzer::MatchCHODCandidate(Double_t trktime, TVector3 posatCHOD){

  std::map<Double_t,int> closestCHOD;
  std::vector<Double_t> tdiff;
  std::vector<Double_t> dtrkcl;
  int iCHOD=-1;
  TRecoCHODCandidate* CHOD;
  TRecoCHODCandidate* goodchod;
  double dtCHOD;

  for (int i=0; i<fCHODEvent->GetNCandidates(); i++) {
    CHOD = (TRecoCHODCandidate*) fCHODEvent->GetCandidate(i);
    //maybe the number 3 should be changed later
    TVector2 posCHOD = CHOD->GetHitPosition();
    tdiff.push_back(CHOD->GetTime() - trktime);
    dtCHOD = fabs(CHOD->GetTime() - trktime);
    dtrkcl.push_back(sqrt(pow(posCHOD.X() - posatCHOD.X(),2 ) + pow(posCHOD.Y() - posatCHOD.Y(),2 ) )) ;
    closestCHOD[dtCHOD] = i;
  }

  if(fCHODEvent->GetNCandidates() == 0) iCHOD=-1; else iCHOD = closestCHOD.begin()->second;

  if(iCHOD< 0) return -1;

  Double_t chi2chod = tdiff[iCHOD]*tdiff[iCHOD]/(9*3*3)+dtrkcl[iCHOD]/(25*25);

  //Double_t chi2chod = dhittime*dhittime/(9*3*3)+dtime*dtime/(4*1.6*1.6)+dist2/(4*30*30);
  //if(fabs(tdiff[iCHOD]) > 10. ) return 0;
  if(dtrkcl[iCHOD] > 50. ) return -1;

  goodchod =(TRecoCHODCandidate*) fCHODEvent->GetCandidate(iCHOD);

  FillHisto("hit_chi2", chi2chod);
  FillHisto("hit_dist",dtrkcl[iCHOD]);
  FillHisto("hit_dtime_vs_dist",tdiff[iCHOD],dtrkcl[iCHOD]);

  GoodTrack->SetCHODDistanceToHit(dtrkcl[iCHOD]);
  GoodTrack->SetCHODTime(tdiff[iCHOD] + trktime);
  GoodTrack->SetCHODdt(tdiff[iCHOD]);
  GoodTrack->SetCHODDiscriminant(chi2chod);

  fCHODCandidate->SetDistanceToHit(dtrkcl[iCHOD]);
  fCHODCandidate->SetDiscriminant(chi2chod);
  fCHODCandidate->SetCHODTime (tdiff[iCHOD] + trktime);
  fCHODCandidate->SetCHODdt (tdiff[iCHOD]);
  fCHODCandidate->SetX(goodchod->GetHitPosition().X());
  fCHODCandidate->SetY(goodchod->GetHitPosition().Y());
  fCHODCandidate->SetGoodTrack(true);

  //std::cout << "sw = " << goodmuv2->GetShowerWidth() << std::endl;
  return closestCHOD.begin()->second;
}

Int_t CHODAnalyzer::MatchCHOD(TrackCandidate* cand,TRecoSpectrometerCandidate* track){

  //fCHODEvent = event;
  Double_t mindist = 99999999.;
  Double_t minchodtime = 99999999.;
  Double_t mintime = 99999999.;
  Int_t minjq1 = -1;
  Double_t dx = -99999.;
  Double_t dy = -99999.;
  Double_t minchi2 = 99999999.;
  TVector3 poschodV = fTools->GetPositionAtZ(track,238960.);
  TVector3 poschodH = fTools->GetPositionAtZ(track,239340.);
  Double_t xtrack = poschodV.X();
  Double_t ytrack = poschodH.Y();
  if (minjq1==-1) {
    Double_t chodParameters[6] = {99999999.,99999999.,99999.,-99999.,-99999.,-99999};
    minjq1 = MakeCandidate(xtrack,ytrack,track->GetTime(),chodParameters);
    if (minjq1>=0) {
      minchi2 = chodParameters[0];
      mintime = chodParameters[1];
      mindist = chodParameters[2];
      dx = chodParameters[3];
      dy = chodParameters[4];
      minchodtime = chodParameters[5];
    }
  }

  cand->SetCHODDistanceToHit(sqrt(mindist));
  cand->SetCHODDiscriminant(minchi2);
  cand->SetCHODTime (minchodtime);
  cand->SetCHODdt (mintime);
  cand->SetCHODX(dx);
  cand->SetCHODY(dy);
  //cand->SetCHODPresent(true);

  fCHODCandidate->SetDistanceToHit(sqrt(mindist));
  fCHODCandidate->SetDiscriminant(minchi2);
  fCHODCandidate->SetCHODTime (minchodtime);
  fCHODCandidate->SetCHODdt (mintime);
  fCHODCandidate->SetX(dx);
  fCHODCandidate->SetY(dy);
  fCHODCandidate->SetGoodTrack(true);

  //Z is the average between horizontal and vertical channels
  fCHODCandidate->SetPosition(dx,dy,239150);

  return minjq1;


}
Int_t CHODAnalyzer::MakeCandidate(Double_t xPos, Double_t yPos, Double_t ttime, Double_t *parameters) {
  Double_t mindist = 99999999.;
  Double_t mintime = 99999999.;
  Double_t minchodtime = 99999999.;
  Int_t jminhitV = -1;
  Int_t jminhitH = -1;
  Double_t dx = -99999.;
  Double_t dy = -99999.;
  Double_t minchi2 = 99999999.;

  TClonesArray& Hits = (*(fCHODEvent->GetHits()));
  for (Int_t jhit=0; jhit<fCHODEvent->GetNHits(); jhit++) {
    TRecoCHODHit *HitV = (TRecoCHODHit*)Hits[jhit];
    Int_t counterV = HitV->GetChannelID();
    if (counterV>63) continue;
    for (Int_t ihit=0; ihit<fCHODEvent->GetNHits(); ihit++) {

      TRecoCHODHit *HitH = (TRecoCHODHit*)Hits[ihit];
      Int_t counterH = HitH->GetChannelID();

      if (counterH<64 || counterH>127) continue;

      Double_t xslab = fCHODPosV[HitV->GetChannelID()]*10;
      Double_t yslab = fCHODPosH[HitH->GetChannelID()-64]*10;

      if (GetQuadrant(counterV)!=GetQuadrant(counterH)) continue;

      Double_t tot1 = HitV->GetTimeWidth();
      Double_t tot2 = HitH->GetTimeWidth();
      Double_t slewcorrV = (tot1<15&&tot2<15) ? fCHODAllSlewSlope[counterV][(counterH-64)%16]*tot1+fCHODAllSlewConst[counterV][(counterH-64)%16] : 0.;
      Double_t slewcorrH = (tot1<15&&tot2<15) ? fCHODAllSlewSlope[counterH][counterV%16]*tot2+fCHODAllSlewConst[counterH][counterV%16] : 0.;
      Double_t tVcorr = HitV->GetTime()-slewcorrV-fCHODAllT0[counterV][(counterH-64)%16];
      Double_t tHcorr = HitH->GetTime()-slewcorrH-fCHODAllT0[counterH][counterV%16];
      Double_t dhittime = tVcorr-tHcorr;
      FillHisto("dhittime",dhittime);

      Double_t avtime = 0.5*(tVcorr+tHcorr);

      if (GetQuadrant(counterV)<=1) avtime += 0.65;
      if (GetQuadrant(counterH)==1 || GetQuadrant(counterH)==2) avtime += 0.2;

      Double_t dtime = avtime-ttime;
      Double_t dist2 = (xslab-xPos)*(xslab-xPos)+(yslab-yPos)*(yslab-yPos);
      FillHisto("hit_dist",sqrt(dist2));
      FillHisto("hit_dtime_vs_dist",dtime,sqrt(dist2));

      Double_t chi2chod = dhittime*dhittime/(9*3*3)+dtime*dtime/(4*1.6*1.6)+dist2/(4*30*30);
      //Double_t chi2chod = dhittime*dhittime/(9*3*3)+dtime*dtime/(9*7*7)+dist2/(4*13*13);
      FillHisto("hit_chi2", chi2chod);
      FillHisto("hit_chi2_vs_dtime",chi2chod,dtime);
      FillHisto("hit_chi2_vs_distance",chi2chod,sqrt(dist2));

      if (chi2chod<minchi2) {
        minchi2 = chi2chod;
        mindist = dist2;
        mintime = dtime;
        minchodtime = avtime;
        jminhitH = ihit;
        jminhitV = jhit;
        dx = xslab;
        dy = yslab;
      }
    }
  }
  parameters[0] = minchi2;
  parameters[1] = mintime;
  parameters[2] = mindist;
  parameters[3] = dx;
  parameters[4] = dy;
  parameters[5] = minchodtime;

  return jminhitH>=0?0:-1;
}

Int_t CHODAnalyzer::GetQuadrant(Int_t channelid) {
  if (channelid<=63) {
    if (channelid>=0  && channelid <=15) return 0;
    if (channelid>=16 && channelid <=31) return 1;
    if (channelid>=32 && channelid <=47) return 2;
    if (channelid>=48 && channelid <=63) return 3;
  } else {
    if (channelid>=64  && channelid<=79)  return 0;
    if (channelid>=80  && channelid<=95)  return 1;
    if (channelid>=96  && channelid<=111) return 2;
    if (channelid>=112 && channelid<=127) return 3;
  }
  return -1;
}

Bool_t CHODAnalyzer::IsGoodGTKVtx(){

  double bestdt= -999;
  //int bestgtk = FindClosestTimeCand( fGigaTrackerEvent, "GTK", bestdt);
  int ngood = 0;

  fGigaTrackerAnalysis->SetTimeShift(0.);
  fGigaTrackerAnalysis->SetReferenceDetector(0);
  fGigaTrackerAnalysis->SetRecoTimingCut(1.2);
  fGigaTrackerAnalysis->SetMatchingTimingCut(-0.53,0.6);
  ////  fGigaTrackerAnalysis->SetReferenceDetector(1);
  ////  fGigaTrackerAnalysis->SetRecoTimingCut(1.5);
  ////  fGigaTrackerAnalysis->SetMatchingTimingCut(-0.55,0.9);

  int nGoodTrack = fGigaTrackerAnalysis->ReconstructCandidates(fGigaTrackerEvent,GoodTrack->GetCHODTime(),1); // Remove previous candidates !




  // Test
  //TClonesArray& Hits = (*(fGigaTrackerEvent->GetHits()));
  //for (int jHit(0); jHit<fGigaTrackerEvent->GetNHits(); jHit++) {
  //  TRecoGigaTrackerHit *hit = (TRecoGigaTrackerHit*)Hits[jHit];
  //  int jS = hit->GetStationNo();
  //  double dt  = hit->GetTime()-timeref;
  //  FillHisto(Form("testdt%d",jS+1),dt);
  //  TVector3 poskaon = GetPosKaonAtGTK(jS);
  //  double deltax = hit->GetPosition().X()-poskaon.X();
  //  double deltay = hit->GetPosition().Y()-poskaon.Y();
  //  if (fabs(dt)<1.2) {
  //    FillHisto(Form("testdp%d",jS+1),deltax,deltay);
  //    FillHisto(Form("testdx%d",jS+1),hit->GetPosition().X(),deltax);
  //    FillHisto(Form("testdy%d",jS+1),hit->GetPosition().Y(),deltay);
  //    FillHisto(Form("testxy%d",jS+1),hit->GetPosition().X(),hit->GetPosition().Y());
  //  }
  //}

  // Find and test the best candidate for one track <--------------------- !!!!!!!!!!
  int bestgtk = fGigaTrackerAnalysis->TrackCandidateMatching(fGigaTrackerEvent,fSpectrometerCandidate,GoodTrack->GetCHODTime(),0,nGoodTrack);
  Double_t discr_best = -1;
  Double_t cda = 9999999.;
  Double_t dt = 9999999.;



  if(bestgtk<0) return false;
  TRecoGigaTrackerCandidate* GTK = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(bestgtk);

  int type = GTK->GetType();
  double gtktime = GTK->GetTime();
  double gtkchi2X = GTK->GetChi2X();
  double gtkchi2Y = GTK->GetChi2Y();
  double gtkchi2T = GTK->GetChi2Time();
  double gtkchi2  = GTK->GetChi2();
  int gtknhits = GTK->GetNHits();
  double gtkchoddt=GoodTrack->GetCHODTime() - gtktime;
  TVector3 vertex;
  TVector3 gtkpos= GTK->GetPosition(2);
  TVector3 gtkpos1= GTK->GetPosition(0);
  TVector3 gtkpos2= GTK->GetPosition(1);
  TVector3 gtkmomentum= GTK->GetMomentum();
  TLorentzVector Pgtk(gtkmomentum, KMass);

  FillHisto("gtk_type", type);

  // Kaon moemntum and position

  // Pion momentum and position

  Double_t partrack[4] = {fSpectrometerCandidate->GetMomentum(),fSpectrometerCandidate->GetSlopeXBeforeMagnet(),fSpectrometerCandidate->GetSlopeYBeforeMagnet(),MPI*1000};
  TVector3 p3pion = fSpectrometerCandidate->GetThreeMomentumBeforeMagnet();
  //TVector3 p3pion = fTools->Get4Momentum(partrack).Vect();
  TVector3 pospion = fSpectrometerCandidate->GetPositionBeforeMagnet();

  // Vertex: first iteration without momentum correction for blue field
  fVertex = fTools->SingleTrackVertex(gtkmomentum,p3pion,gtkpos,pospion,cda); // First interation: no blue field

  // std::cout << "Zero iteration -----------------" << std::endl;
  // cout << "gtkmom = " << gtkmomentum.Mag() << endl;
  // cout << "p3pion = " << p3pion.Mag() << endl;
  // cout << "z vertex = " << fVertex.Z() << endl;

  // Correct incoming and outcoming momentum at origin for blue field
  fVertex = fTools->BlueFieldCorrection(&p3pion,fVertex,fSpectrometerCandidate->GetCharge(),fVertex.Z());
  fVertex = fTools->BlueFieldCorrection(&gtkmomentum,gtkpos,1,fVertex.Z());

  // std::cout << "First iteration -----------------" << std::endl;
  // cout << "gtkmom = " << gtkmomentum.Mag() << endl;
  // cout << "p3pion = " << p3pion.Mag() << endl;
  // cout << "z vertex = " << fVertex.Z() << endl;

  // Vertex: second iteration with momentum corrected for blue field
  fVertex = fTools->SingleTrackVertex(gtkmomentum,p3pion,gtkpos,pospion,cda); // First interation: no blue field

  // std::cout << "Second iteration -----------------" << std::endl;
  // cout << "gtkmom = " << gtkmomentum.Mag() << endl;
  // cout << "p3pion = " << p3pion.Mag() << endl;
  // cout << "z vertex = " << fVertex.Z() << endl;

  discr_best = fGigaTrackerAnalysis->Discriminant(cda,gtkchoddt);

  //cout << "discriminant == " << discr_best << endl;
  if(type < 100) return false;
  FillHisto("gtk_P", gtkmomentum.Mag());
  FillHisto("gtk_choddt",  gtkchoddt);
  FillHisto("gtk_chi2X", gtkchi2X);
  FillHisto("gtk_chi2Y", gtkchi2Y);
  FillHisto("gtk_chi2T", gtkchi2T);
  FillHisto("gtk_chi2", gtkchi2);
  FillHisto("gtk_Nhits", gtknhits);
  FillHisto("gtk_pos", gtkpos.X(),gtkpos.Y());

  double dx12 = gtkpos1.X() - gtkpos2.X();
  double dx23 = gtkpos2.X() - gtkpos.X() ;
  double dx13 = gtkpos1.X() - gtkpos.X() ;
  double dy12 = gtkpos1.Y() - gtkpos2.Y();
  double dy23 = gtkpos2.Y() - gtkpos.Y() ;
  double dy13 = gtkpos1.Y() - gtkpos.Y() ;
  double thetax= dx13/ (gtkpos1.Z() - gtkpos.Z());
  double thetay = dy13/ (gtkpos1.Z() - gtkpos.Z());
  FillHisto("gtk_dX_12",dx12 );
  FillHisto("gtk_dX_23",dx23 );
  FillHisto("gtk_dX_13",dx13 );
  FillHisto("gtk_dY_12",dy12 );
  FillHisto("gtk_dY_23",dy23 );
  FillHisto("gtk_dY_13",dy13 );
  FillHisto("gtk_thetax", thetax);
  FillHisto("gtk_thetay", thetay);

  FillHisto("gtk_Nhits_vs_P", (Double_t)gtknhits, gtkmomentum.Mag());

  FillHisto("gtk_discriminant",discr_best);
  //if(fabs(thetay) > 2e-4) return false;
  //if(fabs(thetax) > 2e-4) return false;
  //if( gtknhits > 5) return false;
  //if(dy12 > 10 || dy13 > 10 || dy23 > 10 || dx13 > 12) return false;
  //vertex = fTools->SingleTrackVertex( fSpectrometerCandidate->GetThreeMomentumBeforeMagnet(), gtkmomentum, fSpectrometerCandidate->GetPositionBeforeMagnet(), gtkpos, fcda);
  ////cout << "Before BTube vertex X,Y,Z =( " << vertex.X() <<" ,  " << vertex.Y() << " ,  "<< vertex.Z() <<")" << endl;
  ////cout << "Before BTube mom X,Y,Z =( " << fSpectrometerCandidate->GetThreeMomentumBeforeMagnet().X() <<" ,  " << fSpectrometerCandidate->GetThreeMomentumBeforeMagnet().Y() << " ,  "<< fSpectrometerCandidate->GetThreeMomentumBeforeMagnet().Z() <<")" << endl;
  //fTracker->SetCharge(1);
  //fTracker->SetInitialPosition(fSpectrometerCandidate->GetPositionBeforeMagnet().X(),fSpectrometerCandidate->GetPositionBeforeMagnet().Y(),fSpectrometerCandidate->GetPositionBeforeMagnet().Z());
  //fTracker->SetInitialMomentum(fSpectrometerCandidate->GetThreeMomentumBeforeMagnet().X(),fSpectrometerCandidate->GetThreeMomentumBeforeMagnet().Y(),fSpectrometerCandidate->GetThreeMomentumBeforeMagnet().Z());
  //fTracker->SetZFinal(vertex.Z());
  //fTracker->TrackParticle();
  //vertex = fTracker->GetFinalPosition();
  if(discr_best < 0.02) return false;
  TVector3 corrmom_btube = p3pion;
  //TVector3 corrmom_btube = fTracker->GetFinalMomentum();
  ////cout << "After BTube vertex X,Y,Z =( " << vertex.X() <<" ,  " << vertex.Y() << " ,  "<< vertex.Z() <<")" << endl;
  ////cout << "After BTube mom X,Y,Z =( " << corrmom_btube.X() <<" ,  " << corrmom_btube.Y() << " ,  "<< corrmom_btube.Z() <<")" << endl;
  //if(gtknhits == 0) return false;
  fcda = cda;
  FillHisto("gtk_dt_vs_cda",gtkchoddt, fcda);
  //if(gtkmomentum.Mag() > 77200 || gtkmomentum.Mag() < 72700) return false;
  //if(gtkchi2T > 20) return false;
  //if(gtkchi2X > 20) return false;
  //if(gtkchi2Y > 20) return false;
  //if(fcda > 15) return false;
  //if(fabs(gtkchoddt) > 3) return false;
  //if(fcda + fabs(gtkchoddt)*5. > 15.) return false;

  ngood++;
  fKMomentum = gtkmomentum;
  //fVertex=vertex;
  //std::cout << "CHOD ============= " << std::endl;
  //cout << "best Gtk == " << bestgtk << " w time = " << gtktime << endl;

  fSpectrometerCandidate->SetMomentum(corrmom_btube.Mag());
  fSpectrometerCandidate->SetSlopeXBeforeMagnet(corrmom_btube.X()/corrmom_btube.Z());
  fSpectrometerCandidate->SetSlopeYBeforeMagnet(corrmom_btube.Y()/corrmom_btube.Z());
  GoodTrack->SetGTK(GTK,bestgtk);
  GoodTrack->SetCDA(fcda);
  GoodTrack->SetGTKTime(gtktime);
  GoodTrack->SetGTKMomentum(gtkmomentum);
  GoodTrack->SetGTKVertex(fVertex);
  GoodTrack->SetTrackMomentumBeforeMagnet(corrmom_btube);

  FillHisto("gtk_Ngood", ngood);
  return true;
}

Bool_t CHODAnalyzer::NoCHANTI(){

  if(fCHANTIEvent->GetNCandidates() == 0) {

    GoodTrack->SetCHANTIPresent();
    return true;
  }

  double bestdt= -999;
  int bestchanti = FindClosestTimeCand( fCHANTIEvent, "CHANTI", bestdt);
  if(bestchanti<0){
    GoodTrack->SetCHANTIPresent();
    return true;
  }


  TRecoCHANTICandidate* chanti = (TRecoCHANTICandidate*)fCHANTIEvent->GetCandidate(bestchanti);
  double chantitime = chanti->GetTime();
  double chantigtkdt = GoodTrack->GetGTKTime() - chantitime;
  double chantichoddt = GoodTrack->GetCedarTime() - chantitime;
  double chanticedardt = GoodTrack->GetCHODTime() - chantitime;

  FillHisto("CHANTI_gtkdt", chantigtkdt);
  FillHisto("CHANTI_choddt", chantichoddt);
  FillHisto("CHANTI_cedardt", chanticedardt);

  if(fabs(chantigtkdt) < 3) return false;

  GoodTrack->SetCHANTI(chanti, bestchanti);
  GoodTrack->SetCHANTITime(chantitime);

  return true;

}

Bool_t CHODAnalyzer::IsGoodCedar(){

  if(fCedarEvent->GetNCandidates() == 0) return false;
  double bestdt= -999;
  int bestcedar = FindClosestTimeCand( fCedarEvent, "Cedar", bestdt);
  if(bestcedar<0) return false;


  TRecoCedarCandidate* cedarcand = (TRecoCedarCandidate*)fCedarEvent->GetCandidate(bestcedar);

  if( cedarcand->GetNSectors() < 5) return false;//ask if lowering to 4
  double cedartime = cedarcand->GetTime();
  double dtcedar = cedartime - GoodTrack->GetGTKTime();
  double dtchodcedar = cedartime - GoodTrack->GetCHODTime();
  FillHisto("Cedar_gtkdt", dtcedar);
  FillHisto("Cedar_choddt", dtchodcedar);
  if(fabs(dtcedar) > 1.8 ) return false;

  if(fabs(dtchodcedar) > 2. ) return false;


  GoodTrack->SetCedar(cedarcand,bestcedar);
  GoodTrack->SetCedarTime(cedartime);
  ///  GoodTrack->SetCedardt(dtcedar);
  return true;

}
int CHODAnalyzer::FindClosestTimeCand( TRecoVEvent* Event, string detector_type, double& minimum){

  std::vector<double> dt;
  int position= -1;
  for (int iCand=0; iCand < Event->GetNCandidates(); iCand++){
    if(detector_type == "GTK"){
      TRecoGigaTrackerCandidate* Cluster = ((TRecoGigaTrackerCandidate*)Event->GetCandidate(iCand));
      double time = Cluster->GetTime();
      double dtchod = fabs(time - GoodTrack->GetCHODTime()) ;
      dt.push_back(dtchod);
    }

    if(detector_type == "Cedar"){
      TRecoCedarCandidate* Cluster = ((TRecoCedarCandidate*)Event->GetCandidate(iCand));
      double time = Cluster->GetTime();
      double dtchod = fabs(time - GoodTrack->GetGTKTime()) ;
      dt.push_back(dtchod);
    }

    if(detector_type == "CHANTI"){
      TRecoCHANTICandidate* Cluster = ((TRecoCHANTICandidate*)Event->GetCandidate(iCand));
      double time = Cluster->GetTime();
      double dtchod = fabs(time - GoodTrack->GetGTKTime()) ;
      dt.push_back(dtchod);
    }

  }
  if(dt.size() !=0){
    minimum= *min_element(dt.begin(), dt.end());
    position = distance(dt.begin(), min_element(dt.begin(), dt.end()));
    dt.clear();

  }
  return position;

}

CHODAnalyzer::~CHODAnalyzer(){

}
