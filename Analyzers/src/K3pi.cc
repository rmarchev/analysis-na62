#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "K3pi.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Geometry.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "Parameters.hh"
#include "AnalysisTools.hh"
#include "GeometricAcceptance.hh"
#include "LAVMatching.hh"
#include "SAVMatching.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

/// \class K3pi
/// \Brief
/// Short description of your Analyzer
/// \EndBrief
///
/// \Detailed
/// Detailed description of your Analyzer\n\n
/// For examples of working Analyzer you can have a look at the examples in the Examples/ directory:\n
/// LKrPhotonMC \n
/// Pi0Reconstruction \n
/// SkimmingNoStrawMuonsTracks \n
/// Or the framework analyzers that can be found in the Analyzers/ directories: \n
/// CedarMCTester \n
/// VertexCDA \n
/// \n
/// All the following classes are available for you to use. Check their documentation for more information:\n
/// NA62Analysis::manip::TermManip \n
/// NA62Analysis::Analyzer \n
/// NA62Analysis::CounterHandler \n
/// NA62Analysis::DetectorAcceptance \n
/// NA62Analysis::EventFraction \n
/// NA62Analysis::MCSimple \n
/// NA62Analysis::NeuralNetwork \n
/// NA62Analysis::ParticleInterface \n
/// NA62Analysis::ParticleTree \n
/// NA62Analysis::StringBalancedTable \n
/// NA62Analysis::StringTable \n
/// NA62Analysis::UserMethods \n
/// NA62Analysis::Verbose \n
///
/// You might also be interested in checking the documentation for the following classes. However you should not
/// in general have to create instances of these. If necessary a pointer to the existing instance is usually
/// available or provided by specific methods.\n
/// NA62Analysis::Core::IOHisto \n
/// NA62Analysis::Core::IOTree \n
/// NA62Analysis::Core::IOHandler \n
/// NA62Analysis::Core::HistoHandler \n
/// NA62Analysis::Core::HistoHandler::Iterator \n
/// NA62Analysis::Core::PrimitiveReader \n
///
/// \EndDetailed

K3pi::K3pi(Core::BaseAnalysis *ba) : Analyzer(ba, "K3pi")
{

    fMaxNTracks = 50;
    fMaxNVertices = 50;

    Parameters *par = Parameters::GetInstance();
    par->LoadParameters("/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/chod_par/chod_slab_parameters.dat");
    par->LoadParameters("/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/chod_par/chodallt0.dat");
    par->LoadParameters("/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/chod_par/chodallslewslope.dat");
    par->LoadParameters("/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/chod_par/chodallslewconst.dat");
    par->StoreCHODParameters();
    fCHODPosV = (Double_t *)par->GetCHODPosV();
    fCHODPosH = (Double_t *)par->GetCHODPosH();
    fCHODAllT0 = (Double_t **)par->GetCHODAllT0();
    fCHODAllSlewSlope = (Double_t **)par->GetCHODAllSlewSlope();
    fCHODAllSlewConst = (Double_t **)par->GetCHODAllSlewConst();


    /// Initialize DetectorAcceptance if needed\n
    /// use of global instance\n
    ///     \code
    ///             fDetectorAcceptanceInstance = GetDetectorAcceptanceInstance();
    /// \endcode
    /// use of local instance\n
    ///     \code
    ///             fDetectorAcceptanceInstance = new DetectorAcceptance("./NA62.root");
    /// Call one of: \n
    ///     \code
    ///     AddParam("paramName", &variableName, defaultValue);
    /// \endcode
    /// for each parameter of the analyzer. These parameters can be set when starting the FW from the command line with the -p option.\n
    RequestL0Data();
    RequestL1Data();
    RequestTree("Spectrometer",new TRecoSpectrometerEvent);
    RequestTree("CHOD",new TRecoCHODEvent);
    RequestTree("Cedar",new TRecoCedarEvent);
    RequestTree("LKr",new TRecoLKrEvent);
    RequestTree("CHANTI",new TRecoCHANTIEvent);
    RequestTree("LAV",new TRecoLAVEvent);
    RequestTree("SAC",new TRecoSACEvent);
    RequestTree("IRC",new TRecoIRCEvent);
    RequestTree("MUV2",new TRecoMUV2Event);
    RequestTree("MUV1",new TRecoMUV1Event);
    RequestTree("MUV3",new TRecoMUV3Event);

    fLAVMatch = new LAVMatching();
    fSAVMatch = new SAVMatching();

    AddParam("BlueTubeFieldCorr",&fBlueFieldCorrection, true);
    AddParam("MaxChi2",    &fMaxChi2,       100.0);
    AddParam("MinZvertex", &fMinZVertex,  50000.0); // [mm]
    AddParam("MaxZvertex", &fMaxZVertex, 180000.0); // [mm]

}

void K3pi::InitOutput(){
}

void K3pi::InitHist(){

    BookHisto(new TH1D("Ntracks", "Number of tracks;Number of tracks", 20, -0.5, 19.5));
    BookHisto(new TH1D("vtxchi2", "Vertex #chi^{2};#chi^{2}", 100,0, fMaxChi2));
    BookHisto(new TH1D("vtxcharge", "Charge of the vertex;Vtx_{Q}", 8, -4, 4));
    BookHisto(new TH1D("xvtx", "Vertex X coordinate;Z [mm]", 1000, -500, 500)); // [mm]
    BookHisto(new TH1D("yvtx", "Vertex Y coordinate;Z [mm]", 1000, -500, 500)); // [mm]
    BookHisto(new TH1D("zvtx", "Vertex Z coordinate;Z [mm]", 100, fMinZVertex, fMaxZVertex)); // [m]
    BookHisto(new TH1D("ptot_all", "Momentum of the three track system;P_{3track} [GeV/c]", 100, 50, 100)); // [GeV/c]

    BookHisto(new TH2D("trackXY_atstraw1", "Track (x,y) at Straw chamber 1;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    BookHisto(new TH2D("trackXY_atstraw2", "Track (x,y) at Straw chamber 2;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    BookHisto(new TH2D("trackXY_atstraw3", "Track (x,y) at Straw chamber 3;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    BookHisto(new TH2D("trackXY_atstraw4", "Track (x,y) at Straw chamber 4;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    BookHisto(new TH2D("trackXY_atCHOD", "Track (x,y) at CHOD;x [mm];y [mm]", 60, -1200, 1200, 60, -1200, 1200));
    BookHisto(new TH2D("trackXY_atLKr", "Track (x,y) at CHOD;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    BookHisto(new TH2D("trackXY_atMUV1", "Track (x,y) at MUV1;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    BookHisto(new TH2D("trackXY_atMUV2", "Track (x,y) at MUV2;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));
    BookHisto(new TH2D("trackXY_atMUV3", "Track (x,y) at MUV3;x [mm];y [mm]", 120, -1200, 1200, 120, -1200, 1200));

    BookHisto(new TH2D("oddtrackXY_atCHOD", "Track (x,y) at CHOD;x [mm];y [mm]", 60, -1200, 1200, 60, -1200, 1200));

    BookHisto(new TH1F("dhittime","HitTime; t_{hit} [ns]", 800, -200, 200));
    BookHisto(new TH1F("hit_dist","Distance to hit;dist [mm]",400, 0, 2000));
    BookHisto(new TH1F("hit_chi2","CHOD Discriminant ;#chi^{2} ",200, 0, 50));
    BookHisto(new TH2F("hit_dtime_vs_dist","dt_{Hit} vs Distance to hit; dt_{hit} [ns];dist [mm]", 200, -100, 100, 400, 0, 2000));
    BookHisto(new TH2F("hit_chi2_vs_dtime","HitTime vs dt; #chi^{2} ;di2st [mm]", 200, 0, 50, 200, -100, 100));
    BookHisto(new TH2F("hit_chi2_vs_distance"," Discriminant vs Distance to hit; #chi^{2};dist [mm]", 200, 0, 50, 400, 0, 2000));

    BookHisto(new TH1F("lkrdist","Track distance to cluster in LKr ;dist [mm]",400, 0, 2000));
    BookHisto(new TH1F("dtlkr","dT_{lkr}; dt_{lkr} [ns]", 800, -200, 200));

    BookHisto(new TH1D("track_lkrenergy", "Energy of the Track released in lkr;E_{lkr} [GeV/c]", 200,0, 100)); // [GeV/c]
    BookHisto(new TH1D("track_eop", "E/p;E_{lkr}/p", 120,0, 1.2)); // [GeV/c]
    BookHisto(new TH1F("eventtime","Average CHOD time of the tracks; T_{Event} [ns]", 800, -200, 200));
    BookHisto(new TH1D("cedar_nsectors", "Number of sectors in cedar;Nsectors", 10, -0.5, 9.5));
    BookHisto(new TH1F("dtcedar","dT_{cedar}; dt_{Cedar} [ns]", 800, -200, 200));
    BookHisto(new TH1F("dtmuv1","dT_{muv1}; dt_{muv1} [ns]", 800, -200, 200));
    BookHisto(new TH1F("dtmuv2","dT_{muv2}; dt_{muv2} [ns]", 800, -200, 200));
    BookHisto(new TH1F("dtmuv3","dT_{muv3}; dt_{muv3} [ns]", 800, -200, 200));

    BookHisto(new TH1F("distmuv1","Distance to closest MUV1 cand;dist [mm]",400, 0, 2000));
    BookHisto(new TH1F("distmuv2","Distance to closest MUV2 cand;dist [mm]",400, 0, 2000));
    BookHisto(new TH1F("distmuv3","Distance to closest MUV3 cand;dist [mm]",400, 0, 2000));



    BookHisto(new TH1D("ptot", "Momentum of the three track system;P_{3track} [GeV/c]", 100, 50, 100)); // [GeV/c]
    BookHisto(new TH1D("pttot", "Momentum of the three track system;P_{3track} [GeV/c]", 100, 0, 1)); // [GeV/c]
    BookHisto(new TH1D("mtot", "Mass of the three track system;M_{3track} [GeV/c]", 300, 0.3, 0.6)); // [GeV/c]

    BookHisto(new TH1D("dxdz",       "Kaon dx/dz",  100, -0.0025, 0.0025));
    BookHisto(new TH1D("dydz",       "Kaon dy/dz",  100, -0.0025, 0.0025));
    BookHisto(new TH2D("ptot_vs_dydz", "PKaon vs Kaon dy/dz",100,65.,85. , 100, -0.0025, 0.0025));
    BookHisto(new TH2D("ptot_vs_dxdz", "PKaon vs Kaon dy/dz",100,65.,85. , 100, -0.0025, 0.0025));
    BookHisto(new TH2D("XYtrim5",    "Kaon (x,y) at z=101.8m;x [mm]; y[mm]", 50, -50, 50, 50, -50, 50));
    BookHisto(new TH1D("Xtrim5", "Kaon x at z=101.8m;x [mm]", 100, -50, 50));
    BookHisto(new TH1D("Ytrim5", "Kaon y at z=101.8m;y [mm]", 100, -50, 50));

    BookHisto(new TH1I("gtk_type" , ";GTK type" ,200,0,200));

    BookHisto(new TH1F("dtgtkchod","dT_{CHOD - GTK}; dt_{CHOD - GTK} [ns]", 800, -200, 200));
    BookHisto(new TH1F("dtgtkcedar","dT_{cedar - GTK}; dt_{Cedar - GTK} [ns]", 800, -200, 200));


    BookHisto(new TH1F("gtk_P", " GTK momentum;P_{K} [MeV/c]",100,65.,85));
    BookHisto(new TH1I("gtk_Nhits", "Number of GTK hits;Nhits" ,100,0,100));
    BookHisto(new TH1F("gtk_chi2","#chi^{2} ; #chi^{2}",300,0.,300.));
    BookHisto(new TH1F("gtk_chi2X","#chi^{2}X ; #chi^{2}",300,0.,300.));
    BookHisto(new TH1F("gtk_chi2Y","#chi^{2}Y ; #chi^{2}",300,0.,300.));
    BookHisto(new TH1F("gtk_chi2T","#chi^{2}T ; #chi^{2}",300,0.,300.));
    BookHisto(new TH2F("gtk_pos","GTK position; x @ GTK [mm]; y @ GTK [mm]", 100,-50,50,100,-50,50));


    BookHisto(new TH1F("gtk_dX_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("gtk_dY_12","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("gtk_dX_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("gtk_dY_13","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("gtk_dX_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("gtk_dY_23","; dx @ GTK [mm]; dy @ GTK [mm]", 100,-25,25));
    BookHisto(new TH1F("gtk_thetax", " ;#theta_{x GTK}  [rad]", 1000, -0.04, 0.04));
    BookHisto(new TH1F("gtk_thetay", " ;#theta_{y GTK}  [rad]", 1000, -0.04, 0.04));
    BookHisto(new TH2F("gtk_Nhits_vs_P", "Number of GTK hits vs P;Nhits;P" ,30,0,30,100,65.,85));

    BookHisto(new TH1F("ptot_aftergtk", " GTK momentum;P_{K} [MeV/c]",100,65.,85));

    BookHisto(new TH2F("pk_vs_dpk", "Pk from K3pi vs Kaon momentum difference using the GTK - same using K3pi ;dP_{K};P_{K} [Gev/c]",100,65,85,200, -10, 10));
    BookHisto(new TH2F("pk_vs_dx", "  ;P_{K};dx_{GTK - K3pi}",100,65,85,120,-20, 20));
    BookHisto(new TH2F("pk_vs_dy", "  ;P_{K};dy_{GTK - K3pi}",100,65,85,120,-20, 20));
    BookHisto(new TH2F("x_vs_kthx", ";x^{K_{3#pi}}_{gtk3};K_{3#pi} #theta^{K}_{x}",240,-40,40,100,0.0005,0.0025));
    BookHisto(new TH2F("gtk_x_vs_kthx", ";x^{gtk}_{gtk3};gtk #theta^{K}_{x}",240,-40,40,100,0.0005,0.0025));
    BookHisto(new TH2F("y_vs_kthy", ";y^{K_{3#pi}}_{gtk3};K_{3#pi} #theta^{K}_{y}",120,-20,20,500,-0.01,0.01));
    BookHisto(new TH2F("gtk_y_vs_kthy", ";y^{gtk}_{gtk3};gtk #theta^{K}_{y}",120,-20,20,500,-0.01,0.01));


}

void K3pi::DefineMCSimple(){

}

void K3pi::StartOfRunUser(){
}

void K3pi::StartOfBurstUser(){
}

void K3pi::Process(int iEvent){

    OutputState state;

    fGigaTrackerEvent  = nullptr;
    fSpectrometerEvent = nullptr;
    fCHODEvent         = nullptr;
    fCedarEvent        = nullptr;
    fLKrEvent          = nullptr;
    fCHANTIEvent       = nullptr;
    //fRICHEvent       = nullptr;
    fMUV1Event       = nullptr;
    fMUV2Event       = nullptr;
    fMUV3Event       = nullptr;
    fLAVEvent          = nullptr;
    fSACEvent          = nullptr;
    fIRCEvent          = nullptr;

    fGigaTrackerEvent  = (TRecoGigaTrackerEvent*)GetOutput("GigaTrackerEvtReco.GigaTrackerEvent",state);
    fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
    fCHODEvent         = (TRecoCHODEvent*)GetEvent("CHOD");
    fCedarEvent        = (TRecoCedarEvent*)GetEvent("Cedar");
    fLKrEvent          = (TRecoLKrEvent*)GetEvent("LKr");
    fCHANTIEvent       = (TRecoCHANTIEvent*)GetEvent("CHANTI");
    //fRICHEvent       = (TRecoRICHEvent*)GetEvent("RICH");
    fMUV1Event       = (TRecoMUV1Event*)GetEvent("MUV1");
    fMUV2Event       = (TRecoMUV2Event*)GetEvent("MUV2");
    fMUV3Event       = (TRecoMUV3Event*)GetEvent("MUV3");
    fLAVEvent          = (TRecoLAVEvent*)GetEvent("LAV");
    fSACEvent          = (TRecoSACEvent*)GetEvent("SAC");
    fIRCEvent          = (TRecoIRCEvent*)GetEvent("IRC");

    fVertexContainer.clear();
    //std::cout << "wtf" << std::endl;
    Int_t  L0DataType    = GetWithMC() ? 0x1  : GetL0Data()->GetDataType();
    Int_t  L0TriggerWord = GetWithMC() ? 0xFF : GetL0Data()->GetTriggerFlags();
    Bool_t PhysicsData   = L0DataType    & 0x1;
    Bool_t TriggerOK     = L0TriggerWord & 0xFF;

    Bool_t ControlTrigger = PhysicsData && TriggerOK;
    Int_t  BurstID       = GetWithMC() ? 0 : GetRawHeader()->GetBurstID();
    Int_t  BurstTime     = GetWithMC() ? 0 : GetRawHeader()->GetBurstTime();

    if(!ControlTrigger) return;
    if(fCHANTIEvent->GetNCandidates() != 0) return;
    if(fLKrEvent->GetNCandidates() == 0) return;
    if(fCedarEvent->GetNCandidates() == 0) return;

    //std::cout << "gtk n = " << fGigaTrackerEvent->GetNCandidates()  << std::endl;


    if(fGigaTrackerEvent->GetNCandidates() >= fMaxNTracks) return;
    if(fCedarEvent->GetNCandidates() >= fMaxNTracks) return;
    if(fLKrEvent->GetNCandidates() >= fMaxNTracks) return;
    if(fSpectrometerEvent->GetNCandidates() >= fMaxNTracks) return;
    if(fSpectrometerEvent->GetNCandidates() < 3) return;

    FillHisto("Ntracks", fSpectrometerEvent->GetNCandidates());

    // Three-track vertices
    int ind[3];
    for (ind[0]=0; ind[0]<fSpectrometerEvent->GetNCandidates(); ind[0]++) {
        for (ind[1]=ind[0]+1; ind[1]<fSpectrometerEvent->GetNCandidates(); ind[1]++) {
            for (ind[2]=ind[1]+1; ind[2]<fSpectrometerEvent->GetNCandidates(); ind[2]++) {
                BuildVertex(ind, 3);
            }
        }
    }

    std::vector<SpectrometerVertex>::iterator vtxit;

    //if(fVertexContainer.size() != 1) return;

    for(vtxit = fVertexContainer.begin(); vtxit != fVertexContainer.end();vtxit++){

        TVector3 VtxPos = (*vtxit).GetPosition();
        Double_t Xvertex = VtxPos.x();
        Double_t Yvertex = VtxPos.y();
        Double_t Zvertex = VtxPos.z();
        Double_t VtxCharge = (*vtxit).GetCharge();
        Double_t VtxChi2 = (*vtxit).GetChi2();
        TVector3 Ptot = (*vtxit).GetTotalThreeMomentum();

        if( VtxPos.x() < -20  || VtxPos.x() > 100 ) continue;
        if(fabs(VtxPos.y()) > 20 ) continue;
        if(VtxPos.z() > 165000 || VtxPos.z() < 110000  ) continue;
        if(VtxChi2 > 20) continue;
        if(fabs(VtxCharge) > 1) continue;

        TLorentzVector trkp[3];

        Int_t ngoodtracks=0;
        // std::cout << "-----" << std::endl;

        for(int i=0;i<3;i++){

            Int_t iTrack = (*vtxit).GetTrackIndex(i);
            TRecoSpectrometerCandidate* Scand = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(iTrack);
            Double_t ptrack   = Scand->GetMomentum()*fmevtogev;
            double pattreco = Scand->GetMomentumBeforeFit()*fmevtogev;
            double chi2 = Scand->GetChi2();
            int charge = Scand->GetCharge();
            int nChambers = Scand->GetNChambers();


            fCHODTime[i] = -99999;
            //Z is the average between horizontal and vertical channels
            fCHODPos[i].SetXYZ(-9999,-9999,-9999);

            if (fabs(ptrack - pattreco) > 20) continue;
            if (chi2 > 20) continue;
            if (nChambers!=4) continue;
            if (!GeometricAcceptance::GetInstance()->InAccptance(Scand, kSpectrometer, 0)) continue;
            if (!GeometricAcceptance::GetInstance()->InAcceptance(Scand, kSpectrometer, 1)) continue;
            if (!GeometricAcceptance::GetInstance()->InAcceptance(Scand, kSpectrometer, 2)) continue;
            if (!GeometricAcceptance::GetInstance()->InAcceptance(Scand, kSpectrometer, 3)) continue;
            if (!GeometricAcceptance::GetInstance()->InAcceptance(Scand, kNewCHOD))         continue;
            if (!GeometricAcceptance::GetInstance()->InAcceptance(Scand, kCHOD))         continue;
            if (!GeometricAcceptance::GetInstance()->InAcceptance(Scand, kRICH))         continue;
            if (!GeometricAcceptance::GetInstance()->InAcceptance(Scand, kLKr))         continue;

            trkp[i].SetVectM((*vtxit).GetTrackThreeMomentum(i),PiMass*fgevtomev);


            Double_t zStraw[4];
            TVector3 pos_atstraw[4];
            TVector3 pos_atlkr = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zLKr);
            TVector3 pos_atmuv1 = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zMUV1);
            TVector3 pos_atmuv2 = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zMUV2);
            TVector3 pos_atmuv3 = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zMUV3);
            //cout << "zLKr = " << zLKr << endl;


            zStraw[0] = 183508.0;
            zStraw[1] = 194066.0;
            zStraw[2] = 204459.0;
            zStraw[3] = 218885.0;

            for (int j=0; j < 4; j++) {
                // pos_atstraw[j] = fTools->GetPositionAtZ(track,zStraw[j]);
                pos_atstraw[j] = AnalysisTools::GetInstance()->GetPositionAtZ(Scand,zStraw[j]);
                FillHisto(Form("trackXY_atstraw%d",j+1),pos_atstraw[j].X(),pos_atstraw[j].Y());
            }

            //std::cout << "chod hits = " << fCHODEvent->GetNHits()<< std::endl;
            Int_t ichod = MatchCHOD(i,Scand);

            //AnalyzeLKr(i,fCHODTime[i]);

            Int_t igoodlkr = MatchLKr(iTrack,fCHODTime[i],pos_atlkr);

            //cout << "-------End with ilk = " << igoodlkr << endl;
            if(igoodlkr >= 0) {
                //if(igoodlkr < 0)continue;
                TRecoLKrCandidate* closestlkrcand = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(igoodlkr);
                Double_t energy = closestlkrcand->GetClusterEnergy()*fmevtogev;
                Double_t eop = closestlkrcand->GetClusterEnergy()/Scand->GetMomentum();
                FillHisto("track_lkrenergy",energy);
                FillHisto("track_eop",eop);

                if(eop > 0.8) continue;
                if(eop < 0.15) continue;
            }

            // std::cout << "i = " << i << std::endl;
            // std::cout << "ichod = " << ichod << std::endl;
            // std::cout << "ilkr = " << igoodlkr << std::endl;
            if(ptrack < 10) continue;

            FillHisto("trackXY_atCHOD",fCHODPos[i].X(),fCHODPos[i].Y());
            FillHisto("trackXY_atLKr",pos_atlkr.X(),pos_atlkr.Y());

            Int_t igoodmuv1= MUV1Match(fCHODTime[i],pos_atmuv1);
            Int_t igoodmuv2= MUV2Match(fCHODTime[i],pos_atmuv2);
            Int_t igoodmuv3= MUV3Match(fCHODTime[i],pos_atmuv3);

            FillHisto("trackXY_atMUV3",pos_atmuv3.X(),pos_atmuv3.Y());
            if(igoodmuv3 >= 0) continue;

            FillHisto("trackXY_atMUV1",pos_atmuv1.X(),pos_atmuv1.Y());
            //if(igoodmuv1 < 0) continue;

            FillHisto("trackXY_atMUV2",pos_atmuv2.X(),pos_atmuv2.Y());
            //if(igoodmuv2 < 0) continue;

            if(Scand->GetCharge()*VtxCharge == -1)
                FillHisto("oddtrackXY_atCHOD",fCHODPos[i].X(),fCHODPos[i].Y());
            //std::cout << "Time of track " << i << "in chod = " << fCHODTime[i] << std::endl;


            ngoodtracks++;
        }

        if(ngoodtracks!=3) continue;

        fEventTime = (fCHODTime[0] + fCHODTime[1] + fCHODTime[2])/3  ;
        FillHisto("eventtime",fEventTime);
        Int_t cedarmatch = CedarInTime(fEventTime);

        if(cedarmatch < 0) continue;
        //std::cout << "cedarmatch = " << cedarmatch << std::endl;
        TRecoCedarCandidate* cedar = (TRecoCedarCandidate*) fCedarEvent->GetCandidate(cedarmatch);

        FillHisto("dtcedar",fEventTime - cedar->GetTime());

        if(fEventTime - cedar->GetTime() > 0.5 || fEventTime - cedar->GetTime() < -2.5 ) continue;


        //TLorentzVector fK4Momentum = trkp[0] + trkp[1] +trkp[2];
        fK4Momentum = trkp[0] + trkp[1] +trkp[2];
        fK3Momentum = fK4Momentum.Vect();
        //TVector3       fK3Momentum      = fK4Momentum.Vect();
        Double_t       M3pi               = fK4Momentum.M()*fmevtogev;  //[GeV/c^2]
        //Double_t       M3pi0              = fK4Momentum0.M(); // vertex fitter not used
        Double_t       dxdz               = fK3Momentum.X()/fK3Momentum.Z();
        Double_t       dydz               = fK3Momentum.Y()/fK3Momentum.Z();


        // The middle of the TRIM5 magnet: z = 101.8 m
        Double_t Xstart = Xvertex + dxdz*(101800.0 - Zvertex);
        Double_t Ystart = Yvertex + dydz*(101800.0 - Zvertex);
        //Transverse momentum of the kaon wrt to the nominal kaon axis
        Double_t shifted_pt = (fK4Momentum.Pt() - 0.0012*fK4Momentum.P())*fmevtogev;
        FillHisto("pttot", shifted_pt);

        if(fabs(shifted_pt) > 0.04) continue;

        fLAVMatch->SetReferenceTime(fEventTime);
        fSAVMatch->SetReferenceTime(fEventTime);

        Bool_t islav   = fLAVMatch->LAVHasTimeMatching(fLAVEvent);
        Int_t  issav   = fSAVMatch->SAVHasTimeMatching(fIRCEvent, fSACEvent);

        if( issav !=0) continue;
        if( islav ) continue;

        FillHisto("mtot",M3pi);

        if(M3pi > 0.497 || M3pi < 0.490) continue;

        //cout << "fK P from vtx = " << Ptot.Mag()*fmevtogev << "k3pi P = "<< fK3Momentum.Mag()*fmevtogev << endl;

        FillHisto("ptot",Ptot.Mag() *fmevtogev);

        FillHisto("dxdz",dxdz);
        FillHisto("ptot_vs_dxdz",Ptot.Mag()*fmevtogev,dxdz);
        FillHisto("dydz",dydz);
        FillHisto("ptot_vs_dydz",Ptot.Mag()*fmevtogev,dydz);
        FillHisto("XYtrim5",Xstart,Ystart);
        FillHisto("Xtrim5" ,Xstart);
        FillHisto("Ytrim5" ,Ystart);

        Int_t goodgtk = GTKMatch(fEventTime,cedar->GetTime());

        if(goodgtk < 0) continue;
        //cout << trkp[0].P() << "Part 2 = " <<  trkp[1].P() << "Part 3 =" << trkp[2].P() << endl;
        //cout << "muv3 cand = " << fMUV3Event->GetNCandidates() << endl;
        FillHisto("ptot_aftergtk",Ptot.Mag() *fmevtogev);
        //FillHisto("ptot_aftergtk",Ptot.Mag() *fmevtogev);
        GTKAnalysis(goodgtk,VtxPos);

    }

    return;
}

void K3pi::PostProcess(){
    /// \MemberDescr
    /// This function is called after an event has been processed by all analyzers. It could be used to free some memory allocated
    /// during the Process.
    /// \EndMemberDescr

}

void K3pi::EndOfBurstUser(){
    /// \MemberDescr
    /// This method is called when a new file is opened in the ROOT TChain (corresponding to a start/end of burst in the normal NA62 data taking) + at the end of the last file\n
    /// Do here your start/end of burst processing if any
    /// \EndMemberDescr
}

void K3pi::BuildVertex(Int_t ind[], Int_t NTracks) {

    if (fVertexContainer.size()==fMaxNVertices) return;
    if (NTracks >= fMaxNTracks) return;

    fVertexLSF.Reset();

    Int_t Charge = 0;
    for (Int_t iTrack=0; iTrack<NTracks; iTrack++) {
        TRecoSpectrometerCandidate *cand =
            (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(ind[iTrack]);
        Charge += cand->GetCharge();
        fVertexLSF.AddTrack(cand);
    }

    if (fBlueFieldCorrection) fVertexLSF.FitVertex(kTRUE); // BF correction included


    // Loose seleciton of vertices returned by VertexLSF
    if (fVertexLSF.GetChi2()>fMaxChi2) return;
    if (fVertexLSF.GetVertexPosition().z()<fMinZVertex) return;
    if (fVertexLSF.GetVertexPosition().z()>fMaxZVertex) return;


    // Save the vertex into a common structure
    SpectrometerVertex Vertex;
    Vertex.SetNTracks (fVertexLSF.GetNTracks());
    Vertex.SetCharge  (Charge);
    Vertex.SetPosition(fVertexLSF.GetVertexPosition());
    Vertex.SetChi2    (fVertexLSF.GetChi2());

    TVector3 TotalThreeMomentum = TVector3(0.0, 0.0, 0.0);
    for (int iTrack=0; iTrack<fVertexLSF.GetNTracks(); iTrack++) {
        TRecoSpectrometerCandidate *Scand = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(ind[iTrack]);
        TVector3 Momentum = fVertexLSF.GetTrackThreeMomentum(iTrack);

        Vertex.AddTrack(ind[iTrack], Scand->GetCharge(), Momentum, Scand->GetThreeMomentumBeforeMagnet());

        TotalThreeMomentum += Momentum;
    }
    Vertex.SetTotalThreeMomentum(TotalThreeMomentum);

    // Monitoring of the vertices in the output
    FillHisto("vtxchi2", fVertexLSF.GetChi2());
    FillHisto("zvtx", fVertexLSF.GetVertexPosition().z()); // [mm]
    FillHisto("yvtx", fVertexLSF.GetVertexPosition().y()); // [mm]
    FillHisto("xvtx", fVertexLSF.GetVertexPosition().x()); // [mm]
    FillHisto("vtxcharge", Vertex.GetCharge());
    FillHisto("ptot_all",Vertex.GetTotalMomentum()*fmevtogev); // [GeV/c]

    fVertexContainer.push_back(Vertex);
}

Int_t K3pi::MatchCHOD(Int_t itrack,TRecoSpectrometerCandidate* track){

    //fCHODEvent = event;
    Double_t mindist = 99999999.;
    Double_t minchodtime = 99999999.;
    Double_t mintime = 99999999.;
    Int_t minjq1 = -1;
    Double_t dx = -99999.;
    Double_t dy = -99999.;
    Double_t minchi2 = 99999999.;
    TVector3 poschodV = AnalysisTools::GetInstance()->GetPositionAtZ(track,238960.);
    TVector3 poschodH = AnalysisTools::GetInstance()->GetPositionAtZ(track,239340.);
    Double_t xtrack = poschodV.X();
    Double_t ytrack = poschodH.Y();
    if (minjq1==-1) {
        Double_t chodParameters[6] = {99999999.,99999999.,99999.,-99999.,-99999.,-99999};
        minjq1 = MakeCandidate(xtrack,ytrack,track->GetTime(),chodParameters);
        if (minjq1>=0) {
            minchi2 = chodParameters[0];
            mintime = chodParameters[1];
            mindist = chodParameters[2];
            dx = chodParameters[3];
            dy = chodParameters[4];
            minchodtime = chodParameters[5];
        }
    }

    //fCHODCandidate->SetDistanceToHit(sqrt(mindist));
    //fCHODCandidate->SetDiscriminant(minchi2);
    //fCHODCandidate->SetCHODTime (minchodtime);
    //fCHODCandidate->SetCHODdt (mintime);
    //fCHODCandidate->SetX(dx);
    //fCHODCandidate->SetY(dy);
    //fCHODCandidate->SetGoodTrack(true);
    fCHODTime[itrack] = minchodtime;
    //Z is the average between horizontal and vertical channels
    fCHODPos[itrack].SetXYZ(dx,dy,239150);


    return minjq1;


}
Int_t K3pi::MakeCandidate(Double_t xPos, Double_t yPos, Double_t ttime, Double_t *parameters) {
    Double_t mindist = 99999999.;
    Double_t mintime = 99999999.;
    Double_t minchodtime = 99999999.;
    Int_t jminhitV = -1;
    Int_t jminhitH = -1;
    Double_t dx = -99999.;
    Double_t dy = -99999.;
    Double_t minchi2 = 99999999.;

    TClonesArray& Hits = (*(fCHODEvent->GetHits()));
    for (Int_t jhit=0; jhit<fCHODEvent->GetNHits(); jhit++) {
        TRecoCHODHit *HitV = (TRecoCHODHit*)Hits[jhit];
        Int_t counterV = HitV->GetChannelID();
        if (counterV>63) continue;
        for (Int_t ihit=0; ihit<fCHODEvent->GetNHits(); ihit++) {

            TRecoCHODHit *HitH = (TRecoCHODHit*)Hits[ihit];
            Int_t counterH = HitH->GetChannelID();

            if (counterH<64 || counterH>127) continue;

            Double_t xslab = fCHODPosV[HitV->GetChannelID()]*10;
            Double_t yslab = fCHODPosH[HitH->GetChannelID()-64]*10;

            if (GetQuadrant(counterV)!=GetQuadrant(counterH)) continue;

            Double_t tot1 = HitV->GetTimeWidth();
            Double_t tot2 = HitH->GetTimeWidth();
            Double_t slewcorrV = (tot1<15&&tot2<15) ? fCHODAllSlewSlope[counterV][(counterH-64)%16]*tot1+fCHODAllSlewConst[counterV][(counterH-64)%16] : 0.;
            Double_t slewcorrH = (tot1<15&&tot2<15) ? fCHODAllSlewSlope[counterH][counterV%16]*tot2+fCHODAllSlewConst[counterH][counterV%16] : 0.;
            Double_t tVcorr = HitV->GetTime()-slewcorrV-fCHODAllT0[counterV][(counterH-64)%16];
            Double_t tHcorr = HitH->GetTime()-slewcorrH-fCHODAllT0[counterH][counterV%16];
            Double_t dhittime = tVcorr-tHcorr;
            FillHisto("dhittime",dhittime);

            Double_t avtime = 0.5*(tVcorr+tHcorr);

            if (GetQuadrant(counterV)<=1) avtime += 0.65;
            if (GetQuadrant(counterH)==1 || GetQuadrant(counterH)==2) avtime += 0.2;

            Double_t dtime = avtime-ttime;
            Double_t dist2 = (xslab-xPos)*(xslab-xPos)+(yslab-yPos)*(yslab-yPos);
            FillHisto("hit_dist",sqrt(dist2));
            FillHisto("hit_dtime_vs_dist",dtime,sqrt(dist2));

            // Double_t chi2chod = dhittime*dhittime/(9*3*3)+dtime*dtime/(4*1.6*1.6)+dist2/(4*30*30);
            Double_t chi2chod = dhittime*dhittime/(9*3*3)+dtime*dtime/(9*7*7)+dist2/(4*13*13);
            FillHisto("hit_chi2", chi2chod);
            FillHisto("hit_chi2_vs_dtime",chi2chod,dtime);
            FillHisto("hit_chi2_vs_distance",chi2chod,sqrt(dist2));

            if (chi2chod<minchi2) {
                minchi2 = chi2chod;
                mindist = dist2;
                mintime = dtime;
                minchodtime = avtime;
                jminhitH = ihit;
                jminhitV = jhit;
                dx = xslab;
                dy = yslab;
            }
        }
    }
    parameters[0] = minchi2;
    parameters[1] = mintime;
    parameters[2] = mindist;
    parameters[3] = dx;
    parameters[4] = dy;
    parameters[5] = minchodtime;

    return jminhitH>=0?0:-1;
}

Int_t K3pi::GetQuadrant(Int_t channelid) {
    if (channelid<=63) {
        if (channelid>=0  && channelid <=15) return 0;
        if (channelid>=16 && channelid <=31) return 1;
        if (channelid>=32 && channelid <=47) return 2;
        if (channelid>=48 && channelid <=63) return 3;
    } else {
        if (channelid>=64  && channelid<=79)  return 0;
        if (channelid>=80  && channelid<=95)  return 1;
        if (channelid>=96  && channelid<=111) return 2;
        if (channelid>=112 && channelid<=127) return 3;
    }
    return -1;
}


Int_t K3pi::MatchLKr(Int_t itrack,Double_t trktime, TVector3 posatlkr){

    std::map<Double_t,int> closestlkr;
    std::map<Int_t,Double_t> distlkr;
    std::vector<Double_t> tdiff;
    std::vector<Double_t> dtrkcl;
    int ilkr=-1;
    TRecoLKrCandidate* lkr;
    double dtlkr;
    //cout << "start matching ----------" << endl;
    for (int iLKrCand=0; iLKrCand<fLKrEvent->GetNCandidates(); iLKrCand++) {
        lkr = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(iLKrCand);
        //maybe the number 3 should be changed later
        tdiff.push_back(3 - (lkr->GetClusterTime() - trktime));
        dtlkr = fabs(3 -( lkr->GetClusterTime() - trktime));
        dtrkcl.push_back(sqrt(pow(lkr->GetClusterX() - posatlkr.X(),2 ) + pow(lkr->GetClusterY() - posatlkr.Y(),2 ) )) ;
        closestlkr[dtlkr] = iLKrCand;
        //distlkr[iLKrCand] = dtrkcl;
        //cout << "i = " << iLKrCand << "dtlkr = " << dtlkr << "dtrkcl = " << dtrkcl[iLKrCand]<< endl;
        //cout << "x cand = " << lkr->GetClusterX() << "x track = " << posatlkr.X() << endl;
        //cout << "y cand = " << lkr->GetClusterY() << "y track = " << posatlkr.Y() << endl;



    }
    //cout << " final i = " << closestlkr.begin()->second << endl;
    if(fLKrEvent->GetNCandidates() == 0) return -1;
    ilkr = closestlkr.begin()->second;

    if(ilkr< 0) return -1;
    //cout << "lkr = " << ilkr << endl;
    FillHisto("lkrdist",dtrkcl[ilkr]);
    FillHisto("dtlkr",tdiff[ilkr]);

    //cout << " final ilkr = " << ilkr << "dt = " << tdiff[ilkr] << "lkrdist = " << dtrkcl[ilkr] << endl;
    if(fabs(tdiff[ilkr]) > 6. ) return -1;
    //if(dtrkcl[ilkr] > 30. ) return -1;

    return ilkr;
}

Int_t K3pi::CedarInTime(Double_t time){

    std::map<Double_t,int> closestcedar;
    int icedar=-1;
    TRecoCedarCandidate* cedar;
    double dtcedar;

    for (int i=0; i<fCedarEvent->GetNCandidates(); i++) {
        TRecoCedarCandidate* cedar = (TRecoCedarCandidate*) fCedarEvent->GetCandidate(i);
        dtcedar = fabs(time - cedar->GetTime());
        FillHisto("cedar_nsectors",cedar->GetNSectors());
        if(cedar->GetNSectors() < 5) continue;
        closestcedar[dtcedar] = i;
    }

    if(fCedarEvent->GetNCandidates() == 0) return -1; else icedar = closestcedar.begin()->second;
    if(icedar < 0) return -1;
    //cout << "cedar = " << icedar << endl;
    return icedar;
}

Int_t K3pi::GTKMatch(double chodtime,double cedartime){

    std::map<Double_t,int> closestchodgtk;
    std::map<Double_t,int> closestcedargtk;
    int igtk=-1;

    std::vector<Double_t> tdiffchod;
    std::vector<Double_t> tdiffcedar;

    for (int i=0; i<fGigaTrackerEvent->GetNCandidates(); i++) {
        TRecoGigaTrackerCandidate* gtk = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(i);
        Double_t dtchodgtk = fabs(chodtime - gtk->GetTime());
        Double_t dtcedargtk = fabs(cedartime - gtk->GetTime());
        tdiffchod.push_back(chodtime - gtk->GetTime());
        tdiffcedar.push_back(cedartime - gtk->GetTime());

        closestchodgtk[dtchodgtk] = i;
        closestcedargtk[dtcedargtk] = i;
    }

    if(fGigaTrackerEvent->GetNCandidates() == 0) return -1;
    if(closestchodgtk.begin()->second == closestcedargtk.begin()->second) igtk = closestchodgtk.begin()->second;
    if(igtk < 0) return -1;
    //cout << "gtk = " << igtk << endl;
    TRecoGigaTrackerCandidate* bestgtk = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(igtk);


    int type = bestgtk->GetType();
    double gtktime = bestgtk->GetTime();
    double gtkchi2X = bestgtk->GetChi2X();
    double gtkchi2Y = bestgtk->GetChi2Y();
    double gtkchi2T = bestgtk->GetChi2Time();
    double gtkchi2  = bestgtk->GetChi2();
    int gtknhits = bestgtk->GetNHits();
    TVector3 gtkpos= bestgtk->GetPosition(2);
    TVector3 gtkpos1= bestgtk->GetPosition(0);
    TVector3 gtkpos2= bestgtk->GetPosition(1);
    TVector3 gtkmomentum= bestgtk->GetMomentum();

    FillHisto("gtk_type", type);
    if(type < 100) return -1;

    FillHisto("dtgtkchod",tdiffchod[igtk]);
    FillHisto("dtgtkcedar",tdiffcedar[igtk]);

    FillHisto("gtk_P", gtkmomentum.Mag()*fmevtogev);
    FillHisto("gtk_chi2X", gtkchi2X);
    FillHisto("gtk_chi2Y", gtkchi2Y);
    FillHisto("gtk_chi2T", gtkchi2T);
    FillHisto("gtk_chi2", gtkchi2);
    FillHisto("gtk_Nhits", gtknhits);
    FillHisto("gtk_pos", gtkpos.X(),gtkpos.Y());

    double dx12 = gtkpos1.X() - gtkpos2.X();
    double dx23 = gtkpos2.X() - gtkpos.X() ;
    double dx13 = gtkpos1.X() - gtkpos.X() ;
    double dy12 = gtkpos1.Y() - gtkpos2.Y();
    double dy23 = gtkpos2.Y() - gtkpos.Y() ;
    double dy13 = gtkpos1.Y() - gtkpos.Y() ;
    double thetax= dx13/ (gtkpos1.Z() - gtkpos.Z());
    double thetay = dy13/ (gtkpos1.Z() - gtkpos.Z());

    FillHisto("gtk_dX_12",dx12 );
    FillHisto("gtk_dX_23",dx23 );
    FillHisto("gtk_dX_13",dx13 );
    FillHisto("gtk_dY_12",dy12 );
    FillHisto("gtk_dY_23",dy23 );
    FillHisto("gtk_dY_13",dy13 );
    FillHisto("gtk_thetax", thetax);
    FillHisto("gtk_thetay", thetay);

    if(gtkchi2X > 20) return -1;
    if(gtkchi2Y > 20) return -1;
    if(fabs(tdiffcedar[igtk]) > 2) return -1;
    if(tdiffchod[igtk] > 1.5 || tdiffchod[igtk] < -2.5) return -1;

    FillHisto("gtk_Nhits_vs_P", (Double_t)gtknhits, gtkmomentum.Mag()*fmevtogev);

    return igtk;

}


Int_t K3pi::MUV1Match(Double_t trktime, TVector3 posatMUV1){

  std::map<Double_t,int> closestMUV1;
  std::vector<Double_t> tdiff;
  std::vector<Double_t> dtrkcl;
  int iMUV1=-1;
  TRecoMUV1Candidate* MUV1;
  TRecoMUV1Candidate* goodmuv1;
  double dtMUV1;

  for (int i=0; i<fMUV1Event->GetNCandidates(); i++) {
    MUV1 = (TRecoMUV1Candidate*) fMUV1Event->GetCandidate(i);
    //maybe the number 3 should be changed later
    tdiff.push_back(MUV1->GetTime() - trktime);
    dtMUV1 = fabs(MUV1->GetTime() - trktime);
    dtrkcl.push_back(sqrt(pow(MUV1->GetX() - posatMUV1.X(),2 ) + pow(MUV1->GetY() - posatMUV1.Y(),2 ) )) ;
    closestMUV1[dtMUV1] = i;

  }
  if(fMUV1Event->GetNCandidates() == 0) return -1; else iMUV1 = closestMUV1.begin()->second;

  if(iMUV1< 0) return -1;

  goodmuv1 = (TRecoMUV1Candidate*) fMUV1Event->GetCandidate(iMUV1);
  FillHisto("dtmuv1", tdiff[iMUV1]);
  FillHisto("distmuv1", dtrkcl[iMUV1]);
  if(dtrkcl[iMUV1] > 70. ) return -1;
  if(fabs(tdiff[iMUV1]) > 10. ) return -1;

  return iMUV1;
}

Int_t K3pi::MUV2Match(Double_t trktime, TVector3 posatMUV2){

  std::map<Double_t,int> closestMUV2;
  std::vector<Double_t> tdiff;
  std::vector<Double_t> dtrkcl;
  int iMUV2=-1;
  TRecoMUV2Candidate* MUV2;
  TRecoMUV2Candidate* goodmuv2;
  double dtMUV2;

  for (int i=0; i<fMUV2Event->GetNCandidates(); i++) {
    MUV2 = (TRecoMUV2Candidate*) fMUV2Event->GetCandidate(i);
    //maybe the number 3 should be changed later
    tdiff.push_back(MUV2->GetTime() - trktime);
    dtMUV2 = fabs(MUV2->GetTime() - trktime);
    dtrkcl.push_back(sqrt(pow(MUV2->GetX() - posatMUV2.X(),2 ) + pow(MUV2->GetY() - posatMUV2.Y(),2 ) )) ;
    closestMUV2[dtMUV2] = i;

  }
  if(fMUV2Event->GetNCandidates() == 0) return -1; else iMUV2 = closestMUV2.begin()->second;

  if(iMUV2< 0) return -1;

  goodmuv2 =(TRecoMUV2Candidate*) fMUV2Event->GetCandidate(iMUV2);
  FillHisto("dtmuv2", tdiff[iMUV2]);
  FillHisto("distmuv2", dtrkcl[iMUV2]);

  if(fabs(tdiff[iMUV2]) > 10. ) return -1;
  if(dtrkcl[iMUV2] > 120. ) return -1;


  //std::cout << "sw = " << goodmuv2->GetShowerWidth() << std::endl;
  return iMUV2;
}
Int_t K3pi::MUV3Match(Double_t trktime, TVector3 posatMUV3){

  std::map<Double_t,int> closestMUV3;
  std::vector<Double_t> tdiff;
  std::vector<Double_t> dtrkcl;
  int iMUV3=-1;
  TRecoMUV3Candidate* MUV3;
  TRecoMUV3Candidate* goodmuv3;
  double dtMUV3;

  for (int i=0; i<fMUV3Event->GetNCandidates(); i++) {
    MUV3 = (TRecoMUV3Candidate*) fMUV3Event->GetCandidate(i);
    //maybe the number 3 should be changed later
    tdiff.push_back(MUV3->GetTime() - trktime);
    dtMUV3 = fabs(MUV3->GetTime() - trktime);
    dtrkcl.push_back(sqrt(pow(MUV3->GetX() - posatMUV3.X(),2 ) + pow(MUV3->GetY() - posatMUV3.Y(),2 ) )) ;
    closestMUV3[dtMUV3] = i;

  }
  if(fMUV3Event->GetNCandidates() == 0) return -1; else iMUV3 = closestMUV3.begin()->second;

  if(iMUV3< 0) return -1;

  goodmuv3 = (TRecoMUV3Candidate*)fMUV3Event->GetCandidate(iMUV3);

  FillHisto("dtmuv3", tdiff[iMUV3]);
  FillHisto("distmuv3", dtrkcl[iMUV3]);

  if(fabs(tdiff[iMUV3]) > 3. ) return -1;
  if(dtrkcl[iMUV3] > 440. ) return -1;

  //std::cout << "MUV3 = " << closestMUV3.begin()->first  << std::endl;
  //std::cout << "MUV3 = " << closestMUV3.begin()->second  << std::endl;
  //std::cout << "MUV3dtrkcl = " << dtrkcl[iMUV3]  << std::endl;


  return iMUV3;
}


Int_t K3pi::GTKAnalysis(Int_t igtk, TVector3 vtxposition){

  TRecoGigaTrackerCandidate* gtk = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(igtk);

  int type = gtk->GetType();
  double gtktime = gtk->GetTime();
  double gtkchi2X = gtk->GetChi2X();
  double gtkchi2Y = gtk->GetChi2Y();
  double gtkchi2T = gtk->GetChi2Time();
  double gtkchi2  = gtk->GetChi2();
  int gtknhits = gtk->GetNHits();
  TVector3 gtkpos1= gtk->GetPosition(0);
  TVector3 gtkpos2= gtk->GetPosition(1);
  TVector3 gtkpos3= gtk->GetPosition(2);
  TVector3 gtkmomentum= gtk->GetMomentum();

  double dp =fK3Momentum.Mag()*fmevtogev - gtkmomentum.Mag()*fmevtogev;
  double pk_k3pi =fK3Momentum.Mag()*fmevtogev;
  TVector3 pos_at_gtk3 = AnalysisTools::GetInstance()->GetKaonPositionAtZ(fK3Momentum,vtxposition,gtkpos3.Z());
  double dx = gtkpos3.X() - pos_at_gtk3.X();
  double dy = gtkpos3.Y() - pos_at_gtk3.Y();
  FillHisto("pk_vs_dpk",pk_k3pi,dp);
  FillHisto("pk_vs_dx",pk_k3pi,dx);
  FillHisto("pk_vs_dy",pk_k3pi,dy);

  FillHisto("x_vs_kthx",pos_at_gtk3.X(),fK3Momentum.X()/fK3Momentum.Z());
  FillHisto("gtk_x_vs_kthx",gtkpos3.X(),gtkmomentum.X()/gtkmomentum.Z());
  FillHisto("y_vs_kthy",pos_at_gtk3.Y(),fK3Momentum.Y()/fK3Momentum.Z());
  FillHisto("gtk_y_vs_kthy",gtkpos3.Y(),gtkmomentum.Y()/gtkmomentum.Z());

  return 1;
}
void K3pi::EndOfRunUser(){
    SaveAllPlots();

}


void K3pi::DrawPlot(){
}

K3pi::~K3pi(){
}
