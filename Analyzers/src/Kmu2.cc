#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "Kmu2.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "MM2Errors.hh"
#include "AnalysisTools.hh"
#include "TrackCandidate.hh"
#include "Geometry.hh"
#include "Event.hh"
#include "Persistency.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


Kmu2::Kmu2(Core::BaseAnalysis *ba) : Analyzer(ba, "Kmu2")
{
  fTool = AnalysisTools::GetInstance();
  //fLAVMatch = new LAVMatching();
  //fSAVMatch = new SAVMatching();
  fSpectrometerCandidate = new TRecoSpectrometerCandidate();

  RequestTree("LKr",new TRecoLKrEvent);
  // RequestTree("Spectrometer",new TRecoSpectrometerEvent);
  RequestTree("CHOD",new TRecoCHODEvent);
  // RequestTree("CHANTI",new TRecoCHANTIEvent);
  // RequestTree("RICH",new TRecoRICHEvent);
  // RequestTree("MUV1",new TRecoMUV1Event);
  // RequestTree("Cedar",new TRecoCedarEvent);
  // RequestTree("MUV2",new TRecoMUV2Event);
  // RequestTree("MUV3",new TRecoMUV3Event);
  // RequestTree("LAV",new TRecoLAVEvent);
  // RequestTree("SAC",new TRecoSACEvent);
  // RequestTree("IRC",new TRecoIRCEvent);
  RequestL0Data();


}

void Kmu2::InitOutput(){
}

void Kmu2::InitHist(){

  BookHisto(new TH1I("ngood","N good STRAW tracks;NGoodtracks",50,0,50));
  BookHisto(new TH1F("pkaon", " GTK momentum;P_{K} [MeV/c]",100,65000.,85000));
  BookHisto(new TH1F("cda", " cda ;cda [mm]", 300, 0, 300));
  BookHisto(new TH1F("zvtx", "  ;zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("pvsmm2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));

  //Cut stage 1
  BookHisto(new TH2F("1pvsmm2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  //Cut stage 2
  BookHisto(new TH2F("2pvsmm2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
  BookHisto(new TH1I("2ngamma","N isolated #gamma `s in time with the track;N_{#gamma}",10,0,10));
  BookHisto(new TH1F("2calenergy",";E_{MUV1 + MUV2 + LKr}[MeV]",200,0,100));
  BookHisto(new TH2F("2trkpvscalenergy","; P[MeV] ;E_{calorimeter}",200, 0, 100 ,200, 0, 100 ));
  BookHisto(new TH2F("2trkpvseop","; P[MeV] ;E_{tot}/P",200, 0, 100, 120, 0, 1.2 ));
  BookHisto(new TH2F("2m1frvsm2fr","; E_{MUV1}/E_{cal} ; E_{MUV2}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("2m1frvslkrfr","; E_{MUV1}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));
  BookHisto(new TH2F("2m2frvslkrfr","; E_{MUV2}/E_{cal} ; E_{LKr}/E_{cal}",120,0,1.2, 120, 0, 1.2 ));

  BookHisto(new TH2F("3pvsmm2", " Track momentum vs Missing mass squared  ;P_{track} [GeV/c];(P_{K} - P_{#pi})^{2}  [GeV^{2}/c^{4}]",200,0,100, 300, -0.15, 0.15));
}

void Kmu2::DefineMCSimple(){
}

void Kmu2::StartOfJobUser(){
}

void Kmu2::StartOfRunUser(){
}

void Kmu2::StartOfBurstUser(){
}

void Kmu2::Process(int iEvent){

  OutputState state;
  fHeader = GetRawHeader();
  fL0Data = fHeader->GetL0TPData();
  fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetOutput("GigaTrackerEvtReco.GigaTrackerEvent",state);
  fLKrEvent = (TRecoLKrEvent*)GetEvent("LKr");
  fCHODEvent = (TRecoCHODEvent*)GetEvent("CHOD");
  // fCHANTIEvent = (TRecoCHANTIEvent*)GetEvent("CHANTI");
  // fRICHEvent = (TRecoRICHEvent*)GetEvent("RICH");
  // fCedarEvent = (TRecoCedarEvent*)GetEvent("Cedar");
  // fMUV1Event = (TRecoMUV1Event*)GetEvent("MUV1");
  // fMUV2Event = (TRecoMUV2Event*)GetEvent("MUV2");
  // fMUV3Event = (TRecoMUV3Event*)GetEvent("MUV3");
  // fLAVEvent = (TRecoLAVEvent*)GetEvent("LAV");
  // fSACEvent = (TRecoSACEvent*)GetEvent("SAC");
  // fIRCEvent = (TRecoIRCEvent*)GetEvent("IRC");


  Int_t  L0DataType    = GetWithMC() ? 0x1  : GetL0Data()->GetDataType();
  Int_t  L0TriggerWord = GetWithMC() ? 0xFF : GetL0Data()->GetTriggerFlags();
  Bool_t PhysicsData   = L0DataType    & 0x1;
  //Bool_t TriggerOK     = L0TriggerWord & fTriggerMask;

  //ol_t FillPlots = PhysicsData && TriggerOK;
  Int_t RunNumber = GetWithMC() ? 0 : fHeader->GetRunID();
  Int_t BurstID   = GetWithMC() ? 0 : fHeader->GetBurstID();

  Int_t ControlTrigger = (Int_t)fTool->SelectTriggerMask(0,L0DataType,L0TriggerWord);
  Int_t PinunuTrigger  = (Int_t)fTool->SelectTriggerMask(2,L0DataType,L0TriggerWord);

  if(!ControlTrigger) return;

  const TObjArray *GoodTrack = GetOutput<TObjArray>("LKrAnalyzer.GoodLKrCandidates",state);

  FillHisto("ngood", GoodTrack->GetEntries());

  if(GoodTrack->GetEntries() == 0 ) return;

  for(int i = 0; i < GoodTrack->GetEntries(); i++){

    fkmu2track = (TrackCandidate*) GoodTrack->At(i);
    fSpectrometerCandidate = (TRecoSpectrometerCandidate*)fkmu2track->GetSpectrometer();

    if( !fkmu2track->IsGTKPresent() || !fkmu2track->IsCedarPresent() || !fkmu2track->IsCHANTIPresent() ) continue;


    fKMomentum = fkmu2track->GetGTKMomentum();
    fVertex = fkmu2track->GetGTKVertex();
    fcda = fkmu2track->GetCDA();

    Double_t ptrack = fSpectrometerCandidate->GetMomentum()*0.001;// [GeV]
    Double_t thetax = fSpectrometerCandidate->GetSlopeXBeforeMagnet();
    Double_t thetay = fSpectrometerCandidate->GetSlopeYBeforeMagnet();
    Double_t kthetax= fKMomentum.X()/fKMomentum.Z();
    Double_t kthetay= fKMomentum.Y()/fKMomentum.Z();
    Double_t input[6]= {1./ptrack,thetax,thetay,fKMomentum.Mag()*fmevtogev,kthetax,kthetay};
    vector<Double_t> par;

    for (int j = 0; j < 6;j++)
      par.push_back(input[j]);

    MM2Errors mm2eror(par,fSpectrometerCandidate);
    Double_t mm2 = mm2eror.GetMM2Muon();


    FillHisto("cda", fcda);
    FillHisto("pkaon", fKMomentum.Mag());
    FillHisto("zvtx", fVertex.Z());
    FillHisto("pvsmm2", ptrack, mm2);

    //CUTComment:: Cut stage 1 on Vertex and momentum
    //1. Closest Approached Distance > 15mm.
    //2. Zvtx to be incide of the fiducial volume of NA62 detector (110 - 160 m)
    //3. Momentum between 15 GeV/c and 65 GeV/c.
    if(ptrack < 15 || ptrack > 65) continue;
    if(fcda > 15.) continue;
    if( fVertex.Z() < FRzmin || fVertex.Z() > FRzmax) continue;
    if( fabs(fVertex.X()) < -20 || fabs(fVertex.X()) > 120) continue;
    if( fabs(fVertex.Y()) > 20) continue;

    FillHisto("1pvsmm2", ptrack, mm2);

    ///CUTComment:: Cut stage 2 LKr EoP and MIP
    if(fkmu2track->GetLKrEoP() > 0.1 || !fkmu2track->GetLKrISMIP() ) continue;
    FillHisto("2pvsmm2", ptrack, mm2);

    Int_t ngamma =fkmu2track->GetNExtraInTimeCl() ;
    FillHisto("2ngamma", ngamma);

    double energy= fmevtogev*(fkmu2track->GetLKrEnergy()+ fkmu2track->GetMUV1Energy() + fkmu2track->GetMUV2Energy()); //GeV
    double m1frac = fkmu2track->GetMUV1Energy()*fmevtogev/energy;
    double m2frac = fkmu2track->GetMUV2Energy()*fmevtogev/energy;
    double lkrfrac  = fkmu2track->GetLKrEnergy()*fmevtogev/energy;
    double eop = energy/(ptrack);

    FillHisto("2calenergy", energy);
    FillHisto("2trkpvscalenergy",ptrack, energy);
    FillHisto("2trkpvseop", ptrack, eop);
    FillHisto("2m2frvslkrfr", m2frac,lkrfrac);
    FillHisto("2m1frvslkrfr", m1frac,lkrfrac);
    FillHisto("2m1frvsm2fr", m1frac,m2frac);

    if(ngamma!=0) continue;
    FillHisto("3pvsmm2", ptrack, mm2);

    std::map<Double_t,int> photonenergy;

    for(int i = 0; i < fLKrEvent->GetNCandidates();i++){
      TRecoLKrCandidate* lkr = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(i);
      Double_t clenergy = lkr->GetClusterEnergy();
      photonenergy[clenergy] = i;
    }
    //std::cout << "--------------------------" << std::endl;
    std::map<Double_t,int>::iterator it = photonenergy.begin();
    for(; it != photonenergy.end();it++){
      //std::cout << "energy = " << it->first << "index = " << it->second << std::endl;
    }
    //std::cout << "First element = " << photonenergy.begin()->second<< std::endl;


  }
  return;
}

void Kmu2::PostProcess(){

}

void Kmu2::EndOfBurstUser(){
}

void Kmu2::EndOfJobUser(){
  SaveAllPlots();
}

void Kmu2::EndOfRunUser(){
  //SaveAllPlots();
}

void Kmu2::DrawPlot(){
}

Kmu2::~Kmu2(){
}
