#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "RICHAnalyzer.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "Parameters.hh"
#include "AnalysisTools.hh"
#include "TrackCandidate.hh"
#include "RawHeader.hh"
#include "GeometricAcceptance.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


RICHAnalyzer::RICHAnalyzer(Core::BaseAnalysis *ba) : Analyzer(ba, "RICHAnalyzer")
{

  fAnalyzerName = "RICHAnalyzer";
  RequestTree("RICH",new TRecoRICHEvent);

  Parameters *par = Parameters::GetInstance();
  par->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/run2015/database/richmirroroff.dat");
  par->LoadParameters("/afs/cern.ch/user/r/ruggierg/workspace/public/run2015/database/richmirrorpos.dat");
  par->StoreRICHParameters();
  fDeltaMisa = (Double_t **)par->GetRICHDeltaMisa();
  fMirrorPos = (Double_t ***)par->GetRICHPos();


  //Analysis tools
  fTools = AnalysisTools::GetInstance();
  fRawHeader = new RawHeader();
  fgeom = GeometricAcceptance::GetInstance();
}

void RICHAnalyzer::InitOutput(){
}

void RICHAnalyzer::InitHist(){

  BookHisto(new TH1I("NGood","N good tracks after LKr;NGoodtracks",50,0,50));

  //RICH
  BookHisto(new TH1I("RICH_Ncandidates", "RICH number of candidates", 50, 0, 50));
  BookHisto(new TH1F("RICHRadius", "RICH radius", 500, 0., 500.));
  BookHisto(new TH1F("RICHAngle", "RICH angle", 200, 0., 1.));
  //BookHisto(new TH1F("RICHMass", "Mass computed with RICH", 1000, 0., 1.));
  //BookHisto(new TH2F("RICHMvsP", "Momentum vs RICH mass", 100, 0., 100., 1000, 0., 1.));
  //BookHisto(new TH2F("RICHRvsP", "Energy as a function of particle", 100, 0., 100000., 500, 0., 500.));

  BookHisto(new TH1F("Track_P", "Track momentum; P_{track} [MeV]", 100,0, 100000));
  BookHisto(new TH2F("Track_slopes", "Slopes taken from the RICH ;dxdz[rad];dydz[rad]", 400, -0.1, 0.1, 400, -0.1, 0.1));
  BookHisto(new TH2F("Track_atRICHf","track @ RICH front plane; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("Track_atRICHmirror","track @ RICH mirror; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));

  BookHisto(new TH1I("Track_RICHNHits", " number of hits for the matched RICH track", 50, 0, 50));
  BookHisto(new TH1F("Track_RICH_choddt","",800,-50,50));
  BookHisto(new TH1F("Track_RICH_dt","",400,-100,100));
  BookHisto(new TH1F("Track_RICH_dist","",800,0,2000));
  BookHisto(new TH1F("Track_RICHRadius", "RICH radius", 300, 0., 300.));
  BookHisto(new TH1F("Track_RICHMass", "Mass computed with RICH", 1000, 0., 1000.));
  BookHisto(new TH1F("Track_RICHAngle", "RICH angle", 400, -0.1, 0.1));

  BookHisto(new TH2F("Track_atRICHf_dxdy","dx dy @ RICH front plane ; dx @ RICH [mm]; dy @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH1F("Track_RICH_dslopey", " ;#theta_{Y RICH} - #theta_{Y track} [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH1F("Track_RICH_dslopex", " ;#theta_{X RICH} - #theta_{X track} [rad]", 1000, -0.04, 0.04));
  BookHisto(new TH2F("Track_RICH_slopes", "Slopes taken from the RICH ;dxdz[rad];dydz[rad]", 1000, -0.1, 0.1, 1000, -0.1, 0.1));
  BookHisto(new TH2F("Track_RICH_dslope", "Slope difference between STRAW and RICH ; xslope_{RICH} - xslope_{STRAW} [rad]; yslope_{RICH} - yslope_{STRAW}[rad]", 1000, -0.04, 0.04, 1000, -0.04, 0.04));

  BookHisto(new TH2F("Track_RICHPvsM", "Momentum vs RICH mass", 100, 0., 100000., 1000, 0., 1000.));
  BookHisto(new TH2F("Track_RICHPvsR", "Momentum vs Radius", 100, 0., 100000., 300, 0., 300.));

  BookHisto(new TH2F("GoodTrack_atRICHmirror","track @ RICH mirror; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
  BookHisto(new TH2F("BadTrack_atRICHmirror","track @ RICH mirror; x @ RICH [mm]; y @ RICH [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));
}

void RICHAnalyzer::DefineMCSimple(){
}

void RICHAnalyzer::StartOfRunUser(){
}

void RICHAnalyzer::StartOfBurstUser(){
}

void RICHAnalyzer::Process(int iEvent){

  //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
  //if(fMCSimple.fStatus == MCSimple::kEmpty){printNoMCWarning();return;}

  fGoodRICHCandidates.Clear();
  fRICHEvent = (TRecoRICHEvent*)GetEvent("RICH");


  OutputState state;
  const TObjArray *GoodCandidates = GetOutput<TObjArray>("LKrAnalyzer.GoodLKrCandidates",state);


  int NGood = GoodCandidates->GetEntries();

  if (NGood==0)return;

  FillHisto("NGood", NGood);
  FillHisto("RICH_Ncandidates", fRICHEvent->GetNCandidates());

  for (int j=0; j< fRICHEvent->GetNCandidates();j++){

    TRecoRICHCandidate* RingCandidate = (TRecoRICHCandidate*) fRICHEvent->GetCandidate(j);

    TVector2 RingCenter= RingCandidate->GetRingCenter();
    double RingRadius = RingCandidate->GetRingRadius();
    //double RingTime    = RingCandidate->GetRingTime();
    //double NeonN       = 1.000067;
    //double RICHAngle   = TMath::ATan( RingRadius/17000. );
    //double RICHMass    = STRAW_P*0.001*sqrt( pow(NeonN*TMath::Cos(RICHAngle), 2 ) - 1.);
    //double slopex      = RingCenter.X()/17000.;
    //double slopey      = RingCenter.Y()/17000.;

    FillHisto("RICHRadius",RingRadius);
    FillHisto("RICHAngle",RingRadius/17000.);
    //FillHisto("RICHMass",RICHMass);

  }


  for(int i = 0; i < NGood; i++){

    fGoodTrack = (TrackCandidate*) GoodCandidates->At(i);

    TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*) fGoodTrack->GetSpectrometer();
    Double_t Ptrack = track->GetMomentum();
    //Double_t ttrack = fGoodTrack->GetSpectrometerTime();
    Int_t igood = -1;

    TVector3 Track_atRICHfront = fTools->GetPositionAtZ(track,fgeom->GetZRICHFrontPlane());
    TVector3 Track_atRICHmirror = fTools->GetPositionAtZ(track,fgeom->GetZRICHMirror());

    FillHisto("Track_P", Ptrack);
    FillHisto("Track_slopes", track->GetSlopeXAfterMagnet(), track->GetSlopeYAfterMagnet());
    FillHisto("Track_atRICHf", Track_atRICHfront.X(), Track_atRICHfront.Y());
    FillHisto("Track_atRICHmirror", Track_atRICHmirror.X(), Track_atRICHmirror.Y());

    igood = MatchRich(track);

    if(igood > -1){

      TRecoRICHCandidate* goodring = (TRecoRICHCandidate*) fRICHEvent->GetCandidate(igood);

      FillHisto("GoodTrack_atRICHmirror", Track_atRICHmirror.X(), Track_atRICHmirror.Y());

    } else {

      FillHisto("BadTrack_atRICHmirror", Track_atRICHmirror.X(), Track_atRICHmirror.Y());

    }



  }


}

void RICHAnalyzer::PostProcess(){

}

void RICHAnalyzer::EndOfBurstUser(){
}

void RICHAnalyzer::EndOfRunUser(){
  SaveAllPlots();
}

void RICHAnalyzer::DrawPlot(){
}

RICHAnalyzer::~RICHAnalyzer(){
}

Int_t RICHAnalyzer::MatchRich(TRecoSpectrometerCandidate* track){

  //Extrapolation to the back plane
  TVector3 extrap = fTools->GetPositionAtZ(track,237326.);
  TVector3 posTrackAtMirror = fTools->GetPositionAtZ(track,236873.);
  TVector3 straw_slope(track->GetSlopeXAfterMagnet(),track->GetSlopeYAfterMagnet(),1.);
  Int_t mirrorid = MirrorSurface(posTrackAtMirror.X(),posTrackAtMirror.Y(),0.,0); // edge effect on mirrors (default: no) ?

  Double_t partrack[4] = {track->GetMomentum(),track->GetSlopeXAfterMagnet(),track->GetSlopeYAfterMagnet(),0.13957018};
  TVector3 versorTrack = fTools->Get4Momentum(partrack).Vect().Unit();

  if (mirrorid==99) return -1; // No ring candidate if track is not hitting a mirror (no protection against edge effect)
  if (mirrorid==2) return -1; // No ring candidate if track is hitting semi-hexagonal Jura


  Double_t tracktime = track->GetTime();
  Double_t trackchodtime = fGoodTrack->GetCHODTime();
  Double_t mindist = 9999999.;
  Double_t mintime = 9999999.;
  Double_t mintimechod = 9999999.;
  Int_t minidring = -1;
  //Double_t minchi2 = 9999999.;
  TVector2 mindeltapos(-9999999.,-9999999.);
  //TVector3 pmtPos = posTrackAtMirror+(17000.)*straw_slope;
  TVector3 pmtPos = posTrackAtMirror+(-17000.)*straw_slope;

  Double_t corrx = 0;
  Double_t corry = 0;
  //Double_t corrx = posTrackAtMirror.X()<0 ? +6.0 : -7.67;
  //Double_t corry = posTrackAtMirror.X()<0 ? +10.32 : -11.1;

  //pmtPos.SetX(pmtPos.X() + fDeltaMisa[mirrorid][0] + corrx + 1.8);
  //pmtPos.SetY(pmtPos.Y() + fDeltaMisa[mirrorid][1] + corry);

  for (int j=0; j< fRICHEvent->GetNCandidates();j++){

    TRecoRICHCandidate* ring = (TRecoRICHCandidate*) fRICHEvent->GetCandidate(j);
    ring->SetEvent(fRICHEvent);

    if (ring->GetNHits()<4) continue;
    if (ring->GetRingChi2()>=10) continue;

    TVector2 RingCenter= ring->GetRingCenter();
    TVector2 deltapos(RingCenter.X() - pmtPos.X(), RingCenter.Y() - pmtPos.Y());
    //TVector2 deltapos(RingCenter.X() - .X(), RingCenter.Y() - pmtPos.Y());
    Double_t dist = deltapos.Mod();
    Double_t RingCenterR = ring->GetRingRadius();
    Double_t RingTime    = ring->GetRingTime();
    Double_t dtime = RingTime - tracktime - 0.6;
    Double_t dtimechod = RingTime - trackchodtime - 0.6;

    if (dtimechod <  mintimechod) {
      //    minchi2 = chi2rich;
      mindist = dist;
      mintime = dtime;
      mintimechod = dtimechod;
      minidring = j;
      mindeltapos = deltapos;
    }
  }

  if(minidring > -1){
    TRecoRICHCandidate* minring = (TRecoRICHCandidate*) fRICHEvent->GetCandidate(minidring);

    TVector2 center= minring->GetRingCenter();
    Double_t nhits = minring->GetNHits();
    Double_t radius= minring->GetRingRadius();
    Double_t time  = minring->GetRingTime();
    Double_t slopex      = center.X()/17020.;
    Double_t slopey      = center.Y()/17020.;
    Double_t NeonN       = 1.000067;
    Double_t RICHAngle   = TMath::ATan( radius/17000. );
    // Double_t RICHMass    = track->GetMomentum()*sqrt( fabs(1/pow(NeonN*TMath::Cos(RICHAngle), 2 ) - 1.));
    Double_t RICHMass    = track->GetMomentum()*sqrt(pow(NeonN*(radius/17000.), 2 ) - 1.);
    //cout << "AtanAngle = " << RICHAngle << "  Angle = " << radius/17000 << endl;
    //cout << "P = " << track->GetMomentum() << "  CosRICHAngle = " << TMath::Cos(RICHAngle) << "  1/(n*cos)^2 - 1  = " << 1/pow(NeonN*TMath::Cos(RICHAngle), 2 ) - 1. <<"Mass ==" << RICHMass << endl;
     //Double_t RICHMass    = track->GetMomentum()*sqrt( pow(NeonN*TMath::Cos(RICHAngle), 2 ) - 1.);
    FillHisto("Track_RICH_choddt", mintimechod);
    FillHisto("Track_RICH_dt", mintime);

    if(fabs(mintimechod) > 2) return -1;

    FillHisto("Track_RICH_dslopex", slopex - track->GetSlopeXAfterMagnet());
    FillHisto("Track_RICH_dslopey", slopey - track->GetSlopeYAfterMagnet());
    FillHisto("Track_RICH_slopes", slopex, slopey);
    FillHisto("Track_RICH_dslope", slopex - track->GetSlopeXAfterMagnet(), slopey- track->GetSlopeYAfterMagnet());


    FillHisto("Track_RICH_dist", mindist);
    //FillHisto("Track_atRICHf_dxdy", mindeltapos.X(), mindeltapos.Y());
    FillHisto("Track_atRICHf_dxdy",center.X() - pmtPos.X(), center.Y() - pmtPos.Y());
    //FillHisto("Track_atRICHf_dxdy", posTrackAtMirror.X(), posTrackAtMirror.Y());


    FillHisto("Track_RICHNHits",nhits);
    FillHisto("Track_RICHRadius",radius);
    FillHisto("Track_RICHAngle",RICHAngle);
    FillHisto("Track_RICHMass",RICHMass);
    FillHisto("Track_RICHPvsM",track->GetMomentum() , RICHMass);
    FillHisto("Track_RICHPvsR",track->GetMomentum() , radius);
    //FillHisto("RICHRvsp",RingCandidate->GetRingRadius(),STRAWCandidate->GetMomentum());


  }

  return minidring;

}
///////////////////////
// Mirror reflection //
///////////////////////
void RICHAnalyzer::MirrorVector(TVector3 *vec, TVector3 *axis) {
  TVector3 S = ((vec->Dot(*axis))/(axis->Mag2()))*(*axis);
  TVector3 d = S-*vec;
  TVector3 ret = S+d;
  *vec = ret.Unit();
}

//void RICHAnalyzer::ReflectedRingCenterAtPmt(TVector3 *slope, TVector3 *posatmirror) {
//    TVector3 S = ((vec->Dot(*axis))/(axis->Mag2()))*(*axis);
//    TVector3 d = S-*vec;
//    TVector3 ret = S+d;
//    *vec = ret.Unit();
//}

Int_t RICHAnalyzer::MirrorSurface(Double_t xin, Double_t yin, Double_t SafeFactor, Bool_t flag) {
  Int_t imirror=0;
  for (imirror=0; imirror<25; imirror++) {
    if (imirror==0 || imirror==7 || imirror==18 || imirror==19 ) continue;
    if (flag==1 && imirror==2) continue;

    Double_t ac1=(fMirrorPos[imirror][0][1]-fMirrorPos[imirror][1][1])/(fMirrorPos[imirror][0][0]-fMirrorPos[imirror][1][0]);
    Double_t bcost1=(fMirrorPos[imirror][1][1]*fMirrorPos[imirror][0][0]-fMirrorPos[imirror][1][0]*fMirrorPos[imirror][0][1])/(fMirrorPos[imirror][0][0]-fMirrorPos[imirror][1][0]);
    Double_t ac3=(fMirrorPos[imirror][2][1]-fMirrorPos[imirror][3][1])/(fMirrorPos[imirror][2][0]-fMirrorPos[imirror][3][0]);
    Double_t bcost3=(fMirrorPos[imirror][3][1]*fMirrorPos[imirror][2][0]-fMirrorPos[imirror][3][0]*fMirrorPos[imirror][2][1])/(fMirrorPos[imirror][2][0]-fMirrorPos[imirror][3][0]);
    Double_t ac4=(fMirrorPos[imirror][3][1]-fMirrorPos[imirror][4][1])/(fMirrorPos[imirror][3][0]-fMirrorPos[imirror][4][0]);
    Double_t bcost4=(fMirrorPos[imirror][4][1]*fMirrorPos[imirror][3][0]-fMirrorPos[imirror][4][0]*fMirrorPos[imirror][3][1])/(fMirrorPos[imirror][3][0]-fMirrorPos[imirror][4][0]);
    Double_t ac6=(fMirrorPos[imirror][5][1]-fMirrorPos[imirror][0][1])/(fMirrorPos[imirror][5][0]-fMirrorPos[imirror][0][0]);
    Double_t bcost6=(fMirrorPos[imirror][0][1]*fMirrorPos[imirror][5][0]-fMirrorPos[imirror][0][0]*fMirrorPos[imirror][5][1])/(fMirrorPos[imirror][5][0]-fMirrorPos[imirror][0][0]);
    Double_t ConvSafeFactor1=SafeFactor*sqrt(1.+pow(ac1,2));
    Double_t ConvSafeFactor3=SafeFactor*sqrt(1.+pow(ac3,2));
    Double_t ConvSafeFactor4=SafeFactor*sqrt(1.+pow(ac4,2));
    Double_t ConvSafeFactor6=SafeFactor*sqrt(1.+pow(ac6,2));

    if (imirror!=2) {
      if ((yin<=ac1*xin+bcost1-ConvSafeFactor1) &&
          (xin<=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.-SafeFactor) &&
          (yin>=ac3*xin+bcost3+ConvSafeFactor3) &&
          (yin>=ac4*xin+bcost4+ConvSafeFactor4) &&
          (xin>=(fMirrorPos[imirror][4][0]+fMirrorPos[imirror][5][0])/2.+SafeFactor) &&
          (yin<=ac6*xin+bcost6-ConvSafeFactor6) && (imirror<23)) return imirror;
    } else {
      if ((yin<=ac1*xin+bcost1-ConvSafeFactor1) &&
          (xin<=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.-SafeFactor) &&
          (yin>=ac3*xin+bcost3+ConvSafeFactor3) &&
          (yin>=ac4*xin+bcost4+ConvSafeFactor4) &&
          (xin>=0.+SafeFactor) &&
          (yin<=ac6*xin+bcost6-ConvSafeFactor6) && (imirror<23)) return imirror;
    }

    if (flag==1) {
      if (imirror==23 || imirror==24){
        Double_t acs1=(fMirrorPos[imirror][1][1]-fMirrorPos[imirror][0][1])/(fMirrorPos[imirror][1][0]-fMirrorPos[imirror][0][0]);
        Double_t bcosts1=(fMirrorPos[imirror][0][1]*fMirrorPos[imirror][1][0]-fMirrorPos[imirror][0][0]*fMirrorPos[imirror][1][1])/(fMirrorPos[imirror][1][0]-fMirrorPos[imirror][0][0]);
        Double_t acs2=(fMirrorPos[imirror][3][1]-fMirrorPos[imirror][2][1])/(fMirrorPos[imirror][3][0]-fMirrorPos[imirror][2][0]);
        Double_t bcosts2=(fMirrorPos[imirror][2][1]*fMirrorPos[imirror][3][0]-fMirrorPos[imirror][2][0]*fMirrorPos[imirror][3][1])/(fMirrorPos[imirror][3][0]-fMirrorPos[imirror][2][0]);
        SafeFactor=-4.;
        Double_t ConvSafeFactors1=SafeFactor*sqrt(1.+pow(acs1,2));
        Double_t ConvSafeFactors2=SafeFactor*sqrt(1.+pow(acs2,2));
        if ((yin<=acs1*xin+bcosts1+ConvSafeFactors1) && (xin>=(fMirrorPos[imirror][0][0]+fMirrorPos[imirror][3][0])/2.-SafeFactor) && (yin>=acs2*xin+bcosts2-ConvSafeFactors2) && (xin<=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.+SafeFactor) && (imirror==23)) return imirror;
        if ((yin<=acs1*xin+bcosts1+ConvSafeFactors1) && (xin<=(fMirrorPos[imirror][0][0]+fMirrorPos[imirror][3][0])/2.+SafeFactor) && (yin>=acs2*xin+bcosts2-ConvSafeFactors2) && (xin>=(fMirrorPos[imirror][1][0]+fMirrorPos[imirror][2][0])/2.-SafeFactor) && (imirror==24)) return imirror;
      }
    }

  }
  imirror=99;
  return imirror;
}
