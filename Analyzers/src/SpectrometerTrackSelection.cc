#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "SpectrometerTrackSelection.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"

#include "Parameters.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;
using namespace Parameters;

/// \class SpectrometerTrackSelection
/// \Brief
/// Loop over all STRAW Spectrometer track candidates
/// Cut applied on fake tracks
/// Selection of the good tracks
/// \EndBrief
///
/// \Detailed
/// Detailed description of your Analyzer\n\n
/// For examples of working Analyzer you can have a look at the examples in the Examples/ directory:\n
/// LKrPhotonMC \n
/// Pi0Reconstruction \n
/// SkimmingNoStrawMuonsTracks \n
/// Or the framework analyzers that can be found in the Analyzers/ directories: \n
/// CedarMCTester \n
/// VertexCDA \n
/// \n
/// All the following classes are available for you to use. Check their documentation for more information:\n
/// NA62Analysis::manip::TermManip \n
/// NA62Analysis::Analyzer \n
/// NA62Analysis::CounterHandler \n
/// NA62Analysis::DetectorAcceptance \n
/// NA62Analysis::EventFraction \n
/// NA62Analysis::MCSimple \n
/// NA62Analysis::NeuralNetwork \n
/// NA62Analysis::ParticleInterface \n
/// NA62Analysis::ParticleTree \n
/// NA62Analysis::StringBalancedTable \n
/// NA62Analysis::StringTable \n
/// NA62Analysis::UserMethods \n
/// NA62Analysis::Verbose \n
///
/// You might also be interested in checking the documentation for the following classes. However you should not
/// in general have to create instances of these. If necessary a pointer to the existing instance is usually
/// available or provided by specific methods.\n
/// NA62Analysis::Core::IOHisto \n
/// NA62Analysis::Core::IOTree \n
/// NA62Analysis::Core::IOHandler \n
/// NA62Analysis::Core::HistoHandler \n
/// NA62Analysis::Core::HistoHandler::Iterator \n
/// NA62Analysis::Core::PrimitiveReader \n
///
/// \EndDetailed

SpectrometerTrackSelection::SpectrometerTrackSelection(Core::BaseAnalysis *ba) : Analyzer(ba, "SpectrometerTrackSelection")
{

  RequestTree("Spectrometer", new TRecoSpectrometerEvent);


}

void SpectrometerTrackSelection::InitOutput(){

  RegisterOutput("Tracks",&fTracks);

}

void SpectrometerTrackSelection::InitHist(){

  BookHisto(new TH1I("NTracks_allEvent","All Events",30,0,30));
  BookHisto(new TH1I("NTracks","After preselection; N tracks",30,0,30));
  BookHisto(new TH1D("Track_momentum_all","track (fake + good) momentum; P [GeV/c]",200,0.,100.));
  BookHisto(new TH1D("Track_charge_all","track (fake + good) charge; Q",6,-3,3));
  BookHisto(new TH1I("Track_Nchambers_all","track (fake + good) N chambers; N chambers",10,0,10));
  BookHisto(new TH1D("Ppatternrecognition","track Ppattern_recognition; Ppr [GeV/c]",200,0.,100.));
  BookHisto(new TH1D("Track_momentum","track momentum before cut; P [GeV/c]",200,0.,100.));
  BookHisto(new TH1D("Track_fitquality","Ppattern_recognition - Pfit; #DeltaP [GeV/c]",400,-100.,100.));
  BookHisto(new TH1D("Track_Chi2","#chi^{2} fit; #chi^{2}",400,-100.,100.));
  BookHisto(new TH1D("Track_charge","track charge; Q",6,-3,3));
  BookHisto(new TH1I("Track_Nchambers","track N chambers; N chambers",10,0,10));
  BookHisto(new TH1D("GoodTrack_momentum","track momentum after cut; P [GeV/c]",200,0.,100.));
  BookHisto(new TH1D("GoodTrack_momentum_scale","track momentum after cut; P [GeV/c]",200,0.,100.));
  BookHisto(new TH1I("NGoodTracks",";Good Tracks",30,0,30));

}

void SpectrometerTrackSelection::DefineMCSimple(){

}

void SpectrometerTrackSelection::StartOfRunUser(){

}

void SpectrometerTrackSelection::StartOfBurstUser(){

}

void SpectrometerTrackSelection::Process(int iEvent){

  //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
  //if(fMCSimple.fStatus == MCSimple::kEmpty){printNoMCWarning();return;}

  fTracks.Clear();

  TRecoSpectrometerEvent *SpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");

  FillHisto("NTracks_allEvent",SpectrometerEvent->GetNCandidates());

  if (SpectrometerEvent->GetNCandidates() == 0 || SpectrometerEvent->GetNCandidates() > 10) return;

  int NTracks = SpectrometerEvent->GetNCandidates();

  FillHisto("NTracks",NTracks);


  int nGoodTracks = 0;
  for (int i=0; i < NTracks; i++) {

    TRecoSpectrometerCandidate *track = (TRecoSpectrometerCandidate*)SpectrometerEvent->GetCandidate(i);

    FillHisto("Track_momentum_all",mevtogev*track->GetMomentum());
    FillHisto("Track_charge_all",track->GetCharge());
    FillHisto("Track_Nchambers_all",track->GetNChambers());

    // fake track
    int *track_hits_indexes = track->GetHitsIndexes();
    int nHits_incommom = 0;
    bool isHit_incommon = false;
    for (int j=0; j < NTracks; j++) {

      TRecoSpectrometerCandidate *track2 = (TRecoSpectrometerCandidate*)SpectrometerEvent->GetCandidate(j);
      if (i==j) continue;
      int *track2_hits_indexes = track2->GetHitsIndexes();

      for (int itrack1=0; itrack1 < track->GetNHits(); itrack1++) {
        for (int itrack2=0; itrack2 < track2->GetNHits(); itrack2++) {
          if (track_hits_indexes[itrack1] == track2_hits_indexes[itrack2]) nHits_incommom++;
        }
      }
    }

    if (nHits_incommom > 1) isHit_incommon = true;

    bool fake = false;
    if (track->GetNChambers() == 3 && isHit_incommon) fake = true;
    if (track->GetNChambers() == 3 && track->GetChi2() > 30) fake = true;

    bool good = true;

    if (!fake) {

      // good track
      double P_pattreco = mevtogev*track->GetMomentumBeforeFit();
      double P_fit = mevtogev*track->GetMomentum();
      double chi2 = track->GetChi2();
      int charge = track->GetCharge();
      int nChambers = track->GetNChambers();

      FillHisto("Ppatternrecognition",P_pattreco);
      FillHisto("Track_momentum",P_fit);
      FillHisto("Track_fitquality",P_pattreco - P_fit);
      FillHisto("Track_Chi2",chi2);
      FillHisto("Track_charge",charge);
      FillHisto("Track_Nchambers",nChambers);

      if (chi2 > 20.) good = false;
      if (TMath::Abs(P_pattreco - P_fit) > 20.) good = false;
      if (nChambers < 4) good = false;

      if (good) FillHisto("GoodTrack_momentum",P_fit);

      // momentum scale
      track->SetMomentum(mevtogev*track->GetMomentum());
      track->SetMomentum(1.0011*(1 + charge*2.15e-05*track->GetMomentum())*track->GetMomentum());

      if (good) FillHisto("GoodTrack_momentum_scale",track->GetMomentum());

      if (good) nGoodTracks ++;
    }

    else good = false;

    STRAWCandidate *trackcandidate = new STRAWCandidate( );
    trackcandidate->SetSTRAWCandidate(track);
    trackcandidate->SetIsFake(fake);
    trackcandidate->SetIsGood(good);
    fTracks.Add(trackcandidate);


  }

  FillHisto("NGoodTracks",nGoodTracks);

  SetOutputState("Tracks",kOValid);
}

void SpectrometerTrackSelection::PostProcess(){

}

void SpectrometerTrackSelection::EndOfBurstUser(){

}

void SpectrometerTrackSelection::EndOfRunUser(){

  SaveAllPlots();

}

void SpectrometerTrackSelection::DrawPlot(){

}
