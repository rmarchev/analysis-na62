#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "LKrAnalyzer.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "Geometry.hh"
#include "Parameters.hh"
#include "AnalysisTools.hh"
#include "TrackCandidate.hh"
#include "TrackSTRAWCandidate.hh"
#include "RawHeader.hh"
#include "LKrAuxClusterReco.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;


LKrAnalyzer::LKrAnalyzer(Core::BaseAnalysis *ba) : Analyzer(ba, "LKrAnalyzer")
{


  fAnalyzerName = "LKrAnalyzer";
  RequestTree("LKr",new TRecoLKrEvent);
  RequestTree("MUV1",new TRecoMUV1Event);
  RequestTree("MUV2",new TRecoMUV2Event);
  RequestTree("MUV3",new TRecoMUV3Event);

  //Analysis tools
  fTools = AnalysisTools::GetInstance();
  fRawHeader = new RawHeader();
  fLKrAuxClusterReco = new LKrAuxClusterReco();
  fGoodTrack = new TrackCandidate();

}

void LKrAnalyzer::InitOutput(){

  RegisterOutput("GoodLKrCandidates",&fGoodLKrCandidates);

}

void LKrAnalyzer::InitHist(){

  BookHisto(new TH1I("NGood","N good tracks after CHOD;NGoodtracks",50,0,50));

  BookHisto(new TH1F("Clusters_n","",30,0,30));
  BookHisto(new TH1F("Clusters_time","",600,-100,500));
  BookHisto(new TH1F("Clusters_ddead","",300,0,3000));
  BookHisto(new TH2F("Clusters_XY","",128, -1260.32, 1260.32, 128, -1260.32, 1260.32));
  BookHisto(new TH2F("EClusters_vs_NCell_raw","",300,0, 100000, 150, 0, 150));

  BookHisto(new TH2F("EClusters_vs_NCell","",300,0, 100000, 150, 0, 150));
  BookHisto(new TH2F("EClusters_vs_NCell_MIP","",300,0, 100000, 150, 0, 150));
  BookHisto(new TH2F("EClusters_vs_NCell_EM","",300,0, 100000, 150, 0, 150));
  BookHisto(new TH2F("EClusters_vs_NCell_Other","",300,0, 100000, 150, 0, 150));
  BookHisto(new TH2F("NCellEnergyratiovsSeedEnergyRatio_raw",";E_{clus}/E_{seed};N_{cells}/E_{clus}",110,0,1.1,400,-20,20));

  BookHisto(new TH1I("IsMIP"," Cluster is MIP",4,0,4));
  BookHisto(new TH1F("MipClusterEnergy","",500,0,10000));
  BookHisto(new TH2F("MipClusterXY","",128,-1260.32,1260.32,128,-1260.32,1260.32));

  BookHisto(new TH1I("IsEM"," Cluster is EM",4,0,4));
  BookHisto(new TH1F("EMClusterEnergy","",200,0,100000));
  BookHisto(new TH2F("EMClusterXY","",128,-1260.32,1260.32,128,-1260.32,1260.32));
  BookHisto(new TH2F("NCellEnergyratiovsSeedEnergyRatio_nomip",";E_{clus}/E_{seed};N_{cells}/E_{clus}",110,0,1.1,200,0,20));

  BookHisto(new TH1I("IsOther"," Cluster is Other",20,0,20));
  BookHisto(new TH1F("OtherClusterEnergy","",200,0,100000));
  BookHisto(new TH2F("OtherClusterXY","",128,-1260.32,1260.32,128,-1260.32,1260.32));

  BookHisto(new TH1F("Track_Clusters_distance","",800,0,2000));
  BookHisto(new TH1F("Track_Clusters_dtchod","",800,-200,200));

  BookHisto(new TH2F("Track_atLKr","track @ LKr; x @ LKr [mm]; y @ LKr [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));

  BookHisto(new TH1F("Track_Clusters_dt","",400,-200,200));

  BookHisto(new TH1F("Associated_Clusters_Energy",";E_{LKr} cluster matching track [MeV]",4500,0,90000));
  BookHisto(new TH1F("Associated_Clusters_EnergyRatio",";E/p",120,0,1.2));
  BookHisto(new TH2F("Associated_Clusters_EnergyRatio_vs_P","; P[MeV] ;E_{LKr}/P",200, 0, 100000, 120, 0, 1.2 ));

  BookHisto(new TH1F("muv3dt","",400,-200,200));
  BookHisto(new TH1F("muv3dist","",600,0,1200));


  BookHisto(new TH1I("TrackIsMIP"," Track is MIP",4,0,4));
  BookHisto(new TH1I("TrackIsEM"," Track is EM",4,0,4));
  BookHisto(new TH1I("TrackIsOther"," Track is Other",4,0,4));
  BookHisto(new TH1I("NextraClusters"," N extra clusters",30,0,30));
  BookHisto(new TH1I("NInTimeClusters"," N in time clusters",30,0,30));
  BookHisto(new TH1I("NInTimeClustersAux"," N in time clusters",30,0,30));


  BookHisto(new TH2F("ExtraCl_dist_vs_dt","",300,0,3000,400,-200,200));
  BookHisto(new TH2F("ExtraCl_ecluster_vs_dt","",4500,0,90000,400,-200,200));

  BookHisto(new TH2F("ExtraCl_dist_vs_dt_after","",300,0,3000,400,-200,200));
  BookHisto(new TH2F("ExtraCl_ecluster_vs_dt_after","",4500,0,90000,400,-200,200));

  BookHisto(new TH2F("ExtraClAux_dist_vs_dt","",300,0,3000,400,-200,200));
  BookHisto(new TH2F("ExtraClAux_ecluster_vs_dt","",4500,0,90000,400,-200,200));

  BookHisto(new TH2I("NMIP_vs_NCluster",";NMIP;NCandidates ",10,0,10,10,0,10));
  BookHisto(new TH2I("NMIP_vs_NEM",";NMIP;NEM ",10,0,10,10,0,10));
  BookHisto(new TH2I("NMIP_vs_Nother",";NMIP;Nother ",10,0,10,10,0,10));
  BookHisto(new TH2I("NEM_vs_Nother"," ;NEM;Nother",10,0,10,10,0,10));


  BookHisto(new TH2F("NonAssociated_atLKr","track @ LKr; x @ LKr [mm]; y @ LKr [mm]", 128,-1260.32,1260.32,128,-1260.32,1260.32 ));


}

void LKrAnalyzer::DefineMCSimple(){

}

void LKrAnalyzer::StartOfJobUser(){
}

void LKrAnalyzer::StartOfRunUser(){
}

void LKrAnalyzer::StartOfBurstUser(){
}

void LKrAnalyzer::Process(int iEvent){

  fNEMClusters=0;
  fNMIPClusters=0;
  fNOtherClusters=0;
  fTrackEM=false;
  fTrackMIP=false;
  fTrackOther=false;

  //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
  //if(fMCSimple.fStatus == MCSimple::kEmpty){printNoMCWarning();return;}
  fGoodLKrCandidates.Clear();
  fLKrEvent = (TRecoLKrEvent*)GetEvent("LKr");
  fMUV1Event = (TRecoMUV1Event*)GetEvent("MUV1");
  fMUV2Event = (TRecoMUV2Event*)GetEvent("MUV2");
  fMUV3Event = (TRecoMUV3Event*)GetEvent("MUV3");


  OutputState state;

  const TObjArray *GoodCandidates = GetOutput<TObjArray>("CHODAnalyzer.GoodCHODCandidates",state);

  int NGood = GoodCandidates->GetEntries();
  FillHisto("NGood", NGood);

  if(NGood < 1 ) return;
  if(fLKrEvent->GetNHits() < 1) return;

  FillHisto("Clusters_n", fLKrEvent->GetNCandidates());
  Int_t runid = fRawHeader->GetRunID();
  Bool_t mcflag = false;

  fTrackMIP   = false;
  fTrackEM    = false;
  fTrackOther = false;

  ClusterAnalysis(mcflag, runid,fLKrEvent);



  for(int i = 0; i < NGood; i++){

    fGoodTrack = (TrackCandidate*) GoodCandidates->At(i);

    fSTRAW = fGoodTrack->GetSTRAW();

    TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*)fSTRAW->GetSpectrometer();

    Double_t ttrack = fSTRAW->GetTime();
    TVector3 Track_atLKr = fTools->GetPositionAtZ(track,zLKr);
    TVector3 Track_atMUV1 = fTools->GetPositionAtZ(track,zMUV1);
    TVector3 Track_atMUV2 = fTools->GetPositionAtZ(track,zMUV2);
    TVector3 Track_atMUV3 = fTools->GetPositionAtZ(track,zMUV3);

    Bool_t LKrassoc= false;
    LKrassoc = AssociateCluster(fGoodTrack, Track_atLKr);
    //int lkrindex  = LKrMatch(fGoodTrack->GetCHODTime(),Track_atLKr);
    int MUV1index = MUV1Match(fGoodTrack->GetCHODTime(),Track_atMUV1);
    int MUV2index = MUV2Match(fGoodTrack->GetCHODTime(),Track_atMUV2);
    int MUV3index = MUV3Match(fGoodTrack->GetCHODTime(),Track_atMUV3);

    if(LKrassoc){
    //if(lkrindex >=0){


      TRecoLKrCandidate *MatchedCluster = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(fMatchedLKrIndex);
      //std::cout << " Track cluster info -------------------------------------" << std::endl;
      //std::cout << " index == " << fMatchedLKrIndex << " energy = " << MatchedCluster->GetClusterEnergy() << "time == " << MatchedCluster->GetClusterTime() << "chod time = " << fGoodTrack->GetCHODTime() << " x = " << MatchedCluster->GetClusterX() << "y = " << MatchedCluster->GetClusterY() << std::endl;
      FillHisto("Track_Clusters_dt", MatchedCluster->GetClusterTime() - ttrack - 1.2);
      FillHisto("Track_atLKr", Track_atLKr.X(), Track_atLKr.Y() );
      FillHisto("Associated_Clusters_Energy",MatchedCluster->GetClusterEnergy());
      FillHisto("Associated_Clusters_EnergyRatio",MatchedCluster->GetClusterEnergy()/track->GetMomentum());
      FillHisto("Associated_Clusters_EnergyRatio_vs_P",track->GetMomentum(),MatchedCluster->GetClusterEnergy()/track->GetMomentum());
      FillHisto("EClusters_vs_NCell", MatchedCluster->GetClusterEnergy(), MatchedCluster->GetNCells() );

      Bool_t closecl = false;
      closecl = CloseCluster(fMatchedLKrIndex, Track_atLKr.X(), Track_atLKr.Y(), fLKrEvent);
      if(closecl){continue;}


      FillHisto("TrackIsMIP", (Int_t)fTrackMIP);
      FillHisto("TrackIsEM",  (Int_t)fTrackEM);
      FillHisto("TrackIsOther",  (Int_t)fTrackOther);
      FillHisto("NextraClusters", fLKrEvent->GetNCandidates() - 1);


      if(fTrackMIP){
        FillHisto("EClusters_vs_NCell_MIP", MatchedCluster->GetClusterEnergy(), MatchedCluster->GetNCells() );
      }

      if(fTrackEM){
        FillHisto("EClusters_vs_NCell_EM", MatchedCluster->GetClusterEnergy(), MatchedCluster->GetNCells() );
      }


      if(fTrackOther){
        FillHisto("EClusters_vs_NCell_Other", MatchedCluster->GetClusterEnergy(), MatchedCluster->GetNCells() );
      }

      //cout << "Before time check !!! " << endl;
      // fInTimeClusters = HowManyInTime(fGoodTrack, Track_atLKr.X(), Track_atLKr.Y(),fMatchedLKrIndex);
      HowManyInTime(fGoodTrack, Track_atLKr.X(), Track_atLKr.Y(),fMatchedLKrIndex);
      //cout << "Aux reco ------------------------------" << endl;
      // if(fInTimeClusters == 0){
      if(fGoodTrack->GetNExtraInTimeCl() == 0){
        // fInTimeClustersAux = HowManyInTimeAuxReco(fGoodTrack, Track_atLKr.X(), Track_atLKr.Y(),fMatchedLKrIndex);
        HowManyInTimeAuxReco(fGoodTrack, Track_atLKr.X(), Track_atLKr.Y(),fMatchedLKrIndex);
        //FillHisto("NInTimeClustersAux", fInTimeClustersAux);
      FillHisto("NInTimeClustersAux", fGoodTrack->GetNExtraInTimeClAux());
      }

      //if(fInTimeClusters != fInTimeClustersAux)
      //cout << "ERRRRRRRRRRRRORRRRRRRRRRRRRRRRRRRRRRRRRRR ----------- nintime =" << fInTimeClusters << " nintimeAux = " << fInTimeClustersAux << endl;
      // FillHisto("NInTimeClusters", fInTimeClusters);
      FillHisto("NInTimeClusters", fGoodTrack->GetNExtraInTimeCl());
      Double_t clusterwidth = TMath::Sqrt(pow(MatchedCluster->GetClusterRMSX(),2) + pow(MatchedCluster->GetClusterRMSY(),2));
      fGoodTrack->SetLKr(MatchedCluster, fMatchedLKrIndex);
      fGoodTrack->SetLKrMatchedX(Track_atLKr.X());
      fGoodTrack->SetLKrMatchedY(Track_atLKr.Y());
      fGoodTrack->SetLKrEoP(MatchedCluster->GetClusterEnergy()/track->GetMomentum());
      fGoodTrack->SetLKrEnergy(MatchedCluster->GetClusterEnergy());
      fGoodTrack->SetLKrISMIP(fTrackMIP);
      fGoodTrack->SetLKrISEM(fTrackEM);
      fGoodTrack->SetLKrISOther(fTrackOther);
      fGoodTrack->SetLKrShowerWidth(clusterwidth);
      //fGoodTrack->SetNExtraInTimeCl(fInTimeClusters);
      //fGoodTrack->SetNExtraInTimeClAux(fInTimeClustersAux);


      fGoodLKrCandidates.Add(fGoodTrack);


    } else {

      FillHisto("NonAssociated_atLKr", Track_atLKr.X(), Track_atLKr.Y() );

    }
  }

  FillHisto("NMIP_vs_NEM",fNMIPClusters,fNEMClusters);
  FillHisto("NMIP_vs_Nother",fNMIPClusters,fNOtherClusters);
  FillHisto("NEM_vs_Nother",fNEMClusters,fNOtherClusters);
  FillHisto("NMIP_vs_NCluster",fNMIPClusters,fLKrEvent->GetNCandidates());


}

void LKrAnalyzer::PostProcess(){

}

void LKrAnalyzer::EndOfBurstUser(){

}

void LKrAnalyzer::EndOfRunUser(){

}
void LKrAnalyzer::EndOfJobUser(){
  SaveAllPlots();
}

void LKrAnalyzer::DrawPlot(){

}

void LKrAnalyzer::ClusterAnalysis(Bool_t mcflag, Int_t runid, TRecoLKrEvent* event) {

  for (int iClus=0; iClus < event->GetNCandidates(); iClus++) {

    TRecoLKrCandidate* thisCluster = (TRecoLKrCandidate*) event->GetCandidate(iClus);

    Double_t Eclus = thisCluster->GetClusterEnergy();
    Double_t tclus = thisCluster->GetClusterTime();
    Double_t ncell = thisCluster->GetNCells();
    Double_t ddead = thisCluster->GetClusterDDeadCell();
    Double_t clusx = thisCluster->GetClusterX();
    Double_t clusy = thisCluster->GetClusterY();

    //if (!mcflag) fTools->ClusterCorrections(runid,thisCluster);


    FillHisto("Clusters_time", tclus);

    FillHisto("Clusters_XY", clusx, clusy );
    FillHisto("Clusters_ddead", ddead);
    FillHisto("EClusters_vs_NCell_raw",Eclus,ncell);

    if(ncell > 2 )
      FillHisto("NCellEnergyratiovsSeedEnergyRatio_raw",thisCluster->GetClusterSeedEnergy()/Eclus, (ncell - 2.21)/(0.93*Eclus*0.001));

    Bool_t isMIPCluster = SelectMIPCluster(iClus,thisCluster,"");
    Bool_t isEMCluster = SelectEMCluster(iClus,thisCluster,"");
    Bool_t isOtherCluster = SelectOtherCluster(iClus,thisCluster,isMIPCluster,isEMCluster,"");


    //cout << isMIPCluster << isEMCluster<< isOtherCluster << endl;
    FillHisto("IsMIP", isMIPCluster);
    FillHisto("IsEM", isEMCluster);
    FillHisto("IsOther", isOtherCluster);


  }

}
Bool_t LKrAnalyzer::AssociateCluster( TrackCandidate* cand, TVector3 Extrap_track){

  std::vector<Double_t> dtrkcl;
  std::vector<Double_t> dt;
  std::vector<Double_t>::iterator VIterator;
  int position = -1;
  Double_t minimum;

  for (int iCand=0; iCand < fLKrEvent->GetNCandidates(); iCand++){

    TRecoLKrCandidate* Cluster = ((TRecoLKrCandidate*)fLKrEvent->GetCandidate(iCand));
    double clustertime = Cluster->GetClusterTime();
    double tracktime = cand->GetCHODTime();
    double clusterx = Cluster->GetClusterX();
    double clustery = Cluster->GetClusterY();
    double distance = sqrt(pow(clusterx - Extrap_track.X(),2 ) + pow(clustery - Extrap_track.Y(),2 ) ) ;

    dtrkcl.push_back(distance);
    dt.push_back(clustertime - tracktime);

  }

  if(dtrkcl.size() ==0) return false;

  minimum= (Double_t)*min_element(dtrkcl.begin(), dtrkcl.end());
  position = distance(dtrkcl.begin(), min_element(dtrkcl.begin(), dtrkcl.end()));
  dtrkcl.clear();



  if(position < 0) return false;


  FillHisto("Track_Clusters_distance", minimum);
  FillHisto("Track_Clusters_dtchod", 3. - dt[position] );

  if(minimum  > 30) return false;
  if(fabs(3. - dt[position])  > 8.) return false;

  fMatchedLKrIndex= position;
  fMatchedLKrdt= dt[position];
  fMatchedLKrdistance= minimum;

  //cout << "before ==" << endl;
  //cout << fTrackMIP <<fTrackEM <<fTrackOther<<endl;
  fTrackMIP   = SelectMIPCluster(position,(TRecoLKrCandidate*) fLKrEvent->GetCandidate(position),"track" );
  fTrackEM    = SelectEMCluster(position,(TRecoLKrCandidate*) fLKrEvent->GetCandidate(position),"track" );
  fTrackOther    = SelectOtherCluster(position,(TRecoLKrCandidate*) fLKrEvent->GetCandidate(position), fTrackMIP, fTrackEM, "track" );

  //cout << "after ==" << endl;
  //cout << fTrackMIP <<fTrackEM <<fTrackOther<<endl;
  if((int)fTrackEM + (int) fTrackMIP + (int) fTrackOther > 1) cout << "ERRRRRORRRRRRRRRRRRRRR" << endl;
  return true;

}



Bool_t LKrAnalyzer::SelectMIPCluster(Int_t iclus, TRecoLKrCandidate *thisCluster, TString str) {
  if (thisCluster->GetNCells()>=6) return 0;
  if (thisCluster->GetClusterDDeadCell()<2) return 0;
  Double_t seedRatio = thisCluster->GetClusterSeedEnergy()/thisCluster->GetClusterEnergy();
  Double_t cellRatio = thisCluster->GetNCells()/(thisCluster->GetClusterEnergy()*0.001);
  if (cellRatio<0.2) return 0;
  FillHisto("MipClusterEnergy",thisCluster->GetClusterEnergy());
  FillHisto("MipClusterXY",thisCluster->GetClusterX(),thisCluster->GetClusterY());

  if(thisCluster->GetClusterEnergy() > 1000) return 0;
  if(str.EqualTo("track")) return true;

  if(fNMIPClusters < 10){ SetMIPClusterID(iclus);}
  AddMIPClusters();
  return 1;
}

Bool_t LKrAnalyzer::SelectEMCluster(Int_t iclus, TRecoLKrCandidate *thisCluster, TString str) {

  if (thisCluster->GetClusterEnergy()<3000) return 0;
  if (thisCluster->GetClusterDDeadCell()<20) return 0;
  if (!fTools->LKrGeometricalAcceptance(thisCluster->GetClusterX(),thisCluster->GetClusterY())) return 0;

  Double_t seedRatio = thisCluster->GetClusterSeedEnergy()/thisCluster->GetClusterEnergy();
  Double_t cellRatio = (thisCluster->GetNCells()-2.21)/(0.93*thisCluster->GetClusterEnergy()*0.001);

  FillHisto("NCellEnergyratiovsSeedEnergyRatio_nomip",seedRatio,cellRatio);

  Bool_t isNotEM = true;
  if (seedRatio>0.23&&seedRatio<0.46&&cellRatio<2&&cellRatio>=1) isNotEM = false;
  if (isNotEM) return 0;

  FillHisto("EMClusterEnergy",thisCluster->GetClusterEnergy());
  FillHisto("EMClusterXY",thisCluster->GetClusterX(),thisCluster->GetClusterY());

  if(str.EqualTo("track")) return true;
  if(fNEMClusters < 10){ SetEMClusterID(iclus);}
  AddEMClusters();
  return 1;
}

Bool_t LKrAnalyzer::SelectOtherCluster(Int_t iclus, TRecoLKrCandidate *thisCluster, Bool_t isMIPCluster, Bool_t isEMCluster, TString str) {
  if (isMIPCluster) return 0;
  if (isEMCluster) return 0;

  FillHisto("OtherClusterEnergy",thisCluster->GetClusterEnergy());
  FillHisto("OtherClusterXY",thisCluster->GetClusterX(),thisCluster->GetClusterY());

  if(str.EqualTo("track")) return true;

  if (fNOtherClusters<10) SetOtherClusterID(iclus);
  AddOtherClusters();

}

Bool_t LKrAnalyzer::TrackISEM(Int_t val){

  if(fNEMClusters==0){ return false;}
  for(int i = 0; i<fNEMClusters;i++){
    if(fEMClusterID[i] == val){
      fTrackEM=true;
      return true;
    }
  }

  return false;
}

Bool_t LKrAnalyzer::TrackISMIP(Int_t val){
  if(fNMIPClusters==0){ return false;}
  for(int i = 0; i<fNMIPClusters;i++){
    if(fMIPClusterID[i] == val){
      fTrackMIP=true;
      return true;
    }
  }



  return false;
}

Bool_t LKrAnalyzer::TrackISOther(Int_t val){
  if(fNOtherClusters==0){ return false;}

  for(int i = 0; i<fNOtherClusters;i++){
    if(fOtherClusterID[i] == val){
      fTrackOther=true;
      return true;
    }

  }

  return false;
}

Bool_t LKrAnalyzer::CloseCluster(Int_t clid, Double_t xlkr, Double_t ylkr, TRecoLKrEvent* event){

  for (Int_t jclus=0; jclus<event->GetNCandidates(); jclus++) {
    if (jclus==clid) continue;
    TRecoLKrCandidate *clus = (TRecoLKrCandidate *)event->GetCandidate(jclus);
    Double_t thisx = clus->GetClusterX();
    Double_t thisy = clus->GetClusterY();
    Double_t dist = sqrt((xlkr-thisx)*(xlkr-thisx)+(ylkr-thisy)*(ylkr-thisy));
    //cout << thisx << endl;
    //cout << "dist = " << dist << endl;
    if (dist<150.) return 1;
  }

  return 0;

}

Int_t LKrAnalyzer::HowManyInTime(TrackCandidate* cand, Double_t xlkr, Double_t ylkr,Int_t clid){

  int nintime=0;

  for (Int_t jclus=0; jclus<fLKrEvent->GetNCandidates(); jclus++) {
    if (jclus==clid) continue;
    TRecoLKrCandidate *clus = (TRecoLKrCandidate *)fLKrEvent->GetCandidate(jclus);
    Double_t energy = clus->GetClusterEnergy();
    Double_t thisx = clus->GetClusterX();
    Double_t thisy = clus->GetClusterY();
    Double_t dt  = clus->GetClusterTime() - cand->GetCHODTime();
    Double_t dist = sqrt((xlkr-thisx)*(xlkr-thisx)+(ylkr-thisy)*(ylkr-thisy));

    //FillHisto("ExtraCl_dt", 3 - dt);
    //FillHisto("ExtraCl_dist",dist );
    //FillHisto("ExtraCl_dt_vs_dist",dist,3 - dt );
    FillHisto("ExtraCl_dist_vs_dt",dist,3 - dt );
    FillHisto("ExtraCl_ecluster_vs_dt",clus->GetClusterEnergy(), 3 - dt);

    //if (dist>150.) continue;

    if(clus->GetClusterEnergy()  < 2000 && fabs(3 - dt) > 5.) continue;
    if(clus->GetClusterEnergy()  > 2000 && (3 - dt < -10. || 3 - dt > 15)) continue;
    //std::cout << "iclus =" << jclus << " energy = " << energy << " time =" << clus->GetClusterTime() << " dt == " << dt << "x = " << thisx << " y = " << thisy << std::endl;

    FillHisto("ExtraCl_dist_vs_dt_after",dist,3 - dt );
    FillHisto("ExtraCl_ecluster_vs_dt_after",clus->GetClusterEnergy(), 3 - dt);

    cand->AddExtraInTimeCl(nintime,jclus);
    nintime++;
  }

  cand->SetNExtraInTimeCl(nintime);
  return nintime;

}

Int_t LKrAnalyzer::HowManyInTimeAuxReco(TrackCandidate* cand, Double_t xlkr, Double_t ylkr,Int_t clid){

  int nintime=0;
  fLKrAuxClusterReco->FindClusters(cand->GetCHODTime(), fLKrEvent);
  //fLKrAuxClusterReco->PrintClusters();
  //fLKrAuxClusterReco->PrintSummary();
  for (Int_t i=0; i<fLKrAuxClusterReco->GetNClusters(); i++) {
    if (i==clid) continue;
    Double_t Energy = fLKrAuxClusterReco->GetCandidate(i)->GetClusterEnergy();
    if(Energy < 2000) continue;
    Double_t x = fLKrAuxClusterReco->GetCandidate(i)->GetClusterX();
    Double_t y = fLKrAuxClusterReco->GetCandidate(i)->GetClusterY();
    Double_t dt  = fLKrAuxClusterReco->GetCandidate(i)->GetClusterTime() - cand->GetCHODTime();

    Double_t dist = sqrt((xlkr-x)*(xlkr-x)+(ylkr-y)*(ylkr-y));

    FillHisto("ExtraClAux_dist_vs_dt",dist,3 - dt );
    FillHisto("ExtraClAux_ecluster_vs_dt",Energy, 3 - dt);

    //if(dist > 150) continue;
    if(3 - dt < -10 || 3 - dt > 15) continue;

    cand->AddExtraInTimeClAux(nintime,i);
    nintime++;
  }


  cand->SetNExtraInTimeClAux(nintime);
  return nintime;

}

int LKrAnalyzer::TimeMatching(Double_t trktime,Double_t kaontime){

  std::map<Double_t,int> closestlkr;
  int ilkr=-1;
  TRecoLKrCandidate* lkr;
  double dtlkr;
  for (int i=0; i<fLKrEvent->GetNCandidates(); i++) {
    lkr = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(i);
    dtlkr = fabs(trktime - lkr->GetClusterTime());
    closestlkr[dtlkr] = i;
  }
  if(fLKrEvent->GetNCandidates() == 0) ilkr=-1; else ilkr = closestlkr.begin()->second;

  std::map<Double_t,int> closestmuv1;
  int imuv1=-1;
  TRecoMUV1Candidate* muv1;
  double dtmuv1;
  for (int i=0; i<fMUV1Event->GetNCandidates(); i++) {
    muv1 = (TRecoMUV1Candidate*) fMUV1Event->GetCandidate(i);
    dtmuv1 = fabs(trktime - muv1->GetTime());
    closestmuv1[dtmuv1] = i;
  }
  if(fMUV1Event->GetNCandidates() == 0) imuv1=-1; else imuv1 = closestmuv1.begin()->second;


  std::map<Double_t,int> closestmuv2;
  int imuv2=-1;
  TRecoMUV2Candidate* muv2;
  double dtmuv2;
  for (int i=0; i<fMUV2Event->GetNCandidates(); i++) {
    muv2 = (TRecoMUV2Candidate*) fMUV2Event->GetCandidate(i);
    dtmuv2 = fabs(trktime - muv2->GetTime());
    closestmuv2[dtmuv2] = i;
  }
  if(fMUV2Event->GetNCandidates() == 0) imuv2=-1; else imuv2 = closestmuv2.begin()->second;

  std::map<Double_t,int> closestmuv3;
  int imuv3=-1;
  TRecoMUV3Candidate* muv3;
  double dtmuv3;
  for (int i=0; i<fMUV3Event->GetNCandidates(); i++) {
    muv3 = (TRecoMUV3Candidate*) fMUV3Event->GetCandidate(i);
    dtmuv3 = fabs(trktime - muv3->GetTime());
    closestmuv3[dtmuv3] = i;
  }
  if(fMUV3Event->GetNCandidates() == 0) imuv3=-1; else imuv3 = closestmuv3.begin()->second;

  if(ilkr< 0) return 0;
  if(imuv1 < 0) return 0;
  if(imuv2 < 0) return 0;
  if(imuv3 < 0) return 0;
  //std::cout << "---------------------" << std::endl;
  //std::cout << "NCAND lkr|muv1|2|3 = " << fLKrEvent->GetNCandidates() << "||"<< fMUV1Event->GetNCandidates() << "||" << fMUV2Event->GetNCandidates() << "||" << fMUV3Event->GetNCandidates() << std::endl;
  //std::cout << "lkr|muv1|2|3 = " << closestlkr.begin()->second << "||"<< closestmuv1.begin()->second << "||" << closestmuv2.begin()->second << "||" << closestmuv3.begin()->second << std::endl;
  //std::cout << "lkr|muv1|2|3 = " << closestlkr.begin()->first << "||"<< closestmuv1.begin()->first << "||" << closestmuv2.begin()->first << "||" << closestmuv3.begin()->first << std::endl;
  //std::cout << " in function = " << closestlkr.begin()->second << std::endl;
  return closestlkr.begin()->second;
}

int LKrAnalyzer::LKrMatch(Double_t trktime, TVector3 posatlkr){

  std::map<Double_t,int> closestlkr;
  std::vector<Double_t> tdiff;
  std::vector<Double_t> dtrkcl;
  int ilkr=-1;
  TRecoLKrCandidate* lkr;
  double dtlkr;

  for (int i=0; i<fLKrEvent->GetNCandidates(); i++) {
    lkr = (TRecoLKrCandidate*) fLKrEvent->GetCandidate(i);
    //maybe the number 3 should be changed later
    tdiff.push_back(3 - (lkr->GetClusterTime() - trktime));
    dtlkr = fabs(3 -( lkr->GetClusterTime() - trktime));
    double dist =sqrt(pow(lkr->GetClusterX() - posatlkr.X(),2 ) + pow(lkr->GetClusterY() - posatlkr.Y(),2 ) );
    dtrkcl.push_back(dist) ;
    closestlkr[fabs(dist)] = i;

  }
  if(fLKrEvent->GetNCandidates() == 0) ilkr=-1; else ilkr = closestlkr.begin()->second;

  if(ilkr< 0) return -1;

  FillHisto("Track_Clusters_distance", dtrkcl[ilkr]);
  FillHisto("Track_Clusters_dtchod", 3. - tdiff[ilkr] );

  if(fabs(tdiff[ilkr]) > 8. ) return -1;
  if(dtrkcl[ilkr] > 30. ) return -1;

  //std::cout << "lkr = " << closestlkr.begin()->first  << std::endl;
  //std::cout << "lkr = " << closestlkr.begin()->second  << std::endl;
  //std::cout << "lkrdtrkcl = " << dtrkcl[ilkr]  << std::endl;

  fMatchedLKrIndex = ilkr;
  //fMatchedLKrdt    = tdiff[ilkr];
  fMatchedLKrdistance= dtrkcl[ilkr];

  return closestlkr.begin()->second;
}

int LKrAnalyzer::MUV1Match(Double_t trktime, TVector3 posatMUV1){

  std::map<Double_t,int> closestMUV1;
  std::vector<Double_t> tdiff;
  std::vector<Double_t> dtrkcl;
  int iMUV1=-1;
  TRecoMUV1Candidate* MUV1;
  TRecoMUV1Candidate* goodmuv1;
  double dtMUV1;

  for (int i=0; i<fMUV1Event->GetNCandidates(); i++) {
    MUV1 = (TRecoMUV1Candidate*) fMUV1Event->GetCandidate(i);
    //maybe the number 3 should be changed later
    tdiff.push_back(MUV1->GetTime() - trktime);
    dtMUV1 = fabs(MUV1->GetTime() - trktime);
    dtrkcl.push_back(sqrt(pow(MUV1->GetX() - posatMUV1.X(),2 ) + pow(MUV1->GetY() - posatMUV1.Y(),2 ) )) ;
    closestMUV1[dtMUV1] = i;

  }
  if(fMUV1Event->GetNCandidates() == 0) iMUV1=-1; else iMUV1 = closestMUV1.begin()->second;

  if(iMUV1< 0) return 0;

  if(dtrkcl[iMUV1] > 120. ) return 0;
  if(fabs(tdiff[iMUV1]) > 10. ) return 0;
  //std::cout << "MUV1 = " << closestMUV1.begin()->first  << std::endl;
  //std::cout << "MUV1 = " << closestMUV1.begin()->second  << std::endl;
  //std::cout << "MUV1dtrkcl = " << dtrkcl[iMUV1]  << std::endl;
  goodmuv1 = (TRecoMUV1Candidate*) fMUV1Event->GetCandidate(iMUV1);

  fGoodTrack->SetMUV1(goodmuv1, iMUV1);
  fGoodTrack->SetMUV1Time(tdiff[iMUV1] + trktime);
  fGoodTrack->SetMUV1Energy(goodmuv1->GetEnergy());
  fGoodTrack->SetMUV1ShowerWidth(goodmuv1->GetShowerWidth());
  fGoodTrack->SetMUV1SeedRatio(0.5*goodmuv1->GetSeedEnergy()/goodmuv1->GetEnergy());

  //std::cout << "----------------" << std::endl;
  //std::cout << "energy = "  << goodmuv1->GetEnergy()<< std::endl;
  //std::cout << "from cand  = "  << fGoodTrack->GetMUV1Energy()<< std::endl;

  return closestMUV1.begin()->second;
}

int LKrAnalyzer::MUV2Match(Double_t trktime, TVector3 posatMUV2){

  std::map<Double_t,int> closestMUV2;
  std::vector<Double_t> tdiff;
  std::vector<Double_t> dtrkcl;
  int iMUV2=-1;
  TRecoMUV2Candidate* MUV2;
  TRecoMUV2Candidate* goodmuv2;
  double dtMUV2;

  for (int i=0; i<fMUV2Event->GetNCandidates(); i++) {
    MUV2 = (TRecoMUV2Candidate*) fMUV2Event->GetCandidate(i);
    //maybe the number 3 should be changed later
    tdiff.push_back(MUV2->GetTime() - trktime);
    dtMUV2 = fabs(MUV2->GetTime() - trktime);
    dtrkcl.push_back(sqrt(pow(MUV2->GetX() - posatMUV2.X(),2 ) + pow(MUV2->GetY() - posatMUV2.Y(),2 ) )) ;
    closestMUV2[dtMUV2] = i;

  }
  if(fMUV2Event->GetNCandidates() == 0) iMUV2=-1; else iMUV2 = closestMUV2.begin()->second;

  if(iMUV2< 0) return 0;
  if(fabs(tdiff[iMUV2]) > 10. ) return 0;
  if(dtrkcl[iMUV2] > 240. ) return 0;
  goodmuv2 =(TRecoMUV2Candidate*) fMUV2Event->GetCandidate(iMUV2);
  fGoodTrack->SetMUV2(goodmuv2, iMUV2);
  fGoodTrack->SetMUV2Time(tdiff[iMUV2] + trktime);
  fGoodTrack->SetMUV2Energy(goodmuv2->GetEnergy());
  fGoodTrack->SetMUV2ShowerWidth(goodmuv2->GetShowerWidth() );
  fGoodTrack->SetMUV2SeedRatio(0.5*goodmuv2->GetSeedEnergy()/goodmuv2->GetEnergy());

  //std::cout << "sw = " << goodmuv2->GetShowerWidth() << std::endl;
  return closestMUV2.begin()->second;
}
int LKrAnalyzer::MUV3Match(Double_t trktime, TVector3 posatMUV3){

  std::map<Double_t,int> closestMUV3;
  std::vector<Double_t> tdiff;
  std::vector<Double_t> dtrkcl;
  int iMUV3=-1;
  TRecoMUV3Candidate* MUV3;
  double dtMUV3;

  for (int i=0; i<fMUV3Event->GetNCandidates(); i++) {
    MUV3 = (TRecoMUV3Candidate*) fMUV3Event->GetCandidate(i);
    //maybe the number 3 should be changed later
    tdiff.push_back(MUV3->GetTime() - trktime);
    dtMUV3 = fabs(MUV3->GetTime() - trktime);
    dtrkcl.push_back(sqrt(pow(MUV3->GetX() - posatMUV3.X(),2 ) + pow(MUV3->GetY() - posatMUV3.Y(),2 ) )) ;
    closestMUV3[dtMUV3] = i;

  }
  if(fMUV3Event->GetNCandidates() == 0) iMUV3=-1; else iMUV3 = closestMUV3.begin()->second;


  if(iMUV3< 0) return 0;

  FillHisto("muv3dt", tdiff[iMUV3]);
  FillHisto("muv3dist", dtrkcl[iMUV3]);
  if(fabs(tdiff[iMUV3]) > 3. ) return 0;
  if(dtrkcl[iMUV3] > 220. ) return 0;

  //std::cout << "MUV3 = " << closestMUV3.begin()->first  << std::endl;
  //std::cout << "MUV3 = " << closestMUV3.begin()->second  << std::endl;
  //std::cout << "MUV3dtrkcl = " << dtrkcl[iMUV3]  << std::endl;
  fGoodTrack->SetMUV3((TRecoMUV3Candidate*)fMUV3Event->GetCandidate(iMUV3), iMUV3);
  fGoodTrack->SetMUV3Time(tdiff[iMUV3] + trktime);


  return closestMUV3.begin()->second;
}

LKrAnalyzer::~LKrAnalyzer(){

}
