#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "SpectrometerAnalyser.hh"
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "Parameters.hh"
#include "Geometry.hh"
#include "AnalysisTools.hh"
#include "TrackCandidate.hh"
#include "TrackSTRAWCandidate.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

SpectrometerAnalyser::SpectrometerAnalyser(Core::BaseAnalysis *ba) : Analyzer(ba, "SpectrometerAnalyser")
{
  fAnalyzerName = "SpectrometerAnalyser";
  RequestTree("Spectrometer",new TRecoSpectrometerEvent);

  RequestTree("GigaTracker",new TRecoGigaTrackerEvent);

  RequestL0Data();
  //Analysis tools
  fTools = AnalysisTools::GetInstance();
  AddParam("AlphaCorr", &fExternalAlpha, -999.999);
  AddParam("BetaCorr",  &fExternalBeta,  -999.999);
  //Straw geometry

  zSTRAW_station[0] = 183508.0;
  zSTRAW_station[1] = 194066.0;
  zSTRAW_station[2] = 204459.0;
  zSTRAW_station[3] = 218885.0; // [ mm ]
  xSTRAW_station[0] = 101.2;
  xSTRAW_station[1] = 114.4;
  xSTRAW_station[2] = 92.4;
  xSTRAW_station[3] = 52.8; // [ mm ]

}

void SpectrometerAnalyser::InitOutput(){

  RegisterOutput("Tracks",&fTracks);
  RegisterOutput("TrackCandidates",&fGoodCandidates);
}

void SpectrometerAnalyser::InitHist(){

  //Good Spectrometer track selection
  BookHisto(new TH1I("NTracks_all", "Number of tracks;NTracks" ,50,0,50));
  BookHisto(new TH1I("NTracks", " 0 < Ntracks < 10;NTracks" ,50,0,50));
  BookHisto(new TH1D("Track_momentum_before","track momentum before quality selection; P [MeV/c]",200,0.,100000.));
  BookHisto(new TH1I("Track_NChambers_before","track N chambers before quality selection; Nchambers",7,0,7));
  BookHisto(new TH1D("Track_charge_before","track charge before quality selection; Q",6,-3,3));

  BookHisto(new TH1D("Track_momentum_abcorr","track momentum after alpha and beta corrections; P [MeV/c]",200,0.,100000.));


  BookHisto(new TH1F("gtk_P_before", " GTK momentum;P_{K} [MeV/c]",100,65000.,85000));
  BookHisto(new TH1I("gtk_Nhits_before", "Number of GTK hits;Nhits" ,100,0,100));
  BookHisto(new TH1F("gtk_dt_before","T_{track} - T_{gtk} ; dT_{chod - gtk}",400,-50.,50.));
  BookHisto(new TH1F("gtk_chi2_before","#chi^{2} ; #chi^{2}",300,0.,300.));
  BookHisto(new TH2F("gtk_pos_before","GTK position; x @ GTK [mm]; y @ GTK [mm]", 100,-50,50,100,-50,50));
  BookHisto(new TH2F("gtk_dt_vs_cda_before", "  cda vs T_{track} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -50, 50,300, 0, 300));

  BookHisto(new TH1F("Vertex_cda_before", " Closest distance approached between the kaon and the track ; cda [mm]", 300, 0, 300));
  BookHisto(new TH1F("Vertex_Z_before", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("P_vs_cda_before", " Track momentum vs cda ; P_{track} [MeV/c];cda [mm]",200,0.,100000, 300, 0, 300));
  BookHisto(new TH2F("zvtx_vs_P_before", " Closest distance approached between the kaon and the track; zvtx [mm] ;P_{track} [MeV/c]",300,0.,300000, 200, 0, 100000));


  BookHisto(new TH1D("Ppatternrecognition_nonfake","track Ppattern_recognition for the nonfake tracks; Ppr [MeV/c]",200,0.,100000.));
  BookHisto(new TH1D("Track_momentum_nonfake","track momentum  for the nonfake tracks; P [MeV/c]",200,0.,100000.));
  BookHisto(new TH1D("Track_fitquality_nonfake","Ppattern_recognition - Pfit; #DeltaP [MeV/c]",400,-100000.,100000.));
  BookHisto(new TH1D("Track_Chi2_nonfake","#chi^{2} fit; #chi^{2}",300,0.,300.));
  BookHisto(new TH1I("Track_NChambers_nonfake","N chambers  for the nonfake tracks; Nchambers",7,0,7));
  BookHisto(new TH1D("Track_charge_nonfake","track charge for the nonfake tracks; Q",6,-3,3));

  BookHisto(new TH2D("Track_momentum_chi2_nonfake","track momentum vs chi2 for the nonfake tracks; P [MeV/c]; #chi^{2}",200,0.,100000.,300,0,300));


  BookHisto(new TH1F("Vertex_cda_nonfake", " Closest distance approached between the kaon and the track ; cda [mm]", 300, 0, 300));
  BookHisto(new TH1F("Vertex_Z_nonfake", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("P_vs_cda_nonfake", " Track momentum vs cda ; P_{track} [MeV/c];cda [mm]",200,0.,100000, 300, 0, 300));
  BookHisto(new TH2F("zvtx_vs_P_nonfake", " Closest distance approached between the kaon and the track ; zvtx [mm];P_{track} [MeV/c]",300,0.,300000, 200, 0, 100000));


  BookHisto(new TH1D("Ppatternrecognition_fake","track Ppattern_recognition for the fake tracks; Ppr [MeV/c]",200,0.,100000.));
  BookHisto(new TH1D("Track_momentum_fake","track momentum  for the fake tracks; P [MeV/c]",200,0.,100000.));
  BookHisto(new TH1D("Track_fitquality_fake","Ppattern_recognition - Pfit; #DeltaP [MeV/c]",400,-100000.,100000.));
  BookHisto(new TH1D("Track_Chi2_fake","#chi^{2} fit; #chi^{2}",300,0.,300.));
  BookHisto(new TH1I("Track_NChambers_fake","N chambers  for the fake tracks; Nchambers",7,0,7));
  BookHisto(new TH1D("Track_charge_fake","track charge for the fake tracks; Q",6,-3,3));

  BookHisto(new TH2D("Track_momentum_chi2_fake","track momentum vs chi2 for the nonfake tracks; P [MeV/c]; #chi^{2}",200,0.,100000.,300,0,300));

  BookHisto(new TH1D("Track_momentum_good","track momentum for the good tracks; P [MeV/c]",200,0.,100000.));
  BookHisto(new TH1D("Track_momentum_bad","track momentum for the bad tracks; P [MeV/c]",200,0.,100000.));

  BookHisto(new TH2D("Track_position_station1_good","track @ station 1; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));
  BookHisto(new TH2D("Track_position_station2_good","track @ station 2; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));
  BookHisto(new TH2D("Track_position_station3_good","track @ station 3; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));
  BookHisto(new TH2D("Track_position_station4_good","track @ station 4; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));



  BookHisto(new TH1F("Vertex_cda_good_wacc", " Closest distance approached between the kaon and the track ; cda [mm]", 300, 0, 300));
  BookHisto(new TH1F("Vertex_Z_good_wacc", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("P_vs_cda_good_wacc", " Track momentum vs cda ; P_{track} [MeV/c];cda [mm]",200,0.,100000, 300, 0, 300));
  BookHisto(new TH2F("zvtx_vs_P_good_wacc", " Closest distance approached between the kaon and the track ; zvtx [mm];P_{track} [MeV/c]",300,0.,300000, 200, 0, 100000));

  BookHisto(new TH2D("Track_position_station1_accrej","track @ station 1; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));
  BookHisto(new TH2D("Track_position_station2_accrej","track @ station 2; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));
  BookHisto(new TH2D("Track_position_station3_accrej","track @ station 3; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));
  BookHisto(new TH2D("Track_position_station4_accrej","track @ station 4; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));

  BookHisto(new TH2D("Track_position_station1_fake","track @ station 1; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));
  BookHisto(new TH2D("Track_position_station2_fake","track @ station 2; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));
  BookHisto(new TH2D("Track_position_station3_fake","track @ station 3; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));
  BookHisto(new TH2D("Track_position_station4_fake","track @ station 4; x [mm]; y [mm]",2000,-1000.,1000.,2000,-1000.,1000.));


  BookHisto(new TH1F("Vertex_cda_fake", " Closest distance approached between the kaon and the track ; cda [mm]", 300, 0, 300));
  BookHisto(new TH1F("Vertex_Z_fake", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("P_vs_cda_fake", " Track momentum vs cda ; P_{track} [MeV/c];cda [mm]",200,0.,100000, 300, 0, 300));
  BookHisto(new TH2F("zvtx_vs_P_fake", " Closest distance approached between the kaon and the track ; zvtx [mm]; P_{track} [MeV/c]",300,0.,300000, 200, 0, 100000));



  BookHisto(new TH1F("GoodTracks_dt", " Time between the good and the non-fake tracks; dt [ns]", 400, -100, 100));
  BookHisto(new TH1F("GoodTracks_Vertex_cda", " Closest distance approached between the good spectrometer tracks ; cda [mm]", 300, 0, 300));
  BookHisto(new TH1F("GoodTracks_Vertex_Z", " Z vertex ; Zvtx [mm]", 500, 0, 500000));
  BookHisto(new TH2F("GoodTracks_dt_vs_cda", " dT vs cda ; dt [ns];cda [mm]",400,-100.,100, 300, 0, 300));
  BookHisto(new TH2F("GoodTracks_P_vs_cda", " Track momentum vs cda ; P_{track} [MeV/c];cda [mm]",200,0.,100000, 300, 0, 300));
  BookHisto(new TH2F("GoodTracks_zvtx_vs_P", " Closest distance approached between the kaon and the track ; zvtx [mm]; P_{track} [MeV/c]",300,0.,300000, 200, 0, 100000));

  BookHisto(new TH1I("NGoodTracks",";N good tracks",30,0,30));
  BookHisto(new TH2F("NGood_vs_Ntot","Number of good tracks vs Total number of tracks",10,0,10,10,0,10));
  BookHisto(new TH1I("NFakeVtx",";N fake 2 track vtx 0 -- No 2nd Vtx , 1 -- 2nd Vtx ", 10, -5,5));


  BookHisto(new TH1F("gtk_P_final", " GTK momentum;P_{K} [MeV/c]",100,65000.,85000));
  BookHisto(new TH1I("gtk_Nhits_final", "Number of GTK hits;Nhits" ,100,0,100));
  BookHisto(new TH1F("gtk_dt_final","T_{track} - T_{gtk} ; dT_{chod - gtk}",400,-50.,50.));
  BookHisto(new TH1F("gtk_chi2_final","#chi^{2} ; #chi^{2}",300,0.,300.));
  BookHisto(new TH2F("gtk_pos_final","GTK position; x @ GTK [mm]; y @ GTK [mm]", 100,-50,50,100,-50,50));
  BookHisto(new TH2F("gtk_dt_vs_cda_final", "  cda vs T_{track} - T_{gtk} ; dT_{chod - gtk} [ns];cda [mm]", 400, -50, 50,300, 0, 300));

  BookHisto(new TH1F("Vertex_cda_final", " Closest distance approached between the kaon and the track ; cda [mm]", 300, 0, 300));
  BookHisto(new TH1F("Vertex_Z_final", " Z vertex ; Zvtx [mm]", 300, 0, 300000));
  BookHisto(new TH2F("cda_vs_P_final", " Track momentum vs cda ; P_{track} [MeV/c];cda [mm]", 600, -300, 300,200,0.,100000));
  BookHisto(new TH2F("zvtx_vs_P_final", " Closest distance approached between the kaon and the track; zvtx [mm] ; P_{track} [MeV/c]",300,0.,300000, 200, 0, 100000));

}

void SpectrometerAnalyser::DefineMCSimple(){
}

void SpectrometerAnalyser::StartOfJobUser(){
}
void SpectrometerAnalyser::StartOfRunUser(){
}

void SpectrometerAnalyser::StartOfBurstUser(){
}

void SpectrometerAnalyser::Process(int iEvent){

  //Parameters *par = Parameters::GetInstance();

  //if(fMCSimple.fStatus == MCSimple::kMissing){printIncompleteMCWarning(iEvent);return;}
  //if(fMCSimple.fStatus == MCSimple::kEmpty){printNoMCWarning();return;}

  fTracks.Clear();
  fGoodCandidates.Clear();
  fHeader = GetRawHeader();
  fL0Data = fHeader->GetL0TPData();
  NA62Analysis::UserMethods::OutputState isEvtValid;
  //fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker");
  //OutputState state;
  fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetOutput("GigaTrackerEvtReco.GigaTrackerEvent",isEvtValid);

  //fGigaTrackerEvent = (TRecoGigaTrackerEvent*)GetEvent("GigaTracker");
  fSpectrometerEvent = (TRecoSpectrometerEvent*)GetEvent("Spectrometer");
  //if(fHeader->GetEventQualityMask() != 0)
  //cout << "BadBurst = " << fHeader->GetEventQualityMask() << endl;
  //if(fGigaTrackerEvent->GetNCandidates() == 0) return;

  fNTracks = fSpectrometerEvent->GetNCandidates();

  FillHisto("NTracks_all",fNTracks);

  if (fNTracks == 0 || fNTracks > 10) return;

  FillHisto("NTracks", fNTracks);

  vector<int> STRAWTrack;
  vector<bool> IsFake;
  vector<int> STRAWFakeTrack;

  Int_t  L0DataType    = GetWithMC() ? 0x1  : GetL0Data()->GetDataType();
  Int_t  L0TriggerWord = GetWithMC() ? 0xFF : GetL0Data()->GetTriggerFlags();
  Bool_t PhysicsData   = L0DataType    & 0x1;
  //Bool_t TriggerOK     = L0TriggerWord & fTriggerMask;

  //ol_t FillPlots = PhysicsData && TriggerOK;
  Int_t RunNumber = GetWithMC() ? 0 : fHeader->GetRunID();
  Int_t BurstID   = GetWithMC() ? 0 : fHeader->GetBurstID();


  //Control trigger
  // if (!SelectTrigger(0,L0DataType,L0TriggerWord)) return;
  //Pinunu trigger
  //if (!SelectTrigger(2,L0DataType,L0TriggerWord)) return;
  //Ctrl and Pinunu
  if (!SelectTrigger(2,L0DataType,L0TriggerWord) && !SelectTrigger(0,L0DataType,L0TriggerWord)) return;


  for (int i=0; i < fNTracks; i++) {

    TRecoSpectrometerCandidate *track = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(i);

    // good track
    double P_pattreco = track->GetMomentumBeforeFit();
    double P_fit = track->GetMomentum();
    double chi2 = track->GetChi2();
    int charge = track->GetCharge();
    int nChambers = track->GetNChambers();


    FillHisto("Track_momentum_before",P_fit);
    FillHisto("Track_charge_before",charge);
    FillHisto("Track_NChambers_before",nChambers);


    // momentum scale (alpha + beta corrections)
    //cout << "Before == " << P_fit << endl;
    if(fExternalAlpha!=-999.999 && fExternalBeta!=-999.999){
        track->SetMomentum((1 + charge*fExternalAlpha*track->GetMomentum())*(1 + fExternalBeta)*track->GetMomentum());
        //cout << "After == " << track->GetMomentum() << endl;
    }
    //Double_t alpha =0.51e-8;
    //Double_t beta  =-1.33e-3;
    //track->SetMomentum((1 + charge*alpha*track->GetMomentum())*(1 + beta)*track->GetMomentum());

    FillHisto("Track_momentum_abcorr",track->GetMomentum());

    //Vertex with nominal Kaon
    MakeGTKVertex(track,"before");
    //MakeNominalKaonVertex(track);

    FillHisto("Vertex_Z_before",fVertex.Z());
    FillHisto("Vertex_cda_before",fcda);
    FillHisto("P_vs_cda_before",P_fit,fcda);
    FillHisto("zvtx_vs_P_before",fVertex.Z(),P_fit);

    //Extrapolation to the STRAW planes
    // straw acceptance.
    Bool_t is_inAcc = false;
    is_inAcc = Acceptance(track);

    TVector3 position[4];

    for (int j=0; j < 4; j++) {
      position[j] = fTools->GetPositionAtZ(track,zSTRAW_station[j]);
    }


    // fake track
    int* track_hits_indexes = track->GetHitsIndexes();
    int nHits_incommom = 0;
    bool isHit_incommon = false;

    for (int j=0; j < fNTracks; j++) {

      TRecoSpectrometerCandidate *track2 = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(j);
      if (i==j) continue;
      int *track2_hits_indexes = track2->GetHitsIndexes();

      for (int itrack1=0; itrack1 < track->GetNHits(); itrack1++) {
        for (int itrack2=0; itrack2 < track2->GetNHits(); itrack2++) {
          if (track_hits_indexes[itrack1] == track2_hits_indexes[itrack2]) nHits_incommom++;
        }
      }
    }

    if (nHits_incommom > 1) isHit_incommon = true;

    bool fake = false;
    if (track->GetNChambers() == 3 && isHit_incommon) fake = true;
    if (track->GetNChambers() == 3 && track->GetChi2() > 30) fake = true;

    bool good = true;

    if (!fake) {

      FillHisto("Ppatternrecognition_nonfake",P_pattreco);
      FillHisto("Track_momentum_nonfake",P_fit);
      FillHisto("Track_fitquality_nonfake",P_pattreco - P_fit);
      FillHisto("Track_Chi2_nonfake",chi2);
      FillHisto("Track_charge_nonfake",charge);
      FillHisto("Track_NChambers_nonfake",nChambers);
      FillHisto("Track_momentum_chi2_nonfake",P_fit,chi2);


      FillHisto("Vertex_Z_nonfake",fVertex.Z());
      FillHisto("Vertex_cda_nonfake",fcda);
      FillHisto("P_vs_cda_nonfake",P_fit,fcda);
      FillHisto("zvtx_vs_P_nonfake",fVertex.Z(),P_fit);

      if(chi2 > 20.) good = false;
      if(TMath::Abs(P_pattreco - P_fit) > 20000.) good = false;
      if(nChambers < 4) good = false;
      if(charge != 1) good = false;

      if(good) FillHisto("Track_momentum_good",P_fit);
      if(!good) FillHisto("Track_momentum_bad",P_fit);

      //?? is it fixed once I do the corrections
      TVector3 TrackP = track->GetThreeMomentumBeforeMagnet();

      if(good && is_inAcc) {

        for (int j=0; j<4; j++) FillHisto(Form("Track_position_station%d_good",j+1),position[j].X(),position[j].Y());

        FillHisto("Vertex_Z_good_wacc",fVertex.Z());
        FillHisto("Vertex_cda_good_wacc",fcda);
        FillHisto("P_vs_cda_good_wacc",fcda,P_fit);
        FillHisto("zvtx_vs_P_good_wacc",fVertex.Z(),P_fit);

        STRAWTrack.push_back(i);
        //fGoodTrack.push_back(track);

      }

      if(good && (!is_inAcc)) {
        for (int j=0; j<4; j++) FillHisto(Form("Track_position_station%d_accrej",j+1),position[j].X(),position[j].Y());
      }

    } else {

      FillHisto("Ppatternrecognition_fake",P_pattreco);
      FillHisto("Track_momentum_fake",P_fit);
      FillHisto("Track_fitquality_fake",P_pattreco - P_fit);
      FillHisto("Track_Chi2_fake",chi2);
      FillHisto("Track_charge_fake",charge);
      FillHisto("Track_NChambers_fake",nChambers);
      FillHisto("Track_momentum_chi2_fake",P_fit,chi2);

      for (int j=0; j<4; j++) FillHisto(Form("Track_position_station%d_fake",j+1),position[j].X(),position[j].Y());


      FillHisto("Vertex_Z_fake",fVertex.Z());
      FillHisto("Vertex_cda_fake",fcda);
      FillHisto("P_vs_cda_fake",P_fit,fcda);
      FillHisto("zvtx_vs_P_fake",fVertex.Z(),P_fit);

      IsFake.push_back(true);
      STRAWFakeTrack.push_back(i);

    }

  }

  fNGoodTracks = STRAWTrack.size();
  fNFakeTracks = STRAWFakeTrack.size();
  Bool_t good_vtx = false;

  for(int i=0; i < fNGoodTracks;i++){

    TRecoSpectrometerCandidate* track = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(STRAWTrack[i]);
    TVector3 Track1_P = track->GetThreeMomentumBeforeMagnet();
    Double_t t1_time = track->GetTime();
    TVector3 Track1_PositionBefore  = track->GetPositionBeforeMagnet();

    for(int j=0; j < fNFakeTracks; j++){

      if(STRAWFakeTrack[j]==STRAWTrack[i]) continue;

      TRecoSpectrometerCandidate* track2 = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(STRAWFakeTrack[j]);

      Double_t cda;
      Double_t t2_time = track2->GetTime();
      Double_t dt =t1_time - t2_time;
      TVector3 Vertex;
      TVector3 Track2_P = track2->GetThreeMomentumBeforeMagnet();
      TVector3 Track2_PositionBefore = track2->GetPositionBeforeMagnet();


      Vertex = fTools->SingleTrackVertex( Track1_P, Track2_P, Track1_PositionBefore, Track2_PositionBefore, cda);

      FillHisto("GoodTracks_dt",dt);
      FillHisto("GoodTracks_dt_vs_cda",dt, cda);
      FillHisto("GoodTracks_Vertex_Z",Vertex.Z());
      FillHisto("GoodTracks_Vertex_cda",cda);
      FillHisto("GoodTracks_P_vs_cda",track->GetMomentum(),cda);
      FillHisto("GoodTracks_zvtx_vs_P",Vertex.Z(),track->GetMomentum());

      if(cda < 15            &&
         Vertex.Z() > 60000  &&
         Vertex.Z() < 200000 &&
         fabs(dt) < 50) good_vtx = true;
    }

  }

  FillHisto("NGoodTracks",fNGoodTracks);
  FillHisto("NGood_vs_Ntot",fNGoodTracks,fNTracks);
  FillHisto("NFakeVtx",(int)good_vtx);

  if(good_vtx) return;

  for(int i=0; i<fNGoodTracks ; i++){

    TrackCandidate* FinalCandidate = new TrackCandidate();
    TrackSTRAWCandidate* fSTRAWCandidate = new TrackSTRAWCandidate();

    TRecoSpectrometerCandidate* final = (TRecoSpectrometerCandidate*)fSpectrometerEvent->GetCandidate(STRAWTrack[i]);
    double P_fit = final->GetMomentum();

    //Vertex with nominal Kaon
    //MakeNominalKaonVertex(final);
    MakeGTKVertex(final,"final");
    FillHisto("Vertex_Z_final",fVertex.Z());
    FillHisto("Vertex_cda_final",fcda);
    FillHisto("cda_vs_P_final",P_fit,fcda);
    FillHisto("zvtx_vs_P_final",fVertex.Z(),P_fit);


    fSTRAWCandidate->SetSpectrometer(final,STRAWTrack[i]);
    fSTRAWCandidate->SetTrackID(STRAWTrack[i]);
    fSTRAWCandidate->SetTime(final->GetTime());
    fSTRAWCandidate->SetCharge(final->GetCharge());
    fSTRAWCandidate->SetGoodTrack(true);
    FinalCandidate->SetSTRAW(fSTRAWCandidate);
    FinalCandidate->SetSpectrometer(final, STRAWTrack[i]);
    fTracks.Add(final);
    fGoodCandidates.Add(FinalCandidate);

  }


  SetOutputState("Tracks",kOValid);
  SetOutputState("TrackCandidates",kOValid);


}

Bool_t SpectrometerAnalyser::Acceptance(TRecoSpectrometerCandidate *thisTrack){
  Bool_t acceptanceFlag = true;
  TVector3 pos(0,0,0);
  Double_t r2 = 999999.;

  // Straw acceptance
  for (Int_t j=0; j<4; j++) {
    pos = fTools->GetPositionAtZ(thisTrack,zSTRAW_station[j]);
    r2 = (pos.X()-xSTRAW_station[j])*(pos.X()-xSTRAW_station[j])+pos.Y()*pos.Y();
    if (r2<75.*75. || r2>1000*1000) acceptanceFlag = false;
  }

  // RICH acceptance
  pos = fTools->GetPositionAtZ(thisTrack,219385.);
  r2 = (pos.X()-34.)*(pos.X()-34.)+pos.Y()*pos.Y();
  if (r2<90.*90. || r2>1100.*1100.) acceptanceFlag = false;
  pos = fTools->GetPositionAtZ(thisTrack,237326.);
  r2 = (pos.X()-2.)*(pos.X()-2.)+pos.Y()*pos.Y();
  if (r2<90.*90. || r2>1100.*1100.) acceptanceFlag = false;

  // CHOD acceptance
  pos = fTools->GetPositionAtZ(thisTrack,238960.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<125.*125. || r2>1100.*1100.) acceptanceFlag = false;
  pos = fTools->GetPositionAtZ(thisTrack,239340.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<125.*125. || r2>1100*1100) acceptanceFlag  = false;

  // LKr acceptance
  Bool_t isLKr=false;
  pos = fTools->GetPositionAtZ(thisTrack,241093.);
  if (fTools->LKrGeometricalAcceptance(pos.X(),pos.Y())) isLKr = true;
  if(!isLKr) acceptanceFlag = false;

  // MUV2 acceptance
  pos = fTools->GetPositionAtZ(thisTrack,244341.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<130.*130. || r2>1100*1100) acceptanceFlag = false;


  // MUV2 acceptance
  pos = fTools->GetPositionAtZ(thisTrack,245290.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<130.*130. || r2>1100*1100) acceptanceFlag = false;

  // MUV3 acceptance
  pos = fTools->GetPositionAtZ(thisTrack,246800.);
  r2 = pos.X()*pos.X()+pos.Y()*pos.Y();
  if (r2<130.*130. || r2>1100*1100) acceptanceFlag = false;

  return acceptanceFlag;
}

void SpectrometerAnalyser::MakeNominalKaonVertex(TRecoSpectrometerCandidate* track){


  //Beam position vector at trim5 and beam momentum
  //(at the moment no GTK assuming 75 GeV Definitions.h)
  Double_t tethax =  0.0012;
  Double_t tethay =  0.;
  Double_t pkaon = 74.95;
  Double_t pkaonz = pkaon/sqrt(1.+tethax*tethax+tethay*tethay);
  Double_t pkaonx = pkaonz*tethax;
  Double_t pkaony = pkaonz*tethay;
  TLorentzVector kaonmomentum;
  kaonmomentum.SetXYZM(pkaonx,pkaony,pkaonz,0.493667);

  TVector3 Kaon = kaonmomentum.Vect();
  TVector3 KaonPos(0,0,101800.);


  fVertex = fTools->SingleTrackVertex( track->GetThreeMomentumBeforeMagnet(), Kaon, track->GetPositionBeforeMagnet(), KaonPos, fcda);

}

void SpectrometerAnalyser::MakeGTKVertex(TRecoSpectrometerCandidate* track, TString str){

  double bestdt= -999;
  int bestgtk = FindClosestTimeCand( fGigaTrackerEvent, track, "GTK", bestdt);
  int ngood = 0;
  if(bestgtk<0) return;

  TRecoGigaTrackerCandidate* GTK = (TRecoGigaTrackerCandidate*) fGigaTrackerEvent->GetCandidate(bestgtk);
  double gtkdt = GTK->GetTime() - track->GetTime();
  double gtkchi2 = GTK->GetChi2();
  int gtknhits = GTK->GetNHits();

  TVector3 vertex;
  TVector3 gtkpos= GTK->GetPosition(2);
  TVector3 gtkmomentum= GTK->GetMomentum();
  TLorentzVector Pgtk(gtkmomentum, KMass);

  fVertex = fTools->SingleTrackVertex( track->GetThreeMomentumBeforeMagnet(), gtkmomentum, track->GetPositionBeforeMagnet(), gtkpos, fcda);

  if(str.EqualTo("before")){
    FillHisto("gtk_P_before", gtkmomentum.Mag());
    FillHisto("gtk_dt_before",  gtkdt);
    FillHisto("gtk_chi2_before", gtkchi2);
    FillHisto("gtk_Nhits_before", gtknhits);
    FillHisto("gtk_pos_before", gtkpos.X(),gtkpos.Y());
    FillHisto("gtk_dt_vs_cda_before",gtkdt, fcda);
  }
  if(str.EqualTo("final")){
    FillHisto("gtk_P_final", gtkmomentum.Mag());
    FillHisto("gtk_dt_final",  gtkdt);
    FillHisto("gtk_chi2_final", gtkchi2);
    FillHisto("gtk_Nhits_final", gtknhits);
    FillHisto("gtk_pos_final", gtkpos.X(),gtkpos.Y());
    FillHisto("gtk_dt_vs_cda_final",gtkdt, fcda);
  }


}

int SpectrometerAnalyser::FindClosestTimeCand( TRecoVEvent* Event, TRecoSpectrometerCandidate* track, string detector_type, double& minimum){

  std::vector<double> dt;
  int position= -1;
  for (int iCand=0; iCand < Event->GetNCandidates(); iCand++){
    if(detector_type == "GTK"){
      TRecoGigaTrackerCandidate* Cluster = ((TRecoGigaTrackerCandidate*)Event->GetCandidate(iCand));
      double time = Cluster->GetTime();
      double dtchod = fabs(time - track->GetTime()) ;
      dt.push_back(dtchod);
    }

  }

  if(dt.size() !=0){
    minimum= *min_element(dt.begin(), dt.end());
    position = distance(dt.begin(), min_element(dt.begin(), dt.end()));
    dt.clear();

  }
  return position;

}
void SpectrometerAnalyser::PostProcess(){
}

void SpectrometerAnalyser::EndOfBurstUser(){
}

void SpectrometerAnalyser::EndOfRunUser(){
  //  SaveAllPlots();
}
void SpectrometerAnalyser::EndOfJobUser(){
  SaveAllPlots();
}

void SpectrometerAnalyser::DrawPlot(){
}

SpectrometerAnalyser::~SpectrometerAnalyser(){
}
Int_t SpectrometerAnalyser::SelectTrigger(int triggerType, int type, int mask) {
  int bitPhysics = 0;
  int bitControl = 4;
  int bitMinBias = 0;
  int bitPinunu = 1;


  if (type&0x2) return 0; // skip periodics


  // Select trigger
  if ((triggerType==0) && (type>>bitControl)&1) return 1; // control trigger.
  else if (triggerType==1) { // physics minimum bias trigger.
    if (!((type>>bitPhysics)&1)) return 0;
    if ((mask>>bitMinBias)&1) return 1;
  }
  else if (triggerType==2) { // physics pinunu trigger.
    if (!((type>>bitPhysics)&1)) return 0;
    if ((mask>>bitPinunu)&1) return 1;
  }
  else return 0; // trigger not found.
}
