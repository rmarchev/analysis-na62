#include <stdlib.h>
#include <iostream>
#include <TChain.h>
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "Parameters.hh"
#include "Geometry.hh"
#include "AnalysisTools.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

DerivativeClass::DerivativeClass(std::vector<double> &x, std::vector<std::vector<double>> cov){

  fdim = x.size();

  std::vector<double> temp ;
  std::vector<double> plus ;
  std::vector<double> minus;
  std::vector<double> delta;

 for (int k = 0; k < arr_len; k++){
    if(k!=i) delta[k]=0;
    // else if(k==0) delta[k] = 1e-4*x[k];
    // else delta[k] = 1e-5*x[k];
    else if(k==0) delta.push_back(1e-4);
    else if(k==3) delta.push_back(1e-4*x[k]);
    else delta[k] = 1e-5;

    temp[k] = x[k] + delta[k];
    delta[k] = temp[k] - x[k];
    plus[k] = x[k] + delta[k];
    minus[k] = x[k] - delta[k];
  }

 return (MM2(plus,mass,track) - MM2(minus,mass,track))/(2*delta[i]);

}

Double_t MM2(Double_t *x,double mass,TrackCandidate* track){


  //Pion (first 3 parameters)
  Double_t Epi;
  Double_t px,pz,py;
  Double_t thetax,thetay;
  Double_t u; // u = 1/p
  Double_t norm;
  thetax = x[1];
  thetay = x[2];

  norm = 1./sqrt(1 + thetax*thetax + thetay*thetay);
  u  = x[0];
  px = thetax*norm/u;
  py = thetay*norm/u;
  pz = norm/u;

  //Kaon (second 3 parameters)
  Double_t Ek;
  Double_t kmass=0.493677; //[GeV]
  Double_t kpx,kpz,kpy;
  Double_t kthetax,kthetay;
  Double_t pk; // u = 1/p
  Double_t knorm;
  pk  = x[3];
  kthetax = x[4];
  kthetay = x[5];
  knorm = 1./sqrt(1 + kthetax*kthetax + kthetay*kthetay);
  kpx = kthetax*knorm*pk;
  kpy = kthetay*knorm*pk;
  kpz = knorm*pk;

  Epi = sqrt( pow(1/u,2) + mass*mass);
  Ek = sqrt( pow(pk,2) + kmass*kmass);
  TLorentzVector Pion,Kaon,KaonOld;
  Pion.SetPxPyPzE(px,py,pz,Epi);
  Kaon.SetPxPyPzE(kpx,kpy,kpz,Ek);

  return (Kaon - Pion).M2();
}
