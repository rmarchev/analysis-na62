#ifndef DERIVATIVECLASS_HH
#define DERIVATIVECLASS_HH

#include <stdlib.h>
#include <iostream>
#include <vector>
#include <TChain.h>
#include "MCSimple.hh"
#include "functions.hh"
#include "Event.hh"
#include "Persistency.hh"
#include "Parameters.hh"
#include "Geometry.hh"
#include "AnalysisTools.hh"

using namespace std;
using namespace NA62Analysis;
using namespace NA62Constants;

class DerivativeClass
{
public:
  DerivativeClass(std::vector<double> &x, std::vector<std::vector<double>> cov);
  ~DerivativeClass();

private:
  Int_t fdim;
};

#endif
