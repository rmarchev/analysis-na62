#ifndef CHODANALYZER_HH
#define CHODANALYZER_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include "GigaTrackerAnalysis.hh"
#include <TCanvas.h>

class TH1I;
class TH2F;
class TGraph;
class TTree;
class TRecoSpectrometerEvent;
class TRecoSpectrometerCandidate;
class TRecoCHODEvent;
class TRecoCedarEvent;
class TrackCandidate;
class TrackSTRAWCandidate;
class TrackCHODCandidate;
class AnalysisTools;
class BlueTubeTracker;
class RawHeader;

class CHODAnalyzer : public NA62Analysis::Analyzer
{
public:
  CHODAnalyzer(NA62Analysis::Core::BaseAnalysis *ba);
  ~CHODAnalyzer();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void StartOfJobUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void PostProcess();
  void DrawPlot();
  //void MakeNominalKaonVertex(TRecoSpectrometerCandidate* track);
  Int_t GetQuadrant(Int_t channelid);
  Int_t MatchCHOD(TrackCandidate* cand, TRecoSpectrometerCandidate* track);
  Int_t MakeCandidate(Double_t xPos, Double_t yPos, Double_t ttime, Double_t *parameters);
  Int_t FindClosestTimeCand( TRecoVEvent* Event, string detector_type, double& minimum);
  int MatchCHODCandidate(Double_t trktime, TVector3 posatCHOD);
  Bool_t IsGoodGTKVtx();
  Bool_t NoCHANTI();
  Bool_t IsGoodCedar();
protected:
  AnalysisTools *fTools;
  BlueTubeTracker* fTracker;
  TVector3 fVertex;
  TVector3 fKMomentum;
  Double_t fcda;
  TRecoCHODEvent* fCHODEvent;
  TRecoCHANTIEvent* fCHANTIEvent;
  TRecoCedarEvent* fCedarEvent;
  TRecoGigaTrackerEvent* fGigaTrackerEvent;
  TrackSTRAWCandidate* fSTRAWCandidate;
  TrackCandidate* GoodTrack;
  TRecoSpectrometerCandidate* fSpectrometerCandidate;
  TrackCHODCandidate* fCHODCandidate;
  //TRecoSpectrometerCandidate* fTrack;
  TObjArray fGoodCHODCandidates;
  Double_t *fCHODPosV;
  Double_t *fCHODPosH;
  Double_t **fCHODAllT0;
  Double_t **fCHODAllSlewSlope;
  Double_t **fCHODAllSlewConst;

  RawHeader* fHeader;
  GigaTrackerAnalysis *fGigaTrackerAnalysis;

};
#endif
