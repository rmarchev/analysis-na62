#ifndef KMU2_HH
#define KMU2_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"

class TH1I;
class TH2F;
class TGraph;
class TTree;
class TRecoSpectrometerEvent;
class TRecoSpectrometerCandidate;
class TRecoCHODEvent;
class TRecoCedarEvent;
class TRecoMUV3Event;
class TRecoMUV2Event;
class TRecoMUV1Event;
class TRecoLKrEvent;
class TRecoLAVEvent;
class TRecoSACEvent;
class TRecoIRCEvent;
class TRecoRICHEvent;
class TRecoCedarCandidate;
class TRecoSpectrometerCandidate;
class TRecoGigaTrackerCandidate;
class TrackSTRAWCandidate;
class TrackCHODCandidate;
class TrackCandidate;
class AnalysisTools;
class RawHeader;
class LAVMatching;
class SAVMatching;
class L0TPData;


class Kmu2 : public NA62Analysis::Analyzer
{
public:
  Kmu2(NA62Analysis::Core::BaseAnalysis *ba);
  ~Kmu2();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void StartOfJobUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void PostProcess();
  void DrawPlot();
protected:

  AnalysisTools*              fTool;
  TRecoCHODEvent*             fCHODEvent;
  // TRecoCedarEvent*            fCedarEvent;
  // TRecoCHANTIEvent*           fCHANTIEvent;
  TRecoGigaTrackerEvent*      fGigaTrackerEvent;
  TRecoLKrEvent*              fLKrEvent;
  // TRecoMUV3Event*             fMUV3Event;
  // TRecoMUV2Event*             fMUV2Event;
  // TRecoMUV1Event*             fMUV1Event;
  // TRecoRICHEvent*             fRICHEvent;
  // TRecoLAVEvent*              fLAVEvent;
  // TRecoSACEvent*              fSACEvent;
  // TRecoIRCEvent*              fIRCEvent;
  // TRecoCedarCandidate*        fCedarCandidate;
  // TrackSTRAWCandidate*        fSTRAWCandidate;
  // TrackCHODCandidate*         fCHODCandidate;
  TRecoSpectrometerCandidate* fSpectrometerCandidate;
  // TRecoGigaTrackerCandidate*  fGTKCandidate;
  TrackCandidate*             fkmu2track;
  Double_t                    fcda;
  RawHeader* fHeader;
  L0TPData* fL0Data;

  TVector3 fVertex;
  TVector3 fKMomentum;
  TLorentzVector fK4Momentum;

};
#endif
