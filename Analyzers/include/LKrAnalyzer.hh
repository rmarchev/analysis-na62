#ifndef LKRANALYZER_HH
#define LKRANALYZER_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class TH1I;
class TH2F;
class TGraph;
class TTree;
class TRecoSpectrometerEvent;
class TRecoSpectrometerCandidate;
class TRecoCHODEvent;
class TRecoLKrEvent;
class TRecoLKrCandidate;
class TrackCandidate;
class TrackSTRAWCandidate;
class AnalysisTools;
class RawHeader;
class LKrAuxClusterReco;

class LKrAnalyzer : public NA62Analysis::Analyzer
{
public:
  LKrAnalyzer(NA62Analysis::Core::BaseAnalysis *ba);
  ~LKrAnalyzer();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void StartOfJobUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void PostProcess();
  void DrawPlot();
  Bool_t AssociateCluster( TrackCandidate* cand, TVector3 Extrap_track);
  Bool_t CloseCluster(Int_t clid, Double_t xlkr, Double_t ylkr, TRecoLKrEvent* event);
  Int_t  HowManyInTime(TrackCandidate*, Double_t , Double_t , Int_t);
  Int_t  HowManyInTimeAuxReco(TrackCandidate*, Double_t, Double_t,Int_t);
  Bool_t TrackISEM(Int_t val);
  Bool_t TrackISMIP(Int_t val);
  Bool_t TrackISOther(Int_t val);

  int TimeMatching(Double_t trktime,Double_t kaontime);
  int LKrMatch(Double_t trktime,TVector3 posatlkr);
  int MUV1Match(Double_t trktime, TVector3 posatMUV1);
  int MUV2Match(Double_t trktime, TVector3 posatMUV2);
  int MUV3Match(Double_t trktime, TVector3 posatMUV3);
protected:
  RawHeader* fRawHeader;
  AnalysisTools* fTools;
  TRecoLKrEvent* fLKrEvent;
  TRecoMUV1Event* fMUV1Event;
  TRecoMUV2Event* fMUV2Event;
  TRecoMUV3Event* fMUV3Event;
  TrackCandidate* fGoodTrack;
  LKrAuxClusterReco* fLKrAuxClusterReco;
  //Double_t fMatchedMIPCluster;
  Double_t fMatchedLKrIndex;
  Double_t fMatchedLKrdt;
  Double_t fMatchedLKrEnergy;
  Double_t fMatchedLKrdistance;
  TObjArray fGoodLKrCandidates;

private:
  Bool_t  fTrackEM;
  Bool_t  fTrackMIP;
  Bool_t  fTrackOther;
  Int_t  fInTimeClusters;
  Int_t  fInTimeClustersAux;
  Int_t  fNEMClusters;
  Int_t  fNMIPClusters;
  Int_t  fNOtherClusters;
  Int_t  fClusterInTimeID;
  Int_t  fInTimeID[10];
  Int_t  fEMClusterID[10];
  Int_t  fMIPClusterID[10];
  Int_t  fOtherClusterID[10];

private:
  void ClusterAnalysis(Bool_t,Int_t,TRecoLKrEvent*);
  Bool_t SelectEMCluster(Int_t,TRecoLKrCandidate* , TString str);
  Bool_t SelectMIPCluster(Int_t,TRecoLKrCandidate*, TString str);
  Bool_t SelectOtherCluster(Int_t,TRecoLKrCandidate*,Bool_t,Bool_t, TString str);
  inline void AddEMClusters() {fNEMClusters++;};
  inline void AddMIPClusters() {fNMIPClusters++;};
  inline void AddOtherClusters() {fNOtherClusters++;};
  //inline void AddInTimeClusterID(Int_t val){fInTimeID[fInTimeClusters]=fClusterInTimeID;};
  inline void SetEMClusterID(Int_t val) {fEMClusterID[fNEMClusters]=val;};
  inline void SetMIPClusterID(Int_t val) {fMIPClusterID[fNMIPClusters]=val;};
  inline void SetOtherClusterID(Int_t val) {fOtherClusterID[fNOtherClusters]=val;};

  TrackSTRAWCandidate* fSTRAW;

};

#endif
