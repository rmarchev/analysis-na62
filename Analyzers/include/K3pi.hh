#ifndef K3PI_HH
#define K3PI_HH

#include <stdlib.h>
#include <vector>
#include "VertexLSF.hh"
#include "BlueTubeTracker.hh"

#include "Analyzer.hh"
#include "MCSimple.hh"
#include "SpectrometerVertex.hh"
#include <TCanvas.h>

class LAVMatching;
class SAVMatching;

class K3pi : public NA62Analysis::Analyzer
{
public:
  K3pi(NA62Analysis::Core::BaseAnalysis *ba);
  ~K3pi();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void PostProcess();
  void DrawPlot();

  Int_t MatchCHOD(Int_t itrack,TRecoSpectrometerCandidate* track);
  Int_t MatchLKr(Int_t itrack,Double_t trktime, TVector3 posatlkr);
  Int_t MakeCandidate(Double_t xPos, Double_t yPos, Double_t ttime, Double_t *parameters);
  Int_t GetQuadrant(Int_t channelid) ;
  Int_t CedarInTime(Double_t time);
  Int_t GTKAnalysis(Int_t igtk,TVector3 vtxposition);
  Int_t GTKMatch(double chodtime,double cedartime);
  Int_t MUV1Match(Double_t trktime, TVector3 posatMUV1);
  Int_t MUV2Match(Double_t trktime, TVector3 posatMUV2);
  Int_t MUV3Match(Double_t trktime, TVector3 posatMUV3);
protected:

  Double_t fMaxNTracks;
  Double_t fMaxNVertices;
  Double_t fMinZVertex;   ///< Lower limit of the Z range for vertices to be saved
  Double_t fMaxZVertex;   ///< Upper limit of the Z range for vertices to be saved
  Double_t fMaxChi2;      ///< Upper limit of vertex chi2 for vertices to be saved

  Double_t fCHODTime[3];
  TVector3 fCHODPos[3];
  Double_t fEventTime;

  Bool_t fBlueFieldCorrection;

  TRecoSpectrometerEvent* fSpectrometerEvent;
  TRecoCHODEvent* fCHODEvent;
  TRecoGigaTrackerEvent* fGigaTrackerEvent;
  TRecoCedarEvent* fCedarEvent;
  TRecoLKrEvent* fLKrEvent;
  TRecoCHANTIEvent* fCHANTIEvent;
  TRecoMUV3Event*             fMUV3Event;
  TRecoMUV2Event*             fMUV2Event;
  TRecoMUV1Event*             fMUV1Event;
  //TRecoRICHEvent*             fRICHEvent;
  TRecoLAVEvent*              fLAVEvent;
  TRecoSACEvent*              fSACEvent;
  TRecoIRCEvent*              fIRCEvent;

  VertexLSF fVertexLSF;
  std::vector<SpectrometerVertex> fVertexContainer;

  void BuildVertex(Int_t ind[], Int_t NTracks);
  TLorentzVector fK4Momentum;
  TVector3 fK3Momentum;
  Double_t *fCHODPosV;
  Double_t *fCHODPosH;
  Double_t **fCHODAllT0;
  Double_t **fCHODAllSlewSlope;
  Double_t **fCHODAllSlewConst;
  LAVMatching* fLAVMatch ;
  SAVMatching* fSAVMatch ;
};
#endif
