//
//  Definition_Rado.h
//
//
//  Created by Riccardo Aliberti on 03/05/13.
//
//
#ifndef _Value_
#define _Value_

const double ZLAV1_Start = 12195.3; // [ mm ]
const double ZLAV2_Start = 12956.3; // [ mm ]
const double ZLAV3_Start = 13717.3; // [ mm ]
const double ZLAV4_Start = 14478.3; // [ mm ]
const double ZLAV5_Start = 15239.3; // [ mm ]
const double ZLAV6_Start = 16590.3; // [ mm ]
const double ZLAV7_Start = 17341.3; // [ mm ]
const double ZLAV8_Start = 18092.3; // [ mm ]
const double ZLAV9_Start = 19318.4; // [ mm ]
const double ZLAV10_Start= 20364.2; // [ mm ]
const double ZLAV11_Start= 21820.3; // [ mm ]
const double ZLAV12_Start= 23883.5; // [ mm ]


// Parameters Value.

// Magneti field.
//static const double EMRadSpeed = TMath::C()* 1.e-9 * 1.e-4 * 1.e-2; // EM radiation speed in particular unit (useful for the magnetic field).
//static const TVector3 MagneticField(0.,0.6928*10000,0.); // magnetic field (only in y direction) in particular unit.

//Masses
const double PiMass = 0.13957018; //[GeV]
const double MuMass = 0.10565837; //[GeV]
const double KMass  = 0.49367700; //[GeV]

const double fFineTimeScale=24.951059536/256.;
const double fgevtomev=1000;
const double fmevtogev=0.001;
// Detectors parameters.
// final collimator.
const double zTRIM5 = 101800.; // [ mm ]
const double zGTK3 = 102400 ; // [ mm ]
// fiducial region.
const double FRzmin = 105000.; // [ mm ]
const double FRzmax = 165000.; // [ mm ]
// magnet.
const double zMagStart = 196345.; // [ mm ]
const double zMagEnd = 197645.; // [ mm ]
const double zMagnet = (zMagStart + zMagEnd)/2.; // [ mm ]
const double dMag = zMagEnd - zMagStart;
// Spectrometer chamber.
const double zSTRAW_station[4] = {183508.0,194066.0,204459.0,218885.0}; // [ mm ]
const double xSTRAW_station[4] = {101.2,114.4,92.4,52.8}; // [ mm ]
const double rSTRAWmin = 75.; // [ mm ]
const double rSTRAWmax = 1000.; // [ mm ]
// CHOD.
const double zCHOD = 239100.; // [ mm ] ----> RICONTROLLARE!!!
const double zCHODVL = 238960.; // [ mm ] Vertical layer (x)
const double zCHODHL = 239340.; // [ mm ] Horizontal layer (y)
const double CHODRMin = 125.; // [ mm ]
const double CHODRMax = 1100.; // [ mm ]
// LKr.
const double zLKr = 241093.; // [ mm ]
const double LKrRMin = 150.; // [ mm ]
const double LKrLMax = 1130.; // [ mm ]
// RICH.
const double zRICH = 219550.; // [ mm ]
const double zRICHFP = 219875.; // [ mm ] Focal plane
const double zRICHMI = 236875.; // [ mm ] Mirror
const double zRICHEntWindow = 219385.; // [ mm ] Entrance window
const double zRICHExtWindow = 237326.; // [ mm ] Exit window
const double lRICH = 17000.; // [ mm ]
const double RICHRMin = 90.; // [ mm ]
const double RICHRMax = 1100.; // [ mm ]
const double nNeon = 1.000062; // Neon (inside RICH) refractive index.
//Cedar
const double ZCedarStart = 6916.5; // [ mm ]
const double ZCedarEnd = 7944.0; // [ mm ]
// MUV1.
const double zMUV1 = 244341.; // [ mm ]
const double MUV1RMin = 130.; // [ mm ]
const double MUV1RMax = 1300.; // [ mm ]
const double XMUV1max = 1100;
const double XMUV1min = 130;
const double YMUV1max = 1100;
const double YMUV1min = 120;
const double RMUV1max = 1100;
const double RMUV1min = 130;
// MUV2.
const double zMUV2 = 245290.; // [ mm ] (2015 with MUV1).
//static const double zMUV2 = 244435.; // [ mm ] (2014 without MUV1).
const double MUV2RMin = 130.; // [ mm ]
const double MUV2RMax = 1300.; // [ mm ]
// MUV3.
const double zMUV3 = 246800.; // [ mm ]
const double MUV3RMin = 130.; // [ mm ]
const double MUV3RMax = 1100.; // [ mm ]
const double XMUV3min = 130;
const double XMUV3max = 1100;
const double YMUV3max = 130;
const double YMUV3min = 1100;

static const double XMUV2min = 130;
static const double XMUV2max = 1100;
static const double YMUV2max = 130;
static const double YMUV2min = 1100;


#endif
