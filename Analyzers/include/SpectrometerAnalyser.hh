#ifndef SPECTROMETERANALYSER_HH
#define SPECTROMETERANALYSER_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

//#include "UserMethods.hh"
#include "TrackSTRAWCandidate.hh"
#include "TRecoSpectrometerEvent.hh"
#include "TRecoGigaTrackerEvent.hh"
#include "TRecoSpectrometerCandidate.hh"

class TH1I;
class TH2F;
class TGraph;
class TTree;
class AnalysisTools;
class TrackCandidate;
class TrackSTRAWCandidate;
class TRecoSpectrometerEvent;
class TRecoGigaTrackerEvent;
class RawHeader;
class L0TPData;

class SpectrometerAnalyser : public NA62Analysis::Analyzer
{
public:
  SpectrometerAnalyser(NA62Analysis::Core::BaseAnalysis *ba);
  ~SpectrometerAnalyser();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void StartOfJobUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void PostProcess();
  void DrawPlot();
  Bool_t Acceptance(TRecoSpectrometerCandidate *);
  void MakeNominalKaonVertex(TRecoSpectrometerCandidate*);
  int FindClosestTimeCand( TRecoVEvent* ,TRecoSpectrometerCandidate*, string, double& );
  void MakeGTKVertex(TRecoSpectrometerCandidate*, TString );
  Int_t SelectTrigger(int triggerType, int type, int mask);
protected:
  AnalysisTools *fTools;
  TRecoSpectrometerEvent* fSpectrometerEvent;
  TRecoGigaTrackerEvent* fGigaTrackerEvent;
  //TrackSTRAWCandidate* fSTRAWCandidate;
  TVector3 fVertex;
  TObjArray fTracks;
  TObjArray fGoodCandidates;
  Double_t xSTRAW_station[4];
  Double_t zSTRAW_station[4];
  Double_t fcda;
  Double_t fExternalAlpha;
  Double_t fExternalBeta;
  Int_t fNTracks;
  Int_t fNGoodTracks;
  Int_t fNFakeTracks;
  RawHeader* fHeader;
  L0TPData* fL0Data;
};
#endif
