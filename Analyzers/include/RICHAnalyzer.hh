#ifndef RICHANALYZER_HH
#define RICHANALYZER_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class TH1I;
class TH2F;
class TGraph;
class TTree;
class TRecoSpectrometerEvent;
class TRecoSpectrometerCandidate;
class TRecoCHODEvent;
class TRecoRICHEvent;
class TRecoRICHCandidate;
class TrackCandidate;
class AnalysisTools;
class GeometricAcceptance;
class RawHeader;


class RICHAnalyzer : public NA62Analysis::Analyzer
{
public:
    RICHAnalyzer(NA62Analysis::Core::BaseAnalysis *ba);
    ~RICHAnalyzer();
    void InitHist();
    void InitOutput();
    void DefineMCSimple();
    void Process(int iEvent);
    void StartOfBurstUser();
    void EndOfBurstUser();
    void StartOfRunUser();
    void EndOfRunUser();
    void PostProcess();
    void DrawPlot();
    Int_t MatchRich(TRecoSpectrometerCandidate*);
    void MirrorVector(TVector3*, TVector3* );
    Int_t MirrorSurface(Double_t xin, Double_t yin, Double_t SafeFactor, Bool_t flag);
protected:
    RawHeader* fRawHeader;
    AnalysisTools* fTools;
    TrackCandidate* fGoodTrack;
    TRecoRICHEvent* fRICHEvent;
    GeometricAcceptance* fgeom;

    Double_t fMatchedRICHIndex;
    Double_t fMatchedRICHdt;
    Double_t fMatchedRICHEnergy;
    Double_t fMatchedRICHdistance;
    TObjArray fGoodRICHCandidates;
private:
    Double_t **fDeltaMisa;
    Double_t ***fMirrorPos;


};
#endif
