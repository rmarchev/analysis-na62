#ifndef PINUNUANA_HH
#define PINUNUANA_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include "GigaTrackerAnalysis.hh"
#include <TCanvas.h>

class TH1I;
class TH2F;
class TGraph;
class TTree;
class TRecoSpectrometerEvent;
class TRecoSpectrometerCandidate;
class TRecoCHODEvent;
class TRecoCedarEvent;
class TRecoMUV3Event;
class TRecoMUV2Event;
class TRecoMUV1Event;
class TRecoLKrEvent;
class TRecoLAVEvent;
class TRecoSACEvent;
class TRecoIRCEvent;
class TRecoRICHEvent;
class TRecoCedarCandidate;
class TRecoSpectrometerCandidate;
class TRecoCHODCandidate;
class TRecoGigaTrackerCandidate;
class TrackSTRAWCandidate;
class TrackCHODCandidate;
class TrackCandidate;
class AnalysisTools;
class RawHeader;
class LAVMatching;
class SAVMatching;
class L0TPData;
class TTree;


class PinunuAna : public NA62Analysis::Analyzer
{
public:
  PinunuAna(NA62Analysis::Core::BaseAnalysis *ba);
  ~PinunuAna();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void StartOfJobUser();
  void EndOfRunUser();
  void EndOfJobUser();
  void PostProcess();
  void DrawPlot();
  Bool_t PlotGTK(int i);
  Bool_t PlotCHANTI();
  Bool_t MUV3Track();
  Bool_t RICHMatch();
  Bool_t MUV2Track();
  Bool_t MUV1Track();
  int FindClosestCluster( TRecoVEvent* , TVector3 , string , double& );
  int FindClosestTimeCand( TRecoVEvent*, string, double&);
  int FindPionMatch( TLorentzVector, TVector3);
  Int_t StrawHitAnalysis(Int_t evttype[]);
  Int_t WhichType();
protected:

  AnalysisTools*              fTool;
  TRecoSpectrometerEvent* fSpectrometerEvent;
  TRecoCHODEvent*             fCHODEvent;
  TRecoCedarEvent*            fCedarEvent;
  TRecoCHANTIEvent*           fCHANTIEvent;
  TRecoGigaTrackerEvent*      fGigaTrackerEvent;
  TRecoLKrEvent*              fLKrEvent;
  TRecoMUV3Event*             fMUV3Event;
  TRecoMUV2Event*             fMUV2Event;
  TRecoMUV1Event*             fMUV1Event;
  TRecoRICHEvent*             fRICHEvent;
  TRecoLAVEvent*              fLAVEvent;
  TRecoSACEvent*              fSACEvent;
  TRecoIRCEvent*              fIRCEvent;
  TRecoCedarCandidate*        fCedarCandidate;
  TrackSTRAWCandidate*        fSTRAWCandidate;
  TrackCHODCandidate*         fCHODCandidate;
  TRecoSpectrometerCandidate* fSpectrometerCandidate;
  TRecoGigaTrackerCandidate*  fGTKCandidate;
  TrackCandidate*             GoodTrack;
  Double_t                    fcda;
  Double_t                    fmm2expectederror;
  Double_t                    fmm2;

  Bool_t fKgoodCedar;
  Bool_t fKgoodCHANTI;
  Bool_t fKgoodGTK;
  Bool_t fKgood;

  TVector3 fVertex;
  TVector3 fKMomentum;
  TLorentzVector fK4Momentum;
  RawHeader* fHeader;
  LAVMatching* fLAVMatch;
  SAVMatching* fSAVMatch;
  L0TPData* fL0Data;

  TTree* fTree;
  Double_t thx,thy,momtrack;
  Double_t kthx,kthy,momk;
  Double_t dm2dx[6];
  Double_t mm2;
  Double_t sigmamm2;


  Double_t fnhitintime[4][4];
  Double_t xSTRAW_station[4];
  Double_t zSTRAW_station[4];


  //Added Parameters
  Bool_t   fFilter;


  //STRAW Analysis directory
  TH1I* fnhitschls[4];
  TH1I* fnhitschss[4];
  TH1I* fnviewschss[4];
  TH1I* fnviewschls[4];

  TH2D* ftrkpos_stationss[4];
  TH2D* ftrkpos_stationls[4];
  TH2F* fdnhits_vs_mm2_ch[4];
  TH2F* fsigmamm2_vs_dnhits_ch[4];
  TH2F* fp_vs_dnhits_chss[4];
  TH2F* fp_vs_dnhits_chls[4];
  TH2F* fp_vs_nclhits_chss[4];
  TH2F* fp_vs_nclhits_chls[4];

  TH2F* fp_vs_dnhits_ch_t1[4];
  TH2F* fp_vs_dnhits_ch_t2[4];
  TH2F* fp_vs_dnhits_ch_t3[4];
  TH2F* fp_vs_dnhits_ch_t4[4];
  TH2F* fp_vs_dnhits_ch_t5[4];

  TH2F* fdnhits_vs_mm2_ch_t1[4];
  TH2F* fdnhits_vs_mm2_ch_t2[4];
  TH2F* fdnhits_vs_mm2_ch_t3[4];
  TH2F* fdnhits_vs_mm2_ch_t4[4];
  TH2F* fdnhits_vs_mm2_ch_t5[4];

  TH2F* fsigmamm2_vs_dnhits_ch_t1[4];
  TH2F* fsigmamm2_vs_dnhits_ch_t2[4];
  TH2F* fsigmamm2_vs_dnhits_ch_t3[4];
  TH2F* fsigmamm2_vs_dnhits_ch_t4[4];
  TH2F* fsigmamm2_vs_dnhits_ch_t5[4];

  TH1F* fhitsdt_ch_view    [4][4];
  TH1F* fhits_dist_ch_view [4][4];
  TH1I* fnclosehits_ch_view[4][4];

  TH2F* fdnhits_vs_mm2     ;
  TH2F* fsigmamm2_vs_dnhits;
  TH2F* fp_vs_dnhits_ss    ;
  TH2F* fp_vs_dnhits_ls    ;

  GigaTrackerAnalysis *fGigaTrackerAnalysis;
};
#endif
