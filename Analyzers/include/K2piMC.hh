#ifndef K2PIMC_HH
#define K2PIMC_HH

#include <stdlib.h>
#include <vector>
#include "Analyzer.hh"
#include "MCSimple.hh"
#include "DetectorAcceptance.hh"
#include <TCanvas.h>

class TH1I;
class TH2F;
class TGraph;
class TTree;
class AnalysisTools;
class TRecoSpectrometerEvent;
class TRecoGigaTrackerEvent;
class TRecoCHODEvent;
class TRecoCedarEvent;
class TRecoMUV3Event;
class TRecoMUV2Event;
class TRecoMUV1Event;
class TRecoLKrEvent;
class TRecoLAVEvent;
class TRecoSACEvent;
class TRecoIRCEvent;
class TRecoRICHEvent;

class TRecoSpectrometerCandidate;

class K2piMC : public NA62Analysis::Analyzer
{
public:
  K2piMC(NA62Analysis::Core::BaseAnalysis *ba);
  ~K2piMC();
  void InitHist();
  void InitOutput();
  void DefineMCSimple();
  void Process(int iEvent);
  void StartOfBurstUser();
  void EndOfBurstUser();
  void StartOfRunUser();
  void EndOfRunUser();
  void PostProcess();
  void DrawPlot();
protected:
  void MakeNominalKaonVertex();
  Bool_t IsGoodGTKVtx();
  Int_t LKrTrack();
  Bool_t MUV1Track();
  Bool_t MUV2Track();
  Bool_t CHODTrack();

  Bool_t Acceptance();
  Int_t FindClosestCluster( TRecoVEvent*, TVector3, string, double&);

  AnalysisTools* fTool;
  TRecoCHODEvent*             fCHODEvent;
  TRecoCedarEvent*            fCedarEvent;
  TRecoCHANTIEvent*           fCHANTIEvent;
  TRecoSpectrometerEvent*     fSpectrometerEvent;
  TRecoGigaTrackerEvent*     fGigaTrackerEvent;
  TRecoLKrEvent*              fLKrEvent;
  TRecoMUV3Event*             fMUV3Event;
  TRecoMUV2Event*             fMUV2Event;
  TRecoMUV1Event*             fMUV1Event;
  TRecoRICHEvent*             fRICHEvent;
  TRecoLAVEvent*              fLAVEvent;
  TRecoSACEvent*              fSACEvent;
  TRecoIRCEvent*              fIRCEvent;
  TRandom3 fRandom[3];
  TRecoSpectrometerCandidate* fSpectrometerCandidate;

  Int_t kID,pi0ID;
  Double_t fcda;
  TVector3 fVertex;

  KinePart* kplus;
  TVector3 fKMomentum;
  TVector3 fPiMomentum;
  TLorentzVector fK4Momentum;
  TLorentzVector fPi4Momentum;

};
#endif
