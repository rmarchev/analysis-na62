set(USER_ANALYZERS SpectrometerAnalyser CHODAnalyzer LKrAnalyzer PinunuAna )

set(TARGET_EXEC Pinunu)
set(ANA_LIBS SpectrometerAnalyser CHODAnalyzer LKrAnalyzer PinunuAna  LKrClusterCorrections GigaTrackerEvtReco  )

set(EXTRA_LIBS )
set(EXTRA_LIBS_DIRS )
set(EXTRA_INCLUDE_DIRS Analyzers/include/ PhysicsObjects/include $ANALYSISFW_PATH/Toolslib/include )