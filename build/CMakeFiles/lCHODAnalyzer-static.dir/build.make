# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.2

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /afs/cern.ch/sw/lcg/contrib/CMake/3.2.3/Linux-x86_64/bin/cmake

# The command to remove a file.
RM = /afs/cern.ch/sw/lcg/contrib/CMake/3.2.3/Linux-x86_64/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/build

# Include any dependencies generated for this target.
include CMakeFiles/lCHODAnalyzer-static.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/lCHODAnalyzer-static.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/lCHODAnalyzer-static.dir/flags.make

CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o: CMakeFiles/lCHODAnalyzer-static.dir/flags.make
CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o: ../Analyzers/src/CHODAnalyzer.cc
	$(CMAKE_COMMAND) -E cmake_progress_report /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o"
	/cvmfs/sft.cern.ch/lcg/releases/LCG_84/gcc/4.9.3/x86_64-slc6/bin/g++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o -c /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/Analyzers/src/CHODAnalyzer.cc

CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.i"
	/cvmfs/sft.cern.ch/lcg/releases/LCG_84/gcc/4.9.3/x86_64-slc6/bin/g++  $(CXX_DEFINES) $(CXX_FLAGS) -E /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/Analyzers/src/CHODAnalyzer.cc > CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.i

CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.s"
	/cvmfs/sft.cern.ch/lcg/releases/LCG_84/gcc/4.9.3/x86_64-slc6/bin/g++  $(CXX_DEFINES) $(CXX_FLAGS) -S /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/Analyzers/src/CHODAnalyzer.cc -o CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.s

CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o.requires:
.PHONY : CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o.requires

CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o.provides: CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o.requires
	$(MAKE) -f CMakeFiles/lCHODAnalyzer-static.dir/build.make CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o.provides.build
.PHONY : CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o.provides

CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o.provides.build: CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o

# Object files for target lCHODAnalyzer-static
lCHODAnalyzer__static_OBJECTS = \
"CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o"

# External object files for target lCHODAnalyzer-static
lCHODAnalyzer__static_EXTERNAL_OBJECTS =

../lib/liblCHODAnalyzer-static.a: CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o
../lib/liblCHODAnalyzer-static.a: CMakeFiles/lCHODAnalyzer-static.dir/build.make
../lib/liblCHODAnalyzer-static.a: CMakeFiles/lCHODAnalyzer-static.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX static library ../lib/liblCHODAnalyzer-static.a"
	$(CMAKE_COMMAND) -P CMakeFiles/lCHODAnalyzer-static.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/lCHODAnalyzer-static.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/lCHODAnalyzer-static.dir/build: ../lib/liblCHODAnalyzer-static.a
.PHONY : CMakeFiles/lCHODAnalyzer-static.dir/build

CMakeFiles/lCHODAnalyzer-static.dir/requires: CMakeFiles/lCHODAnalyzer-static.dir/Analyzers/src/CHODAnalyzer.cc.o.requires
.PHONY : CMakeFiles/lCHODAnalyzer-static.dir/requires

CMakeFiles/lCHODAnalyzer-static.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/lCHODAnalyzer-static.dir/cmake_clean.cmake
.PHONY : CMakeFiles/lCHODAnalyzer-static.dir/clean

CMakeFiles/lCHODAnalyzer-static.dir/depend:
	cd /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/build /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/build /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/build/CMakeFiles/lCHODAnalyzer-static.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/lCHODAnalyzer-static.dir/depend

