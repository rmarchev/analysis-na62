// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME TrackCandidateDICT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/include/TrackCandidate.hh"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_TrackCandidate(void *p = 0);
   static void *newArray_TrackCandidate(Long_t size, void *p);
   static void delete_TrackCandidate(void *p);
   static void deleteArray_TrackCandidate(void *p);
   static void destruct_TrackCandidate(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrackCandidate*)
   {
      ::TrackCandidate *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TrackCandidate >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TrackCandidate", ::TrackCandidate::Class_Version(), "TrackCandidate.hh", 23,
                  typeid(::TrackCandidate), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TrackCandidate::Dictionary, isa_proxy, 4,
                  sizeof(::TrackCandidate) );
      instance.SetNew(&new_TrackCandidate);
      instance.SetNewArray(&newArray_TrackCandidate);
      instance.SetDelete(&delete_TrackCandidate);
      instance.SetDeleteArray(&deleteArray_TrackCandidate);
      instance.SetDestructor(&destruct_TrackCandidate);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrackCandidate*)
   {
      return GenerateInitInstanceLocal((::TrackCandidate*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrackCandidate*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TrackCHODCandidate(void *p = 0);
   static void *newArray_TrackCHODCandidate(Long_t size, void *p);
   static void delete_TrackCHODCandidate(void *p);
   static void deleteArray_TrackCHODCandidate(void *p);
   static void destruct_TrackCHODCandidate(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrackCHODCandidate*)
   {
      ::TrackCHODCandidate *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TrackCHODCandidate >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TrackCHODCandidate", ::TrackCHODCandidate::Class_Version(), "TrackCHODCandidate.hh", 7,
                  typeid(::TrackCHODCandidate), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TrackCHODCandidate::Dictionary, isa_proxy, 4,
                  sizeof(::TrackCHODCandidate) );
      instance.SetNew(&new_TrackCHODCandidate);
      instance.SetNewArray(&newArray_TrackCHODCandidate);
      instance.SetDelete(&delete_TrackCHODCandidate);
      instance.SetDeleteArray(&deleteArray_TrackCHODCandidate);
      instance.SetDestructor(&destruct_TrackCHODCandidate);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrackCHODCandidate*)
   {
      return GenerateInitInstanceLocal((::TrackCHODCandidate*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrackCHODCandidate*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TrackSTRAWCandidate(void *p = 0);
   static void *newArray_TrackSTRAWCandidate(Long_t size, void *p);
   static void delete_TrackSTRAWCandidate(void *p);
   static void deleteArray_TrackSTRAWCandidate(void *p);
   static void destruct_TrackSTRAWCandidate(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrackSTRAWCandidate*)
   {
      ::TrackSTRAWCandidate *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TrackSTRAWCandidate >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TrackSTRAWCandidate", ::TrackSTRAWCandidate::Class_Version(), "TrackSTRAWCandidate.hh", 11,
                  typeid(::TrackSTRAWCandidate), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TrackSTRAWCandidate::Dictionary, isa_proxy, 4,
                  sizeof(::TrackSTRAWCandidate) );
      instance.SetNew(&new_TrackSTRAWCandidate);
      instance.SetNewArray(&newArray_TrackSTRAWCandidate);
      instance.SetDelete(&delete_TrackSTRAWCandidate);
      instance.SetDeleteArray(&deleteArray_TrackSTRAWCandidate);
      instance.SetDestructor(&destruct_TrackSTRAWCandidate);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrackSTRAWCandidate*)
   {
      return GenerateInitInstanceLocal((::TrackSTRAWCandidate*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrackSTRAWCandidate*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr TrackCandidate::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TrackCandidate::Class_Name()
{
   return "TrackCandidate";
}

//______________________________________________________________________________
const char *TrackCandidate::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackCandidate*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TrackCandidate::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackCandidate*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TrackCandidate::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackCandidate*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TrackCandidate::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackCandidate*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TrackCHODCandidate::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TrackCHODCandidate::Class_Name()
{
   return "TrackCHODCandidate";
}

//______________________________________________________________________________
const char *TrackCHODCandidate::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackCHODCandidate*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TrackCHODCandidate::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackCHODCandidate*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TrackCHODCandidate::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackCHODCandidate*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TrackCHODCandidate::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackCHODCandidate*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TrackSTRAWCandidate::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TrackSTRAWCandidate::Class_Name()
{
   return "TrackSTRAWCandidate";
}

//______________________________________________________________________________
const char *TrackSTRAWCandidate::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackSTRAWCandidate*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TrackSTRAWCandidate::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackSTRAWCandidate*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TrackSTRAWCandidate::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackSTRAWCandidate*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TrackSTRAWCandidate::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackSTRAWCandidate*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void TrackCandidate::Streamer(TBuffer &R__b)
{
   // Stream an object of class TrackCandidate.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TrackCandidate::Class(),this);
   } else {
      R__b.WriteClassBuffer(TrackCandidate::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrackCandidate(void *p) {
      return  p ? new(p) ::TrackCandidate : new ::TrackCandidate;
   }
   static void *newArray_TrackCandidate(Long_t nElements, void *p) {
      return p ? new(p) ::TrackCandidate[nElements] : new ::TrackCandidate[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrackCandidate(void *p) {
      delete ((::TrackCandidate*)p);
   }
   static void deleteArray_TrackCandidate(void *p) {
      delete [] ((::TrackCandidate*)p);
   }
   static void destruct_TrackCandidate(void *p) {
      typedef ::TrackCandidate current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrackCandidate

//______________________________________________________________________________
void TrackCHODCandidate::Streamer(TBuffer &R__b)
{
   // Stream an object of class TrackCHODCandidate.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TrackCHODCandidate::Class(),this);
   } else {
      R__b.WriteClassBuffer(TrackCHODCandidate::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrackCHODCandidate(void *p) {
      return  p ? new(p) ::TrackCHODCandidate : new ::TrackCHODCandidate;
   }
   static void *newArray_TrackCHODCandidate(Long_t nElements, void *p) {
      return p ? new(p) ::TrackCHODCandidate[nElements] : new ::TrackCHODCandidate[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrackCHODCandidate(void *p) {
      delete ((::TrackCHODCandidate*)p);
   }
   static void deleteArray_TrackCHODCandidate(void *p) {
      delete [] ((::TrackCHODCandidate*)p);
   }
   static void destruct_TrackCHODCandidate(void *p) {
      typedef ::TrackCHODCandidate current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrackCHODCandidate

//______________________________________________________________________________
void TrackSTRAWCandidate::Streamer(TBuffer &R__b)
{
   // Stream an object of class TrackSTRAWCandidate.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TrackSTRAWCandidate::Class(),this);
   } else {
      R__b.WriteClassBuffer(TrackSTRAWCandidate::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrackSTRAWCandidate(void *p) {
      return  p ? new(p) ::TrackSTRAWCandidate : new ::TrackSTRAWCandidate;
   }
   static void *newArray_TrackSTRAWCandidate(Long_t nElements, void *p) {
      return p ? new(p) ::TrackSTRAWCandidate[nElements] : new ::TrackSTRAWCandidate[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrackSTRAWCandidate(void *p) {
      delete ((::TrackSTRAWCandidate*)p);
   }
   static void deleteArray_TrackSTRAWCandidate(void *p) {
      delete [] ((::TrackSTRAWCandidate*)p);
   }
   static void destruct_TrackSTRAWCandidate(void *p) {
      typedef ::TrackSTRAWCandidate current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrackSTRAWCandidate

namespace {
  void TriggerDictionaryInitialization_libTrackCandidate_Impl() {
    static const char* headers[] = {
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/include/TrackCandidate.hh",
0
    };
    static const char* includePaths[] = {
"/cvmfs/sft.cern.ch/lcg/releases/LCG_84/ROOT/6.06.02/x86_64-slc6-gcc49-opt/include",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_84/Geant4/10.01.p02/x86_64-slc6-gcc49-opt/include/Geant4",
"/cvmfs/sft.cern.ch/lcg/releases/XercesC/3.1.1p1-8ccd5/x86_64-slc6-gcc49-opt/include",
"/cvmfs/sft.cern.ch/lcg/releases/qt/4.8.4-f642c/x86_64-slc6-gcc49-opt/include",
"/cvmfs/sft.cern.ch/lcg/releases/qt/4.8.4-f642c/x86_64-slc6-gcc49-opt/include/QtCore",
"/cvmfs/sft.cern.ch/lcg/releases/qt/4.8.4-f642c/x86_64-slc6-gcc49-opt/include/QtGui",
"/cvmfs/sft.cern.ch/lcg/releases/qt/4.8.4-f642c/x86_64-slc6-gcc49-opt/include/QtOpenGL",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/RecoBase/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/Service/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/EventDisplay/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/RICH/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/RICH/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/LKr/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/LKr/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/Spectrometer/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/Spectrometer/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/GigaTracker/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/GigaTracker/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/LAV/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/LAV/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/IRC/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/IRC/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/CHANTI/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/CHANTI/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/Cedar/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/Cedar/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/CHOD/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/CHOD/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/NewCHOD/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/NewCHOD/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/MUV1/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/MUV1/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/MUV2/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/MUV2/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/SAC/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/SAC/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/MUV3/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/MUV3/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/MUV0/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/MUV0/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/HAC/Persistency/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction/HAC/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC/Persistency/include",
"/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis/include",
"/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis/ToolsLib/include",
"/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis/Examples/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/Analyzers/include",
"/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis/Analyzers/CalibrationTools/include",
"/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis/Analyzers/MonitoringTools/include",
"/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis/Analyzers/Physics/include",
"/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis/Analyzers/PhysicsTools/include",
"/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis/Analyzers/TestTools/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/.",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/Analyzers/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/PhysicsObjects/include",
"/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/NALYSISFW_PATH/Toolslib/include",
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.06.02-6cc9c/x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/Pinunu/build/PhysicsObjects/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libTrackCandidate dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/include/TrackCandidate.hh")))  TrackCandidate;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/include/TrackCandidate.hh")))  TrackCHODCandidate;
class __attribute__((annotate("$clingAutoload$/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/include/TrackCandidate.hh")))  TrackSTRAWCandidate;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libTrackCandidate dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef G4_STORE_TRAJECTORY
  #define G4_STORE_TRAJECTORY 1
#endif
#ifndef G4VERBOSE
  #define G4VERBOSE 1
#endif
#ifndef G4UI_USE
  #define G4UI_USE 1
#endif
#ifndef G4VIS_USE
  #define G4VIS_USE 1
#endif
#ifndef G4MULTITHREADED
  #define G4MULTITHREADED 1
#endif
#ifndef G4UI_USE_TCSH
  #define G4UI_USE_TCSH 1
#endif
#ifndef G4INTY_USE_XT
  #define G4INTY_USE_XT 1
#endif
#ifndef G4VIS_USE_RAYTRACERX
  #define G4VIS_USE_RAYTRACERX 1
#endif
#ifndef G4INTY_USE_QT
  #define G4INTY_USE_QT 1
#endif
#ifndef G4UI_USE_QT
  #define G4UI_USE_QT 1
#endif
#ifndef G4VIS_USE_OPENGLQT
  #define G4VIS_USE_OPENGLQT 1
#endif
#ifndef G4UI_USE_XM
  #define G4UI_USE_XM 1
#endif
#ifndef G4VIS_USE_OPENGLXM
  #define G4VIS_USE_OPENGLXM 1
#endif
#ifndef G4VIS_USE_OPENGLX
  #define G4VIS_USE_OPENGLX 1
#endif
#ifndef G4VIS_USE_OPENGL
  #define G4VIS_USE_OPENGL 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu/PhysicsObjects/include/TrackCandidate.hh"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"TrackCHODCandidate", payloadCode, "@",
"TrackCandidate", payloadCode, "@",
"TrackSTRAWCandidate", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libTrackCandidate",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libTrackCandidate_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libTrackCandidate_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libTrackCandidate() {
  TriggerDictionaryInitialization_libTrackCandidate_Impl();
}
