export ANALYSISFW_PATH=/afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis
export ANALYSISFW_USERDIR=/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu
export NA62MCSOURCE=/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC
export NA62RECOSOURCE=/afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction

if [[ ! $LD_LIBRARY_PATH =~ $ANALYSISFW_PATH/lib ]]; then
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ANALYSISFW_PATH/lib
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ANALYSISFW_USERDIR/lib
    export PATH+=:$ANALYSISFW_PATH
fi

source $ANALYSISFW_PATH/scripts/env.sh
