setenv ANALYSISFW_PATH /afs/cern.ch/work/r/rmarchev/private/NA62/Framework/na62fw/NA62Analysis
setenv ANALYSISFW_USERDIR /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/Pinunu
setenv NA62MCSOURCE /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62MC
setenv NA62RECOSOURCE /afs/cern.ch/user/r/rmarchev/work/NA62/Framework/na62fw/NA62Reconstruction

if ( "$LD_LIBRARY_PATH" !~ "*$ANALYSISFW_PATH/lib*" ) then
	setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${ANALYSISFW_PATH}/lib
	setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:${ANALYSISFW_USERDIR}/lib
	setenv PATH ${PATH}:${ANALYSISFW_PATH}
endif

source $ANALYSISFW_PATH/scripts/env.csh
